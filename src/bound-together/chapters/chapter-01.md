---
title: Bound Together
twitter: https://twitter.com/Howlsgirl/status/1330209814304215051
notes: >
  He pulled against the ropes with all his might, but they wouldn't give.

  "Don't bother," a voice said.

  He looked up to discover a thin girl bound with the same rope. Although it was dark, he could see her bruised eyes and bloody wrists.

  "I already tried."
date: 2021-01-12
teaser: >
  Partel wakes up to find himself without memory and bound in a nearly featureless cell with a young lady. He doesn't have long to live but he has to know why and how it had happened.
summary: >
  Partel wakes up to find himself without memory and bound in a nearly featureless cell with a young lady. He tries to escape and fails. He starts to talk to the young lady who reveals that she is magically feeding off him and that she's his daughter.
---

> They would be later become known as the Harbringers of Quiet, a cadre of young men and women who chased after battles to provide healing, rebuilding, and food for the people displaced by war. --- Nakail da Korsan, *The Silent Harbringers*

Partel could barely think past the headache that throbbed in his head and the twisted gurgling in his gut. His eyes stubbornly refused to open, like a crypt door that had been sealed over with a century of concrete. He tried to focus on one eye, but only his eyes only burned in the darkness and refused to budge.

He already knew not to wipe his eyes clear. He could feel the tight bonds of a rope around his wrist. A quiet part of him thought it could be chain but it was too smooth and creaked when he twisted his wrists back and forth.

Without opening his eyes, he arched his back and pushed his bound wrists toward his rear. If he could get them under, he would have more options for breaking free. But, even as he strained his shoulders down and twisted his body, he couldn't get his wrists low enough to hook under his buttocks.

"Damn," he muttered. Twisting back and forth, he explored his bounds while bracing himself on the ground. He was on a hard, smooth surface, ceramic or wood. His body slid a bit and he decided to try pushing himself toward some furniture or a wall; he wanted anything that could be used as a brace.

The rope binding his wrists stopped him with less than a foot of slack. He grumbled and twisted again. There was no give around his wrists but he continued to work back and forth until he felt the skin burning and the faint smell of blood rising up.

Partel changed tactics, scissored his wrists as far as they would go and then jerked his arms hard away from each other in an attempt to snap it. It failed but he tried again.

"Don't bother," a voice said. It was a woman or a girl, it was hard to tell with her higher tone.

Somehow, he wasn't surprised there was someone else near him.

He relented with his bonds and focused on opening his eye. He rubbed them against fabric that draped over his shoulder, but then fought a way of nausea that threatened to disgorge whatever was in his stomach. He almost vomited just to relieve the discomfort but couldn't. Instead, he continued to rub his eyes with his shoulder until he could see through the cracks.

Partel groaned while he blinked, forcing his eye open. It felt like he was tearing his eyelashes off his skin but he managed to force one open eye and looked around.

She was a young thing, maybe in her late teens or early twenties. Her face was shadowed, with red and shadows surrounding her eyes. She sat on the ground a few yards away from him, both arms wrapped around her chest and clinging to her shoulders with the fingertips. Long hair, ratty and split, poured down her face and obscured part of her sullen look.

"I already tried," she said in a whisper.

He tried to give her an encouraging grin. "Doesn't hurt to try."

A ghost of a smile flickered across her face, then disappeared as if it has been erased. "No, I guess it doesn't. It never hurts, right?"

Renewing his efforts, Partel grunted and twisted.

She sat there, unmoving. Her dark-rimmed eyes watched his movements with an expression that looked sad more than worried.

He kept glancing up at her, a niggling feeling in the back of his head that suggested he knew who she was. But, for all of his physical efforts and wracking of his thoughts, he couldn't identify the young girl across from him.

After what felt like hours, he had to relent. He wasn't getting free. With a groan, he slumped forward. "I can't break free."

"I know."

"Why?"

"I already tried," she said in a quiet voice. Then she turned to reveal that her wrists were also bound behind her. Streaks of blood dribbled out from where the material had dug into her flesh, adding to a dark stain that had marred the stone underneath her. Even from a distance, he could see the droplets glistening on the rocks.

He cocked his head. "How long have you been here?"

Another sad smile. She ducked her head and lifted herself slightly and then down again. She let out a low groan that he could barely here. The rope rustled while she shifted her body from one side and then the other before settling down. She had a long dress that trembled with her movements. Little flecks of dust hung in the air around her.

He looked around. They appeared to be in a cell some sort but there was no bed, box, or anything else that suggested they would be there for any length of time. A frown crossed his brow. Why were they alone? For how long? Nothing around them suggested it was going to be a long incarceration, which meant someone should be coming from them sooner or later.

Partel glanced at her. "You aren't answering. How long have you been here?"

"Long enough."

"Can't be that long," he said with a grin. "No chamber pot. No bed." More revelations rose up, bubbling up from his subconscious. Curious of their source, he still gestured with his chin as he spoke. "Your dress isn't crushed on the sides, you've been kneeling since you put it on. I'm guessing... a few hours at most judging from how you're moving."

A smile flickered across her face for a moment. "You always noticed the little things." She sniffed and shook her head. "Damn you."

His body tensed, his stomach rolled with a fresh wave of nausea that he had woken up with. "I've done this before?"

She look away, her gaze drifting toward the door. "Yes."

An uncomfortable silence filled the room.

He watched her squirm slightly. It was obvious that she grew more uncomfortable with every passing second. Her eyes remained locked on the door and away from his gaze. He sighed. "What's your name?"

She shook her head.

"Does that mean I should just call you 'Girl?'" A memory rose up. "No... Glory?"

She jerked violently as he had punched her. A tear ran down her cheek. "Please don't," she whispered.

"Don't do what?"

"Don't notice things, just..." She groaned. "Just don't ask questions, please?"

The tension of her muscles worried him. She was holding something back, straining against it.

He cleared his throat. "What's wrong?"

"Nothing." Her voice was tense. Another tremor rippled along her arms. Slowly, she inched to wrap her arms around her stomach.

He shook his head. "That doesn't look like nothing. What's wrong?"

She looked at him, tears escaping her dark-rimmed eyes. "I'm hungry."

Partel didn't have to look around. Somehow, he understood exactly why there were only two people in the room. "There is only one thing in here you can eat, isn't there?"

Glory shuddered and a fresh tear ran down her cheek. "Damn you."

A grim joy flooded through him. He forced himself to look at her in a different light, to see how she was thinner than healthy with a paleness that would have had any parent upset. She was not eating enough. He rolled his eyes. "How long since you last ate?"

"Last moon."

Partel groaned and tried to see any sign of light from the outside. There was none. "Help me out, how many days?"

"Um, thirty... thirty-two?"

"You haven't eaten in thirty days!?" His voice beat against the walls. "What kind of monsters wouldn't let you do that? How are you... Damn the Couple, I'll kill whoever---"

"It was me."

His rant continued for a few more words before her words registered. He stared before slumping down. He felt old and tired for a moment.

Glory shrugged and her dress rustled. "I only have to eat when the moon darkens. It isn't that much but it takes about a week." She groaned and bent over.

Another wave of nausea rose up inside Partel. A groan escaped his own lips as he tightened his stomach muscles. Panting, he chuckled. "Let me guess, when you're hungry, you are really hungry?"

She nodded, peering at him through the ratty ends of her hair.

When she groaned again, he could feel it in his stomach. It was a wave of sapping strength and discomfort. No doubt, her hunger and his misery were connected in some way. He wanted to ask for specifics, but he probably knew enough: she was feeding on him by proximity.

Partel tried to recall what he had been doing a week ago, but his memories were fuzzy and vague. He couldn't remember much more than his name and waking up. To his surprise, he wasn't upset t hough.

His mind spun furiously. She said that he had done this before, which meant he had fed her more than once. The emptiness of the cell and their position implied someone had gotten the process of feeding Glory down to nearly a science.

Partel was cattle. But he had done it more than once which meant he had survived. He sighed and fought down the wave of nausea. Forcing his attention on her, he cleared his throat. "What do I have to do?"

She looked at him, her eyes slowly widened. Then a tortured look crossed her face. "How can you do that? How can you just give yourself up that easily? Every single time." Her tone was accusing but also pained.

Partel couldn't help but chuckled.

Her face darkened.

He shrugged. "I can't help it. I can't remember and I'm curious. It's obvious we've done this before and I survive, but I don't know the reason. What I do know is that you are in pain and hungry."

Another wave of nausea slammed into him. He let out a hiss of pain and leaned toward her. The wave grew stronger until he felt like he was seconds from throwing up.

Every part of him screamed to pull away but he forced himself to inch closer to Glory. The nausea grew even more intense, almost overwhelming, but he continued to move forward until he was literally gagging.

The air around her shimmered.

She let out another groan, but there was also a moan of relief mixed in with the voice. After a glance, she buried her face in her hands.

"That... work?" he asked through gritted teeth.

She nodded.

"Then, I'll just stay here." He didn't know why, but he wasn't worried about dying. The rest was just pain and discomfort, he could get through those. Another wave slammed into him and he could almost feel something being drawn out of him, like a loose thread coming off a shirt with someone who refused to stop pulling.

Seconds felt like hours. Before he knew it, he was dripping sweat and his entire body shook with exhaustion. He forced his eyes open after they had drooped shut. "Feeling better."

"Yes, damn it," she said through her fingers.

He groaned. "Then tell me why I don't remember."

He caught her glare for a second.

"There is a reason, right?"

"Yes."

"Then what?"

She didn't answer. Her hands tightened against her face.

"Come on. Why won't you tell me!"

"Because I hate this! You just give yourself. Every single time! You keep telling me you love me, but you can't believe that. I'm a monster, Papa! A monster! I'm not your little girl anymore and I haven't been for sixty years!"

Partel leaned back on shock. "I'm... your father?"

"Yes," she said with a scream. "You've been this for fifty years and I keep telling you to stop! Every time, I have to watch you go! Every. Single. Time!" she paused to let out a sob, "You keep dying to feed me and then tell say you love me!"

The tears were sheet down your face.

Glory rose up on her knees. "I'm not your little girl! I'm a Couple-damned monster and you. just. won't. stop!"

"Oh, dear," he said. Her father. No wonder she looked familiar.

His stomach heaved. He felt dizzy. He didn't have any other option. "I can get closer, can't I?"

"No."

He inched closer, fighting against the wave. "I can touch you, can't it?"

Her lips clenched together.

Partel kept moving. "I love you, don't I?"

She sniffed and nodded. "Yes, Papa."

He brought himself right up to her. Her presence burned his skin, whatever she used to feed was raking along his nerves and it felt like a fire had been ignited deep in his bones. With sweat running down his face, he rested his head on her shoulder. "Right here?"

Her sobs shook her head. "Right there, Papa."

"I don't remember this anymore."

"I know... you stopped remembering a long time ago."

He smiled to himself despite the agony that ravaged his body. He wanted to throw up, scream, or simply give up and die.

But he remembered the feeling that coursed through his thoughts: determination, love, and hope. It drove him forward despite the pain in his burning chest or the way his vision blurred once again.

She rested her head against his face.

"Do I have a life beyond this?"

A sniff. "No. Not anymore."

"What do you do now? Something... good?" He groaned through the words. "Something worth all this?"

"I follow after wars and help those had their families and farms destroyed by bloodshed. It was... it was what we all did before this happened."

He could almost imagine what she was talking about. "Something about houses? I remember a hammer."

A sad smile. "Rebuilding houses, planting new farms, providing seeds, materials, and helping hands." She sighed. "I haven't slept in years, so I work all night and day. I teach skills that were lost and leave behind books to help the orphans when I leave. While the others sleep, I build, repair, and craft. Every year, every moth. I work until I have to come back to Mama and you. Twenty-five days out, seven days in."

"Your mother is alive?"

No answer.

"Do you feed on her?"

"No. I can't."

"I don't understand."

The rope around his wrist loosened. He pushed himself up but there was no one behind him. To his side, the door creaked open to reveal an empty hall. He expected to see someone but there was nothing, not even a whisper of a sound.

The door closed and then swung open again, tapping against the frame in a rhythm that was deliberate.

"She's here too. The tower now. She came one with the stone when you became my... my...."

He grinned. "Larder?"

An intense wave of agony drove into him. Without a doubt, he knew he was coming to the end. He reached out to wrap one arm around her waist and hold her tight. "I love you, Glory."

His other hand reached out to press against the stone. He imagined he could feel his forgotten wife's heart underneath his grip.

Glory hugged him back, free of her bounds. "I love you, Papa," she whispered. "I'm sorry for what has happened."

He wanted to ask how it happened but he knew it wouldn't help him before he died. "I'm glad you are helping others. That is the right thing. You are doing good."

His throat seized up.

She hugged him tighter, burying her face into his shoulder.

Partel gulped to clear his voice. "Be glorious, my Glory."
