---
title: Promises Made
date: 2019-10-02
summary: >
  A grandmother should never see her child's child go off to war. Not when she had suffered through a war of her own, risking her life. But Majoril didn't have a choice when Janir decided to join the army.
---

> To ensure strength of character and society, every citizen between the age of twenty and thirty must give five years of service to community and country. --- Faunil da Disrobin, *The Perfect Country*

Despite being a brilliant summer afternoon, it was the darkest day of Majoril's life. She stood in the door frame of her cottage while watching her grandson checking his travel packs one last time.

"I'll be fine, Nana." Janir rubbed his palm over his recently shaved head. It was a precautionary measure, there was always a chance that the barracks would still have lice and bedbugs.

Fifty years ago, no one had told Majoril about the pests and she had found out the hard way. Her lips pressed into a tight line as the faded memories tried to well up from the depths of her past. Tears started to blur her eyes and she blinked them away.

Her grandson looked up and the smile froze on his face. He stood up and came over, carefully stepping over her tulips. "Nana, I'm going to be fine."

"I-I..." Her voice caught in her throat. She reached up and wrapped her aching arm around his shoulder and pulled him close. "I don't want to lose you too."

He had to lean over to bury his face against her shoulder. His arms wrapped around her, squeezing tightly. Despite the encouraging words, he knew there was a risk too. "Only five years. Five years and I'll be back. I promise."

Majoril started to cry. "Don't make that promise, boy. Not all of us have a home when we're done."

Her home didn't exist when she had gotten out of the army. The village had been destroyed in the Kormar-Gepaul border war and her home was now just a gutted foundation buried in the dirt. She spent ten years looking for family but couldn't find any before she finally settled down and had children.

He smiled, his eyes filled with the hope of a twenty year old. "I will because you're here. When I get out, I promise I'll return. No matter what river or mountain is ahead of me, I will come back."

Not trusting her ability to speak, she hugged him tightly until the wagon came to pick him up.
