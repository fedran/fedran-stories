---
title: Hopelessly Stuck in a Rainy Field
date: 2019-12-02
summary: >
  Visod had the grand plan of driving across through countries in his brand new steam-powered automobile. Before the end of his first day, he had managed to get stuck in the middle of a field. Could things get any worse?
---

> With the advent of steam engines, suddenly the so-called world records opened up to new possibilities. --- *The Rise of Steam*

Storm clouds boiled across the night sky but at least it wasn't raining. That was a small comfort for Visod as he had to yank each foot from the heavy muck just to work his way around his stranded vehicle. Wind buffeted his face, bringing the smell of an approaching rain to add to his comfort.

He reached a steep part of the field and planted his hand against the steel coils. The pipes were hot and he flinched but almost lost his balance. Swearing, he patted around until he found a cooler curve of metal and used that to push himself to the back.

The moonlight was a poor choice of lighting but it was his only option. He had to peer around until he found a spot on the bumper to set down his tools. After making sure it wouldn't fall, he focused on the other side of the car where a cloud of steam billowed out from a ruptured pipe.

A low whistle told him where the burst was but he couldn't see it through the cloud. He frowned and waved his hand, trying to clear it find it.

It was too dark.

"Damn machine," he grumbled. He blew at the steam and kept waving with his hand. "Where are you?"

The wind blew harder, steering the steam directly into his face. The heat slammed into him and he stumbled back, choking as he felt it prickling along his skin. He yanked back harder, his feet slipped on the mud.

"Shit!" he screamed as he flailed down, smacking hard on the ground. Mud splashed everywhere as he landed hard.

Visod glared at the boiling clouds above him. "Damn it."

This was not how his cross-country trip was supposed to go. He had only been on the road for two days and he wasn't even out of Tarsan. Two flooded roads had forced him to find a different route and he wasn't even over the Holigor Mountains. He was behind schedule for his "easy" trip he planned.

A droplet splashed on his face.

He glared at the sky. "Really? Now?"

In the distance, he could hear the hiss of rain as the wave of it came rushing toward him. The wind grew louder, blowing across his body to a crescendo.

With only a few droplets for warning, even Visod wasn't ready for the sheet of rain that beat down on him. There was nothing one moment and then he was almost drowning in the next.

He sputtered and spit out what had gotten into his mouth. He dug his hands into the mud in a halfhearted attempt to get up but then gave up when he slipped. The back of his head smacked loudly in the muck.

Visod let the rain spray his face, soaking his clothes and making the mud into a swill that would never let his precious vehicle out. A sob, obscured by the splattering rain, ripped out of his throat. This was supposed to be an easy trip. Just hop on his freshly constructed vehicle and drive across the country. Riders do it all the time on horseback, why couldn't an automobile? He wasn't even ten leagues out of Rapsutil and he just started this morning. Vehicles were supposed to be faster and more efficient, why was everything going wrong?

The rain stopped sharply.

He looked up to see the sky had been blotted out and an old woman was leaning over him. The rain smacked loudly against her poncho. Sheet of water poured down either side of her head.

Visod jumped in surprise. His foot scraped through the mud and he smacked back against the muddy water that had gathered around him.

"You shouldn't be sleeping at the bottom of a valley. That's where it floods." Her voice sounded like gravel, harsh and cruel.

"Of course," he muttered.

She held out a hand. In the light, it looked like she was mostly wrinkles. Rain poured along the ridges and dripped from gnarled fingers. Underneath her poncho, she wore a heavily patched pair of trousers and a baggy shirt.

He looked at her. "How old are you?"

Her face twisted into a scowl. "About as old as you are an asshole."

She pulled away and the rain hammered across his face.

He sputtered and scrambled up. "Sorry! Sorry, that was rude."

Hunched over, she pawed at the ground until she picked up a thick walking stick. The peaked hood of her poncho turned until he could see her eyes glimmering inside. "Yes."

Picking up a lantern, she started back up the hill toward the road.

Visod reached out. "Wait!"

"What, Asshole?"

He flinched.

She looked at him, a hint of a smile. He could tell that she was waiting for him to rise up and get offended.

He held up his hands. "Any chance you could help me?"

Visod pointed to the still steaming vehicle.

"What is that thing? Are you farming at night?"

"No, it's an automobile!" He realized he had pulled himself straight when he said it.

She blinked.

"It's a vehicle."

"What does it do?"

"It's... it's it travels along the road. Don't you read? It's the latest rage in... *Emerging Wizardry*" His voice trailed off as he realized she probably didn't read the foremost journal of engineering and design.

She shrugged. "I've heard of it."

"That's good because---"

"I remember wiping my ass with some of it when I had the runs. It was near the public latrine. Nice paper, good grip."

Visod closed his mouth with a snap. "That wasn't what I meant."

She grinned broadly. "I know."

He froze, confused and stunned.

The old woman turned and carefully walked over to his vehicle. "So why is this in a field?"

Visod blushed. "I hit a slick spot and slipped off the road. I was in a hurry and over-corrected."

"Must be hard to drive one of these things. They look heavy."

"Half a ton of iron and wood. Took me almost a year to build most of it."

She poked it with her stick. "How does it work?"

Visod brightened. "You see, there is a fire core in the middle. That's an enchanted rune of fire that...." His voice trailed off when he realized she didn't care in the slightest bit. "You don't care."

"Not in the slightest bit," she said cheerfully. She waved her hand through the step. "Looks like it broke over here."

Wiping the rain from his face, he staggered through the mud to the damage. "It was too dark to fix it. That's when I got up."

"Stupid thing to do, yelling at a box in the middle of the night."

The muscles in his jaw tightened. "Yes, I know."

She held her lantern. With the yellow light piercing the steam, it was clear to see that the gash was only a few inches across.

Seeing how easily it was to repair, he let out a sigh of relief. His trip may not be entirely ruined.

The he looked at the muddy field.

His joy faded.

"Shit."

"Focus on one thing. How do you fix this thing?" Before he could stop her, she reached out. Her fingers almost grazed the surface before she yanked it back. "Hot."

"It has a boiler inside. Could you please hold the light there?" Visod asked as he fumbled for his tools.

"Since you asked nicely."

He pulled out a few metal traps and a leather pad. Fitting the pad against the opening, he wrapped the straps around them and then ratcheted them down. The faint clicking of the gears grew strained as he pulled it tight. By the time he couldn't tighten it, the steam was clear and he could breath again.

Visod realized that the old woman had lifted her poncho to shield his work from the rain, making it easier for him to test the patch and make sure it was secure.

"So what happens now?"

"Well, I pour more water into the boiler and then wait until the pressure---" He tapped the fill cap for the boiler and then a nearby gauge. "---gathers up. Then it will be able to move the wheels and take us along the road."

He looked at the field. "Not that I'm going anywhere tonight."

"Eh," she said. "How long?"

"Well, there's enough water falling on me. I'll just need..."

She shucked off her poncho. Her frizzy gray hair plastered against her face in an instant.

"... a..."

She stretched out the poncho and used it to form a funnel toward the boiler. Almost instantly, water sluiced down and splattered against the copper side.

"... bucket," he finished quietly. Then he dove in to open it up and direct the water into the boiler. Steam rolled around his fingers as he listened carefully to the changing sound as the water poured inside.

By the time he could seal off the boiler, he felt a lot better about his prospects. "That's pretty handy."

"One things I've learned is that anything can be done with a good poncho, a reliable light, and a sturdy stick." She spoke cheerful as she shook the poncho clear and draped it over her shoulder.

"That's good advice."

"Took me a while to get it down to three." The old woman picked up her stick and then hobbled toward the front of his vehicle. Without a word, she crawled into the passenger side and sat down.

Confused, he came around and got into the driver seat. The steering wheel was tilted to the side but that was because the wheels were cocked. Around it, he could see the three gauges that measured the slowly building pressure. A canopy over them held back the air. "Now what?"

"You said we wait," she said, staring into the night. "How long until it heats up?"

"About a half hour."

"I'm in no hurry."

He grunted for a moment. Twisting, he settled into his seat.

The gauges slowly built up pressure.

They sat in relative silence.

He looked at her and then away.

She scratched her nose.

Rain splashed down across the hood of the car. It ran across the metal before peeling away. Underneath was his travel belongings: repair kits, a formal suit, two changes of clothes, and a good bottle of Dablux Merlot 93. Though, with the events of the day so far, he wondered if he would have picked something else to get him through a month's driving.

"Um...?" he started.

The old woman scratched her ear.

"What are you doing?"

"Waiting for this thing to get going."

Visod cleared his throat. "I meant, am I driving you to the nearest town?"

"Probably."

When she didn't expand on it, he stared across the mud. Even the small bit lit by her lantern, he couldn't imagine how he was going to get a half ton of iron through the slurry of mud and muck. It looked hopeless but he was still sitting, as if a road was going to appear by magic.

"... then what?"

"It depends. Where else are we riding?"

He tensed. "We?"

"You are wandering, right? I'm going to wander with you." She said it as if it was a fact of the world, like the ocean falling off the edge of the world or the wind blowing across the mountains.

"No, I'm driving across three countries in a month." He sat up straight and started to straighten his bow before he realized he wasn't wearing one. "It's a challenge."

"A race? Others trying to beat you in these things?" She tapped the floor board with her stick.

"No, just me."

Finally, she turned to look at him. "Why?"

Visod cringed. "Just because. No one else had done it, I wanted to be the first."

She smiled broadly, the wrinkles of her face smoothing out and her eyes sparkling. "So, you're wandering."

"I have a purpose."

"Same thing, you are going from one point to another simply for the journey. You are doing it just to go somewhere else but you don't really know where, right?"

He started to argue, but she was right. He grunted and nodded.

She smiled broadly. "I can help there."

"How?"

"Wandering is what I do. The path is always open as long as I'm going somewhere new."

He looked back at the field. "How?"

"Luck mostly, but also a willingness to change my direction like the winds. Keep moving forward and it will work out." She stamped her stick on the floor again. "Well, sometimes you have to go back first."

"You mean magic?"

She shrugged and grinned.

"I'm not going to use magic to do this trip," he said sharply. "That's my challenge, not to use powers. It's going to be fantastic, I know it."

She gestured toward the field of mud. "Including being stuck in mud in the middle of the night? Was that part of your plan?"

Guiltily, he looked out into the darkness. If he had access to magic to get him back on the road, he would take it in an instant. "I'd be lying if I said yes."

She let out a wheeze.

Concerned, Visod looked over.

The old woman was laughing.

"What? What's so funny?"

"You'd take a road if it showed up in front of you, wouldn't you?"

He cringed but the nodded. "I really want to do this. I just want to do it with at least magic as possible." He cocked his head. "I mean, I have to recharge the fire core, but that's it! Just the fire rune and nothing else!"

"Well, and maybe a clear road before you?"

"Yes...?"

She leaned over and peered at the gauges. "These things ready?"

He looked over them. Everything was pressurized. "Looks like it."

The old woman sat back. "Then let's get going!"

"We're in the middle of a field."

"Try it," she said cheerfully.

"It's muddy and raining and there is no traction!"

She looked and then gestured to the steering wheel. "Try it."

"Look, it isn't going to work." He reached down and opened up the pressure valves that brought the steam flowing to the gears and pistons that would drive the wheels.

The pipes rattled for a moment and then the wheels began to roll. Mud splattered everywhere as the metal rims spun helplessly in the muck.

"See, we aren't---"

The vehicle lurched forward.

He froze, releasing the handle. Stunned, he turned and stared at her.

She smiled and gestured with her stick. "Come on, lets go wandering."

Tensed, he squeezed the handle again. The stick vibrated underneath his hand as steam rattled through the pipes, flooding the system. The wheels spun in the mud, spraying it everywhere in thick sheets that sailed into the air.

His car lurched forward, the wheels somehow catching on rocks and sticks to

Wheels spun for a moment and then the car lurched forward.

This time, Visod gripped the handle tightly and locked it into place. He grabbed the steering wheel and held on as the vehicle jumped and jerked across the field. He aimed for the road but the car kept going to the side, sliding further down the hill. "No, no, no!"

"Just go with it."

"Easy for you to say!" He turned to snap at her. She sat casually on the bench, holding her stick lightly as she bounced and moved with the car. There wasn't even a hint of worry across her wrinkled face.

Visod grunted and held on tighter.

The wheels suddenly caught something and the entire thing lurched forward. It took him a second to realized they were on a smaller footpath that somehow had avoided the mud. With a whoop, he twisted and wove the car along the path toward the road until it launched itself up the hill and onto the gravel path.

Three wheels slammed down. The fourth joined them after a heartbeat of terror. Before he knew it, they were racing down the road with only her lantern lighting the way.

Trembling, Visod slowed the steam pressure to a more sane pace. His heart pounded violently in his chest, blood rushing in his ears. He was also smiling with the rush that sang through his veins.

The old woman let out a cackle.

As his pulse slowed, he held out his other hand. "My name is Visod."

"Wanderlust. Lust for short." She took it firmly, shaking it a few times.

"Really? That's your name?"

"Why not?" She said with a toothy grin. "I mean, that wasn't the name I was born with but I like Wanderlust enough. It's kept me going for the last sixty years."

He thought for a moment and then shrugged. "Wanderlust it is."

She was going to be a strange companion on his trip but, somehow, he didn't think he had a chance without her.
