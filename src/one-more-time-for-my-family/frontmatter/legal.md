---
title: Legal
---

Copyright © 2020 D. Moonfire\
Some Rights Reserved\
Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International

Cover art by D. Moonfire

All characters, events, and locations are fictitious. Any resemblance to persons, past, present, and future is coincidental and highly unlikely.

Some themes that appear in this story:
body horror,
death of main character,
death of named characters,
gore,
graphic death,
graphic violence,
home invasion,
undead,
violence toward children, and
violence.
There is no sex or graphical scenes.
There is no rape.

Broken Typewriter Press\
5001 1st Ave SE\
Ste 105 #243\
Cedar Rapids, IA 52402

Broken Typewriter Press\
[https://broken.typewriter.press/](https://broken.typewriter.press/)
