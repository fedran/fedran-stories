**He Would Do Anything For His Family**

Garkinal was a loving husband, a good father, and a devout man. He lived his life with pride but that didn't protect him when a trio of murderers break into his home and kill him before capturing his family.

He couldn't leave his family in the hands of cruel men. With his dying breath, he prays to the Divine Couple that they would give him one more chance to save them.
