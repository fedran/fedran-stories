---
title: Frozen Ropes
date: 2020-02-09
summary: >
  Garkinal was dead but he was still moving. Unable and unwilling to investigate why, he could only shuffle forward as he sought out someone who needed him: his wife.
---

> Only for terrible hatred and revenge will Death hesitate but for a moment. --- Gladston Kaber-Macnasis, *The Nature of the Living Dead*

Garkinal leaned to the side to lift his foot off the icy ground and lurch forward to move it. His hip didn't seem to work like it used to and he wasn't sure why. His step ended with a thud and he had to pause to remember how to move his other foot.

Around him, the world was filled with ice and snow. One bare foot crunched through the shell. He should have been cold, but he felt nothing as he dragged his ankle through the ice before tilting to the side to lift it again.

He was among the trees. He could tell that from the branches that smacked against his face and the bark that scraped his shoulders. Icy leaves clung to his face, half-blinding him.

Garkinal hesitated for a moment. It took another few steps before he stopped moving. His joints creaking, he looked up into the dark sky above him. Memories blew across his mind, howling and confusing. He remembered a sharp blow in his belly. There was a moment when everything grew dark. But then he was walking again.

Deep inside, he felt something moving him. It was a desperate need to keep moving forward. He was answering a call but he couldn't hear any voice over his ragged thoughts and the wind whipping past him.

His body swayed as he yanked his other foot free of the ice. The shoe over his toes was cracked and ripped. How long was he in the dark? He didn't remember snow and ice when he died.

In the distance, he heard a whinny of a horse. All of his senses focused on the sound as it echoed against the trees. His entire body tensed as he rotated toward it. The need to move forward redoubled and his feet started to shuffle faster toward creature.

Garkinal didn't know why he was drawn toward the sound but it was irresistible. He had to move faster. He gripped the side of a nearby tree and pushed himself toward the demanding sound.

Something tugged on his other hand. He stopped and tried to make another step. When he couldn't, he glanced down to see what was stopping him. It was coils of ice-covered rope that had caught on a broken branch.

He groaned and tried to tug them free. When he couldn't, he shook his head and tried to drop them.

His fingers refused to relax their grip.

Confused, Garkinal peered down at his withered hand. The skin was thin and stretched over his bones. He could see rents in the flesh but no blood stained the opening. When he tightened his grip---the only thing he could do---visible tendons flexed.

Swinging his head, he traced the rope from where it was coiled around his hand. Two loops hung below his wrist but the third curved back up and into a gaping wound in his stomach.

He wasn't holding rope, he was holding his own intestines.

He would have been disgusted but that part of him was long dead. Lifting his hand up, he snapped it down.

The coils of his frozen entrails twisted and moved on their own. They rolled over each other as they peeled off the end of the branch and hit the ground with a wet snap. When he pulled them closer, the ground underneath them was steaming as it cracked.

Turning back, he limped toward the sounds of the horse. He concentrated on each foot in front of the other. His organs tugged on the ground behind him, reminding him that he was long dead.

He crossed over a bubbling stream. His feet splashed into the water but he felt no pain from the icy tempature. He kept going. Behind him, his intestines dragged through the water and the sound of bubbling stopped instantly. When he turned around, he saw that the touch of his organs had frozen the stream solid.

He turned back and kept following the call.

Before he knew it, he was out in the middle of the road. His head rolled to the side as he looked down to see his family's horse pulled a wagon toward him.

He knew the wagon, he had built it.

He knew the woman driving. She was his wife at one point.

He knew the man next to her. He had killed Garkinal.

The urge to move grew stronger. He turned toward the wagon and shuffled toward it. His thoughts silenced as he focused on his murderer. Behind him, the coils of his intestines shifted and undulated, moving with the anger that rose inside him like an ice storm.

"What the hell? Run him over!" commanded his murderer as he jammed a knife against her.

Garkinal turned to face the horse.

Rain winched away from the blade.

"Now!"

She snapped her reins and the horse moved into a run.

Garkinal swayed to the side to move his foot. It crunched loudly as bones ground against each other. Underneath his bare feet, rocks cracked as they froze through and shattered.

The horse threw up its head and reared back in fear.

He swung his other hand forward, launching the coils of his frozen intestines toward his murderer. The ice-covered entrails snapped forward, curving to avoid the horse and his wife to wrap around the murderer's neck.

Garkinal yanked his hand back and Barn was torn from his seat. His body bounced off the back of the horse before landing hard on the ground.

Fueled by Garkinal anger, his entrails wrapped around his arms and legs before dragging him closer.

Barn bellowed out in fear and anger.

Garkinal didn't care. He pulled harder until Barn was at his feet. Reaching down, he wrapped his withered hand around the man's neck and picked him off the ground.

Barn's knife scraped against Garkinal's ribs. The scrape ratteled him but there was no pain. Only anger.

"You... hurt... my... family," gasped Garkinal, his voice coming out in a moan of icy wind. His lips cracked as he stared at the writhing murderer.

Barn stabbed him again. Rapid thrusts scraped against bones and pierced long-death flesh. His eyes were wide as he cut blindly.

Garkinal squeezed down, both his hand and the intestines that answered to his will.

Barn's skin grew icy and started to crack. His breath came out in a cloud tinged with red. Then, his skin began to darken as blood vessels burst. His eyes grew cloudy and then stopped moving as icicles formed across his eyes.

Garkinal kept squeezing until the body in his grip was nothing more than a solid hunk of ice. Then he tossed it aside.

Barn's corpse shattered on the ground.

Garkinal looked at the broken remains impassively.

Then a cry in the distance caught his attention. It was Janis, his daughter, and the sound was distant but clear as if she was next to him. She was frightened. He didn't know where she was, he just knew he had to go to her. With a groan, he leaned to the side and lifted his foot.

"Gar?" Rain's voice stopped him. "Is that you?"

He stopped and slowly turned back, his eyes not really seeing. His memories filled in the gaps: of his wife in her beautiful dress the night he was killed, when he first met her in the general store, and the one time they were cross-country skiing.

She held her hand out. "I-Is that really you?"

Garkinal stared for a moment. His memories fighting in his mind. He didn't feel like the man who married her. He was obviously dead, killed by a murderer. "Not... any... more."

"It is you. H-How?"

He didn't really know how he was moving, or what had happened. The only thing he remembered was the last few moments of his life. "I prayed. The... Couple... answered. Maybe."

Tears sparkled in her eyes. "I'm sorry." She took a small step closer. "I'm so sorry."

Something in his frozen heart cracked. He reached out for her with his free hand. He thought about the countless times they talked late into the night, the conversations over dinners, and all the times they spent with their children. "Did they... hurt you?"

She nodded and rubbed her shoulder. "They did. I did what I could to save the littles."

He smiled as he reached out to touch her cheek.

Her tears stopped rolling down her cheek as they grew bright. Then they froze in place, each one glittering in the light. Her breath came out in a fog around his rotted wrist.

Garkinal brushed them away. They fell as snow.

He leaned back and managed to step back. "One more..."

His entrails slithered back into his grip in a tight coil. It was his weapon against the men who hurt him. He didn't know how long he would last, but he knew the Couple would give him enough to finish the job. "time..."

The air around him grew icy. Ice crackled underneath his feet. "... for my family."

Lurching to the side, he turned around and began to shuffle home. His children were in danger and he had to kill the men who had harmed them.
