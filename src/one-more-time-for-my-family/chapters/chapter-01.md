---
title: Dinner Time
date: 2020-02-08
summary: >
  Garkinal loved his family and the best thing he could do for them is take them out to enjoy the local festival. However, all the cheer was ruined when their house was invaded by murderers.
---

> The blessed children bow their heads at every meal to give thanks to the Couple who granted them life. --- *The Marriage* 5:22

Garkinal tilted the steaming casserole away from his wife as they approached in the short hallway between the kitchen and the dining room. The potholders scuffed along the wall as he tilted his body to let her pass.

She swayed her hip out, skimming close to him, before twisting to bring her cheek near his mouth.

Obediently, he kissed her. The short hairs of his beard caressed her lower jaw. He took a deep breath to bring in the scents of candy, perfume, and smoke; all the smells of an enjoyable visit to the local festival.

The bottom of her evening dress caught against his leg before they pulled away, two parents crossing in the sea.

"I'm so hungry," announced Janis, their youngest daughter.

"Well, it's a good thing your mother made roast before we went to the harvest fair." Garkinal set the casserole down in the center of the table. When he spotted a hand reaching past him, he smacked it with the pot holder. "It's hot, Fal."

Falim yanked his hand back. "Can't you just cool it down with your magic?"

"You know the Couple doesn't want us to use our powers frivolously. You can wait."

"But it's been hours since we've eaten."

"Well, it takes a while to get back home. At least you got to sleep in the wagon."

His son made a face. "Janis snored."

Janis stuck out her tongue. "So did you!"

Garkinal leaned over. "Your mother also snored and I think she even drooled."

Both kids burst into laughter.

He patted Falim on the head. "Just give it a second to cool down. Your mother is coming back with bread and cheese. Don't say anything about the snoring."

"What about dessert?" asked Janis, bouncing on her chair. Her dress bounced with her movements, the material still a little stiff from when it was picked up that evening at the fair. She insisted on wearing it as soon as they had bought it for her birthday last month.

"Patience." Rain came out of the hallway with a platter of thick bread and thin layers of cheese. It would go complement of the casserole made up of alternating slices of roast meat, potatoes, and carrots.

"Did you see the size of that cake? It's huge!" Janis held out her hands far apart, easily triple the size of the actual cake they had purchased.

Rain set down the bread on the table and doled it out. When she finished, she smacked the top of of Falim's head to stop him from eating. "Prayers first."

"Yes, Mama." Falim put down the bread and bowed his head.

Garkinal chuckled and sat down at the head of the table. Reaching out to his children, he rested the backs of his hands against the tablecloth and waited for their small, delicate hands to rest on his.

Eyes closed, he took a deep breath. "Blessed are we in the your grace. Cherished are we with your gifts. Loved are we in---"

The front door of their small house slammed open. The glass panes that Garkinal had fashioned himself shattered, dropping to the ground in high-pitched snaps and cracks.

Janis let out a scream of surprise.

Garkinal got up thinking that the latch on the door had gotten lose. But when he saw three men standing in his living room, he came to a halt with his hand still on the back of his chair.

They were all armed, their short shorts shone in the oil lights on each side of the door. With the light behind them, they looked like nothing more than shadows.

The middle invader looked straight at him.

"What are you doing?" Garkinal asked loudly. His feet weren't moving despite the surge of fear that pounded in his veins and the tightness in his chest.

Instead of saying anything, the man stormed forward. His sword swung back.

Garkinal looked around for something to shield himself. Heart pounding, he grabbed the casserole dish from the table. Before the heat seared his fingers, he flung it with all his might at the leader as he entered the dining room.

The dish shattered against the man's chest, sending hunks of meat and vegetables in all direction. Some of it splattered across the white tablecloth and up against the ceiling.

The invader staggered back. "Shit!"

Garkinal flailed for Janis. "Get out!" he yelled, unable to look away. His hand smacked against plates and flatware. When he felt a butter knife, he grabbed it. "Get the kids out now!" he begged.

"Daddy!"

"Back door!" he screamed as he lurched forward.

The lead intruder shoved himself off the wall and charged forward. His sword swung to the side, whistling through the air.

Garkinal jerked back and then lunged forward. His own knife flashed out, but it was small and wooden. He had to lunge forward to cut at the man but missed by inches.

He didn't see the invader's left fist before it smashed into the side of his head. Stars exploded across his vision as he was thrown back. His feet skittered against the floor. He tried to catch the table with his free hand but missed.

His children's screams still beat against the walls and his ears but he was dazed from the sudden attack to turn around.

Then sharp agony tore through his stomach. The hilt of the invader's sword slammed against his gut as the end of the blade tore out his back.

The pain sapped all the strength out of Garkinal's legs. He sank to the ground, only dimly aware of the invader twisting the blade hard and then ripping it out of his stomach. It was followed by the sensation of coils of rope being poured into his lap.

Blinking through the pain that nearly blinded him, Garkinal looked down to see his intestines poured out across his legs and the ground. Torrents of blood poured out from gaping wound in his stomach.

"Get the rats, I'll deal with the mother," ordered the lead invader.

Garkinal tried to pull himself up but his hands were numb and slick. He couldn't seem to get them to work. Helpless, he pawed at his seat but only left bloody footprints on his seat.

The invader snorted and walked around him. He pulled back one foot.

Garkinal knew the kick was coming. His body wouldn't move.

It caught his face and his nose and jaw cracked from the impact. Then his shoulder hit the ground. Unable to control himself, his body slumped face-first into the pile of his own organs and blood.

Garkinal's breath came out in a slow wheeze.

Out of his sight, he heard his daughter scream shrilly. Then a smack silenced her.

Tears burned in his eyes as he tried to push himself back to his feet. He had to save his family, he had to keep fighting.

His bloody fingers slipped but he didn't stop try.

The world grew darker.

The back of his throat tickled but he couldn't cough.

The sound of Falim sobbing grew louder.

"I got the boy," announced one of the invaders.

Later, it was Rain that let out an inhuman wail. She let out a scream as her footsteps rushed across ground. Then she was next to Garkinal. "Gar! Please, Couple, don't let him die!"

She tugged on his clothes but it felt like she was miles away.

He couldn't move.

Then she let out a shriek and jerked away.

"Listen, you stupid cow." It was the lead invader again. He spoke in a low, sharp voice. "You have a choice. You listen to directions and you and your littles get out alive. You start screaming or fighting and you're going to join your husband in a shallow grave out back."

Garkinal's heart broke at the sound of his family in terror. He tried to move again but his body refused to listen.

"Obey and live," repeated the invader. "Make a choice right now or I cut your throat."

Rain sobbed and her body shook.

"Good. Now, this is what's going to happen. We need a place for the winter, nice and quiet with no surprises. You're going to be a good host. No warnings, no calling out to friends, no running away. Nothing that gives us away or I'm going to gut your children like a chicken."

Rain let out a gasping cry.

Garkinal cringed at the sound.

"Do you understand?"

"Y-Yes. Yes!"

"Come on." The man's voice faded. "Tell your children to behave. I need to clean up this mess but Cal is going to stay here and watch over you."

Rain's cries grew quieter.

Garkinal couldn't see her but he could imagine her shuffling to the other side of the dining room where the children were crying themselves. He wanted to do something but his body felt empty and drained.

He was dying.

"Get his feet, Rom."

Barely aware of his own body, Garkinal felt the sickening sensation of his limp body being dragged out the front door and across the porch. The fading part of his mind could still register the steps as they scraped his back and the thump as his head cracked against the stone path.

His murderers dragged him to the far end of the yard before dumping him by a tree. His head slammed against the trunk and a few black stars swamp across his vision.

"He's still twitching, Barn."

"He'll be dead by morning. Grab the rest of his guts and pile them on top."

"Really?"

"Suck it up and just get it done. You didn't have a problem moving corpses at the bank."

"That was---"

"Just do it. I'm going back inside."

The man named Rom grumbled as he threw hot coils of Garkinal's organs on his body. "So fucking disgusting. You better hope the animals eat you, but Cal's going to clean this shit up tomorrow."

He spat and then walked away, leaving Garkinal to die in the dark.

To his horror, he didn't die alone. Pain rolled across his body and he felt more scared than he had in his entire life. But his fading thoughts were focused on his family. They were going to suffer because he couldn't save them. He wasn't good enough.

His lips moved as he tried to whisper a prayer. The noises didn't come out but he imagined he was praying one last time.

Blessed are we in your grace.

A cold wind blew across his body, sending little spasms along the parts still dying.

Cherished are we that they grant us our daily bread.

A snowflake landed on his cheek.

Loved are we in your presence.

His thoughts were fading, drifting away into black spirals. Everything hurt.

Comforted are we in your embrace.

With his final thoughts, he pleaded to the Divine Couple with all his might. Please, let me save them. One more time for my family.
