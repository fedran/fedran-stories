---
title: One More Time
date: 2020-02-10
summary: >
  There was one more thing Garkinal needed to do before he could rest. His children were in danger and he had to rescue them.
---

> Blessed are we in your grace... --- Prayer to the Divine Couple

Time had no meaning for Garkinal as he shuffled past the wooden gate that he built years ago. He could have been walking for five minutes or an hour or even a day.

Behind him, he heard the crunch of Rain's feet and the squeak of the wagon. They were walking behind him in silence that was only punctuated by the occasional sob of his wife.

He wanted to look back but he didn't know how long he would still be moving. Could he risk seeing his wife one last time if it meant his children would suffer.

Garkinal kept his gaze locked on the front door of the small house. He had made it with Rain and her parents many years ago, all to prepare for the two children he could hear whimpering inside. A low moan ripped out of his throat in a cold wind.

He needed a strategy but his mind refused to think about anything other than bursting in the front door and attacking. The coils in his hands sifting and twisted with his thoughts; he could feel the air growing colder with their movement.

A sob came from behind him. "Oh, Love," she whispered.

He couldn't look back, he had to keep moving.

One foot after the other. He had to sway with every stop. Every stomp felt like it was more difficult than the last.

Then he reached the door. He tried to grab the handle but his body wasn't working right. Instead, he slammed face first into it with a thud. Garkinal backed up and groaned. He failed for the latch, trying to get his icy fingers to pull the lever.

"L-Let me," whispered Rain as she reached around him. Her body pressed against his. Heat burned his skin but it didn't matter as he enjoyed the caress. As soon as she opened the door, she yanked her hand back.

Garkinal could only groan before he thudded into the house.

Compared to the icy air around him, the heat of the house caused the air to waver and fog. It poured out across the floor as he shuffled toward the dining room.

One of the invaders was sitting in Garkinal's seat, as if he was the head of his family. He had his short sword strapped to his side but the weapon was sheathed. He was eating from a plate. He leaned back with a smile. "That you, Barn? You're later than I expected. Did she finally---?"

The man stopped as his eyes opened.

Garkinal threw the ropes in his hands. They sailed down the hall and landed on the invader's head and shoulders.

"What the---?"

Fueled by his rage, his intestines wrapped around the man's face and squeezed down. The air rippled as the temperature dropped rapidly, coating his body with ice in seconds.

Garkinal continued to shuffle into the room as his entrails froze his victim.

"Cal!" bellowed Rom from out of sight. "Get out of---"

There was a thud and then Falim let out a cry.

"---way!"

Rage burned through Garkinal. He squeezed down on Cal's neck as the room grew colder. He felt more than heard the crack of Cal's neck snapping inside the flesh confines of his body.

A sword came down on Garkinal's intestine, slicing it apart. Rom burst into sight as he rushed to his ally. "Cal!"

Cal's body slammed face-first into his meal, then his head rolled to the side at an unnatural angle.

Rom turned around. He was a handsome man, if it wasn't for the mask of rage on his face and the sword in his hand. His eyes stared at Garkinal for a moment, then widened. "What in the hell are you?"

Garkinal let out a groan and tried to summon his entrails. Only the part still connected to him responded. The rest of it was limp around the corpse's body.

Rom didn't wait for an answer. He raised his sword and charged with a yell.

Garkinal tried to lift his arm to block the blow but he was too slow.

The sharp edge slammed into his shoulder, snapping bone and rending rotting skin. Tendons snapped as his shoulder went limp. Rom followed the attack with a kick that caught Garkinal in the knee.

Garkinal fell. He tried to stop himself but his hands flailed helplessly before he crashed down to one knee. The impact jolted him but there was no pain.

Finally able to get his limbs working, he grabbed Rom by his belt and pulled himself up.

"Get off!" Rom slashed down with this sword, cutting and slicing into Garkinal's shoulder and head.

Ice formed underneath Garkinal's grip as he pulled himself up. Everything hurt but he had to finish. The short length of his entrails snapped around to envelop Rom. They pinned one arm against his chest.

Immediately, the bitter cold poured into the entry hall. Fog gathered around their feet and Rom's body crackled with it.

Garkinal stared directly into the man's face. "You... hurt my family."

"I-I'm...." The words ended as Rom's eyes and tongue froze into place. Soft cracks reverberated through his body as his heart gave one last beat before it was stilled forever.

Garkinal released Rom and the body thudded at his feet. He swayed for a moment as the rage poured out, leaving him with only foggy thoughts and a sense of the end. He shook his head and then shuffled over to his seat at the table.

In the dining room, Janis was helping Falim to his feet. They were both wearing their day-to-day clothes but even a brief look showed that they had bruises on their faces and they were moving stiffly.

His son gasped. "Janis!"

Garkinal swayed as he reached his chair. With a groan, he tried to pull Cal out of it.

Rain spoke up from behind him. "Kids, help your f-father." Her voice cracked.

Janis and Falim didn't move.

Rain came around. There were tears on her face as she tugged Cal aside, tilting the chair until it fell.

Garkinal tried to help but he couldn't get his limbs to move. He was getting closer, freezing over now that his family was safe. A smile crossed his lips, or at least he thought he was smiling.

Then Janis was there. Together, she and her mother pulled the corpse out of the chair and dropped it to the ground. Janis put the chair back on its feet and then yanked her hands away.

Garkinal sank into the chair. "Sorry."

Falim was on his other hand. He looked frightened and Garkinal would have done anything to not cause his own children to fear him.

"Sorry."

"No, no," gasped Rain. "Don't say that."

He kept looking at his son. A profound sadness filled his heart as he realized he wasn't going to last much longer. "I... I won't see you grow up."

Tears sparkled in Falim's eyes. "D-Daddy?"

Garkinal tried to lift his hand but it wouldn't work. The sword had cut through the tendons and that side of him was limp. He took a deep breath. "I need... you to grow up to be a good man. I can't come back. Be honest, be faithful."

Falim nodded, tears in his eyes.

It took all of Garkinal's effort to swing his head to the other side to look at his daughter. "I... I will miss you."

"Daddy...."

He tried to smile. "I will miss your beautiful smile and... seeing... you grow up to be a good woman. I wish I could, but... but... I'll have to do it by the Couple's side."

TO his surprise, Janis rushed forward and hugged him. Her sobs shook his body.

He patted her head as he looked at his wife. "I'm---"

"I love you," she said. She wiped the tears from her face. "I love you more than anything else. Don't you ever forget it, Gar. Never, ever forget it."

Garkinal managed a smile. "I will... always... love you."

No one said anything for a long moment. Only the soft sobs and cries filled the dining room.

Garkinal's thoughts grew colder and darker.

Then, he realized he needed to do one more thing. He looked across the table and then sighed. "Blessed are we..."

Janis stiffened against his body.

"...in your grace."

Rain let out another cry.

"Cherished are we with... with..."

Falim pulled back his chair and sat down. "... with your gifts," he finished.

Garkinal smiled. "Loved..."

Janis and Rain went to their seat as they joined in the prayer.

"... are we in your presence."

"Comforted are we in your embrace."

"Surrendered are we by your commands."

"Thankful are we for your guidance."

Then silence. They looked at each other and then at him.

Garkinal knew the end had finally came. With his last thoughts, Garkinal lifted his head to the heavens. "Honored am I... for giving me one more time."
