---
title: A Poisoned Gift
format: Flash
genres: [Slice of Life]
contentWarning: >
  Some themes that appear in this story: racism.
summary: >
  Lareal hated the dalpre with a passion.
---

He hated those furry bastards.

Twenty years ago, they were all in slave chains for occasional traders. Now that some country has "given" the lazy dalpre land to call their own, they have been steadily streaming through the passes.

He smirked. Wait until they see their new "home". All that sand and rock isn't going to be fun with the dalpre's fur.

That's what they deserve for fighting for "freedom".

In ten years, they'll be slaves again. Just as they were first created.
