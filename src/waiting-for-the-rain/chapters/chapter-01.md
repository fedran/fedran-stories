---
title: Waiting for the Rain
format: Flash
genres: [Slice of Life]
summary: >
  There were few things Hakas enjoyed in life, sitting in the rain wasn't one of them.
---

She hated the wait, sitting in the rain with nothing more than a few branches to protect her from the rain.

She peeked up at the sky and sighed. It was the color of her old priestess robes. She was glad she was out of them, though she longed for the heavy cotton to protect her from the damp.

A rattle of wood drifted through the trees. She perked up. Maybe she would find warmth tonight. A quick robbery, a night to celebrate, and weeks of eating before she returned to the road.
