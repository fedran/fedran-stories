---
status: alpha
title: Brickpunk
summary: "Defending ones's home against an intruder is more important than anything else. An elderly couple fights against the local baron in a mobile machine they built out of bricks from their quarry."
---

> One of the curious inventions that had no relation on Farimon's research was the rise of mechanical humanoids driven by spell and blood. --- Jastor Disal-Mesiam, *Machines of War*

"Damn it, Gertrud, get number two furnace working again! I'm almost out of the Agatha Red on the second cannon!"

She screamed something back in response, but Eustas couldn't hear it as his hundred ton mecha slammed into the side of the quarry. A line of bricks shattered along the rough rock wall and he threw himself to the side, steam engines groaning under the weight of moving so much hardened clay to the side. He felt the entire mecha shudder as the baron's dark bricks, Morning Ash by his guess and from a quarry nearly a hundred miles away, crashed into his left shoulder. He groaned and wiped the sweat from his brow, yanking on one of the hundreds of handles that filled his cockpit. The coal furnace roared to life, drowning out his wife's swearing and giving him the energy to righten the brick robot before it fell.

Falling meant death in this battle. Pulling up his left arm, he twisted the steam vent that pumped hot water and air down his arm, launching a single large brick the side of a man's head toward the baron's own black clay monstrosity. Aiming true, it smashed against the glass plate over the baron's head, but he couldn't afford any more shots as warning gauges shot into the red, then the brighter red.

He glanced at an indicator above his right shoulder. Still dark. Underneath it, Gertrud had playfully wrote "Oh, Shit" under it when they installed it together.

He smiled at the thought, then lurched the mecha forward. It pounded sluggishly along the bottom of the quarry, crushing rock into dust as he tried to find a better position.

Along the upper ridge, the baron did the same, circling around as shattered stone chips poured from the edge.

Peering through his smoke-blurred visor, he saw the baron's mecha glowing brighter with every step. Frightened and nervous, he reached over and pulled out a folder. Flipping it open, he stared at the hastily drawn images of the baron's mecha.

"Gertrud, what happens when the baron starts glowing?"

She stopped swearing long enough to yell up the tiny ladder. "Slag attack, chest cannon that blasts with superheated coal fire. You better damn well hope he isn't glowing."

"Um... he's glowing."

She went back to swearing, her white hair plastered against her head as she scraped bricks into place.

He thought furiously, mind spinning even as he yanked the mecha around to the only standing building at the base of the quarry. A blast of bricks, each one heavier than a horse, crashed into it, destroying his only shield for protection. He started to swear, then remembered the village priest chiding him for swearing too often.

Instead, he bit his lip. "Wait! Does that mean he'll lose his furnace."

"We're about to lose ours!"

"What!?"

He hated when his voice hit that high-pitched sound. His eyes rose up to the indicator. Thankfully it was off.

Then the "Oh, Shit" light flickered on.

"Oh, shit," he said. Damn the priest.

"My tits burning down here! Suggestions? And do it fast!"

"What's going?"

"Furnace one and three, maybe thirty more seconds!"

He thought furiously, feeling the seconds slipping away as the heat rose in the cockpit. "Redirect everything to the right side, overcharge the Gatling!"

"Um, honey, we haven't tested that!"

"He's glowing brighter!"

He had to remind his wife, just in case she managed to forget in twenty seconds. She swore loudly but he could feel when his mecha slowed down, settling into place. Hundreds of tons of brick over steel, powered by three furnaces and frantically armed in the middle of the night. Only his right side still functioned as he brought up his arm, the hundreds of carefully laid bricks that lined the entire arm spinning as heat poured off them. Clay dust ignited into flame as he aimed it up to the edge of the quarry.

The baron's mecha had split open, a gaping wound in his gut with a boiling storm of superheated plasma and coal. As Eustas yanked on the lever, he felt his mecha jerk violently and a stream of bricks flew out to stitch a line up the side of the quarry.

One way or another, this fight was over.
