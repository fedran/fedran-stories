---
title: Bullies
summary: >
  Still getting used to his new clan, Tsubàyo struggles with the bullying from many of the warriors. No one seems to be willing to stand up for him and he quickly reaches his limits.
date: 2020-05-09
---

> There is a distinction between the clan as the spirit itself and the clan as the members affiliated with the spirit. While many times, they are in agreement, there are times when they are not. --- *Clans of the Desert*

Tsubàyo yanked on the brush from the knot in his horse's mane. The pull took all of his strength and he could hear hairs ripping and snapping.

<<Ouch,>> came the mental complaint. It sung in the back of his head, the masculine thoughts of the horse he was grooming intruding directly into his thoughts.

He started to reach up but then stopped. Taking a deep breath, he pressed his other hand against the sun-warmed side of Ryachuikùo's neck. <<I didn't mean to pull so hard. Forgive me?>>

His horse let out a sigh of his own before he leaned into Tsubàyo. A wave of raw emotions, love and trust and affection, blossomed across Tsubàyo's mind and he couldn't help but bask in the comfort his mount gave him.

Tsubàyo's hands shook as he resumed brushing, this time with more care for his horse's comfort and less on his thoughts. The steady strokes of the brush against the short hairs were comforting. It was far different than picking sand out of his toes like most of his teenage years.

There were fifteen other Pabinkúe milling around in front of the table. Their conversations rose and felt as they talked about having dinner together or something interesting they had seen while on patrol. None of them included Tsubàyo as they spoke, but they never did. Instead, he just got the occasional glare or pointed look.

Ducking his head, he focused on brushing his horse and waiting for his turn with the stable master, Pabinkue Maporéku. She always took her time as she spoke to each rider as they handed over their horse. It was a nightly ritual but, at the moment, Tsubàyo just wanted to go home, have some dinner, and then crash for the night. He was tired of his fellow Pabinkúe and their words behind his back.

<<I could use one of those carrots though.>> Ryachuikùo pictured a stack of carrots in a basket by the front entrance of the stable. The deep purple stalks were treats for all the horses with a rich taste that Ryachuikùo loved.

Tsubàyo chuckled. "For your suffering, old man?"

Ryachuikùo's brown eyes stared at him. <<No, because I want it. But if pulling my mane would get me one, then... I'm in agony.>>

With a grin, Tsubàyo set down the brush and headed over to the basket. He circled around instead of going throught the knots of conversation. At the basket, he grabbed two of the large carrots.

"Those are for real riders," said Ukagòi as he yanked the carrots out of Tsubàyo's hand and back into the basket. "Not daylight-loving bastards like you."

Tsubàyo glared at the lighter-skinned man. He was a clan warrior, with far more fighting capabilities that Tsubàyo. "I am one of you, Great Pabinkue Ukagòi. I have been for many months." It had been five months since he had come to the Seven Villages, not that any of them would acknowledge it. Weeks of being being snubbed and insulted. Only a few spoke to him with scorn.

Ukagòi stroked his black beard and chuckled. Even though he was about the same age as Tsubàyo, he had more chest and facial hair. He had a necklace with one silver and two copper claws dangling among the beads; it represented the three battles that he had been in since becoming one of the clan warriors.

The side of Tsubàyo's face itched. He reached up and scratched the heavily scarred tissue. It always throbbed when he was angry or annoyed.

"You aren't one of us," spat Ukagòi. "You may ride our horses and wear our colors, but you'll never be one of us, killer."

The throb grew stronger. He tensed. "Great Pabinkue Mikáryo didn't think so."

Ukagòi laughed. "Mikáryo is a joke. Everyone knows the real reason she won't stay near the villages is because no one likes her. That and her perversions."

Tsubàyo ground his teeth together. As much as he disliked Mikáryo when she had handed him over to be beaten by the Shimusògo, he had grown to appreciate her forward nature and friendliness. She was one of the few people in the clan who welcomed him.

The other warrior in the group strolled up. It was Ukagòi's sister, Zumafín. She had the same sneer on her face.

Ukagòi shoved Tsubàyo's hand away from the carrots. "I'm never going to forget you killed one of ours to get in here. A pathetic little sun-lover that got kicked out of his own clan."

Zumafín came around to stand behind Tsubàyo.

Pinned by the two warriors, little warning bells ran off. The last time he was surrounded by warriors, the Shimusògo had beaten him until he couldn't breathed and pissed blood for a week. He turned his head to watch one and then the other. A prickle of fear rose up as he dropped his hand to the knife at his belt.

Ukagòi glanced down and then smiled. "Go for it, Ugly. I'll even give you one shot before I break your legs and snap your wrists."

Ryachuikùo's thoughts blossomed inside Tsubàyo. Their visions blurred together until Tsubàyo was seeing the fight from two angles, his own eyes and his mount's. The horse's desire to protect rose up as he shifted from one side to the other. The difference in vision, the wider angle from the horse, was only faintly disorienting but he quickly adapted to it.

Zumafín pulled her own weapon out slightly. The scrape of metal on leather was a whisper of noise but stopped. From Ryachuikùo's sight, Tsubàyo could see she only drew it out an inch or so. It was a threat, nothing more.

Another horse reached out to him. The warmth that of the equine thoughts calmed him even more. A third set of senses overlaid themselves on his and he could see the approaching fight from an additional vantage point. Moments later, more horses joined in until he could sense the entire stable yard as if it was nothing more than a tiny model. He saw everyone from all angles. He could hear their breath across twelve horses' senses. His skin felt the heat of bodies in the wind, mapping each one out relative to the herd.

A rush of power rolled through him. He shivered at the sensation of the herd in his mind. They comforted him even as they waited for an order. The warriors may be faster, stronger, and more powerful than Tsubàyo but even they couldn't take on all their horses at the same time.

The only two mounts that weren't in his mind were Ukagòi and Zumafín's. He could sense their horses hesitating, unsure if they would remain close to the mind of the warriors or split off to join the rest of the herd in Tsubàyo's thoughts.

A sharp clapping broke the standoff.

Pabinkue Maporéku stepped out of the door clapping her hands. "What in the light-blinded idiocy are you all doing out here!?"

Tsubàyo flinched and mental cut off the horses from his thoughts. The individual personalities faded away reluctantly, peeling away until only Ryachuikùo remained connected to him.

Ukagòi gestured at Tsubàyo. "Just telling this useless pile of horse shit that he doesn't belong. He spent all day riding up river to chase a rumor instead of saying on patrol."

Tsubàyo snapped back. "You told me too!"

"I did no such thing," Ukagòi said with a smirk. "I said there was a family up there but not anymore."

Enraged, Tsubàyo almost drew his knife. He had to force his hand away to point at Ukagòi. "You said no one had heard from them in days! Someone should check on them!"

"Of course not, they died years ago."

Zumafín snickered. "I heard that. Didn't the rest of you?"

There was uncomfortable shuffling and silence, then a few of the other riders nodded.

Tsubàyo glared at Ukagòi. "Chobìre shit in your skull."

Ukagòi laughed. "You even swear like a sun-lover! Pabinkúe is a night spirit, Ugly! We follow Chobìre, not that damn sun spirit you worship."

"I don't worship him!"

"You should, because you aren't Pabinkúe!"

"I am---"

"Quiet!" bellowed Pabinkue Maporéku.

Tsubàyo cringed.

The old woman took a few more steps to put herself between Tsubàyo and Ukagòi. She shoved her fingers into Tsubàyo's chest. "You, walk away."

"Like the---"

"Walk away, Boy!"

Tsubàyo stepped back, his body shaking with anger. "I---"

"Walk!"

He glanced at his horse.

"I will deal with Great Pabinkúe Ryachuikùo," she said referring to the horse respectfully. "But you go home. Right now."

Ukagòi waved his fingers behind Maporéku's back toward Tsubàyo.

Tsubàyo glared at him but he decided to obey. Reaching over, he grabbed three of the purple carrots and then stormed over to Ryachuikùo. <<Here, you deserve this unlike the rest of these assholes.>>

Ryachuikùo sent a wave of affection and joy as he chomped down on all three. They stuck out of his mouth like quills as he nosily crushed through them. <<Yummy.>>

Then, with everyone staring at him, Tsubàyo turned and stomped toward home.

"Bye!" Zumafín called out to him.

Tsubàyo hesitated. He ground his hands together but then forced himself to keep walking.
