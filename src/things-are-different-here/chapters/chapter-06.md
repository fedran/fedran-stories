---
title: The Morning After
date: 2020-06-13
summary: >
  After a night of confession, Tsubàyo wasn't ready to face the clean or Maporéku. However, he had duties to the clan and he would do it, even if it was going to result in a miserable evening to say the least.
---

> Even in moments of intense disagreement, the clan's unity binds tighter than anger. --- Woger Desaol-Kran, *Societal Norms Among the Desert Barbarians*

After a night of confession, Tsubàyo felt horrible as he staggered down the road toward the stables. The last of the daylight was fading and he desperately needed the little rush of pleasure that came with the moon rose above the horizon. It would be an hour before that happened.

With a groan, he reached out to Ryachuikùo with his mind. <<Evening.>>

The horse sent a wave of joy and affection.

<<I take it you slept well?>>

<<Like a foal in a warm bed of hay.>>

Tsubàyo chuckled dryly. He looked ahead to see that the Pabinkúe riders were already gathering to get the day's details and collect their horses. It was the morning ritual and one he never liked, but he dreaded meeting Maporéku after the previous night's dinner.

Then he saw Ukagòi laughing in a knot of riders.

Tsubàyo's stomach twisted in discomfort. He considered turning around and heading back home. He didn't. He had a duty. He was a Pabinkúe now and now he realized there were others who would stand next to him. Steeling himself, he forced himself to keep walking.

Ukagòi's voice carried over the din. "Oh look what rolled in horse shit. I thought I could smell something coming."

As the laughter rose up, Tsubàyo tried not to think about his sour stomach. He ducked his head and circled around to keep away from the abusive warrior.

He didn't get far before Zumafín step in front of him. He looked up at the slender warrior. She had her hands rested on her hips as she peered at him. "Where do you think you're going?"

Tsubàyo was tired of the two warriors. Every day was a trial, every time they found some way of making his life. He thought about his knife in his sheath and his fantasy from the night before.

Her eyes narrowed.

He kept his hand away from his sheath. "Just doing my job. Nothing more. Please just let me do that."

"Well, not over here. This is my spot now." She stepped back and leaned against the tree trunk where he used to stay most mornings. With a smirk, she crossed her arms over her chest.

Tsubàyo stepped back.

He thumped against Ukagòi's chest. The warrior had walked up behind him while Zumafín was speaking.

Ukagòi shoved him forward, laughing as he did. A few of the other riders also joined in.

Tsubàyo
A familiar anger rose up. He wanted to lash out at either one of them. It didn't matter if he would lose. He didn't even care if they broke his bones. He had to do something to stop it.

"Got a problem, killer?"

Tsubàyo balled his hand into a fist. The leather of his work gloves creaked for a moment. Then he thought about Fimúchi's look. He shook his head. "Pabinkúe accepted me, Great Pabinkue Ukagòi."

It took effort to keep his voice as calm as possible.

"You're still a murderer. There is no place here for you, no matter who you claim accepts you." Ukagòi poked him with two fingers into Tsubàyo's sternum. It hurt from the impact that staggered Tsubàyo back.

"Why won't you get the hint?" asked Zumafín. "No one wants you here."

For the briefest of moments, Tsubàyo almost said something about Adatái's depression and how she died but he dismissed it instantly. It wasn't his story to tell and he would take a thousand hits from his bullies than ruin the moments he had last night.

Raising his head, Tsubàyo looked directly into his eyes. "I am not proud of what I had done. I will never be proud of that night, but you don't have to keep reminding me. Pabinkúe has accepted me and that is the only thing I can accept."

He remembered how the clan spirit had spoke to him, the sound of a thousand horses in his mind speaking in almost perfect unison.

Ukagòi gaped for a moment and then his face twisted into a scowl. "Just because you---"

"Boys!" snapped Maporéku from the side.

All three of them looked at her as she strode through the crowd, the others parting around her to avoid her wrath. She stomped up to them, leading both Tsubàyo's and Ukagòi's horses.

Ryachuikùo radiated a wave of affection and joy to greet him. <<Good evening, Great Pabinkue Tsubàyo.>>

Feeling his horse's affection gave more weight to his words. The clan had accepted him, just not everyone inside it.

Edochyòbi, Ukagòi's horse, gave a passing of greeting before withdrawing his thoughts.

She jammed the reins into both of their hands. "You two need to stop fighting. Right now."

Ukagòi snorted. "It isn't a fight against a pathetic pile of horse shit like this. He doesn't have the guts or the ability."

Tsubàyo watched Maporéku carefully, unsure of her response. He could imagine she was finally going to yell at him, now that she was away from Tabyái and Fimúchi.

Her jaw tightened.

He tensed himself as he waited for the screaming.

Maporéku turned sharply on Ukagòi. "Then I guess you shouldn't be harassing him, should you?"

Ukagòi's smile froze. "W-What?"

"You are more powerful than him? Yes? A warrior of Pabinkúe blessed with the powers fight? You have dedicated your live, body, and spirit to this clan."

Ukagòi's lips pressed into a thin line. He shook his head. "Not for him."

The old woman jammed her fingers into his chest. It was the same two finger jab that he had used on Tsubàyo. Despite her being much smaller, Ukagòi staggered back a step.

"We are all Pabinkúe," she stated firmly. "Accept that or move to another stable."

Ukagòi looked frightened for a moment but quickly regained his composure. His face twisted in a scowl. "I'm not going to leave for a murderer like him. He'll be the one that leaves before I do."

Tsubàyo tensed, uncomfortable in the situation. Looking around, he could see some of the other bystanders were also twisting and shifting as they watched the older stable master speaking to the warrior.

She crossed her arms over her chest.

The air around Ukagòi's body began to waver. Unlike most of the clan, he could use his magic without the moon above the horizon. The pale blue energies danced his dark skin, lighting up the shadows of his face with flickering light.

Maporéku shook her head.

He shook his head. "I won't leave. You can't make me."

"I'm the stable master. My word is law when it comes to the horses who remain here. I am the charge of the comfort and care for your companions while you sleep." Her voice was steady and brimming with confidence. It was terrifying but also comforting that she appeared to be standing up for him.

Tsubàyo cringed, thankful it wasn't him even though he dreaded when she finally turned on him.

"When I say you are no longer welcome in my house, you will find a new place to house Great Pabinkue Edochyòbi."

Zumafín came around to stand behind him.

Ukagòi snarled. "I will take this to directly to Kamùji."

Tsubàyo inwardly groaned. Fimúchi's father made it clear he didn't like Tsubàyo for the same reasons Ukagòi didn't. There was no love between the two men and he suspected it would only get worse.

"And I will take it to Pabinkúe, you insolent foal."

An uncomfortable silence descended over the yard.

Ryachuikùo stepped up to Tsubàyo and bumped him gently. <<Are you willing to stand for yourself?>>

Tsubàyo didn't look to the side. He thought about how the five women responded to his story, the tears but also how they didn't lash out. He wasn't sure Maporéku wouldn't attack him in minutes, but for a moment, he felt welcome. <<Yes. What can I do? What should I do?>>

<<Great Pabinkue Maporéku asks if you want to stop the bullying.>>

Tsubàyo inhaled sharply. She did? <<Yes!>.

<<She suggests you call the herd to stand behind you. It is one way of showing that Pabinkúe is listening.>> Ryachuikùo's voice started to blur and grow hazy, as if there was another voice speaking at the same time as his.

Tsubàyo had heard the multiple voices of the horses before, but only when they spoke for the clan spirit. He wondered if he was hearing Maporéku speaking through Ryachuikùo.

Encouraged, he reached out for the surrounding equines. <<Stand by me, please?>>

A chorus of agreements rose up in his mind. In the corner of his vision, he saw one horse after the other sink into the ground. They disappeared into the shadows of the riders standing in the sun or the darkness of each other.

Their minds appeared behind him. He felt it as a cool wave of comfort as their senses melded with his own. They were staring at the stand off from different angles, each one adding detail to the scene.

"You can't speak for the clan, old woman. Only Kamùji can and he isn't here."

The words caught Tsubàyo's attention. He cast out his senses looking for Kamùji's horse, Zukejùfa. To his surprise, the clan leader was only a few chains distance from them. His mind meshed with the leader's horse instantly and Zukejùfa sent a wave of kinship back.

"I don't need the clan leader. The spirit is here. If you won't acknowledge my authority, you will accept Pabinkúe's!" Maporéku jabbed her hand into Ukagòi's chest again.

He grabbed her wrist. "You don't have that right. You cannot speak for her. He's a murderer and he doesn't deserve to be here."

Tsubàyo felt a surge of anger. He reached for the connection with Zukejùfa. <<Will you stand with me?>>

The answer came as the horse appeared next to him, crawling out of the darkness of his own shadow. The heavy body shuddered the ground as the leader's horse stood tall with Ryachuikùo and Tsubàyo between the two.

Tsubàyo let out a sigh of relief, then he noticed a boot against Zukejùfa's side. Fear flooded his veins as he looked up into Kamùji's nonplussed expression. He cringed.

Kamùji shook his head once in disapproval but then looked toward Maporéku and Ukagòi.

The pleasure of using magic rolled through Tsubàyo's senses. He reached out for more of the horses surrounding the stables. He asked for them to stand next to him, to prove his right to be in the clan.

They accepted.

Then he felt Edochyòbi and Gadòbi accept his thoughts. He had rarely connected to Ukagòi and Zumafín's horses but the connection grew strong. He decided to risk everything. <<Stand by me?>>

There were already a hundred horses quietly standing behind Tsubàyo. He could feel their presence in his mind and their thoughts in his head. They had accepted him, the spirit had accepted him.

Edochyòbi's thoughts were dark. <<I will only do this once.>>

Gadòbi was already dropped into shadows. The massive creature quietly sank into the ground, leaving not even a whisper of sound as he disappeared.

Edochyòbi joined him.

Ukagòi and Zumafín stood alone.

Lost in his own argument Maporéku, Ukagòi twisted her wrist. "Listen, you old woman. I don't care what you say. I will stay in any stable I want. You are nothing more than a caretaker and I'll be damned if I'm going to let an old woman tell me what I can or cannot do."

Next to him, Zumafín seemed to realize her horse had disappeared. She looked around behind her and then around. When her eyes lit on her mount, they widened with surprise. With a shaking hand, she reached up for her brother's arm and tapped it sharply.

Ukagòi started to shrug her off, but then he froze.

Kamùji cleared his throat.

Sweat prickled on Ukagòi's brow.

"I believe you were going to bring an argument to me?" said the clan leader in a low, threatening voice.

Maporéku gestured toward Tsubàyo and the horses behind him. "Or Pabinkúe? I believe she's here too."

Ukagòi's eyes scanned across the gathered horses that stood behind Tsubàyo. Different emotions crossed his face. Anger was obvious but Tsubàyo had never seen fear before.

Zumafín stepped back and away from her brother.

The abandoned warrior looked around the yard.

The riders that had joked with him were all silent. They all seemed entranced by the event but there was no question many of them wished they were somewhere else.

Tsubàyo felt the same thing. As much as it seemed like things were going his way, he knew it was only a moment before things turned. But until then, he just wanted it to end as soon as possible.

Ukagòi sighed. "Shit," he muttered. He turned to Tsubàyo and bowed deeply. "I apologize, Great Pabinkue Tsubàyo. I was out of line."

Tsubàyo felt no joy seeing his bully cowed. He just wanted it to end.

He looked up to see Maporéku watching him. He cringed and let out a sigh.

She nodded.

<<I don't like this,>> he sent to Ryachuikùo.

<<She says you did well.>>

<<Why?>>

Ryachuikùo bumped him. <<I'm not going to be a messenger for you. Hang around, both Great Pabinkue Kamùji and Maporéku both wish to speak with you. You also owe me at least two... no three carrots. Bunches of carrots.>> The horse threw back his head. <<I'm hungry.>>

He groaned.

Maporéku shot a glare at him. She turned back to Ukagòi. "You have a choice, stay or leave this stable. If you stay, I don't want to hear any more crap between you and Great Pabinkue Tsubàyo."

"I think it would be best if I find a new stable, Great Pabinkue Maporéku." While there was a sullen anger in his voice, he didn't seem to be seething. Tsubàyo wondered if he would be getting an unexpected visitor in the next few days.

Tsubàyo felt the subtle twinge of the horses wanting to leave. He gave him a wave of thanks. One by one, they headed back to their riders or returned to the stable. The ones that came from further away simple dropped into shadows and disappeared from sight.

While they did, Maporéku and Ukagòi spoke quietly. They finished and Tsubàyo noticed that almost everyone had left the yard except for him, Ukagòi, Maporéku, and Kamùji.

Ukagòi bowed to Tsubàyo again, then to Kamùji. "I apologize. I was out of line and let my anger drive my actions. I... will always have trouble accepting you, but I will keep it to myself from now on."

Kamùji said, "Good. I know that he came under the worst of circumstances, but Pabinkúe has accepted him and so will we. You did the right thing, Great Pabinkue Ukagòi."

Ukagòi looked relieved as he swept up on his horse and then dove forward. The shadows opened up and swallowed the horse.

Tsubàyo was left alone with Kamùji and Maporéku. He looked at both of them. His muscles tensed until his chest ached. "I---"

Kamùji interrupted him by holding up his hand. "Have you spoken to Fatobúpi?"

"I-I don't know who she is, Great Pabinkue Kamùji."

With a growl, Kamùji shook his head. "My former wife has brought to my attention that Fatobúpi, who was asked to guide your introduction to the clan, has failed to do her duties. The fact you are not aware of her name, nor had basic necessities, is an insult to Pabinkúe and to me."

Tsubàyo tensed.

Kamùji bowed. "I apologized myself for my inaction. Great Pabinkue Tabyái has graciously offered to help you become one of us."

There was the same sullen anger in his voice. He didn't like Tsubàyo, but he was being honest about his response. The only thing Tsubàyo was bow and accept it. "Thank you, Great Pabinkue Kamùji."

Kamùji reared his horse and then plunged into a shadow.

Tsubàyo stared at the spot, unsure of his feelings.

"That was the closest he will ever come to admitting he was an asshole."

He jumped Maporéku's voice.

"Tsubàyo? I'm sorry also. I had made an assumption about Adatái's death and treated you with disrespect." She rested her hand on his shoulder. "You are welcomed to my stable and if you ever need someone to talk to, please don't hesitate to ask... Tabyái."

He jerked. "What?"

She squeezed his shoulder. "I'm not serious. Ask any of us. Any time, morning or night. Even Hedáchi or Eramína. Your story has given them some closure in Adatái's death. That was something only you could have given and I can't thank you enough for sharing."

"I...."

Maporéku held up her hand. "Any time. I've heard that you can reach across the entire Seven Villages with your mind so you will have no trouble finding a horse near me. I always have one within my range."

She released his shoulder to pat it. "You did good, Tsubàyo. I hope that you feel that you've finally come home after all this time. I'm sorry that it has taken so long."

"T-Thank you."

With a smile, she turned and walked away. A few steps later, she held up her hand. "Today's duties are with Great Rojikinomi Fimúchi in the fields. I have no doubt you can find her horses if you try."
