---
title: Getting Even
summary: >
  Alone with his thoughts, Tsubàyo can't help but see the similarities between his early years with Shimusògo and his current time with the Pabinkúe. Seeing very little difference, he prepares to deal with Ukagòi in the same manner: by ambushing him and a drawn-out fight.
date: 2020-05-16
---

> In absence of a guiding hand, most young adults will revert to their early days even if those days left a sour taste in their mouths. --- *The Abandon Children*

Tsubàyo stomped down the dirt road with a foul mood flooding his thoughts. He replayed the argument at the stable endlessly in his head, working through thousands of "what if" scenarios. None of them were pleasant and each scenario turned more violent and destructive.

Both Ukagòi and Zumafín had been bullying him ever since he had arrived. It was insults that never stopped and being jostled in close quarters. He suspected the handful of sand fleas in his bedding was just another example. Sending him out on a fool's errand to a ruined household just to mock him was obviously Ukagòi's ideal of mocking him.

His face twisted in a scowl. It was just like growing up among the Shimusògo. None of the elders ever did anything to stop the fighting then either. They would just turn their heads away as if they couldn't see the pushing or arguments. The only time Tsubàyo had gotten in trouble was when the fight disrupted their meals or they broke something.

The fighting never stopped in the valley. He couldn't count the number of times a brawl had been broken up by Yutsupazéso screaming only to have it resume a few hours later in the tunnels near the entrance of the valley. He had suffered with his head smashed into the side of the rock tunnel or tripped over the edge. Even months later, he could feel the faint rasp of scars on his knees and elbows.

Tsubàyo shook his head. Spotting a rock ahead of him, he kicked at it hard and sent it flying into a nearby field. It bounced off a row of plants and then hopped over another before landing in the ditch between the short green leaves that sprouted from the ground.

On the far side of the field, he spotted a farmer leading a horse while kneeling every few rods. While the farmer was too far away to identify, the way they knelt down reminded him of Fimúchi. She was a farmer and a member of the Rojikinòmi clan who lived among the Pabinkúe.

His foul mood began to lighten. Only days after he arrived in the Seven Villages, completely lost and confused, she had shown him compassion that few others did. He smiled at the memory of having dinner with her occasionally or working long hours in the field until well into morning.

Then he remembered how her father, the Pabinkúe leader, had continually tried to keep them away. Kamùji allowed Tsubàyo to join the clan but it was clear that he hated that Tsubàyo had killed another Pabinkúe to do so. Or that he had stolen horses.

Grumbling, he resumed his walk home. Kamùji wasn't much better than Yutsupazéso. He looked the other way when others mocked and bullied Tsubàyo. He wasn't going to protect Tsubàyo any more than his old clan would. No, if he was going to stop the fighting, he would have to do it under his own terms.

His boots scuffed on the ground as he stopped sharply. Then he smiled grimly. He knew exactly what to do. There was always the equivalent of the tunnels near the entrance, the places where teenagers fought out their own disagreements. If not, he would just head over to Ukagòi's cabin.  One way or the other, they would stop this fight.

A plan in mind, he hurried home to the simple wooden cabin that made up his new home. In the months since he had arrived, he had done little to decorate it because nothing felt right. However, there was a small vase of flowers that Fimúchi had given him along with a book of poetry which he had only paged through once. Both were predominately placed on the counter by the eating area.

Slamming the door behind him, he hurried around the cabin: a quick wipe down with a wet rag, a change of clothes that didn't smell of dust and sweat, and then to gather up a brace of fighting knives. Ukagòi may be a warrior, but without his horse he was just as hampered as Tsubàyo.

With every item he gathered in preparation for a fight, he let his mind drift through his plans when he confronted Ukagòi. It had to be short, but he had to win soundly. It was better if he started on the offensive and attack first. Tsubàyo stopped to heft his best fighting knife before shoving it into a sheath and buckling it to his belt.

He looked around for a glove to protect his knuckles. The closest he could find was a pair of work gloves that Fimúchi had given him. They were perfect for punching. But, when he rested his fingers on the leather, he couldn't take them. It didn't seem right.

With a sigh, he turned around looking for anything else needed to beat Ukagòi. A small meal? No, he'll eat later. Best not to have a full stomach.

He hesitated but then remembered how Ukagòi had "jokingly" bumped him dangerously close to a ledge over the river that ran through the Seven Villages. Only a last-minute grab for his horse's mane kept him from plunging into the water.

With a scowl, he stomped toward the door.

Fimúchi stood on the other side, her hand raised to thump on the door.

Tsubàyo froze, a sudden fear as if he had just been caught by Yutsupazéso slammed into him. His hand tightened on his side, pinning the hilt of his knife against his side. Following the fear was a strange twisting in his stomach he felt whenever he was near her.

She was different than the girls he had grown up with. The Shimusògo spent their days running across the desert and scraping food off the bottom of the bowl. Fimúchi had curves in places he couldn't stop staring though he felt guilty for sneaking glimpses of her large breasts or the curve of her hips.

He didn't know what to do or look, so he retreated into staring into her dark green eyes and the tight braids on both sides of her hair. He remembered when she said she was going to grow the top out but hadn't seen her for a few weeks. Her look startled him as much as her appearing at his door. "F-Fimúchi? What are you doing here?"

She smiled and twisted the front of one foot. "I saw you walking home and you looked like you could use some company."

A flush burned on his cheeks. He gripped the knife tighter to his side. He wasn't sure why, but he didn't want her to know that he was going out to pick a fight. He gestured half-hardheartedly back toward the stables. "No. I mean, I was heading out."

"Oh," she smiled brightly. Her eyes trailed down. "Want some company? Where are you...?"

He cringed as her gaze focused on his knife.

There was an uncomfortable silence, punctuated by the song birds that were just beginning to wake up.

"Where you going?" she asked again, but the tone had sharpened into a blade.

"I-I'm..." He pulled the knife off his belt and hid it behind his back.

Her eyes narrowed. She push her fists on her hips. "What do you think you are doing, Tsubàyo?"

He opened his mouth.

"The truth, please."

Closing his mouth with a snap, he gestured angrily toward the side. "I'm tired of everyone just picking on me! Every single time, they are shoving and mocking and beating on me! They send me off on fool's errands, get me worried and then laugh when it's a joke! They throw fleas in my bag, worms in my lunch!"

He stepped back but couldn't stop speaking. Anger flushed through his body as he balled his hands into fists and shook them. "Every damn time! And no one ever does anything about it! I'm so sick and moon-damned tired of all this crap!"

Panting, he looked at her. When he saw her glare, he turned away from her.

"Do you think beating up someone is the answer?"

As much as Tsubàyo wanted to say yes, he knew that wasn't the answer. He hesitated but then shook his head. Somehow, talking to her was worse than Yutsupazéso and he didn't think that was possible.

Fimúchi shook her head but then smirked. "Yeah, I don't believe you at all."

Tsubàyo hesitated and then gave her a pained smile.

She smiled and then gestured for him to move aside. "Come on, let me in."

Without waiting for an answer, she strode in and shoved him aside. "If you think I'm going to let you get your ass kicked tonight, you are mistaken. You have decided to have dinner with me and I'm looking forward to you using that---" She pointed at his knife. "---to cut up the vegetables."

He gaped in surprise as she crossed the cabin and headed straight for the kitchen area. She bent over and he found himself staring at her ass. She yanked open one of the lower cabinets and pulled out a few pans. When she stood up, he looked away sharply.

"Stop staring and come over here. What do you have in the ice box?" When he looked back at her, she was peering inside. Clicking her tongue, she shook her head. "You don't have a lot of food in here. When did you go to the market last?"

Tsubàyo frowned. "I... It's been a while." About a week or so, actually.

"Is my father paying you at least?"

Tsubàyo nodded. Then, he rushed over to where he had been keeping the monthly payments. When he got his first pouch of pyābi, he was so excited that he bought too many sweets and drinks. A few days later, after starving for the night, he found that he had to buy more substantial food but he didn't always remember until he was hungry long after the markets had closed.

Fimúchi sighed and shook his head. "You got lost, didn't you? That shouldn't have happened."

She leaned against him, her breast pressing against his arm. Then she pushed away. "Come on, you need a proper meal."

He glanced at the door.

"I'm not letting you leave, Great Pabinkue Tsubàyo. Not to get into a fight," she said affectionately. "Besides, I think you should invite some guests tonight."

Tsubàyo sighed and then looked up in surprise. "What?"

She started counting chairs in his room including two padded stools near the door. When she glanced at him, there was a strange look in her eyes. "I really think you should meet some people. Do you think you could handle say... four more? You have six places to sit in here if we use the corner of the bed."

"Why?"

Fimúchi walked over to him and pick up his hand, holding it tight both of hers. "Because right now, you are lost. Things are different here and we don't let people stay alone like this for long. You need to know who to ask for help as much as they need to know they can help. I have some... friends who might be willing. They have been curious about you."

She squeezed. "If you are up to it, they really want to hear your stories too."

He cringed. "They don't want to hear any of that. I did... something terrible to get here."

"Really?" she asked with sad smile. "Didn't you tell me the same thing? I seem to recall enjoying your company for hours in the field. Or did you forget that."

He glanced to the side. When he took a deep breath, he caught her scent: of fresh earth and wildflowers. "No, I liked that."

"Good because I'm not the only one. I think you'll do better if you let others know where you came from. Please? They'll bring dinner and you don't have to say anything you don't want to."

His heart was pounding in his chest, slamming against his ribs as he felt the warmth of her body and her smile. He thought about his fantasies of attacking Ukagòi. As much as he hated it, they were fantasies. Ukagòi was a warrior, someone with the powers of defend his clan against intruders. No matter what skills Tsubàyo had, going one-on-one was a foolish idea.

Fimúchi smiled brightly. "Thank you. Do you know Great Pabinkue Tabyái?"

Tsubàyo shook his head. "Sorry."

"Wait... her mare is named... um... Atokáchi?"

He smiled. That name he knew. Most of the horses in the clan reached out for his mind when they were passing. They left their names behind. "I know Great Pabinkue Atokáchi. She is..." He frowned and reached out with his mind, spread out his thoughts as he felt the surrounding equine minds in his own. After a few seconds, he focused on Atokáchi who was being brushed by an older woman. "Found her. She's over in in the White Cloud stables."

Fimúchi patted his hand. "I'm impressed. That is a few leagues away. Tell her to tell Tabyái to bring her sister and her kin. Maybe for some conversation but definitely to bring food." Then she stopped and held up her finger. "Tell her to bring her favorite nut bread, the kind we were talking about it."

There was something in the way she said it. Tsubàyo gave her a quizzical look.

Fimúchi's eyes flashed and then she looked guilty for a moment. "Only ask if you want to have company. I really think you need someone with you tonight."

He started to open his mouth. The anger had dissipated and he realized he was craving her attention. He nodded slowly. "Yeah, you're probably right."

"Willing to tell your stories?"

"I suppose."

Fimúchi winked at him. "Then send the message and let's get ready for company. I'm going to hurry over to your neighbors and see if I can borrow something to start."

Tsubàyo stared at her in shock as she left the cabin. He felt strange, dizzy and hot at the same time. It was the same thing he felt whenever she was nearby and he wasn't sure why.

Then he reached out and relayed the messages.
