---
title: Memories
date: 2020-06-06
summary: >
  Tsubàyo is encouraged and pressed into revealing the one thing he didn't want to talk about again: the death that led him joining the clan. However, when five women ask him for details, he couldn't deny all of them.
---

> It would be less painful to cut one's throat than to press for the details of a mother's child's death. --- *A Song of Tears and Blood*

"... I was so angry at Jìmo after that. It felt like pulling teeth with every single action. Every command. He didn't want to be there. He should have stayed with Mípu and Dòhu. He... probably would have been happier with them."

Tsubàyo's throat was raw but he had to keep going. He took a sip from the wine and let the unfamiliar tang tickle his tongue before he swallowed it. The other bottle was empty and there was only a few inches left in the bottom of everyone's mugs.

He thought about those last fateful moments. Fimúchi's hand in his helped keep his courage as he realized he was about to described how he killed someone they probably knew.

Tsubàyo took a deep breath. "I was wandering in the dark, circling around the light from the fires. I don't know why but I kept seeing those dark horses in the corner of my vision. I could hear them... I didn't know at the time, but it was as if they were all calling to me. Just so many voices speaking as one."

He glanced up to see all of the Pabinkúe at the table were glassy-eyed as if remembering it themselves. Now, he knew it was the voice of Pabinkúe herself calling to him. It hurt to keep going but he had been speaking for hours and couldn't stop. "I just wanted one horse. I was going to ride it and get away from the Shimusògo and everyone else. Just one horse and I was going to ride until the sun rose."

The first clear voice in his head had been Ryachuikùo. The stallion was Adatái's. Usually one of the Pabinkúe bonded with one horse until they died. He still didn't know why the stallion had accepted him but he still remembered the first words that had blossomed in his consciousness: <<I've been waiting for you.>>

"H-He said he was waiting for me."

Eramína let out a sob.

He froze and looked at her.

She shook her head and then leaned into her mother's shoulder.

Hedáchi hugged her tightly and stroked her dark curls. "Please, don't stop." There were tears on her cheeks.

Tsubàyo gulped. He looked helplessly at Fimúchi who nodded in encouragement. "I-It was so easy. I just reached up and Ryachuikùo walked right up to me. It was so dark around me, but somehow I could see him clearly. When I crawled up and sat down on his back, it was... it was...." He didn't know how to describe it.

"Like you belonged there?" whispered Maporéku.

"Yeah," he answered in a low voice. "Everything felt so right in that moment, like I was touching all the horses at once. I could see the entire herd at the same time, twenty-three sets of eyes looking into the darkness. Even Mikáryo's Datobàpo's was there for just a moment."

His heart ached for that feeling again. Reflexively, he reached out Ryachuikùo. It didn't matter how far his horse was, the comfort of his thoughts in Tsubàyo's mind helped.

<<Go on,>> projected the equine.

"Please? Don't stop?" whispered Eramína again.

Tsubàyo nodded slowly. "I was about to ride away when Adatái grabbed my wrist. She had her knife out and tried to attack me. The blade cut my thigh, but the cloth caught it. I... kicked her as hard as I could."

The words froze in his throat. He stared at table as he remembered how she fell back. Even in the dark, he could see every detail as she cracked the back of her head on the rock she had used to reach him. Her body, wrapped in the black armored cloth the Pabinkúe used, crunched loudly and then slid down unnaturally. The blood smear on the rock almost glowed in the light.

In his mind, the memory brought a pang of sadness from his connection. <<I miss Adatái,>> said Ryachuikùo in his mind. <<She was my best friend since I was a foal.>>

<<Why did you accept me then?>>

<<It was time.>>

"Bàyo?" asked Tabyái. "C-Can you..." She choked on her own tears. "... please?"

He had to wipe tears from his own eyes. "I wasn't thinking when I did it. I just wanted to get away. She had jumped on a rock to get to me. When she fell back, it hit the back of her head. There was a crack."

Eramína let out a cry and dropped her head ot the table, holding her arms over her head. Hedáchi, sobbing herself, draped hrself over her daughter.

Tsubàyo stared directly at her. "I'm sorry. I didn't know what I was doing. I just... she died there. I-I saw the blood. And then she wasn't moving."

Everything hurt as he forced the words out. "I was so scared and I just kicked Ryachuikùo to move. He did and then we were running away."

Eramína looked up, her brown cheeks shimmering. "Why? Why did he abandon her."

"It was time," said both Tsubàyo and Hedáchi at the same time.

Hedáchi's eyes widened as she stared at him. He saw that Maporéku and Tabyái were also staring.

He gestured toward the stables even though they were a league away. "Rya-Ryachuikùo told me. He said it was time. I-I don't know why? I really don't. Why did he let me? Why did he leave her?"

Hedáchi squeezed her daughter as she spoke to Tsubàyo. "Adatái had lost herself. She struggled to wake up in the evening for months. None of us were able to help her because she didn't... or couldn't help herself. The trip with Mikáryo wasn't her idea at first, but then she hoped that seeing the rest of the desert would give her something to strive for."

Eramína lifted her head. "Ryachuikùo abandoned her."

Hedáchi hugged her. "No, love. They said goodbye. I have no doubt that she knew what was happening. Great Pabinkue Tsubàyo gave her a proper ending, a warrior's death."

<<She did.>> Ryachuikùo's mind was sad but steady. <<That trip was the end for her and we both knew it before we left. It wasn't anything we would tell Great Pabinkue Mikáryo or anyone else, but the decision had been made before you touched my mind.>>

He let out a choked sob. <<Why didn't you tell me earlier?>>

<<You weren't ready.>>

Tsubàyo couldn't form words, either with his thoughts or his throat.

Ryachuikùo sent a wave of affection.

No one said anything for a long moment.

Tsubàyo felt uncomfortable. He tensed, waiting for the screaming or the crying. One of them was going to attack him, he knew it. He wouldn't stop them though. He deserved it.

Hedáchi broke the silence by standing up, pulling her daughter to her feet. "T-Thank you for the dinner and the company. I think we should go now."

Everyone else stood up with them. Tsubàyo didn't know what to say.

Hedáchi looked at him.

He held his breath.

"Thank you, Great Pabinkue Tsubàyo. A mother always wants to know how she lost her daughter."

He froze in shock. His blood ran icy as he started to shake.

Hedáchi and Eramína left without another word.

Maporéku sighed and headed after them. "Come on, Tabyái, we should get going ourselves."

Tabyái nodded. "I need to clean up."

Maporéku held out her hand. "Let Múchi and Bàyo do that. We need to go now."

Then she looked at Tsubàyo. "I'll see you tomorrow at the stables."

"Y-Yes, Great Pabinkue Maporéku."

They watched the other women left. Then he and Fimúchi cleaned up the mess in silence. He cleaned the dishes while she transferred the rest of the food into smaller containers for the cold box.

He didn't know what to say so he said nothing. Everything felt wrong and he was waiting for Fimúchi to start yelling at him. A small part of him was still wondering why no one screamed at him already.

When they finished, she moved to the door and turned around.

Tsubàyo stopped in front of her. He cleared his throat. "Why didn't anyone yell at me?"

Her eyes were dark and there were tears on her cheeks. "Thank you. I know this was hard." She reached out to take his hands in hers. "I also hope you realize that things are different here. We aren't the Shimusògo. This isn't the valley you grew up. No one is going to beat you like Yutsupazéso."

She reached forward and kissed his cheek. "May I come over to dinner tomorrow?"

He realized he wanted to see her again. "Please?"

Fimúchi smiled and kissed him again, this time on his lips. "I'll be here at sunrise then."

Tsubàyo could only stare as she walked away in the afternoon's light.
