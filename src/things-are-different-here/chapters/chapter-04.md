---
title: Surprise Guests
date: 2020-05-30
summary: >
  Tsubàyo realizes that Fimúchi's plans were more than just a random visitor when her mother shows up at the door. Surprised and startled, he lets her in and quickly finds himself losing even more control over the meal.
---

> Society has a way of accepting the wayward clan who appear unexpectedly. They are given home, food, and shelter. Someone steps forward to guide them into the culture of their new clan and buffer them from the politics until it is no longer overwhelming. --- Kosobyo Janóki, *The Unexpected Clan*

A half hour later, Tsubàyo checked on the heavy dish inside of the oven. The thinly sliced vegetables and beef bubbled and sizzled and the smell caused his stomach to rumble with anticipation. He used a wooden spoon to check between some of the slices. "Not pink anymore."

Fimúchi leaned next to him, her body warm and the smell of her flowery perfume teasing him. "Good. Just give it about ten minutes or so and it should be ready. It smells good."

The flush of heat rose. He stood up and tapped the tray on the side of the pot of stew on top and then rested it across the opening. Inside, the contents were bubbling slowly as tubers rose and fell into the rich depths.

"Where is Réku and Dáchi?" Tabyái asked as she came out of Tsubàyo's bedroom. She wiped her hands on her waist and then headed straight for the door.

Tsubàyo stepped away from Fimúchi. "Are they on horse?"

"Probably not this early in the morning. Why?"

"I could have probably found them if they were."

Tabyái stopped and stared at him. "Where were you when you sent a message to Káchi? Atokáchi?"

"Here, why?"

"Really."

Tsubàyo wasn't comfortable with her last word. He looked around. He spotted movement through the window and leaned to the side to see three women coming up carrying canvas bags. He didn't recognize two of them but when he saw Maporéku, the old stable master who took care of his horse, his blood ran cold. While she greeted him with insults for some time, she had softened her attitude over the last few months. Now, she was indifferent toward his presence as long as he took care of Ryachuikùo.

The feeling of dread rose as they turned toward his house. "I... I think they are here."

Tabyái smacked the happily. "Finally!" she said cheerfully and pushed herself up. Coming up to the door, she flung it open as the three women came up the two steps to the porch. "Réku! Dáchi! Come on in!"

Tsubàyo's chest hurt as he watched the dour stable master come in. She was in her sixties with a wrinkled face and hazy eyes. She leaned to the side for a moment and then gestured for him.

Steeling himself for another berating, he stepped forward.

She looked him over a few times, and then she sighed. "This should have happened months ago."

Before he could respond, she held out the bag and gave what could have been called a smile if he squinted. "Welcome home and to the Seven Villages."

Tsubàyo's jaw dropped.

"Come on," she said as her smile widened. "It isn't snakes."

"Réku," said Tabyái in a low voice. "We're celebrating his new home. Be nice."

Maporéku looked back and then she rolled her eyes. She smiled, this time with more honesty and then gave a short bow. "Welcome, you should have been greeted with more respect months ago and I apologize for my part."

"I... I...." Tsubàyo found it hard to form the words.

"Open the bag at the table," she said before she stepped aside. The smile remained on her face.

Tsubàyo carried the bag over to the table and sat down. All five of the women gathered around, taking chairs as they stared at him. His hands trembled as he looked inside. It was a bundle of light blue fabric with yellow flower patterns. On one edge, he saw "Pabinkúe" and "Rojikinòmi" embroidered in subtle colors.

"Careful, there is something inside."

He pulled the bundle out. There was a picture inside, an oil painting of the mushroom forest. He could smell the paint as he stared at it.

Tsubàyo didn't know how to respond. No one had ever given him something like the painting before. He stared at it as his mind seemed to go blank.

"The fabric are drapes for your windows. Tabyái said they were your favorite colors. I hope they work, but it doesn't look like you have a lot of things here that would cause problems."

"He doesn't have much of anything," said one the younger of the two other women. She had a scowl on her face as she looked around his bare cabin.

"W-Why?"

The women said nothing.

"I mean, why are you doing this? Why this?" He held up the painting.

Tabyái reached out and rested her hand on his. Her skin was warm. "Because we should have done this the week you came into the Villages. You are new to Pabinkúe and we don't shun those chosen by the Herd. No matter how you came to us, what had happened, that shouldn't have stopped us from at least treating you with the respect you deserve."

Maporéku reached out and rested her hand on top of Tabyái.

Tsubàyo stared at them.

Tabyái pulled her hand back. "Now, open Hedáchi's."

Hedáchi was the older of the other two women. She wore Pabinkúe colors with a button-down shirt and a dark blue skirt. Her bag had a horse head embroidered on it and the clan's name along the straps. "Here you go, plus the bag is yours. It is good for shopping for food."

He pulled the bag over and peered inside. There was a quilt inside. The panels he saw were all blocked designs that looked like horses in various positions.

"There is another painting too. I got it at the same place as Maporéku."

The painting was older, with just a hint of dust on the edge. It was a summer scene of the covered bridge he had mentioned earlier.

His throat felt tight. "T-Thank you."

He saw a flash of emotion in Hedáchi's eyes but it disappeared when she smiled. "I'm sure you'll love it. It will help make this place feel like your old home."

Tsubàyo

She turned to the last woman. "Eramína?"

There was reluctance when Eramína pushed over her bag. He didn't know why, but it looked like she was struggling with the anger that most of the clan had shown him. However, the cuff of her shirt had Rojikinòmi embroidered on it. "It isn't much, but... welcome to the Seven Villages."

Her gift was a stack of plates and bowls along with eating utensils. They were all a pale yellow and sturdy.

"Thank you," he said, struggling with the emotions that were rising up. He wiped his face and set them down.

"Now, let's eat," Tabyái announced.

Along with the dish in the oven and the stew, the women had brought over a nutty bread, a crock of cream, and a pie that smelled of berries. All of them passed the food around and soon they were eating.

It was very good food.

As soon as she finished the stew, Fimúchi cleared her throat. "I'm curious. Could you tell us about your old clan?"

He scowled. He didn't want to bring up the anger of the Shimusògo again. It would just remind him of how the other Pabinkúe treated him. It was only a few hours ago he was wanting to ambush Ukagòi just thinking about how the Pabinkúe weren't much better than his former clan. He looked around at the others. Well, maybe not all of the Pabinkúe. He wasn't sure why Eramína came when she obviously detested him.

Fimúchi rested her hand on his. "Please?"

He looked up to see that the others were staring at him. All of them except for Eramína looked interested. Between her expression and his own reluctance, the idea of bringing up his past didn't sit well. He pushed his plate away. "I don't think you really want to hear that."

Fimúchi spoke softly. "Please? I think they need to hear it as much as you need to be heard."

He looked at her. "Why?"

"Because you've been hurting so much. I saw it before, but I didn't realize how much until this evening." Her eyes shimmered as she looked toward the counter where his work knife rested on the gloves she had given him. "I didn't know what to do without help."

Tabyái pushed her own plate away from her. "She's been worried about you for a while now. And Réku mentioned that you've been having trouble with some of the other riders."

The thought of Ukagòi and Zumafín brought a sour taste to the back of his throat. He scowled.

Fimúchi patted his hand. "I'm sorry I didn't tell you but I couldn't be sure you wanted help. You've been... distant lately and I thought it was because you didn't want me around."

Tsubàyo shook his head. "No!"

Then he realized he had yelled that. Blushing, he shook his head again and rested his hand on hers. "No, you've been wonderful. I would---"

There was a snicker.

He glanced over with a sinking sensation. Tabyái and Maporéku were smirking. Hedáchi had her hand over her mouth and was obviously trying not to smile.

Tsubàyo yanked his hand back and shoved them under the table.

Tabyái cleared her throat. "What we were trying to say was that we want to know where you came from. We know how you arrived, but your story before then is a blank."

He cringed. "You don't want to know that. I... killed someone."

Both Hedáchi and Eramína inhaled sharply.

Tsubàyo clenched his hands under the table. "I know her name was Great Pabinkue Adatái, but I don't know anything else about her."

There was an uncomfortable silence in the room.

"Can you tell us about what happened with Tái... Adatái?" Hedáchi's voice was tense but pleading.

His eyes burned. "Why?"

"Because we want to know how she died. Please? It's really important to us," pleaded Hedáchi.

He looked at her. The curiosity had gotten more intense. All of the women were staring at him and he felt like he was about to be gutted. Slowly, he shook his head. "I-I can't."

Eramína sighed.

Hedáchi reached up to take her hand and squeeze it. Then she turned back to Tsubàyo. "Just start at the beginning? Tell us about you being a kid? Were you a stubborn brat or a flower?"

For the briefest of moments, he though about Pidòhu. The slender boy was so weak and easy to bully. He wasted so many hours beating on him only to have the Pabinkúe do the same thing to him.

He took a long, deep breath. "No... I was the shit in this case. Got into a lot of fights, got beaten up, did my share. Actually, probably did  more than my share of beating than being beaten."

When no one interrupted him, he decided to keep talking. "There were these two weak-boned boys in the valley: Rutejìmo and Pidòhu. They were... pathetic..." He sighed. "Or at least I thought they were. Right up to the point the clan abandoned us in the middle of the desert."

There were a few gasps of surprise.

Somehow, that encouraged him. "We woke up in the morning to find that all the adults had ran away in the night. It was just the five of us: Rutejìmo who whined the entire time, Pidòhu who didn't really do anything, ..."

He had arrived in the Seven Villages five months ago after a month of traveling with Mikáryo. Somehow, it felt like yesterday as he lost himself in the story.
