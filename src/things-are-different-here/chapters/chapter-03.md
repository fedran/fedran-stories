---
title: Sweeping In
date: 2020-05-23
summary: >
  His rage interrupted by Fimúchi, Tsubàyo finds himself preparing a meal with the young woman. He can't help but wonder if she is going to spend more time with him but she had other plans in mind for that morning.
---

> The blood of the desert does not come out and say what is wrong. They guide the thoughts toward it, until there is only one logical conclusion that must be taken. --- Mudagre Afokanìmo

The smell of roasting peppers and tubers filled his cabin. It was probably the first time a full meal had been prepared since he arrived and he felt guilty for doing little to help it. Fimúchi had gathered up a startling supply of vegetables, fresh cuts of beef, and even two unfamiliar bottles of wine.

"Just slice those thinly," said Fimúchi as she rubbed spices into the side of the meat. The iron oven had a full load of charcoal inside it and the air wavered behind her. Even from the other side of the cooking area, he could feel the heat radiating from opening.

He turned back and focused on his task. He didn't know the recipe or how thing it should be, so it took care to cut each one almost paper-thin. He got into the rhythm and finished one purple carrot and move to the next.

Someone knocked at his door. He set aside his knife before he looked up at the entrance and then to Fimúchi.

She looked over her shoulder. "It's probably my m... friend. Answer the door? It's your house after all."

"You seem to have moved in."

She smiled sweetly and said, "Just for dinner."

His heart beat faster. With a flush on his cheeks, he wiped his hands on his trousers and answered the door.

Outside was an older woman carrying a large canvas bag with one hand and balancing a ceramic pot that steamed. "Here, hold this," she said with a smile and then handed over the bag. "The pot is hot and you don't have a pad."

Tsubàyo took it and was surprised how heavy it was. He stepped back as he looked over the newcomer. The smile on the side of her cheek when she smiled reminded him of Fimúchi and they had similar sorrel skin color. The only difference was that the older woman's hair was jet black and she wore dark blues and greens of Pabinkúe instead of the yellows and browns of Rojikinòmi.

He glanced at Fimúchi who was bent over her cooking.

She glanced at him and then back again. There was a furtive look in her eyes but a slight smile on her lips.

It only took him only a second to realize Tabyái was related to Fimúchi. Probably her mother judging from their broad noses and the way they held their heads. He hesitated in case he was wrong but then decided it couldn't get any worse. He bowed deeply to Tabyái. "Good morning, Great Pabinkue Tabyái."

Then, he called out to Fimúchi. "Your mother is here."

Fimúchi's hand stopped stirring.

Tabyái laughed and she patted his hand. "Oh, you're a bright one."

He thought about his plans to attack Ukagòi. "Maybe not the smartest though."

"No, but you're young. Everyone young is stupid. Come on. And don't use 'great' anymore, just call me Byái."

"Mother," Fimúchi said in a exasperated tone that sounded as if there were many conversations behind it.

"Yes, Múchi?"

Fimúchi tensed for a moment and then sighed. Shaking her head, she returned to her cooking.

Tabyái looked around. "Haven't decorated much, have you? I would... you should have at least a painting or drapes."

He shook his head.

"What is Búpi doing?" she muttered and then carried the pot over to the counter. She pulled a ceramic plate from a large pocket in her dress with one hand. Setting it down, she then slid the pot onto it. "Here's the stew. Give me a second."

She closed her eyes and her face grew slack.

In the back of his mind, he felt the ripple of her telepathically reaching out for her horse. It felt like fingers up and down his spine. While he caught glimpses of images and emotions, he couldn't overhear. Curiosity rose and he felt along the telepathic connection to see that she was talking to her more, Atokáchi. He withdrew before intruding any further.

A few droplets of sweat prickled on her brow.

Then she let out a gasp. "That was a bit far."

After discretely wiping her brow, she turned and looked at Tsubàyo. Her eyes scanned him from toes to head.

He shivered from the appraising look. Then he started to turn away as her eyes focused on the scars that covered the side of his face.

Tabyái's hands snapped out to catch him in place.

He tensed, his hand sliding toward his belt.

"I will beat you," she said smoothly without looking down.

With a blush, he lowered his hands.

"What are your favorite colors?"

Confused by the question, he answered automatically. "Blues and yellows."

A smile quirked the corner of her lips. "What's your favorite place in the Seven Villages?"

"Today, when I was sent on a wild errand up to an abandon house, there were all these trees with folded papers hanging from them. The ground was covered with this glowing moss or mushrooms and I thought it... was... nice?"

Tabyái released him and looked him over. "Anywhere else?"

"The Sakobi River, right at the covered bridge down the road from here. You know where the three roads meet? Why?"

She stepped back and held up her fist. Her eyes unfocused and he felt her reaching out for her horse again. The connection faltered. She frowned and pushed harder. After a second, her connection reached her mount and she sent another set of images before it slipped away.

When she looked around again, she caught him staring.

He looked away.

She slapped her hips and let out a happy grunt. "What can I do? Anything you need done? Your bed is a mess, let me get that, Bàyo."

Tsubàyo turned and watched her helplessly. It was quickly obvious that she was going to clean regardless of what he did. Confused and disjointed, he headed back to the cutting board.

"Mama is like that," whispered Fimúchi. "I hope she didn't upset you."

He frowned and shook his head. He expected to be yelled at or beaten, not have someone clean up his messy bed or the clothes tossed in a corner. "No... it's just...."

Tsubàyo didn't know how to say he was surprised not upset. He glanced at the blade he was going to use to attack someone for a moment and then over to the work gloves that still rested on the corner of the counter. With a shrug, he resumed cutting food for his unexpected visitors.
