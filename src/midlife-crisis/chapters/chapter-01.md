---
status: alpha
availability: public
title: Midlife Crisis
format: Story
genres: [Action, Drama]
summary: >
  Randal was bored. Bored of living life and bored of slaughter caravan guards to rob them. There wasn't anything left for him anymore other than efficiently ending lives. Then he met her.
---

> There comes a time when a man's heart yearns for a different path to have been taken decades before. --- Madikoma Ochinái

Randal yawned as he ducked underneath the crossbowman's wild swing. Coming up, he punched his spear into the man's gut. It easily sliced into flesh until the motion was stopped by his grip right behind the blade. As soon as his knuckles struck fabric, he yanked up and disemboweled his opponent.

The man's crossbow, a poor melee weapon, slipped from his fingers as he stared down in shock. His low lip trembled as he tried to say something.

Randal waited only a heartbeat before twisting the blade up to end the man's suffering. He stared into the man's eyes, trying to dredge up some emotion from his own heart as he watch the light fade. When nothing came, he tilted his spear back and let the corpse fall off the blade. It bounced once on the roof of the wagon and then plummet into the melee below.

On the other side of the roof, the final archer managed to get his dagger out of his sheath. With a scream, he charged over the rain-slicked surface toward Randal.

For a moment, Randal considered just letting the guard run him through. Pain would break through the miasma that had hung over Randal for the last few years and maybe he would be able to feel something new as he joined the bodies on the ground below.

But, years of fighting took over before he could steel himself for suicide. Using the heavy weight of the spear's blade, he let the haft slide through his hand before getting a grip in the middle. Stepping forward, he spun around and brought the blade across the man's exposed throat, right between the gap of his helmet and his shirt.

The blood spray followed his blade, curving in the air as it was drawing the edge. He continued his sweeping attack until he pointed behind him and toward the group. The blood followed his movements and sprayed into the battle below.

Looking around, Randal looked for another opponent, but the wagon roof had been cleared of archers. He pulled his other hand from his pocket, but when he realized he still didn't need it, he shoved it back in.

Thirty years of robbing and all he could do was slaughter. He sighed and stomped toward the back of the wagon. He was on the fourth of a five wagon train. Behind him was nothing but corpses and shattered steam engines. Once he finished the last one, he would be done for the fight.

Screams rose up from both sides of the wagons. The guards had outnumbered the bandits by three to one, but the ambush took out most of them in the first few seconds. He could hear Vikas, their leader, bellowing on the right side of the last wagon.

"Someone kill this fucker before he takes out my eye!"

Randal sighed. At least half of them were going to die anyways, why not the leader? Not that he would take charge. He had done that when he was younger and didn't enjoy it. Let some other idiot try to lead a group of disgruntled sociopaths.

Between the last two wagons, a steam engine chugged helplessly as it tried to drive through the wagon in front of it. The wheels spun helplessly in the thick slop of mud beneath it. The fourth wagon's engine was nothing but steaming wreckage and the heavy weight of the merchant's wares kept the final wagon from escaping.

A crossbow bolt whistled through the air, clipping his left arm. The shock of pain coursed along his nerves, a burning blast of agony that traveled up his arm. He looked at his attacker, one of the four archers remaining on the next wagon's roof. The other three were frantically trying to reload their bows, their faces pale as they felt their death looming over them.

He decided to give them a chance to kill him and hopped on the wagon. The heavy iron frame didn't shift with his weight. He concentrated on its insides, mapping out the complicated set of pipes by the water flowing through them. Finding a weak joint, he gathered the pressure around it so it would rupture.

His eyes never left the archers. One of them managed to get his crossbow lined up with Randal's chest. They were only two yards away, an impossible shot to miss.

He sighed and then threw his spear underhanded into the man's chest. As soon as the haft slipped out of his fingers, he ducked to the side.

The crossbow went off as the man screamed. The bolt missed him by only inches.

Randal lost his balance. "Shit," he muttered and pulled his hand from his pocket to regain it. He slipped along the boiler but managed to slam his foot back down and used the rain coating the metal to grip his foot. His magic held him safely in place long enough for him to step back on the top.

Two of the remaining crossbowmen hesitated, the tips of their bows lowering slightly.

Body singing with rapidly fading excitement, Randal stepped on the roof of the final wagon and grabbed the end of the spear sticking out of the dead man's chest. Yanking it out, he thrust forward thrice, once into each man's throat and dropping them instantly.

The blood that shot into the air curved back down and splashed around his spear. He swirled the blade around, picking it back up, and then swung his blade over the edge of the wagon. The blood followed, curving to match his swing, and then pouring out of sight when he stopped his swing.

"Damn the fucking gods, Ran!" screamed Vikas.

Randal shrugged and shoved his hand back into his pocket.

"Are you done stroking yourself up there?"

"Yeah," muttered Randal. He stepped over a corpse and then hopped down from the back of the wagon. The mud rose up to meet him and cushioned his impact. It sank around him and flowed away, leaving him untouched by the muck that covered everyone else.

On the ground, the battle was already over. There were bodies everywhere, both brigands and the guards. A few of them were still twitching and Randal spotted more than a few that had grievous wounds that no bone-setter or bandage could save.

Vikas stormed up, his boot sloshing through the mud. His beard and head was soaked with blood. He had managed to bandage up his own injuries; it looked like he got cut along the arm and a few strikes on the legs and hips. He was younger than Randal by twenty years, but the scowl reminded Randal of growing up with his father. "And why the fuck did you dump blood over the edge again? I told you stop doing that!"

Randal shrugged, his hand still in his pocket. "It always follows the blade."

Vikas wiped the top of his head and then shook the blood off his hand. "Stop fucking doing that. I'm tired of it."

Randal shrugged again.

Vikas' gaze scanned over Randal. "Did you just fight with your fucking hand in your pocket again?"

Randal looked down at his hand and nodded.

"Oh, the Divine Couple, are you getting depressed again!?" He said it as if it was a vile thing. "Every few weeks, you act like there is no point in life. Get laid or something, damn it!"

Struggling with the words, Randal tried to explain his feeling, or lack of them. The rush from being injured had already faded, leaving him nothing but a burning ache from his cut. He looked around at the surviving robbers. They were yelling and cheering, happily stripping bodies and gathering up the loot to fight over later. He remembered when he was in their place, but he couldn't remember what the excitement felt like anymore. He sighed and turned back to Vikas. "I'm not sure why I'm doing this anymore. What is the point?"

"The point," Vikas said as he clapped a hand on Randal's shoulder, "is that you are the best fucking spear in this part of the country and you can pretty much slaughter every fucking guard here without breaking a sweat."

"But why?"

Vikas' body tensed. Randal could feel the water and blood on the younger man's body map out his actions. On the far side, out of Randal's sight, the leader was tightening his grip on the hilt of his sword. Water coursed along the edge of the blade, clinging to the nicks and rust that punctuated the smooth line.

Randal watched Vikas and wondered what was wrong with him. Vikas was as good of a fighter as he was and even that failed to bring anything more than a dull fear bubbling out of his thoughts.

Releasing Randal, Vikas stepped back. "Just... just go finish off the ones that won't survive. We need to get going." He spun around and raised his voice. "All right, we're divvying up the loot in fifteen minutes! Get all the pretties up by the front wagon."

Cheers rose up from the brigands. They spread out among the dead and poured into the wagons to pull out anything of worth.

Randal yawned and headed toward the two with grievous wounds.

One of them was a brigand, a young woman he saw masturbating right before the ambush. Now, she was sobbing as she tried to keep her guts inside. Her one good eye looked up right before he buried his spear in it, killing her instantly.

The other was a guard who had his arm cut off. He was defiant as he glared at Randal, but there were tears in his eyes.

Randal ended him just as quickly. He had no taste for torture or prolonging suffering. Too many brigands had nursed septic wounds only to succumb in agony. A quick end is what everyone deserved, including himself.

He worked his way down one side of the wagons, making sure the dead remained dead and the suffering was ended quickly. His thought never drifted away from his internal struggles. Why was he doing this? What did he have after forty-two years of living? Wanted in most cities across Kormar and thirty years of sleeping in a tent.

As he did, the brigands ignored him as they looted the wagons. He saw barrels and heavy items tossed to one side and the easily portable goods, such as gold, crystal, and gems throw on a set of wagon doors ripped off and tossed on the mud. Later, they would be bidding on the spoils using shares they earned from fighting with Vikas.

Randal didn't bother with loot anymore. He took his share of the coins and had them converted scrips before sending them to a bank in a city by the coast. He didn't know why he was saving the money, he couldn't even enter the city to retrieve them, but he kept squirreling away the money in hopes that his life would eventually turn around.

Thirty years of robbing and he had nothing but skills to slaughter. He sighed and continued working his way down the wagon, lost in dark thoughts, and a vague desire for something new.

On the far side of the first wagon, he came up to a young guard trying to pull herself up using the spokes of the wagon wheel. Her body was slick with mud and blood, but she was also the first victim who was still moving strongly.

He pulled back his spear to cut off her head, but then stopped as she braced knee against the spokes and pulled herself out of the muck. The end of her leg was missing, severed at her ankle. She had a cloth wrapped around it, tied down with what looked like a crossbow string.

Randal frowned and peered at the ground below her. He could see where she had crawled through the mud and traced it back. A few yards from the wagon, he spotted both of her boots and the shattered remains of her crossbow.

Recognition blossomed and he remembered her from the top of the first wagon. She was aiming down into the melee when he cut her feet out from underneath her. But, he never would have expected her to survive the fall or the combat, but somehow she was still moving.

Sobs echoed against the wagon. She was terrified.

His frown deepened as he stepped closer. Years ago, he would feel the thrill of power but now he only felt a growing curiosity at her actions.

The young woman, he guessed twenty from the condition of her body, reached up for a leather strap hanging from the driver's corpse. A few inches above the strap was his knife.

Her hands slipped on the wheel and she dropped her hands to clamp down on the rim again. The cries turned into a wail of agony. Her knees thumped loudly against the spokes and slipped off the mud. She dropped a foot and hit the ground hard.

Emotions welled inside Randal, straining against the malaise that filled his thoughts. He knew they were rising, but he couldn't identify them inside his head. He didn't know if he was impressed or angry that she was trying to save herself.

She would never walk again. With the other guards slaughtered, the only thing left in her pitiful short life was slavery and violence. He knew she knew that, but she was also still fighting.

Trembling, she gripped the wheel and pulled herself up again. "Oh Couple, please help me. Please, please, please..." She whispered the words to herself like a mantra. He had to take another step to listen to them as she hauled herself up on the wheel and reached for the knife again.

Randal wasn't a religious man. He knew the Divine Couple wouldn't come down to save her any more than anyone else around him would help. He rested the butt of his spear in the mud and leaned against it, fascinated with the drive to survive in a woman half his age.

He wanted to ask her what kept her struggling, to see the determination and fear that he barely felt himself. It wasn't the brief rush of adrenaline that keep her moving, but a steady beat of terror that refused to let her give up.

One of the highwaymen came up. "Hey, look at that ass."

The injured guard stiffened. Her long brown hair shook as she started to look back, but then stopped. She turned back to the wagon and gripped the wheel tightly.

"I'm going to get me---"

"No," Randal said in his graveled voice.

The man tried to step around Randal. It was Garon, a former farm-hand who got kicked out for stealing. He joined the band a few months ago and was talented with a pair of daggers and a short bow.

Randal brought the haft of his spear to block him. "No."

Garon glared at Randal and scratched his crotch. "Why do you care? From what I hear, you don't like girls."

Randal gave him a long, hard look. "I don't like anyone, but you don't get her."

A scowl twisted across Garon's face. "Well, I want her." He grabbed the hilts of the daggers at his belt.

Randal sent power into the water around the butt of his spear. The mud boiled up and gripped the end. As he felt the shudder of it taking hold, he relaxed his grip.

"Well, fuck you. I'm going to have---"

At Randal's mental command, the mud yanked the spear back and tilted it so the blade was aimed for Garon's chest. Randal gripped the haft and shoved forward, using the mud holding the end and his own muscles to slam the blade between Garon's chest, piercing his heart with a single slice.

There was a brief gasping gurgle as Garon glared at him and then a thud as the two daggers hit the mud.

"Randal!" snapped Vikas.

Randal released the mud's grip on his spear and tilted it to dislodge the corpse on the blade. As soon as the body was clear, he swung it up and slammed the butt back into the mud. A splatter of blood fell from the blade and painted Randal's shoulder. He looked over his shoulder at his approaching leader. "Yes?"

"Why the fuck did you just kill him?" Vikas stopped next to him, his hand on his sword.

Around Randal and Vikas, the other brigands stopped and stared.

Randal peered over his shoulder at the young woman. She had slipped off the wheel while Randal was killing Garon and sat in the mud, one hand still clinging to the bloody spoke. Her bright eyes swam with tears and her entire body shook with the pain he imagined burned through her senses. It was the first time he saw her gaze and he could feel the determination and fight still burning.

It would take just a single swing of his spear to end her life instantly, but then he would have extinguished the first thing that had stirred his thoughts in years.

"Randal? Why the fuck did you kill him!?"

He turned back to Vikas. "He was going for her."

Vikas' right eyebrow rose and his jaw dropped. He glanced at the girl and then back at Randal. "You... you wanted a woman?" A smirk stretched across his face. "You were interested in anyone? I don't believe this."

Randal's jaw tightened as Vikas laughed. He knew that his disinterest had stretched out for a few years, but he didn't think it was worthy of mocking.

Vikas released his sword. "Well, you have years of shares to burn through. Throw her on the loot pile and we'll bid on her like everything else. We have rules, you know."

Randal was suddenly tired of robbing people. For thirty years, he had been ambushing merchants and killing anyone who got in his way. And, for just as long, he had been sleeping in lean-tos and in the mud, afraid to enter any city because of the price on his head. Decades and all he had was an empty life and broken thoughts.

He struggled to hide the epiphany from Vikas. With effort, he managed to keep it from his face and shrugged.

Vikas shook his head, the smile still on his lips. He turned around and raised his voice. "All right, bidding on this crap starts now. Remember, you got one share for every fight you've survived with me." He looked over his shoulder at Randal and then at the young woman. The smile grew wider. "Good luck. Bring her along, Ran. You'll get front row."

Randal watched as Vikas marched off with the others, barking out commands.

With a sigh, Randal turned and stepped over the young woman's feet to her.

She looked up at him, tears streaming down her face. "P-Please don't do this." Her voice was hoarse from crying and screaming. A long cut marred her cheek, it would scar if she lived.

Her hand slipped on the wheel and she slid down a few inches before tightening her grip. He watched her knuckles turn white from the effort to keep her up.

"I have to." He braced his spear and pulled his other hand free of his pocket.

"W-What is going to happen?" She had a soft, raspy voice. Her shoulders shook with the effort to keep her up against the wheel.

He looked into her eyes and hesitated. Tears had never moved him before, but there was something about her drive that made him pause. It was like finding a flower in a field of shit, something bright and delicate.

Randal opened his mouth to ask what drove her, but then closed it. A blush started to build under his beard and he coughed to clear his throat. "We always have this auction at the end. It's how we divvy up the loot. You are just loot to them... us."

"What's going to happen to me?"

"Really? You need to ask? Do we look like children picking flowers here?"

"I-I don't want that." She glanced up at the knife dangling above her. "I don't want to be a prize."

He took a deep breath and plucked her from the wheel with his off-hand. She was light, maybe eleven stones with her armor, and he easily tossed her on his shoulder. "Don't have a lot to say about that, do you?"

Up close, he could smell her perfume. It was a light, fruity scent and completely at odds with the blood and gore that streaked through the mud. He frowned as he felt something swelling in his chest, desire or something else. It wasn't comfortable and he realized that he longed for the moments when he felt nothing.

"Just don't take me there." He felt her tears splashing on his back as she cried. He expected her to ask him to kill her, but the words didn't come out. Instead, she cleared her throat. "I'll do anything for you, no resistance, no fighting."

He turned and stepped back over the corpses. "I don't like people anymore."

"I have money."

"I have more. I don't spend it and I've been doing this for a long time." He was also talking more than usual.

Her hand braced against his back. "Please? Anything? I'll do anything."

He hesitated at the edge of the wagon. Her other hand was working toward the knife in his belt. It was a dull blade, used for cooking, but the gall of using his own weapon brought a surprise smile to his lips. He shifted her body to bring the weapon out of reach.

Her body slumped over his shoulder, but her muscles were still tight.

He marveled at her fight. Even having her feet severed, she didn't give up like most of his victims. He felt the tension in her body and the way she kept looking around even as she cried. She wasn't surrendering, not yet at least.

Randal carried her into the gathered brigands. Besides Vikas and Randal, twenty men and women survived the attack on the merchant caravan. All of them looked hungry for loot and more than a few eyes were focused on the woman draped over his shoulder.

"Ah, Ran, the grand prize is here. Just toss her down and we'll start bidding."

Randal started to throw her down, but stopped. He frowned and straightened his back. Turning, he looked over his shoulder at her. "Why do you fight?"

She stared at him in shock.

A ripple of surprise ran through the other brigands.

Vikas looked at him. "Randal? What the fuck are you doing?"

He ignored everyone else. "Why? You haven't given up. You are crippled for life but still going for a blade. What keeps you moving? Why are you fighting?" In his heart, he felt emotions boiling up. Excitement and curiosity, the need to feel passion for something, anything.

She gulped. "M-My brother. I promised...."

"Randal!" snapped Vikas.

Randal looked at the leader and then shook his head. "Um, no."

Vikas dropped his hand to the hilt of his sword. "What did you say?"

"I'm...." Randal struggled with the words. The men and women around him were vile and disgusting, happy to spent their pointless lives killing and looting. He used to be one of them, but then everything faded away until he had nothing left. Life had worn him down to the bone. But the young woman on his shoulder, she had something that he wanted back: fear and fight.

He cleared his throat. "No."

A scowl darkened Vikas' face. "Repeat that?" As he spoke, he began to draw his sword from the scabbard.

"I'm done." It felt better than Randal thought it would. "I'm done robbing people and killing. I just want..." He looked at the woman's backside bent over his shoulder and then down to her hastily bandaged ankles. "... to take her to a church or something and get someone to look at her feet. And then... I don't know, take her home to her brother." He wasn't entirely sure what he was saying.

She stiffened in his grip, a sharp inhalation of surprise punctuating the uncomfortable silence.

Around Randal, he heard the scrape of metal against metal as warriors unsheathed their weapons. There were other sounds of violence, a low grumble of murderers and rapists having their prize yanked away from them.

He sighed.

With a snap of his finger, he commanded the mud to launch the spear and tilted the blade toward Vikas. The ground swelled and shoved forward, shooting the spear from Randal's slack fingers and across the short distance.

Vikas dodged to the side and the spear impaled an older man in the chest, pinning him to the wagon. The impact rocked the wagon and echoed across the fight.

"All right," snapped Vikas, "the person to kill him gets his shares and the bitch."

Fear poured into Randal. His heart beat faster as he looked around for a weapon to use. The woman on his shoulder was already reaching for his knife and he held himself still for the heartbeat it took for her to grab it.

Only a foot away was another of the other brigands. A middle-aged man who killed his wife and fled the law. He was a bastard and a cheat. Randal took a half step toward him and grabbed his face. Concentrating, he summoned his power and latched on to the liquid inside the man's body. Randal yanked his hand away and blood poured out of the man's eyes, mouth, and nose. It streamed into Randal's hand and formed into a visceral whip.

The man's scream died in a dry, wheezing gasp. His desiccated corpse hit the ground with a wet smack.

The other hesitated, he hoped by the shock of seeing him rip a man's liquids out of his body.

Randal stepped back, swinging the guard back, and then snapped the whip forward. It wrapped around the haft of his spear. As soon as it caught, he yanked back and ripped it from the wagon and the body it pinned.

Vikas swung at the spear with an underhand blow, clipping it.

Instead of flying to his hand, his spear shot into the air.

Randal swore and then jumped after it, using the water in the mud to launch himself straight up. The explosion of mud pushed others away, but he couldn't look down to find out if it would give him much of an advantage.

The woman on his shoulder only tightened her grip on him. He was surprised she didn't scream and was thankful for it at the same time.

He used the moisture on the spear to bring it to his hand. It smacked into his palm at the apex of his jump and he brought the blade down into the skull of the nearest standing man. The haft vibrated from the impact of body and his landing. He swung the spear around to throw the body free from his weapon.

In the brief pause, a wave of exhaustion dragged at his senses. Using magic to kill someone was far harder than using it to enhance his fighting. Even a single death was enough to bring black spots swimming across his vision. He groaned and pushed through the exhaustion to turn toward the others.

Vikas charged from the crowd, his blade glowing bright with his own magic. Randal brought the haft of his spear up to parry the blow, but Vikas' magic threw him back.

Randal dropped to his knees as he slid back from the impact. He felt it through his shoulder and back, muscles burning from where they tore. He gasped and staggered to his feet, cutting through the throats of nearby brigands as soon as he could swing his spear.

Vikas raced after him, boots smacking in the mud. "I'm so fucking tired of you crap!" he screamed as he brought the sword in a sharp blow.

Randal parried it and then brought his grip near the blade of his weapon. He knew that Vikas was trying to drive him back against the wagon or into the bushes surrounding the road. Both would spoil the longer weapon and leave him vulnerable.

He kneed Vikas in the groin, but the younger man shifted to take it in the hip. Randal tried to bring his other hand into the fight, to get the leverage he needed to fight with the spear properly, but the young guard's body stopped him. He gripped her tighter to avoid losing her in the fight.

Vikas slashed down.

It looked like a wide strike until Randal realized it was aimed for her, not him. Gasping, he spun around and took the hit against his shoulder. The magical blade cut through the light armor and bounced off the bone. The force of it drove him down, bending him almost in half before he could stop himself. He tried to stand back up, but the agony slowed him down.

Fear filled him. Vikas had won and it would only be a heartbeat before the blade pierced his back.

Vikas swore and the attack never came.

Randal gasped and stood up, staggering from the weight of the guard and the injuries that throbbed in his body. He was bleeding and some of the muscles were cut. If he had to attack, it would be agony.

To his surprise, Vikas had backed away and clutched his hand. Blood seeped from between his fingers. "You fucking bitch!"

Randal glanced over to see that she had stabbed Vikas with Randal's knife. The blade dripped with blood as she waved it around. With an approving nod, he focused back on Vikas and the rest of the brigands surrounding him.

"New plan," growled Vikas. "Kill both of them. And then we'll take her to a church to bury her. And her feet. And his head while we're at it."

Agony throbbed along his cuts. Randal breathed deeply, trying to find some way of finishing the fight quickly.

"I'm sorry."

He stiffened at her whisper. A smile crossed his face. "I'm not. It's just... oh, screw this." Switching his grip to an underhand one, he drew back his spear and aimed at it Vikas. With a grunt, he threw it with all his might at the leader's face.

Vikas rolled his eyes and stepped to the side.

Randal reached out with his will into the mud at Vikas' feet. It boiled up around his boots and slid him back into the path of the spear.

With a loud thunk, the blade buried into his right eye socket and burst out the back of his skull.

"Oh, fu---"

Struggling to keep standing, Randal gripped the liquid in Vikas' body and yanked back. Blood, sweat, and tears burst out of the now-dead man's corpse and flew back, dragging the spear with it. The heavy haft of his weapon smacked into Randal's palm just as a wave of dizziness punched him in the gut.

His eyes blurred and a ringing filled his ears. He looked up at the rest of the brigands and tried to speak in as threatening of a tone as he could. "Anyone else?"

The grip on his spear tightened as his legs began to give out. He clung to it and scowled, praying to gods he didn't believe in that they would choose to run instead of fight.

To his relief, they ran.

Randal's arm gave out and she fell off his shoulder. He clung to the spear, biting his lip to keep him consciousness. He had used too much power and the empty rawness that remained clawed at his thoughts. Black sparks drifted across his vision and he could feel oblivion dragging him down, pulling him toward unconsciousness.

He kept his position as long as he could but he didn't have any fight left. As he slipped to the ground, hitting the mud with his knees, he realized that he may never wake up. Fear filled his thoughts and he smiled.

It didn't matter if he woke up or not.

At least he felt something.
