---
title: Wounds
triggers: [Body Horror, Self-Harm, Amputation]
character: Balar
date: 2020-10-02
teaser: >
  Balar was a thief and a murderer. Happily so, as long as he got his money. Armed with an enchanted shovel, he had a long career behind him and a blood future ahead.
summary: >
  Balar was a thief and a murderer. Happily so, as long as he got his money. Armed with an enchanted shovel, he had a long career behind him and a blood future ahead.
---

> Every door on every street has its own story. Not all of them are centered around the people who live, die, and bleed inside. Some stories are the creatures that roam the walls or feed on the corpses. Some aren't living at all. --- Jaboril Hasik, *Behind Door 28C*

Storm clouds rippled across the sky, spewing rain like vomit across the streets. The wet sloppy droplets smacked against the roofs and walls before dropping to the cobblestones. A heavy mist blew in and out of the evening dark, obscuring feet, rats, and potholes alike.

It was the perfect weather for a bit of thievery.

Balar leaned just inside an alley while peering across the street to a brownstone on the other side of the road. It was a cute little place with a wrought iron fence and raised gardens on each side of the stone path leading up to the door. If he was the type of guy to retire, that would be the kind of place.

He scoffed. More likely, he would just use the brownstone as a safe house. The entire neighborhood would be a far better cover for laying low, ordering food, and kicking back while the heat died down.

The brownstone was owned by an old bat who lived by herself. Almost no one ever saw her outside of her place, but judging from the vibrant garden, she spent too many hours puttering around and taking care of it. He didn't know the types of flowers, but he guessed they were roses. That sounded like a flower, right? He shrugged and ignored it, he didn't care.

He wondered why she had bothered to set up umbrellas to protect some of her plants. A few lacquered parasols towered over the flowers closest to the house. Even from across the road, he could hear the water splattering off the hard ridges before pouring into a series of pots.

Balar didn't care about the flowers or the old lady. Rumor had it that she had survived three marriages and had a sizable fortune buried in the back garden. All he had to do was get rid of her and then dig out the backyard before it stopped raining. At least the wet ground would be easy to haul.

He sighed and grabbed his shovel. "The things I do for my weight in money."

Making sure the road was empty and the neighbors' lights were dark, he walked across the dark road and hopped over the fence. His heavy boots thudded into her garden, crushing the delicate flowers.

The sweet smell of the petals rose around him. He scoffed and dropped the blade of his space into the nearest parasol. It cracked and collapsed, crushing the plant underneath it. He gave it a vindictive twist before pulling the blade free.

Balar smiled to himself and tromped through the rest of the garden to the front door. Taking one last look at the empty street, he reared back. Flipping the shovel around, he slammed the blade with all his might into the latch.

There was a flash of light from his enchanted tool and the wood shattered from the impact. The metal bar that pinned the door shut snapped in half, falling to the ground with a loud clatter. He ducked inside while sliding his hand further down the shaft of his instrument.

The inside of the house wasn't much better than the garden. Flower pots sat in all of the windows. The old bat had vases everywhere, all of them filled with fresh flowers.

He sniffed loudly and then pulled a face. It smelled like some sort of creature had vomited petals everywhere. He stopped and then shook his head. No, that was a terrible analogy.

Balar decided just to focus on murder and robbery. He enjoyed that. He had to stop to correct his thoughts. He enjoyed robbery, murder just happened.

In a brief moment, he wondered if he should just walk away. To give up on breaking and entering, from stealing entirely. He could go home and swear off the last years of his life.

The feeling passed.

Moving his hand down near the head of his shovel, he bounced the sharpened blade against a tables and left a deep gouge in the wood.

Behind him, he heard the clacking sounds of a horse's hooves against the cobblestones. Fear crawled over his spine. He grabbed the broken door and pushed it closed. It wouldn't latch, but at least he could give it the impression of being closed as long as it took for the horse to pass.

The sounds outside grew louder.

His heart pounded. This was always the dangerous point of breaking and entering. He didn't have anything of worth to take with him if he had to break out and there was no way of bluffing his way out of it. He peered around the room in hopes of seeing a pile of money somewhere among the flowers, but didn't spot anything.

As the horse passed, he caught a hint of noise. Cocking his head, he turned and peered down the hallway toward the back of the brownstone. It may have been his imagination, but he thought he also saw a flicker of light.

Tightening his grip on his spade, he kept his eyes locked on the back of the house while the horse finished passing and the street grew silent.

Balar waited a long count before he cracked open the door and peered outside.

The street was empty and dark again.

He breathed a sigh of relief. Turning back to the house, he got ready to investigate the noise.

Before he shut the door, something caught his attention. He turned back to the garden out front and looked to the side. The parasols had all tilted, bent back to expose the flowers to the rain. The heavy flowers quivered with the rain striking the petals. It looked like a dozen little creatures staring at him.

The only parasol that remained in place was the one he had destroyed.

Balar frowned. "That's strange."

For the briefest of moments, he considered just walking out the door and escaping the flowery hell.

The lure of money brought himself back to his senses. He abandoned the door and hefted his shovel. Lowering himself slightly, he crept down the hallway toward the back of the building. He kept himself alert, waiting for a surprise attack or even an old lady in a nightgown looking for a midnight snack.

Like most brownstones, the back of the house had a kitchen area and a small eating area. True to the old lady's obvious obsession, the kitchen had dozens of little herb gardens on every surface. They were on the counter and the windowsill. A set of circular pots lined the table, leaving only space for a single person to eat.

Balar peered around before cocking his head to listen.

Someone hummed a song outside the back door.

He frowned. Was the old bat gardening at midnight? In the rain?

Walking over to the back door, he peered through the small glass panes and into the back yard. Fortunately, there was one street light that shone into the yard and let him see.

The old woman knelt in the grass. She moved slowly but surely while she dug a hole using a small spade. A few flowers rested in the rain next to her; the roots were wrapped in canvas and the petals fluttered in the rain. She wore an old nightgown; the rain had plastered the fabric against her thin frame.

Balar shook his head. The old bat truly was insane.

She picked up one of the flowers and unwrapped it. With her other hand, she wove what looked like a gold necklace into the roots before setting both down into the hole she had just dug.

"Masks of Shadows, steal my heart," whispered Balar.

A cold shiver ran down his spine. She was planting flowers and jewelry in her backyard. In her nightgown. In the rain.

Balar considered himself a lucky man. He eased the back door open.

The old woman didn't respond.

He slipped his hand down to the far end of the shovel. Grabbing it with both hands, he stepped off the back porch and walked across the grass. Rain splattered against his back, but the sensations quickly faded as he focused intently on his victim.

She made no indication that she was aware of him.

He let his spade swing back. It didn't even make a sound as the magically sharp edge cut through leaves and roots. He held his breath to avoid alerting her and then brought the shove up and over his head.

The supernaturally sharp blade cut into the back of her skull. It didn't even jerk on its path down her spine before the blade plunged into the grass below.

The old woman's body froze for a moment. The small spade in her hand tumbled out of her slack fingers. Then her split body peeled apart, falling in opposite directions to slump loudly to the ground.

Balar chuckled and used the shovel blade to knock the withered knees apart so he could kneel down where she had been. Her leg fell away at an unnatural angle revealing a dry spot under her body. With a smile, he dug into the dirt and yanked the flower free from the soil. The golden necklace tumbled to the ground.

He tossed the flower aside and grabbed the necklace. It was heavy with a large gemstone. Easily worth a couple hundred crowns and enough to keep him drunk for days. Laughing to himself, he dug into the next flower she had planted. His bare fingers dug into the wet soil. He could feel roots and stems but kept digging until his fingers wrapped around another chain.

With a triumphant grunt, he yanked it free. Another heavy necklace worth a few hundred crowns.

Balar looked around. There were hundreds of freshly planted flowers in the back yard. Each one of them had the potential for a prize underneath.

And he had a shovel. One that would make short work of even a hundred holes.

He grabbed his tool and use it to pull himself up. He had a lot of plants to dig up before morning.

It took him a moment to realize that he had not stood up. His hand slid down the shaft of his spade. With a frown, he grabbed it with both hands and tried to pull himself up.

Something held him down.

He peered down but it was too dark to see clearly. His legs felt as if they had fallen asleep. With a frown crossing his face, he reached down with one hand to feel around the ground.

His fingertips came up against roots.

Roots? He looked around again while focusing on his fingers. His eyes caught on the corpse of the old lady. He didn't notice it before, but her inner organs looked wrong in the dim light. Confused, he leaned closer to peer down.

Balar didn't spot the usual organs he expected: lungs, intestines, or anything else. Instead, the old lady's corpse had been packed with petals. They were the same types of flowers as outside, the ones protected by the parasols.

A cold sensation filled his body, it could have been fear but he wasn't sure. He hadn't killed the old lady, she was nothing but a shell. His earlier thoughts came back, giving up crime and heading home seemed like a better idea than ever before.

Something moved to grab his fingers.

Balar yanked his hand up. It was harder than before; something tugged at his fingertips. He pulled with all his might until he felt strands snapping. With a lurch, he pulled his palm into the light.

Tendrils were wrapped around his fingers, the torn ends oozing sap as they wiggled around.

"By the Mask!" he gasped.

Frantic, he grabbed his shovel and yanked it from the ground. The magical blade easily came free. Without giving a second to hesitate, he drove it into the ground on both sides of his knees to slice the roots and then lurched forward over the hole in the ground.

One leg came free but the other caught on the roots. He spun the spade and hacked away at the roots. After years of using his choice of weapons, he didn't even graze himself with each blow. Dirt and plants flew in all directions.

Balar grunted in fear and yanked with his might. The sensations were returning to one leg but his trapped limb continued to grow more numb. He could almost feel the sensations dying up his leg, like a poison.

"Shit, shit, shit," he gasped. He jammed the blade underneath his trapped limb and cut it free. The shovel blade shone with a dull glow and he crawled away from the boiling mass of roots and vines that had quietly erupted from the old woman's corpse.

He saw the two necklaces tumble away but dismissed them. His robbery had turned into a trap and he needed to escape more than he needed money.

Balar grunted with the effort to pull himself up but he saw that the ground underneath had begun to boil. Waves rippled underneath the grass and the flowers around him quivered to life. The movements were unnatural, moving against the rain that still splattered against the petals.

He tried to regain his feet but his trapped leg had lost all feeling. His effort pulled him more into the light from outside the garden and he could see the tendrils were wrapped around his limb. Blood oozed from where the roots had pierced his skin. His flesh boiled with the plants that started to crawl underneath his skin and dig into his numb muscles.

Balar almost screamed out in panic. He looked around for some place of safety, but the entire back yard had turned into a writhing mess of animated plants. He was trapped, caught like a fly.

Grabbing his shovel, he used the blade to cut away the roots and leaves that dug into his limbs. Tiny little cuts and slices soon left him bloody. It hurt but he couldn't risk letting them keep digging.

Despite cutting close enough to shave, the writhing sensation underneath his skin only grew more intense. He looked for more to cut but couldn't see any in the dim light. It was too late, they were inside him.

Death had come for Balar, but the murderer wasn't ready to give up quite yet. He looked around frantically for some way to safety. He was going to get out and then head straight out of town.

His eyes lit on the back door. It stood near the side of the yard and next to a tall fence that separated the houses. More importantly, the old lady or whatever the hell was in the house didn't have climbing vines on the walls. Nothing but white-painted wood. If he could make it there, he would be able to crawl up and escape.

Balar knew he would have been hard-pressed to succeed even if he was in good shape. But, with plants eating him from the inside, pulling himself free wouldn't be enough.

He knew what to do, but he didn't want to do it. He just didn't have time to make a decision.

"Ah, shit in my skull," he muttered before he gripped the shovel haft near the blade. Closing his eyes, he took a deep breath, and then slammed the blade hard into his thigh just below his hip.

The supernatural weapon cut easily through muscle and bone. With a sickening sensation, he heard but didn't feel his left leg snap. It fell away. A spurt of blood splashed across the grass which seemed to ignite everything into a fury. Even the grass appeared to boil, buckling the ground underneath and creating ripples that raced across the yard.

Balar didn't see the expected flash. There wasn't the heat he needed to stop the bleeding. He let out a groan and channeled as much of his will into the blade and slammed down, slicing off a thin slice of his leg.

The second blow came with a flash of light and the smell of burning flesh. Pain exploded from his injury and he let out a cry of agony.

Fortunately, the agony faded quickly with a surge of adrenaline that rushed through his veins. Knowing he only had moments before the rush would subside and he would be in agony, Balar fumbled with the leg that had been cut off. With blood spraying everywhere, he yanked the trouser off the limb and frantically use it to wrap the end of his leg. His fingers kept scraping against the burnt flesh that his shovel had cauterized.

If he was a different person, tossing his severed leg aside would have been surreal, but he didn't have time think about it. He slammed the spade into the ground and pulled himself up. He hopped across the yard. His blade cut through roots and leaves, leaving divots in the ground behind him. His foot kicked and tugged at the roots that caught his toes.

He slammed into the back door.

Inside, the herb pots had come to life and were writhing with anticipation. No doubt every plant in the house was going to kill him. He didn't even want to know what the flowers under the parasols would do.

With the ground heaving underneath him, he swung his shovel and slammed it into the back of the house. It thudded loudly into the solid wood. With a grunt, he grabbed the shaft with both hands and pulled his leg away from the clutching ground.

Blood dribbled from his wound. His movements had opened up the cut but at least it wasn't pouring out from an artery. He could hear it splattering loudly on the ground. Dizziness washed over him. He couldn't stop; if he did, he would be dead. With fear-fueled strength, he pulled himself further up until he could jam his body between the door frame and a nearby window. He grunted and swung his remaining foot to catch the bottom of the window. It slipped off from the blood and rain coating it.

Underneath him, the wooden step coming out of the house cracked with the plants moving underneath.

He stared down with concern. Without a doubt, he should have walked away from the robbery. If he managed to get out, he was going to stop robbing.

"I promise!" he grunted and tried to get his foot brace again. Every muscle in his arms and legs ached with the effort to hold himself up on the spade. He couldn't find the words but he would do anything to escape: stop robbing, stop killing, stop anything. Hell, he'd donate money to orphans.

Balar's toes finally caught on the windowsill. Gasping in triumph, he shifted his position and put his weight on his foot so he could yank his shovel free. The blade easily slipped out with the proper grip; it was part of the magic when he had enchanted it.

Beneath him, the living plants were in a fury as they chased after the river of blood that poured out from his body.

A wave of dizziness slammed into him. His fingers grew slack.

"No!" he screamed and forced his fingers to grip tighter. He reached out for the side of the fence but missed.

"Please!" he gasped. "I promise I'll never do this again!"

He swung again.

"I swear!" he screamed at the top of his lungs.

To his relief, his fingers caught the edge of the fence. For the briefest of moments, he could hold himself still but then his fingers began to slide on the rain-drenched edge. Desperate, he dropped his shovel and grabbed the fence with both hands.

"Fuck!" he screamed.

Balar wasn't a fool. He already knew what would happen if he tried to rescue his instrument.

Using the last of his strength, he hauled himself over the fence and threw himself over the other side. It was too dark to see but he tried to throw his arms ahead of him to catch himself.

The ground crunched into his body. The bones in his one shoulder snapped followed by his one good leg. Gravity's cruel claws drove him harder into the ground with a flash of tearing muscles and more broken bones.

The impact drove the air from his lungs. He struggled to move, his mouth opening and closing but no air flowed into his stunned body.

With tears in his eyes, he looked up to see someone standing in the back door of the neighbor's house. Their forms were only a blur in the rain and his agony.

Shaking, he held up one hand. His lips worked silently, trying to beg for help, but no noise came out.

Oblivion rushed up, pounding in his ears and darkening his vision. He didn't know what would kill him, blood loss or the bloody roses next door. If he managed to survive, Balar knew he would honor his promise. No more killing, more stealing.

Only if he lived.

"P-Please?" he gasped before slumping the ground.
