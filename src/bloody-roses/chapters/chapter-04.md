---
title: Surrender
date: 2020-10-29
character: Balar
contentWarning: >
  Some themes that appear in this story: body horror including eyes, death of named characters, and graphic violence.
summary: &summary >
  Balar managed to make it to spring with his new life of helping the city guard investigate crimes. But he wouldn't get much further before his betrayal came to hurt him.
teaser: *summary
---

> Blood brothers never forgive a betrayal, even if it may take a lifetime to execute their revenge. --- *Jack of South and Waters*

Somehow, Balar had made it through the winter but he was in a far different situation than the before.

Last year, he remember celebrating the beginning of spring by blowing through a huge sack of money he had gotten from the fence. It was a robbery of opportunity, he had gotten a rumor than a rich old man had died in his house. Balar had rushed over and broken in, looting everything he could while stepping over the rotting corpse.

Things were drastic different with the next spring. He groaned with the effort to pull himself into bed. The stub of his left leg twinged with pain when he accidentally bumped it against the rusted rail at the foot of the bed. He muttered under his breath and finished yanking himself onto the mattress.

He had found a different calling, helping two guards with their investigations. It didn't pay nearly as much, but he found an appreciation for the consistency of having a weekly stipend in his pocket.

Mudd, the mage-captain, had provided a series of drop boxes and locations for information and payment. They were scattered throughout the district and were difficult to monitor. Balar was thankful for that almost as much as the money. If Balar's old associates found out that he was talking to guards, they would be less than pleased.

Balar groaned and tugged the thin blanket up. He had just had his weekly cleaning, twenty minutes with a bucket and a rag in the basement. His shorts were thin and more suited for the summer months, but they were just right as he sank between two freshly cleaned blankets.

He let out a low moan. "This is the life."

"You should enjoy it while you can," said a woman.

Balar froze. He was not expecting a woman in his room. Nor did he plan on it being a specific one. He looked around toward the only shadowed corner. "Evening, Two Toes."

Two Toes was a muscular woman just under six feet. She normally wore dark makeup to contrast her pale skin, but there was only a few dark shadowed underneath her eyes. She had a curved knife in her hand, the blade had been painted black to avoid reflecting in the light.

A shiver of fear ran down Balar's spine. He didn't have to look around to know where he had left his knife. He also didn't bother trying to reach for it, Two Toes had too much confidence. She had taken away his weapon, he was sure of it.

She shook her head. "Don't you dare." Her eyes glittered in the light from the lamp outside her window. "They arrested my Kisnar. They beat her up and threw her into the street."

Balar shuddered.

"I know it was you. You were the one that told the guards about Ralonix job. I don't know, but you disappeared." She ground her teeth together. "They took her."

He knew that he was responsible. When he walked out of Steler's planning session, he had headed straight for Mudd and Viola, his assistant and a lieutenant. It was one of the biggest arrests in recent news and managed to get to the front page of the rags for days.

Balar sighed.

Toes waved her knife. "What? You aren't going to deny it?"

"I had to."

"Why? Because you wanted to live the rich life?" She scoffed as a tear ran down her cheek. "You wanted to get out? You couldn't just walk away? I haven't seen her for months!"

He gulped and reached down for his left leg. His fingers dug into the end. The wound from where he had amputated himself had mostly healed. It was tender but no longer ached every minute. More importantly, he didn't feel a wiggling sensation just underneath the skin. "I can't explain it."

Two Toes was on him in a flash. She jammed the edge of her knife against his throat, the sharp edge just cutting into the flesh. "Try. Tell me why you lost my Kisnar." She sniffed and more tears ran down her cheeks. "I lost my wife because of you!"

The knife dug deeper, the edge dangerously close to his artery.

He looked at her. "Toes---"

Two Toes yanked her hand back. The sliver of reflective edge on her blade flashed in the streetlight. In the same smooth gesture, she pulled away from him.

Balar slapped his hand to his throat before the pain registered. Hot blood burst out from his fingers, spraying against his hand. It felt like he was holding onto liquid flame as the stench of blood surrounded him.

"Burn in all the hells," hissed Toes. She backed toward the window until the sill pressed up against her back. Her eyes were hard as they stared at him.

Balar knew that looking at her pleadingly would be useless. She came to do one thing and he was dying. The tension in his body drained out of him and he lost his balance. With his free hand, he tried to catch his side table. His hand wasn't working as quickly as he expected and he tumbled over the side of his bed.

His forehead struck the side of his table and a burst of pain blinded him. He crunched against the ground, his face and hip catching the hard wood.

Trembling, Balar tried to clutch his neck tighter to keep the blood from spraying out from between his fingers. He knew he was dying, but his body refused to give up. He dug his fingernails deeper as  desperation fueled his muscles.

Blinded by his position, he couldn't see or hear of Two Toes had remained behind. He flailed helplessly to the ground with an effort to find something, anything, that could stop his life from pouring out from his throat. He remembered his blanket and pawed helplessly for him but his position made it impossible to reach.

He wished he had friends still. They had all left him, either by his betrayal or when he walked away from his family. No one who would be close enough to save him. The guards weren't his friends, he remembered think for many years. It was still true, Mudd and Viola would never be more than associates. Either might try to save him, but it would take a miracle for them to come through the door.

He knew he wasn't that lucky.

His strength continued to pour out of him in a crimson wave. The heat and stench was overwhelming.

With the last of his strength, he managed to flip himself on his back while clutching for his blanket. His bloody fingers caught the edge and he pulled it off. When the fabric slumped over his chest, he stuffed it into his neck.

Darkness began to gnaw at the edges of his senses. He could feel it, a suffocating pain that wavered on his peripheral vision and turned his legs cold. It wouldn't be much longer.

The blanket began to pull out of his slack fingers.

He tried to clutch it but his digits refused to answer.

Balar tried to say anything but the only noise came out was a gurgle and a gasp. Tears ran down his face, burning his eyes even as his vision darkened.

His hand dropped to the wooden floor, limp and useless. His other hand was still pressed against his wound, swimming in the hot pool of blood that began to fill his lungs.

He closed his eyes and tried to steel himself for death.

A new sensation tickled the hand. It was a tingling, a faint sensation of wriggling.

He shook it weakly.

His numb hand caught on something. It moved along his skin, crawling from his fingertips down to his knuckles. His arm slipped to the side but then caught. Through the darkness of death, he could see his elbow shaking.

The movement gave him focus. He struggled to lift his hand. It took all of his willpower to force the numb limb up and into his vision.

Through the haze and darkness, it looked like it was wavering.

He shook with the effort to pull it closer.

There was something on his hand. Red and black worms rolling over his skin. They were working their way down his wrist and arm.

Balar gasped and peered closer.

No, it wasn't worms. It was roots. Tendrils of a plant had ripped out of his skin and was wiggling around like tiny flowers along a garden. The tingling sensation redoubled and he realized that he had felt it before, almost a year ago when he had broken into the brownstone at 28C and found his life at risk when the plants inside had attacked him.

Fear somehow gave him more strength and focus. He tried to brush the root away with his hand but it only smeared more blood on his skin.

The tendrils moved faster, groping for the smears before digging back into his flesh. He could feel them burrowing deep into his skin, wrapping around bone and joint in a tight web of horror.

Balar realized the phantom sensations from the last winter were not his imagination. They were the plant settling into his body, preparing for the moment when he died. They were feeding off his blood and then would expand.

More roots and tendrils ripped out of his legs and back. They wiggled and twisted, writing against his flesh.

He tried to roll over, but he felt resistance. He cried out and forced himself to continue forward, past the sensation of tearing plants, and continued until his face smacked against the floor.

Where he was lying before, the plants were writhing in the pool of blood. He could see them digging into the wooden planks. One of them found purchase and sank a few inches into the wood. A moment later, the blood sank in a line that followed the plank before a tendril rose up a few inches away. In a matter of seconds, the supernatural plant had drank up the blood along the crevice.

Shock rippled through his fading thoughts. The plant was trying to set roots down. It was a weed, a parasitic weed that used his body to carry its seed to a new location.

It would turn his apartment building into the same thing that had happened to 28C.

For one of the first times in Balar's life, he thought about the hundred other men who lived in the boarding house. They wouldn't know what was coming. They would be victim to the weed as it took root in the building and then spread along the wooden floors to consume everyone.

He shuddered and closed his eyes. He was already dying. The only thing that seemed to keep him moving was the plants writhing inside his dying corpse. He just had to slump to the ground and let the plant take over.

He relaxed the tension, sinking into the writhing that boiled underneath his flesh.

His mind drifted back toward home. His brothers wouldn't ever see him again. NO doubt, that would give them a sigh of relief. He had betrayed his mother's will when he turned her magical tool into a weapon of murder and death; the former warrior had tried to teach all of them that violence wasn't the answer.

His shovel.

His mother had hand-crafted it for him just as she made the hoe, rake, and other tools for his brothers. They were suppose to bring peace, food, and harmony to the world but none of them really knew what to do with enchanted farming tools. He found it could cut through rock, stone, and bone easily enough and left for Rougan to make his name.

Balar's mind focused on the shovel. He had left it behind in 28C. Mudd had said he couldn't find it.

His head rolled to the side where the plants were still burrowing into the wooden planks. He noticed where the blood had been consumed, the tendrils were moving slower and wilting.

The weed needed blood.

He needed his shovel. He didn't know why, but it became important that he find it. The act would also take the plant away from the boarding house and the men who didn't deserve to die like he did.

Balar didn't think he had anything left to move. He forced his hand down on the ground and pushed up. Even thought there little blood left in his body, the plants wrapped around his bones and muscles twisted and moved. They acted as his limbs as he pushed himself up into a sitting position.

He had to use his face as a brace to pull himself up from the ground. Dying tendrils littered his bed, the remains of a weed that didn't have enough to eat.

Balar's mind grew foggier. He almost lost his focus when he had forgotten why he stood up. After a moment of swaying, he concentrated on the one thing he could picture clearly: his shovel. He had to get it.

He managed to turn around and stagger to the door. He hit it with his face and shoulder. Through the pain, he fumbled with the lock until his slack fingers caught it.

With a smear of blood, he pried open the door and thumped against the wall to the stairs. Behind him, he could tell that he was leaving behind a trail of gore and plants; he only hoped they would die before anyone else was infected.

Balar didn't make it down the stairs. He lost his balance and tumbled down them, cracking bone and smashing his limbs along the wall. When he hit the ground, the wet air flew out of his lungs with a splatter.

"Are you okay?"

He forced his hand against the ground. The plants writhed underneath the skin, causing the flesh to boil and twist. He couldn't tell if he was moving or the weed, but he somehow got back to his feet.

His eyes wavered and darkened. He waved at it, trying to clear his face, but his hand struck nothing.

Someone swore backed off.

He tried to wave at the wavering in front of him, but his hand was behind it. Trembling, he reached to touch his eyeball.

The movement was further in.

Balar pressed harder, rolling his finger along the orb until he could get a clear look at what was blinding him.

It was a flower. A tiny flower with countless petals. He had seen it before, back when he had broken into the brownstone. It was the plants that were outside the front door, the ones that were almost looking at him as he broke through the front door. It was also the same type of plant that filled the old lady that he had killed in the backyard.

The need to find his shovel rose into a fevered pitch. He lurched toward the front door.

Someone swore. "Get the damn guards!"

Balar ignored him. He had to get his shovel. There was a reason, but he couldn't remember anymore. He felt drained. An empty shell filled with worms, but the shovel was important.

Almost blind and staggering violently, he made his way down the street. There were screams and cries. He had to ignore them, it was the only way to keep moving forward.

He heard someone coming close. "Are you okay?"

Balar waved at them. He opened his mouth but only blood and tendrils spilled out form his lips. He could feel them twisting and shaking off his chin. Soft petals dripping with blood caressed his throat and shoulder.

"By the gods!" Thankfully, the person backed away yelling for the guards.

He continued to move blindly forward, but he knew where to go. The path to 28C had been burned into his mind, the place he had lost the most treasured thing in his life, his mother's dying gift.

His shovel.

He had to get it.

There were more people coming closer.

Balar shook his head violently and waved his hands but didn't stop moving.

"Everyone back!" bellowed a familiar voice. It was Wathin, one of the mage-captains that had questioned him earlier. Balar could remember the handsome man that almost got him to spill his guts.

The air crackled around him. Balar could picture Wathin's weapon, a halberd that crackled with energy. He half expected Wathin to cut him down. Balar didn't know if that would stop the weed or let it spread out.

He decided the gods would know. He reached out toward his destination and kept staggering forward.

"Clear the path!" bellowed Wathin. Then, in a quieter voice. "You are heading back, aren't you?"

Balar nodded and then swayed.

"Eighteen blocks forward, I'll tell you were to turn."

He almost sobbed to relief. He focused on making each step forward. He had to keep going, to get his shovel. He didn't remember why, so he just concentrated to the feel of it in his hand and the way his mother looked when she presented it to him for his birthday. A small shred of joy, thought long forgotten, rose up and pushed back the horrid sensation of his entire body about to burst like an overripe milk weed pod.

One step in front of the other.

His hand reached for his shovel.

He had to get it.

Balar needed it.

Wathin gave him more directions.

He obeyed. When he bumped against the wrought iron gate of the innocuous brownstone at 28C, he almost collapsed.

"Keep going."

Somewhere ahead of him, a door creaked open.

The flowers in his eyeballs began to quiver. He could feel the roots burrowing into his skull and sinking into his brain. Flashes of light, color, and perfumes flooded over him.

There was an old lady at the door, holding it open. He had killed her once before but she wasn't really dead. She was just a pod, a shell holding the weed that had consumed him.

She held out her hand.

Even blinded, he could feel the energies of his shovel. His birthright. He trembled as his hands grabbed the shaft and held it tight. It wasn't his muscles moving anymore, it was the blood weed. It gave him strength even when his own body failed.

The old lady stepped away from the door. Inside, the entry hall was filled with flowers of all types. They were looking at him, watching to see what he would do.

Balar turned to look at the crowd outside the gate.

In the front was Wathin, the guard held his weapon horizontally to prevent anyone from getting closer.

It didn't matter anymore to Balar. He was going to die, but cutting off his limbs wouldn't matter. He could feel the roots burrowing into his chest and tearing apart his organs. He was nothing more than food for the thing that had consumed him.

Balar stepped back and shut the door, sealing his fate with the brownstone at 28C.
