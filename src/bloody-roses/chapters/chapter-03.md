---
title: Betrayal
date: 2020-10-25
character: Balar
summary: &summary >
  Balar's promise to stop stealing and murdering had left him homeless and starving. A chance encounter with an old protégé gives him a taste of his old life, but then he is reminded brutally of his promise.
teaser: *summary
---

> The will of the gods is subtle and fleeting, but for the stone-headed man, sometimes a more serious blow is required to remind him of their will. --- *The Will of the Divine Couple*

A cold wind blew down the street, kicking up flecks of snow and drawing ripples in the slush. Balar shivered and clutched the ratty blanket tighter around his shoulders. The small metal cup in his hand clicked with the two coins at the bottom. It hadn't been a very productive day but he was hoping to get at least a crown before nightfall; that way, he could afford a hot meal at the local church instead of his usual hunk of jerky with a bit of bread and water.

His stomach rumbled and a headache throbbed along the right side of his head. It had been a long time since he had eaten to his fill. It all started three months ago when he decided to turn his back on his life of crime and try to make an honest living. Having to cut off his own leg to survive a haunted house filled with killer plants was a good reason; it was impossible to rob someone without a left leg.

His skills weren't suitable for clean living and there was no way the Three Gods could convince him to return home. No one in his village liked him nor could he come back with his tail between his legs.

Another wind tickled his nose. He tugged up the scarf and looked around at the road. He had camped out near the corner of two busy roads, but it was after the normal working hours. The sun had already dipped below the horizon and the last of the light was weakly dragging its corpse across the skies. His prospects for begging looked slim.

Balar sighed. He should have lied to the guard. He should have found a way of robbing houses and stores. It didn't really matter if his missing leg would have made that impossible. All that mattered was the knot in his stomach and the shivering that wouldn't stop.

To his left, he heard boots crunching on ice. Dragging his scarf down his face, he steeled himself against the cold and held up the carved wooden cup in his hands. "Just a few crowns, sir," he asked the passing man wearing a black Inverness cape.

The stranger stopped and turned around. "Bal?"

When Balar didn't register, he pulled back a knitted cap from his head to reveal a face that Balar had seen before: Steler, a protégé back when Balar worked with teams to strip away houses while the occupants were at parties. Steler had the same philosophy as Balar did at the time: get everything of value, kill anyone who saw.

"What are you doing down there, Bal?" Steler spun on his heavy boots and came over. He squatted down. "I haven't seen you in months."

"Had a bad spill." It felt strange talking to a man who had once called Balar "a great teacher" and gave a percentage of his take for almost a year in fees.

Steler's eyes scanned over Balar's body. It was the look of a professional thief, no doubt looking for anything of value. Then the eyes focused on Balar's half-covered left leg.

Without a word, Balar tugged the blanket away to reveal where his leg ended about six inches from the hip. The wound had healed over but there were still discolored flesh and a few scabs left from his injury.

Steler whistled. "I heard you got hurt, but I never realized it was that bad."

Balar resisted the urge to snap. Before he lost his leg, he thought he had a thousand friends that would jump up and help. Instead, they left him behind to fend for himself. Once he had no more value in stealing or spending money, he ceased to exist in their eyes. He tugged the blanket over his leg. "Bad spill."

Telling everyone that he fell was much easier than explaining that he used a magical shovel to cut off his leg to avoid the supernatural plants. His hand twitched but he didn't scratch himself; at least the sensation of having roots crawling in his leg had faded in the last month. Now, it was almost normal except for the starvation, freezing, and loneliness.

Steler looked around and then held out his hand. "Come on, old man. Let's get you warm and maybe get you back into the game."

Balar hesitated for a moment but the promise of warmth lured him. He took the offered hand. As Steler pulled him to his feet, Balar used his crutch for balance. It took him a moment to steady himself and then another to gather his things.

Steler shook his head in disappointment and then turned his shoulder in a clear gesture for Balar to use it. "Come on."

Balar gripped the cape tightly and together they headed down the street. It was a painfully slow process, with Steler's heavy boots a counterpoint to Balar's crutches.

"I got a big job going on," Steler said as they walked. "I've been planning for better part of three months. Coming up with the contingencies like you suggested, trying to find the most efficient way to get in and then out. I could use your help in spotting things I missed."

Balar felt his thoughts returning to his happier days. "How big of a crew?"

"Six. I got Two Toes, Kisnar, Fangol, Spittoon, and the Hawk Twins."

"Pretty young crew."

Steler shrugged. "Fangol and Toes been at it for a few years. Those are my points. And if you got Toes---"

Balar chuckled. "Then you get Kisnar. At least they are keeping their wedding vows. Twins are on watch?"

Steler nodded and then pointed to an alley. "Down there. No, the Twins are doing the second story, hit the bank from three directions. Spitton is watch this time. He's got a new mechanical bow that can punch a foot-deep hole into stone at three chains."

Balar nodded. It felt good to do something, anything. He missed it.

Steler slipped away to open an unmarked door on the side of the alley. He gestured inside.

His crutch rattling, Baler took the invite and worked his way down the three short steps. He hated the feeling that he was only inches from missing and falling. It was just one more reminder how crippled he had become.

The basement room looked like every storage hall Balar had clandestine meetings in before: a lantern hanging from the ceiling joists, an impromptu table made from crates, and papers tacked onto the side of more crates. He had made more than a few plans to break in someone's house or business in rooms like the one in front of him.

Balar scanned the labels. They were all yarns and textiles and dyes. He could smell the chemical scent that permeated the room, it must have been a fabric weaving business.

Four men and two women looked up from various places in the room. He had met all of him, but none of them since he lost his leg. They weren't friends but they also were enemies.

No one moved. Balar could feel the snow dripping off his face.

Steler came around him. "He's just here to check our work."

Two Toes, a sour-faced woman with black lipstick, glared at Balar. "He's been out of it for a while."

Her wife, Kisnar, smacked her lightly. "He isn't going with us, right, Stel?"

Balar shook his head. The warmth of the basement was more than he had in days. He wanted to crawl into the fabric and sleep until spring. Or at least, to get out of the wind.

"Good," said Toes in a curt voice.

Steler guided Baler to the table. The first thing the old thief saw were diagrams for a bank. The spiral pattern was distinct for Tarsan-style banks, with reinforced walls and guard posts. It was a big bank, probably one of the twelve that deal with trade monies through the various networks.

Balar scanned across the designs. He was glad to see that Steler had learned from one of Belar's earlier mistakes and didn't add identifying names. However, he was familiar with most designs and could identify the bank by the layout.

Spitton, an old woman knitting in the corner, sighed. "I'm going to be dead before you finish looking at it."

"Well," Balar said as he tested the table. It could hold his weight and he rested against it. "If you are going to rob a Ralonix, it probably wouldn't hurt to take your time and do it right."

He looked up to see Steler's muscles tightening. Pointedly, he gestured to a list of schedules with last names on that. "Use aliases for schedules, remember? Rooster, mouse, snake? Never identify people on paper."

"Damn it," muttered Steler. "How did you know it was a Ralonix?"

Balar leaned on his crutch to point behind the counters. "Ralonix always had these triple doors and no other bank has them. Plus the way the communication offices are arranged. This bank likes to have them in one big room to watch over them while others spread them out to avoid feedback."

Some of the tension left the room.

Balar smiled to himself. Maybe he could get a job planning heists? He had a lot of knowledge it would use the skills he head. More importantly, he wouldn't be robbing anyone himself.

It was a technical point, but the gods wouldn't begrudge him a warm home, right?

He followed the various lines that the crew had arranged. An assault on the front and back at the same time. They went through a couple loops of the spiral. About a third of the walls were marked in colors, the others left blank. When he spotted a place where the lines went through a wall to get behind a guard post, he stopped. Leaning over with a grunt, he tapped it. "Going through here?"

Toes grunted. "There is a heavily-guarded post around that corner, coming around while they are distracted by the first squad is the only way we'll crack it."

Balar shook his head before tapping the wall. "That isn't a good idea."

The female robber stood up. "Like shit it isn't. I went through those walls myself. The ones with wards are marked clearly, I double checked! If you think I'm going to pound my way through a ward, you're stupid as you smell!"

Balar steeled himself. He tried to favor the rash robber with an easy smile. "These are designed by Decrail."

She scoffed.

"The alchemist?"

"So?"

Balar tensed. "The one who is fond of putting acid in glass and nestling it between walls that looked like obvious weak points?"

Her eyes grew wider and her arm tensed.

He tapped the map. "This is only one loop from the vault. You have to assume that every wall that isn't warded is going to be alchemically trapped instead. It lasts longer, is harder to detect with magic. When you were scanning, did the wards seem really powerful?"

Toes ground her teeth together.

Kisnar nodded. "They were. Really bright."

"That's to hide the magic in the traps. It gives you a false sense of security before your face melts off."

"Then what do you have in mind, old man?" asked Toes with a sharp tone.

"I suggest---"

Any word that came out of his mouth ended when he felt something crawling along his left leg. It was delicate, no more than an earthworm burrowing through the earth. Except that it wasn't on his skin, it was deep inside the muscle. A quiver of something that shouldn't have been there.

Memories rushed back, of the frantic desperation to cut off his own leg to prevent the supernatural plants from crawling through his flesh. He had begged the gods to save him, if he only stopped robbing.

"Bal?" said Steler in quiet voice. "Are you okay?"

The wiggling sensation grew more pronounced. He couldn't tell if it was moving up or his imagination, but he could easily picture it snaking through his muscles and burrowing deeper, curling around the bones for purchase like some weed.

He thought about his musings only a few minutes earlier. Was the sensation coming from the gods? Were they reminding him that he had promised to stop crime if he lived.

Trembling, he reached down and pressed his hand against his leg. He couldn't feel anything underneath his fingers, but the squirming sensation continued to wiggle inside him.

"Bal?"

The gods had to be talking to him.

"I-I have to go," Balar said. "It's good. Just be... be careful of the traps."

He turned and hobbled toward the door. "Sorry," he said.

With the eyes boring into his back, he fumbled with the stairs and then the door handle. He started to tilt back until he grabbed it with both hands and twisted hard. Flushed with embarrassment, he snatched his crutch and limped outside.

The door closed behind him.

The wriggling sensation continued to writhe inside him. He could imagine it was digging into his intestines, squeezing in places that caused his pulse to race.

"Stop," he whispered. "Just stop growing. I'm not going to do it, I promise."

The muscles along his side continued to spasm. He whimpered and held himself.

"Balar? What's wrong?" Steler asked as he came out the door. He shut it behind him.

"N-Nothing. I just... just... I just lost my edge."

Steler looked into his eyes for a long count. "Are you sure?"

"Y-Yeah. You're going to do great, it's a good plan."

With a frown, Steler stared.

Balar forced his hand away from his leg. "I promise you. You're going to be rich."

Steler patted him on the shoulder. Then he dropped his hand into Balar's. When he pulled back, there was a few bills in his palm, each of them hundred crowns. It was more money than Balar had seen in months.

"Stel?"

Stel waved and turned away. "Be careful, Balar. If you need me, leave a note at the Drunken Horse."

It was a seedy dive that Balar used to favor in better days. Many of the thieves and robbers drank there, at least until the next guard raid and then they would scattered to dozens of others equally disgusting places.

Balar shoved the bills into his pocket. If he stretched it out, he get a small bit of warm food and the cheapest boarder house to make it through the winter.

Hating how his life had changed, Balar situated his crutch and began to hobble down the alley.

With every step, the writhing in his leg and side felt like it was growing worse. His imagination painted an image of roots wrapping around his organs as the sharpened points worked their way toward his heart and lungs.

He twisted in discomfort. "Come on, I'm not robbing anyone."

His breath fogged with his cry but the crawling didn't subside.

Struggling to move, Balar forced himself to hobble down the street. There were a number of cheap boarder houses. For a few extra crowns a night, they would have some hot food in the morning for the tenants. The idea of having some comfort, and at least one hot meal, kept him going for almost a block.

The writhing sensation redoubled.

Growing more panicked, he stopped and pulled up his clothes to stare at his wound. The icy touch of wind against his bare flesh hurt, but it was nothing compared to the horror he expected to see.

Nothing looked out of the ordinary, other than the dread of having some sort killer plant growing in his body like it was a pot of dirty.

It had to be the gods. It had to be the world telling him that he had violated his oath.

He whimpered and looked around at the empty street. He couldn't cut it out this time, not with the feeling that it was burrowing deep into his gut.

A few blocks away, a pair of city guards started to walk down the street toward him.

Balar thought about the two mage-captains that had questioned him when he first escaped the backyard where he encountered the killer plants. One of them, the fat one, had surprised him by trading information about how he lost in leg in exchange for walking free.

He frowned. "What was his name. Wat... no... Dirt. Dirty. Slime. Mud. Mudd!" Mudd, the strangely monotone captain with probing questions. Maybe he could appease the gods by talking to him.

Balar froze. Was he really going to betray Steler and the others by telling a guard what was happening? He couldn't. The underbelly of the city would gut him in an instant.

More importantly, guards were not his friends. Nothing good ever happened when he talked to the guards.

A twinge in his leg made his decision for him.

Balar crossed the road and headed toward the guards. "Excuse me? I have a question."

Twenty minutes later, Balar was hobbling toward the edge of the district. The guards had promised to send word to Mudd but Balar didn't trust them. All guards were assholes. But they did tell him that Mudd was investigating a murder at the edge of the Goldstone District. It was only twenty or so blocks.

The wiggling sensation kept him company, reminding him that he had betrayed the gods with his desire for warmth and food and the old days.

A plain-looking woman going the opposite direction suddenly turned around and then came up even with him. "Balar?"

He jumped. "Y-Yes?"

She reached up to tug her white leather gloves over her hands. He stared at them for a second, remembering the same type of gloves on Mudd when Balar was questioned.

A prickle of fear ran along his spine. He glanced at her, at the sober outfit and button-down shirt. She looked cold, as if she had a cloak but abandoned it.

She dug into her pocket and pulled out a notebook. On her wrist was a guard bracelet. The ruby indicated she was another mage.

He stumbled.

The woman caught his elbow. "Careful now, it's slippery in this weather." She lowered her voice. "I was told that you asked for Mudd?"

He glanced around. No one seemed to have noticed but the windows were open and there was always someone watching. He ducked his head. "Yes. I was hoping... to talk to him."

"About what?" She had the same piercing tone as Mudd.

Trembling, Balar reached down and pressed his hand and grabbed his stump. He could still feel the wriggling sensation of the plant growing inside him. It was twisting around his stomach and making it hard to breath.

He hated that he couldn't tell if it was anxiety and imagination that made it impossible to tell if it was real.

"I... I broke my promise. T-The one I made to him."

She leaned closer and spoke in a low voice that wasn't quite a whisper. "Not to commit a crime?"

Leaning back, she looked him over. "It doesn't look like you have a farm instrument. What was it? A hoe?"

Balar shook his head. "No, that's my brother. I had the shovel."

She looked surprised for a moment and then shrugged. "He forgot to tell me that."

"Do you think I could... talk to him, Captain...?"

"Lieutenant, but why don't you call me Viola? It seems like you aren't in a good place to use my ranks."

"No," he sighed. "I don't think this ends well for me."

"Want to talk about it?"

Everything inside him screamed out in protest. Don't trust cops. Don't tell them anything. They will never do anything but betray you.

The squirming sensation increased. He imagined he could feel the pressure burrowing their way toward his groin, a deep pain that was so far beneath the surface that he could only feel it as pressure.

Sweat prickled along his brow.

He tried to convince himself the feelings were just his imagination, a remembered terror from his ill-fated night. He was fine. It had been months and no plants had burst out of his chest since that happened.

Why had it changed now? He struggled with his emotions, on the edge of bursting into tears and losing it. He shook his head as he thought about how the rest of the community would handle it. They would slaughter him, string him up and let him dangle. There was no chance talking to the guards, even innocently, could ever be seen in a promising light.

Viola watched him, her gaze taking his face. It felt as if she could hear his thoughts or see directly into his soul.

"Shit," he muttered and looked around again.

"Need me to leave?"

He opened his mouth to say yes. He couldn't betray Steler and the others. Even if he wouldn't get a fraction of the money they were going to steal, there was a sense of honor among thieves. He couldn't let the guards know the plans.

Viola nodded. She dug into her pocket and pulled out a few bills. "How about I---"

"Steler is about to rob the main Ralonix bank!" he gasped in a rush.

The smile on her face froze. Then she slowly put the bills back into her pocket. "Well, that was not what I expected."

He cringed and looked around. "Can we... find somewhere private to talk?"

She nodded. "You know of the Golden Flagon?" It was a massive pub on the edge of the Goldstone District.

Balar had robbed the Flagon twice during a festival but he was sure that the owners would never be able to picture his face from the fires. He nodded and stepped back.

"About midnight? Mudd and I should be done with our investigation."

"It would be bad if they caught me."

"One of the local politicians is having a fundraiser tonight. There will be a lot of drunken rich people, something that you would be reasonably expected to check out."

Viola stepped back and nodded. "Good luck."

He froze, unsure of what to say or do.

A cold wind blew past him, cutting through his clothes and bringing a chill to his body. He shivered violently while she walked away.

Balar waited until she was out of sight before he dropped his hand to his severed leg. The wiggling sensation had subsided and all he could feel was the shivering from the icy cold wind.

He had made the right choice.

If it came down to betrayal or his life, there was only one thing he could choose. He gave the surrounding buildings one last look before he headed toward the border houses. He had enough time for a warm meal and a bed before meeting with the guards.
