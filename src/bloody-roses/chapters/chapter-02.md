---
title: Regrets
date: 2020-10-18
character: Balar
summary: &summary >
  Hospitalized after his near-death experience with the living plants, Balar find himself trapped again, this time by two city guards investigating his crimes.
teaser: *summary
---

> Secrets are like poisonous snakes stuffed into your pants. --- *Confessions of the Soul*

It took all of Balar's willpower to stop moving but he couldn't concentrate on remaining still for only a few seconds at a time. Twinging muscles and bone-deep aches forced him to move again. There was no comfortable position, no way to arrange his body that wouldn't hurt seconds later.

Every time he moved, sharp pains radiated from his injuries. Cuts and scrapes covering his limbs were nothing new, but he had never had so many across so much of his body. He glanced down at his limbs and hated seeing so many of them crisscrossed with bloody and stained bandages.

His gaze focused on his left leg. More importantly, his attention sharpened on where it ended six inches past the joint of his hip. There was nothing there except for heavily wrapped bandages soaked with blood.

One of the pale-skinned healers came in, a sour-faced man wearing red and white. He had a small tray of rolled bandages and a knife. With a glare, he came over and set it down on the table next to Balar. "The guards will be back for you in a few hours." There was no compassion in the baritone voice.

Days ago, Balar would have said nothing. Nothing good ever happened by proclaiming innocence or trying to deny it. However, the brush with death and horror still haunted him and he found it impossible to not say something. "No doubt," came his quiet reply.

It wasn't many words, but for Balar, it felt like he had just confessed to a murder. He took a deep breath and let it escape through his teeth. Inwardly, he berated himself for even letting two words escape.

He repeated the mantra that had kept him safe for years: don't talk to the healers, the guards are never your friends, just keep quiet.

The healer grabbed Balar's left thigh and dug his fingers into the joint. With a twist, he pulled the injured limb to the edge of the bed.

Pain cut across Balar's senses. He arched his back and the rest of his breath came out in a loud hiss of agony.

"You are a damned murderer. Everyone knows it." The healer yanked down, tearing off the bandages in a single swoop. "The guards wouldn't have brought you here and they wouldn't have chained you to a bed if you were innocent."

With a cry, Balar gripped the edge of the bed tightly. The iron manacle around his wrist rattled loudly. The pain tearing through him made it impossible for him to correct the healer; he was a thief who wasn't opposed to killing anyone who got in his way. Somehow, he doubted the healer would appreciate the distinction.

The healer gestured down to the severed end of his leg. The entire end had been coated in a thick, sticky poultice that kept him from bleeding to death and stave off infections. "Too bad whoever did this didn't aim higher. A foot and you'd be less than a man." He scoffed. "Or at least gut you properly. The world will be a better place with your corpse."

The urge to say something rose up but even Balar had trouble believing it. The healer would laugh at him if he said he cut it off to avoid a supernatural plant that had burrowed into his leg and was trying to eat him out from the inside. He shuddered and clamped his mouth shut. The guards weren't your friends, he told himself. Don't trust anyone.

The healer grumbled as he bent over to inspect the wound. With a frown furrowing his brow, he leaned over and peered at it. "What of the Divine Grace?"

Balar tensed. The memories of his amputation came back, of the terrible sensations of plant tendrils digging into his flesh and along his bones as they crawled for his crotch. It was as if worms were eating him out from the inside. He had cut off his leg in desperation to stop the plant.

Even the thought of the plants caused phantom wriggling to caress against his senses. He imagined the tendrils had returned, burrowing up around his bones as they tried to kill him again.

The healer didn't look up. Instead, he shoved his fingers into the bloody wound and began to twist and probe.

Balar let out a sob of pain. He twisted trying to escape the healer's probing. The manacles rattled loudly.

The healer let out a chuckle and then started to pull.

"Damn the gods!" Balar's voice echoed against the walls, higher pitch than he had ever heard before.

"Got it!" cried the healer. He pulled on something.

It felt like a thousand thorns were tearing out of his leg.

The sharp smell of blood flooded the air. The healer slowly drew out something long and string-like. It was a plant tendril; the end of which was deep inside his stomach, twisting around his organs. With every tug the healer made, Balar felt it slipping off of his hip bones and ripping out from his groin.

Balar let one last cry. "Fuck!"

The healer yanked it free. The bloody tendril swung back and forth. Frowning, he stepped away and peered at it. "This is a root. What is a root doing in you leg?"

Panting, Balar stared at it and whimpered. It had gotten deeper into his body than he thought. It was writhing inside him. He shuddered with the sensation as his imagination began to paint more squirming tendrils across his senses, a phantom feeling of death inching closer but never reaching the deep ache of whatever the root had just torn open.

The door to the chamber banged open. A red-cloaked guard came sweeping in. He had a massive pole weapon, a halberd of some sort. It crackled with energy and the lights in the room flickered in response.

The healer turned with a shock. "You can't have that thing in here! We have people recovering from injuries and you could kill them with the feedback."

"They're fine," snapped the guard. He looked around the room and then headed straight toward Balar. The butt of his weapon thudded against the ground, rattling the floor boards more than just footsteps would have provided.

Balar cringed. He didn't like the guards but the mage-captains were the worse. They had magic and were always pushing with their questions.

The captain stopped next to Balar's bed. "I'm Mage-Captain Wathin, the investigator."

"Coming to arrest him?" asked the healer, still holding up the bloody root.

"No, not at this point," said a second mage-captain in the door. The second one was an overweight man with only a fringe of hair. He had the captain's bracelet but wasn't wearing the typical cloak.

"Pity, the bastard deserves to be in prison." The healer tossed the root on the tray.

Balar stared at the tendril. Everything else was less important than the overwhelming fear that the plant still crawled inside his body. His eyes ached with the effort to see any hint of movement. Any suggestion that it was still alive.

The second mage came further into the room. "Our investigations have not concluded. We will not charge anyone until that point. Anything else would be a violation of the law."

The healer scoffed.

Wathin leaned against the bed, between Balar and the tray. "Balar, right?"

Balar looked up. "Y-Yes, sir."

Wathin was a handsome man but he smelled of perfume and smoke. Balar knew the type, a pretty boy who liked to play with others. He had the look and attitude of the buddy guard, the one that got you to confess all your sins in exchange for an easy smile and friendly but empty promises.

Balar's jaw tightened. He had to be careful around the captain. He looked away from Wathin's bright brown eyes and toward the tray with the bloody root. When he realized Wathin stood in the way, he tried to peer around. The phantom, squirming sensation in his leg redoubled until he had to slump down.

"So, Balar, what were you doing in The Hagril's backyard with your leg cut off? Seems kind of unusual, don't you think?"

Balar glanced at the guard. Don't say anything. Don't trust the police.

Wathin focused off a disarming smile at him. "Who cut off your leg? That wound was very straight and clean. I'd say a sword cut and I know you don't have one of those on you."

Balar almost said something, but he couldn't. The guards couldn't be trusted. They could never be trusted.

"He isn't going to say anything, Wathin," said the other mage with a chuckle.

"Why do you say that, Mudd?"

"Tension in the muscles, the flex in his cheeks. Not to mention, he is focused on the contents of the tray more than you. He's a mousetrap."

Balar tensed and looked up sharp at the other mage, Mudd.

Mudd didn't seem to be triumphant or probing. There wasn't any excitement in his voice; in fact, he seemed more clinical and monotone than Balar would have expected. The mage walked to the tray and peered at it. Balar couldn't see what he was looking at.

The mage bent over before pulling out a pair of white leather gloves and slipping them on. He picked up the bloody root and inspected it carefully.

Balar's phantom sensations grew and he squirmed with discomfort.

Wathin stepped away from both of them, turning around and standing next to the healer.

Fighting back the urge to whimper in pain, Balar watched Mudd carefully.

"This is a root, isn't it? Looks like the tap root of a perennial of some sort."

Wathin said, "You saw plenty of those in the neighbor's house. The entire place is littered with them." He shuddered. "Too many plants, as far as I'm concerned."

Balar's head snapped up. He looked in fear at Wathin's and Mudd's face. Neither looked like they were about to arrest him for murder.

"Yes, but for an abandoned house, those plants were in excellent upkeep. Not to mention, a well-manicured backyard."

Balar almost missed what Mudd said. Abandoned? The house wasn't abandoned. He distinctly remembered killing the old lady in the back. His body tensed when he remembered what came next. She wasn't an old lady, the inside of her corpse had been filled with flowers and roots.

"A few questions, if you don't mind," Mudd said. He set down to the root and then pulled a notebook out of his pocket. He wrote something down for a few moments and then circled around the bed. "What severed your leg?"

Balar clamped his mouth shut. His mind still ran furiously. If he remained silent, then they could just get him for breaking and entering the house, not murder.

Mudd looked at him and then back to his injury. "Sharp blade. Looks like a single cut of considerable force. Judging from the cut marks, the blade was wide. At least seven inches across."

"A seven-inch sword? Sure it wasn't a downward slash?" asked Wathin with a frown furrowing his brow. Mudd was obviously saying something that either disturbed or surprised Wathin.

"No, I determined the profile of the weapon from cuts I found along the front door and flooring. The same instrument was used on the front gate. Despite the wide blade, it was used for slashing and piercing."

Wathin leaned toward Balar. "Do you have a sword?"

Balar struggled. There was nothing good talking to the guards, even if he was honest. He looked up at Wathin and couldn't help but want to respond. He groaned and fought it for a moment, then he sighed. "No."

"What cut off your leg?" asked Wathin.

Balar glared at him. Keeping quiet during interrogation was always the hardest, but he was a free man because he knew the merits of keeping his mouth quiet. He shook his head.

Wathin sighed. "Come on, was it a friend of yours?"

Balar refused to answer.

Mudd chuckled.

Wathin sighed. "What?"

"Mousetrap."

"Mouse... damn it." Wathin turned back to Balar. "Do you want to go back to the guardhouse? I could throw you into interrogation."

"Please," muttered the healer. "Get him off the street."

Mudd and Wathin turned on the healer. Neither said anything but the tension in the room grew uncomfortably.

The healer twisted for a moment. "I-I should finish wrapping this up."

"Please," said Mudd.

No one said anything as the healer finished treating Balar's wounds. Then, with a huff, he left and shut the door firmly behind him.

Balar tensed.

Mudd leaned over and picked up Balar's clothes. Setting them on the end of the chair, he inspected each piece including turning his boots over. "I notice you have mud and broken leaves from the plants in the front gardens from next door, 28C."

He plucked a crushed petal covered in blood from one of the ridges. "The species of this plant is distinct, I have never encountered them anywhere except in that house and yard."

Balar tensed.

Mudd moved up. He took Balar's hand.

Surprised, Balar didn't resist.

"Are you a farmer?"

"I used to be." Balar realized he had given away something that could be used against him. The guards were not a friend, he told himself.

Mudd peered down at Balar's fingers. "Yet you have steadily used something with a relatively thick shaft. Judging from the calluses, I would say you...." His voice trailed off.

Balar tensed and looked at both of the guards.

The mage released his hand and stepped back. He pulled out a notebook and began to flip through the pages.

"Mudd? You have something?"

"Yes...." Mudd's said distractedly. "I'm looking for...."

The mage did not say anything for a long moment.

Wathin sighed and leaned against Balar's bed. "That's the problem with Mudd. He is very focused on finding the source of a crime. Of course, if you were to explain why someone cut off your leg---"

"No one cut off his leg, Wathin." Mudd still flipped through his book without looked up.

Balar tensed.

"What?"

"He did it to himself with a... a... shovel I think. Wide blade. Should be about eight inches across and sharp, maybe magically so."

Balar felt all the blood drain from his face. The world spun around him and he clutched the railing tightly. They were going to arrest him. He was going to be in prison for the rest of his life.

Wathin looked down in surprise. "You cut off your own leg?"

Trembling, the words spilled out of Balar's mouth before he could stop them. "Y-Yes."

"Why?"

Balar managed to clamp down on any other words.

Wathin turned to look at Mudd. "Why?"

The bald mage didn't look up. "Judging from his reaction to the bloody root next to you, of which was pulled out of his leg as we were entering the room, and a suggestion of self-injury, I'm going to say that he was in mortal peril."

Mudd finally put down his notebook. "Don't you think it's interesting that the house is on the registry without any reasons?"

Wathin shrugged. "Most of the registry is blank."

Balar looked back and forth. "R-Registry?"

Wathin rested an easy hand on his shoulder, the kind gesture setting off warning bells in the back of Balar's head. "Registry of Forbidden Locales. It's the list of places the guards are not to investigate or allow anyone to break in during a dark and stormy night."

The phantom squirming sensation redoubled. Balar shifted his leg as he fought the urge to reach down and see if there were plants still crawling underneath the skin.

Mudd stared at Balar. "Would you be willing to make a deal with me?"

Wathin's hand tensed.

Balar shook his head. Never make deals, never trust the guards.

Mudd sighed. "I want to know what tried to kill you. I suspect you aren't planning on breaking into any more houses in the near future, are you? Maybe use this opportunity to stop your life of crime?"

Balar thought back to his screamed-out prayer as he was crawling for his life. He said he would stop robbing. It hung in the back of his head, but there was a doubt. He didn't have many other skills besides stealing. Without his leg, his options were significantly curtailed.

Mudd flipped his book again. "Dame Hagril reports hearing a man screaming 'Please. I promise I'll never do this again.' Seconds later, the suspect crawled over the fence and landed on the ground."

Balar froze, his heart pounding. Everything hurt and his skin crawled as he stared at Mudd with horror and trepidation.

Wathin sighed. "You are really going to do this, Mudd?"

Mudd shrugged. "It serves the greater good."

The two mages stared at each other for a long moment.

Then, with a sigh, Wathin shook his head. "You better be right about this." He patted Balar on the shoulder. "Better not be back here, Mousetrap."

To Balar's surprise, the combat mage grabbed his halberd and walked out of the room. His body could be seen through the door, but he didn't walk away. Instead, he leaned on the far side of the door and shut it with his weight.

Slowly, Balar looked back at Mudd.

The remaining mage glanced at the door. Then he picked up a chair and brought it over. "Balar is your given name?"

Balar clamped his mouth shut.

"You came to Rougan three or so years ago?"

It was startling how close Mudd had guessed when Balar had left home to come to the bigger city.

"I've been tracking a series of crimes since then with a common pattern of a thief breaking and entering using a wide-ranged weapon. While the crimes were mostly focused on high-profile robbery, there were a number of murders executed during the process."

The monotone way Mudd listed Balar's crimes somehow made the litany more terrifying.

Mudd gestured to Balar's boots. "You haven't changed those since you arrived in town. The tread mark is somewhat distinctive and enough to suggest you were the criminal in question."

A whimper escaped Balar's lips. He had been caught.

Mudd sighed and tapped his notebook. "I should have figured out it was a spade. Wide blade made more sense given the pattern of the wounds. Enchanted?"

Rapidly losing control of the conversation, Balar managed to keep his mouth shut.

"I heard of a set of farm implements made by a former weapon smith in the Village of Kas."

"M-my mother." Balar realized he had just revealed more than he intended. "Shit."

"You realize it was probably destroyed. It isn't among the inventory of 28C."

The idea that his spade was missing struck him like a blow. His mother had made it and four others for his brothers. Each one had been perfectly crafted and enchanted to be more effective. Only Balar had decided to use it for the very purpose his mother detested: to kill.

Balar shook his head and looked down.

"This might be an appropriate time to revisit that promise you made."

"Shit."

"Without your shovel, having your leg amputated, and a near-death experience, I suspect you are at a crossroads of life." Mudd continued to speak in a monotone, without a waver or hint of emotion.

The mage closed his notebook. "You have something I would like to trade. A simple exchange."

Don't trust guards. Never trust deals.

"Between your boots, spade, and time of arrival, I'm sure I can confidently connect you to twenty-three robberies and sixteen murders. Three years isn't that long of a time and a number of questions would track your movements enough to ensure I could get a conviction for at least half of them. Furthermore, with the murder weapon identified, I could probably double the number of potential crimes."

Balar choked. He looked around, wondering how hard it would be to escape his manacles and throw himself out of a window. He wouldn't get far with only one leg but it might be enough, Mudd looked fat enough he would be a struggle.

Mudd held up a finger. "As I suggested, your crossroads has given me an opportunity to populate the registry with information that could save lives. I don't like missing data, too many times a key piece of lost information could have saved lives."

Balar ground his teeth together. Don't say anything.

"Given that your career as a thief and murderer appears to have come to an end, I propose a trade. Information about 28C in exchange for me not connecting you to your previous crimes."

"Why would I trust you? You're the city guard."

Mudd shrugged. "That is the crux of the problem. You have no reason to trust me and I benefit either way from your choice. Either I solve three years of cold cases and send you to prison---"

Balar squirmed.

"---or you give me detailed information of your experiences of 28C and I fail to connect you to your previous crimes."

With the pressure of the question and the cold, calculating way Mudd presented it, Balar shuddered. It seemed like an easy way out, a simple way.

He shook his head. It was never easy. The guards would screw over anyone to get their crime. Mudd would no doubt take it down as a confession and then charge him with the murders.

Mudd stood up. The chair legs scraped against the ground. "I'm aware that you cannot trust me. There is no reason for you to do so and there is no proof that I will honor my end of the bargain."

The mage shoved his notebook into his pocket and then headed for the door. "My offer stands until you make a choice, a month passes, or I find your enchanted shovel."

Balar tensed. He watched as Mudd opened the door.

Outside, Wathin stepped forward and turned around.

Mudd gestured him to the side. "Come on, Wathin, decisions have to be made."

"Decisions?" asked the other captain.

"Not by us."

Wathin glanced at Balar and then shrugged. Together, they turned and headed away.

Balar made a choice, a desperate and frantic one. "Captain!"

Mudd turned and peered into the room.

"Both of you promise? A deal?" Inwardly, he screamed at himself. Don't trust the guards. But, he also didn't have much of a choice. It was only a matter of days before Mudd found Balar's shovel and he would be thrown in jail. At least a deal would give him a chance.

Mudd turned back. "I made the offer."

Wathin sighed. "I trust Mudd's judgment. Not to mention, he's the forensic mage. If I don't hear anything, then I can't do anything." He tapped his halberd against the ground and turned to stride away.

Mudd watched him and then re-entered the room. Shutting the door, he pulled out his notebook. "Let's trade."

Balar knew he was going to be making a terrible mistake. Years of keeping quiet and now he was going to confess to a crime.

"Now, you were walking down the street at midnight and noticed the door of 28C was open. Naturally, as a good citizen, were there any other details you notice?"

Stunned, Balar stared at him in surprise.

Wathin groaned. "Damn it, Mudd."

Mudd ignored the other captain. He sat down on a chair and held up his notebook. "Well, you wouldn't have been casing the joint to rob it. So, you decided to see if there was someone in danger? Or was it just an open door that concerned you?"

Balar looked back and forth.

Wathin strode over to a chair on the far side of the room. He grabbed it and then dragged it loudly to the door. Turning it around, he planted himself down. "Yes," he said in a sardonic voice. "You were just an innocent man who had nothing but good intentions."

The thief looked back and forth.

"Just go with it, Balar. Mudd is trying to be nice."

Balar started to whisper his mantra but then stopped. "Fine, I was..."

"... walking down the the street..." prompted Mudd.

"... when I noticed the door open." Before he knew it, he was revealing the deals of his own robbery to two captains, something he would have never done an hour before.
