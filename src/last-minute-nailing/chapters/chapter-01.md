---
title: Last Minute Nailing
format: Flash
genres: [Slice of Life]
summary: >
  Sometimes a father has to step up to prevent his son from making a mistake.
---

"Why am I up at one in the morning?" grumbled Gad.

"Because I need you, Papa. I said I'd get this work done and I'm out of time."

"I told you not to take it." He sighed and looked at the gutted hall. Piles of planks lined the walls. "The Society's party is tomorrow?"

His son sighed.

"Fine." Gad stepped into the room. The planks launched themselves into the air and flew to their position. He waved his hand and sent nails streaming after them. With aching joints, he drove them home.
