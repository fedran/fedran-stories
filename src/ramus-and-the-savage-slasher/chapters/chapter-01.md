---
status: alpha
title: Ramus and the Savage Slasher
format: Story
character: Ramus #of Saradan
genres: [Action]
contentWarning: "Some themes that appear in this story: racism."
summary: >
  Ramus's next adventure involved a killer from the desert slaughtering innocent people on the streets. But, no matter how powerful his enemy is, Ramus will always defeat him. The Seven Forbidden Gods of Saradan have deemed it so!
---

> The adventures of Ramus are epic adventures seen by a man who lived in his own reality. But for the insanity of his words, there is no doubt that Ramus was more than capable warrior able to back his words. --- *The Curious Life of Ramus of Saradan*

It was dark and steamy night along the canals of Padirnin City. The moon shone between the dark fingers of clouds and the light sparkled off the rain-slicked cobblestones. A foul wind rippled down the streets, kicking up mist and papers to swirl them around Ramus' boots as he strode down the street.

The broad-shouldered man wore a heavy tunic of the brightest white, a shining star of masculinity in a world that wrapped itself in delicate suits and sword canes. No, Ramus followed the old ways, born from forbidden jungles of Saradan and raised by a tribe of mystics and warriors. He needed no sword, only the raw power of his naked fists and the swell of his chest.

He had just come from the University of Padirnin after presenting his new technique for forging steel that could cut through stone. The idea came to him while he was freeing the Village of Stone Creek from an pack of *ryochevnons*. It only took a man of his brilliance a month to perfect the formula and forge a weapon as proof.

Even when he finished to the applause of the country's greatest minds, all marveling at the wonders of his intellect, he stunned them by donating the women to the university instead of keeping it for himself. It was the greatest weapon of the age, but Ramus needed nothing but his fists.

Brimming with the glow of the accolades of the entire university, it didn't cause Ramus' head to swell nor his chest to rise with pride. He did it purely to help others, doling out the secrets when the world was ready to understand. It was for the greater good, nothing more.

Ramus smiled and took a deep breath, breathing in the misty air and letting it out with a rush. The air trembled with his mighty breath, but he was careful not to release the power of Fourth Forbidden God of Saradan for something as trivial as blowing the storm away.

He strode down the street with purpose, looking for one more way of helping the city before he was forced to slumber in the mansion he owned near the center of town. He knew there was someone in danger out there, there always was when he was strolling the streets. He took another deep breath, this time to sample the air. It stank of evil and the stench would not let him sleep until it was vanquished.

Stretching out his hands, Ramus called on the Third Forbidden God of Saradan. He wove his fingers into the mystical patterns he learned from watching the Sacred Waterfall of Kavadoom. His senses radiated out from his fingers, rippling on the winds as his vision expanded as if he was looking down at the city. Waves of power coursed through him, kicking up rain from the streets and washing it clean with the scent of flowers that no mortal man---besides himself--had ever sniffed.

His senses traced along the streets, rolling down the streets of the slumbering city he now called his own. He sensed a couple in steamy embrace. As his senses passed over him, he let his mind forget what he saw but not before a brief mental critique of the gentleman's lack of skill in the art of passion.

A few blocks down, he felt a store front that had an unlocked door. With a twist of his mind, he inspected the store, found that the key was not inside, and locked the door for the owner.

Ramus was about to stretch out his senses into the surrounding forests when he heard it. A scream for help! Someone was in danger!

"Seven Forbidden Gods of Saradan!" he cried and rushed to help. His boots crushed rocks as he sprinted down the road, shattered by the very force of his need to rescue the helpless damsel! He blasted his way across streets and blocks, eating up the distances in a quarter of the time that mere mortal men required.

The cries came from a fog-filled street near the canals of the city. He could hear her desperate whimpers, the rapid beat of her heart, and the stench of freshly spilled blood. It was hers!

With a growl, he charged with nothing but his bare hands in front of him. The fog peeled back to reveal the evil that stalked his victim. It was a black-skinned devil from the savage deserts! A creature so foul that the evil caused his eyes to burn with green flames!

At his feet, his victim was splayed out on the ground. Blood scored the side of her body, tearing open her blouse. Ramus could see blood oozing down her side and pooling in the cracks of the cobble stones.

The creature lifted its hand. There were claws at the end! They dripped with poison!

Ramus had to do something! With a mighty roar, he charged. The rocks shattered underneath his feet! The air trembled with the might of his bellow! He swung his fist!

The blow caught the demon in the shoulder. The impact which would have snapped a mortal man's bones only shook the devil and spoiled the poisoning slash. The claws pierced the woman's side again! They cut into her side! They cut her corset! Blood! It splattered against the cobblestones!

The black-skinned devil staggered back and snarled at Ramus.

Ramus growled back, the powerful rumble of his mighty chest far louder than the foul demon's cry.

The creature slashed at Ramus!

Ramus blocked the creature's wrist with his left hand. He punched with his right! Muscles born of years of slavery pummeled the creature, cracking bone and crushing organs. He punched again, moving faster than mortals could see1

The devil cried out but wasn't staggered! He slashed again! This time scoring three wounds against Ramus' shoulder! Poison burned through his veins, blackening them instantly as it raced for his heart.

Ramus drew back to punch again, this time with the force that could shatter a building.

The foul creature slashed again, moving faster than any mortal man. But this time, he was aiming for the helpless woman!

Unable to take the opening to slay the beast, Ramus moved with blinding speed! He scooped up the woman! He pulled her close to his powerful, muscular chest and dove back before the foul demon's claws could end her life!

The stones shattered from the poisonous fangs struck the road instead of her flesh. The creature screamed out in words that no civilized man would ever let pass his lips. It was the cry of a vicious beast losing his prey.

Ramus didn't run away. He didn't retreat from battle. The young woman needed him! Her wounds were too grievous to survive long and there was venom flowing through her pale skin. But, he couldn't heal her in the presence of the beast, so he withdrew to nobly save her!

He raced back toward the nearest healers, the map of the city burned with crystal clarity across his mind. He could not forget anything, it was his curse, but it was one he somehow managed to survive.

Looking down, he could see the black veins were creeping up her neck. No matter how fast he could run, he couldn't summon the speed to bring her to the healers who wouldn't be able to save her fast enough. It had to be him! He had to save her!

Jerking to a stop, he dove into an alley and raced along the other side. His speed picked up garbage which tumbled behind him! He shattered windows with the force of his passing!

A few blocks away was one of Ramus' many mansions. He raced forward, holding one hand in front of him. The front door shattered! The run fluttered behind him as he sprinted up the stairs! Moments later, he burst into the bedroom and threw her down on the bed as he prepared to save her life!

He grabbed the front of her dress and pulled. Mighty muscles, hardened into rocks with his years of slavery and the mystic training of his forbidden masters, surged and the fabric tore! His grip rendered the dress and the corset underneath, tearing open and revealing her most private of secrets to his gaze!

Ramus trained his mind to look away from the beauty that the Divine Couple had graced the poor woman and peeled back the fabric from the wound that gushed grievously. Blood soaked into the fabric of her clothes and into the blanket underneath. She wouldn't survive without his help!

Raising his hands, he summoned the power of the Seventh Forbidden God of Saradan. Green lighting streaked from the sky! It lit up the room as it crackled with a force of one of the lost gods, scoring the walls and carpets with the sheer power. He turned his hand down and planted it against her wound. He could feel the heat of her injuries as well as her body. The curve of her hip and swell of her breast tickled his senses. His steel mind forced the sensations of her private parts from his palm, though it took all of his training to do so.

The green power of the Forbidden God sank into her body, the jungle mysteries bringing forth the power of healing. Underneath his hands, he felt the skin sealing up and the wound fading. The black veins of the desert devil faded into the dusky blue that the Divine Gods intended. He cleansed her blood, curing her of a childhood illness as he repaired the damage the devil had wrought. He even found a mole and used nothing but the power of the Forbidden God to remove it from her nearly perfect skin.

With an afterthought, he removed the poison from his own body. The foulness wouldn't have stopped him, but he enjoyed the purity of his own muscular body. It was nothing for someone blessed by the Forbidden Gods.

Moments later, he pulled his large hand back from her nubile flesh. Even the blood that soaked into her dress was gone! The skin was unblemished! She was healed!

The power of the Forbidden God fled him, withdrawing until it was once again needed by the greatest hero in the land.

The woman's eyes fluttered. She moaned as she opened her eyes, her crystalline brown gaze looking up at the man who saved her. "It's you," she breathed, her chest rising and falling with her growing excitement.

"Yes," he said simply.

Ramus knew what she wanted, the flush of her cheeks and the pebbled crests that rose for him. But, he couldn't enjoy the rewards of his rescue until evil was defeated.

With a snap of his hand, he grabbed the blanket underneath her and whipped it out! Her body shuddered once, sinking into the soft mattress. He spread the blanket over her exposed skin, covering the delectable flesh with a blanket given to him by a thankful tribe from the jungles. He tucked her in and stood up.

He was a hero and he had evil to once again defeat.

Walking away took all of the willpower he had earned from his years of studying with the masters of the steppes. The old masters taught him to resist all discomforts, even the mighty ones that would cause a lesser man to submit and his trousers to tear.

Turning around, he stormed down the stairs and back into the street. He once again called on the power of the Third Forbidden God of Saradan, the power of observation. The smell of lost flowers surrounded him in a caress of heat and moisture before spreading out across the streets. In his mind, the map of the city laid out for his gaze, not unlike the woman upstairs.

He sought out the evil, tracing it via the foul stench of the desert that surrounded it. The forbidden powers of the god quickly caught the black-skinned demon, this time it was underneath in the sewers, no doubt the lair of the beast. The smell was fresh, the tracks still shimmered. The creature had just entered the city. No doubt in preparation of preying on the helpless citizens. But, it was a fool to think that his city didn't have a protector. It had Ramus! And he would defend it with his mighty fists!

This time, he didn't hesitate. Clapping his hands together, he summoned the power of the Second Forbidden God of Saradan, the power of inhuman speed. Digging his boots into the cobblestones, he felt it pour into his mighty muscles and quicken his stoic heart. The energy rippled along his skin and beat deep in his chest. Around him, the drizzle of rain slowed and stopped, as if waiting for the power of the forbidden mysteries.

With a roar, he charged forward. The stones cracked underneath his footsteps! The fog was sucked behind him! Pulled in by the inhuman speed granted by forbidden powers! He covered the entire length of the city in a matter of seconds!

He skidded to a halt near the beast's lair, tearing up the street with his effort. Looking around, he found the nearest sewer cover. With his mighty muscles, he ripped it out of the ground and tossed it aside! It cracked into the side of wagon, shattering wood!

Ramus frowned for the briefest of moments. He dug into his pocket and pulled out a handful of crowns, enough to pay for the wagon three times over. Tossing them aside, he jumped into the darkness of the sewer. The coins landed in a perfect stack right by the driver's side.

He hit the sewer tunnel with a thud that shook the air! He looked around, his lips peeled back at the filth and grime that a creature would choose to live in. With a flex of power, he took a deep breath and let it out. As he did, he hummed to summon the power of the Fourth Forbidden God of Saradan. His exhalation pushed the sewage away and dried the ground, giving him the perfect footing to hunt the beast.

The power of the Third Forbidden God of Saradan still flowing through his veins, he easily identified his quarry. With growl, he stormed toward the foul beast that preyed on the street and attacked innocent women! His boots splashed in the muck. They crushed the skulls of rats that got in his way!

It took him little time before he was inside the lair of the beast. The stench of the desert assaulted him. It choked him but he summoned the techniques that he learned when he was betrayed in the Marsh of Infinite Deaths. Slowing his breathing, he walked into the lair with no concern for his safety. He was a hero and heroes didn't sneak, they charged. He flexed his fists in preparation for victory.

The black-skinned devil rose up with a hiss. Venom dripped from its fangs as it glared with burning green eyes.

Ramus felt a shiver run down his skin, but it wasn't fear but anticipation of the creature's defeat.

"I will defeat you," roared Ramus!

The creature answered in the foul tongue of the desert, staining the air with its demonic words.

Ramus knew the words but wouldn't dare let them linger in his mind. He wouldn't sully himself with lesser tongues. He balled his hands into fists and prepared for a might battle!

The demon charged! Its claws flashed in the light!

Ramus caught it with an uppercut! His mighty muscles drove it into the ceiling of its lair!

But the impact only shattered stone and cracked mortar. The creature dug its evil claws into the stone and skittered away like a cockroach fleeing the light.

Ramus charge, bounding over the corpse of the black-skinned devil's victims. He gave a moment of silence for them, a sadness for lives he could not save, but he couldn't afford to give the creature a chance to escape. He landed in a fetid pool of sewage and punched up.

His mighty fist crushed the creature against the ceiling! The impact would have cracked bone but the foul devil only hissed.

Slashing out, the creature's claws glinted in the air. Venom splashed everywhere! The stone scorched! Water bubbled! It struck his arm! The claws dug deep! Clear to the bone!

Only the mystical art of tightening his muscles kept the blades from cutting his into his arteries. He staggered back, the pain almost overwhelming! He lash out, sure he would destroy the creature.

The desert devil's eyes flashed and Ramus missed! Only foul magic could cause the greatest hero to ever miss!

The demon howled and attacked again! Claws slashed at Ramus' arms and chest, slicing through his tunic and into his skin. It scraped against his bones and cut through the muscles.

Ramus fell back, clutching his chest! His blood poured down through his fingers! It soaked his chest and stained his shirt! He looked down at the crimson that soaked his body, the heat that burned across his wounds. His own blood! Spilled!

He looked up as the righteous anger flowed through his veins. With a snarl, he summoned the First Forbidden God of Saradan and let the raw power filled his body. The blood along his chest stopped flowing! The muscles clamped down and stopped his bleeding!

With a roar that shook the air, he grabbed his tunic and tore it open. Muscles glistened as he flexed them, bending the very air around him with nothing more than the fury that burned through his body.

The demon hissed and covered its face, unable to look at the brilliance that was Ramus enraged.

Ramus gave the devil no quarter. He balled his mighty fist and threw a punch. The impact shattered bone and drove the creature into the stone. It broke the rock behind the creature! It left a mighty crater!

He continued to assault it, pounding with his inhuman strength. Each strike shattered more of the creature's body, pulverizing its bones and bursting flesh.

Venomous blood poured out but Ramus refused to give in!

"You attacked the innocent!" he roared and slammed down. The creature's legs were destroy in an instant.

"You preyed on my city!" he bellowed and shattered the creatures' chest. Foul blood gushed out of the creature's body, but it burned away from the righteous fury of Ramus.

"You have ripped my tunic!" he screamed and tore the creature's arms off, one after the other. He briefly considered beating the devil to death with its own limbs, but true justice required his bare hands. He threw them aside into the foul sewage where they belongs.

The black-skinned devil cried for mercy! It whimpered with desperation! It tried to surrender!

Ramus gave it the mercy that it had given its victim. With a powerful roar that shook the brings, he brought both of his hands down and crushed the devil's skull! Bones flew! Blood splattered! Gore painted the walls!

Panting, Ramus pulled his hands back. Blood and brains dripped from his fingers. It splashed to the ground, splattering loudly against his legs. The crimson stain of the creature's blood soaked his skin up to his elbows.

Summoning the power of the Second Forbidden God of Saradan, Ramus let the energy flow through his veins. It burned away the gore! It seared away the blood! It repaired his tunic given to him by his blind masters!

When he crawled out of the sewer, it was as if he had never fought. Except that the fog had faded and the air was once again fresh. Ramus had saved the city from a foul creature preying on it. Storming over to the sewer cap, he dropped the heavy metal into place.

He stepped back and took a deep breath. He had done it. He had saved the city from a creature preying on it.

There was only one thing left to do.

Minutes later, he stood in front of one of his many mansions. The woman he saved stood in the door, her body wrapped with a chest. Her cheeks were pink with desire! Her breasts heaved against the thin fabric! He could smell her excitement!

With a steady heart, he scooped her up from the foyer and carried her back to the bed where he saved her life. Soon, she would know why he would soon be known as the Eighth Forbidden God of Saradan, the god of love.
