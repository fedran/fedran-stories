---
title: I Will Hurt You Only Once
date: 2019-11-15
summary: >
  Late at night, Gichyòbi is interrupted by his daughter who has questions about Rutejìmo's anger toward his clan. It is up to him to decide if he is going to risk his daughter's future magic to promise her that he would only ever hurt her once.
---

> The strongest magic comes from the deepest trauma. That's the cruel price of magic. --- Galdinol Misfanic, *Civilization, or The Cause for Magic Leaving Our World*

Gichyòbi yawned as he headed to the front door. Joints in his knees and shoulder popped and cracked from sitting too long. He reached the entrance and pushed it open to let the cold desert air in.

Behind him, Rutejìmo and Mapábyo walked hand and hand. The younger couple were leaning against each other. Both looked exhausted but that made sense after a day of running across the desert and then talking until almost morning.

Gichyòbi let his senses enter the stone underneath him. He felt the vibrations coming toward him from all directions. He could easily distinguish the distinct feel of other members of the Wamifūko clan compared to the few strangers still walking around so late in the morning. One of them was a few blocks away but in the path of the couple leaving.

He sent a request through the stones, his thoughts vibrating along the rock. (Could someone move the drunk on Red Facet Passage near Rolling Rock Street away? The Shimusògo are heading to the Glorious Saber Inn.)

A patrolling Wamifūko warrior changed directions and headed straight for the stranger. By the time Rutejìmo and Mapábyo left, their path would be clear.

It only took a second for everything to happen. Gichyòbi held out one large hand. "Be safe walking to the inn."

Mapábyo stood on her toes to kiss his cheek. She was a wiry girl with nearly obsidian skin. Her eyes were dropped with exhaustion. "Thank you, Chyòbi. Dinner was lovely."

He hugged her firmly before releasing her.

Rutejìmo hugged Gichyòbi but said nothing. His body was tiny compared to Gichyòbi's muscular frame but there was a strength in the racer's body. He gave Gichyòbi a weak smile before pulling away.

Mapábyo caught his hand, guiding him through the door almost much as she was drawing him closer to comfort him.

Gichyòbi closed the door behind them but kept his senses tracking them through the stone as they shuffled to the inn.

Kidóri, his wife, came up to hug him from behind. She was much shorter than him but almost as wide. Her curvy body felt right pressed against his back. Her love and warmth brought a smile to his lips. "Jìmo looks like he has one hand on death's breast."

He grunted in agreement.

"He needs a break," she announced as she released Gichyòbi. Underneath his feet, he could feel her other words. (Gichyòbi says to reschedule the Shimusògo visit with the clan council for two days. Have someone put the notice under their door so they don't have to leave.)

He chuckled and sent a pulse of approval.

Somewhere on the far side of the Wamifuko City, one of the awake clerks acknowledged with a vibration of the rocks.

She yawned. "Clean up, please?"

"Of course, my queen of rocks. Your order shall by my only command." He bowed dramatically as he grinned.

She smiled and looked over her shoulder, her dark hair obscuring much of her face. She was beautiful and he let her know with a focused pulse of energy along the rocks. Her smile grew wider and she rocked her hips from one side to the other. "Don't stay up too late."

With a wiggle of her ass, she headed up the stairs.

Gichyòbi admired her until she was out of sight and her footsteps faded into the depths of their room. Then he heading into the living room to pick up the discarded cups and plates from hours of talking.

In the kitchen, he set everything into the sink and opened the valve to the hot water. As the steaming liquid poured over the dishes, he headed straight for the ice chest and pulled out a small cask of his current batch of stout. It was a southern recipe from one of the clans near the pale lands and required a few ingredients that cost him a pretty coin.

Gichyòbi poured himself a large mug before turning the cast to the ice box. Humming to himself, he took a long swallow before setting it next to the sink and began to wash dishes.

It wasn't much longer before he felt the delicate touch of bare feet on the stone ground. Judging from the weight and stride, it was Nifaópi, his daughter.

He grinned. Without looking back, he said, "Aren't you supposed to be sleeping?"

"I was thirsty." She pulled out a drawer enough for her to climb up it and onto the counter. Her nightgown caught on the drawer and he reached over to free it before she ripped the blue fabric.

Nifaópi turned around and sat on the edge of the counter. Without asking, she reached over and picked up his mug. Sniffing at it, she pulled a face.

He laughed. "Water?"

"Yes, Great Papa." Nifaópi set the mug on the other side of her body, away from him and his drink.

Gichyòbi got another mug and filled it with colder water before handing it to her. As she grabbed hers, he reached over and rescued his own.

"Salute!" she said with a grin.

They tapped mugs and both took deep drinks.

With a satisfied sigh, he set the mug down on the far side of the side and returned to cleaning the dishes.

"Papa?"

"Yes, my diamond?"

"Why does Jìmo get so angry when he talks about his family?"

Gichyòbi hesitated for a moment, startled by the question but also dreading it. He concentrated on scrubbing meat from the plate as he tried to formulate his thoughts as he delved into an uncomfortable topic. "Great Shimusogo Rutejìmo's family hurt him badly while he was growing up."

"They hit him?"

"No, not really. They were..." He struggled with the words to describe what the Shimusògo had done. "... they let him think that no one loved him."

"No one came to kiss his knees when he scraped them?"

Gichyòbi chuckled. "No, my diamond, no kisses of scraped knees and no hugs before bed."

"That's mean. Why were they so mean?"

He tensed. "He needed to be hurt when he was young otherwise he wouldn't...." He trailed off. There were somethings he wasn't supposed to say to a child.

"But he's old now."

"Old?" He looked at her. "He's not even thirty."

Nifaópi looked at her father and then rolled her eyes just like her mother. "Fine, he's really old."

"I'm forty-three!"

"And you are so old, you're almost dead," she said in a serious tone.

He gave her a mock glare. "See if I'm going to feed you tomorrow."

She stuck her tongue out at him. Then she hopped off the counter to grab some bread and cheese from a pantry before returning. With her eyes focused on Gichyòbi's face, she deliberately tore off a hunk of both and shoved them into her mouth. The gap of her missing front tooth only added to the challenge.

"Fine, he's old. The problem is that his family never stop hurting him. They had gotten used to treating him differently that they forgot to learn how to love him."

Nifaópi had to swallow loudly to clear her mouth. A fleck of cheese remained on the corner of her lip. "Pábyo treats him nicely."

"Mapábyo loves him and is probably the first one to help him see that he's amazing and wonderful and a good man."

"Everyone needs a Pábyo."

"Yeah, everyone does." Gichyòbi finished another plate and set it on the counter.

Nifaópi bit down on the cheese and ripped off a hunk with her mouth.

"Tear it off first!" he snapped.

She chewed loudly, her mouth open. Then she grinned and closed it before finishing. After a moment of watching him clean, she stood up on the counter and stepped over the sink to grab a towel. Pushing his mug further down the counter, she sat down and started to dry the dishes.

"Thank you."

"Will they ever stop being mean to him?"

"Probably. Now that it's obvious what he's become, its easier to accept that he has always been different." He couldn't explain how Rutejìmo had become one of the kojinōmi, a caretaker of the dead. Nifaópi wasn't ready to understand the confusing world of those who lived outside of society.

She finished a dish. "You won't hurt me, will you?"

Gichyòbi froze as the dreaded question came up. He should have known it was coming from the line of questions, but it still hit. As he closed his eyes, he reached out for his wife through the stone. (Nifaópi is asking about hichifūma.)

Hichifūma, the secrets they kept from their children until they go through their rite of passage and bind themselves to one of the clan spirits. It was the reason Rutejìmo had been hurt so badly.

Kidóri's response shook the ground faintly. (Are you going to tell her?)

(She asked about Rutejìmo's pain.)

The problem with stone-speaking was that emotions couldn't be sent. He wished he could see his wife's face to know her thoughts. Instead, he had to hold himself still.

(Do you want to tell her?)

He nodded. (I think we should. It's worth the risk after seeing how much he hurts.)

"Papa, is this a secret?"

He turned to see his daughter staring at him. Her emerald eyes were dancing as she took in his face.

"The ground is shaking, you're talking to Mama."

Gichyòbi chuckled. "Clever girl."

He pulled his hands from the water and dried them. "Can you listen to something I'm going to say and keep it a secret? You can't tell anyone, but most of all, not your brother."

"Why?"

"Because this is a secret that can't be forgotten. Even though it won't affect you for many, many years, its going to be there and it will change your life." His chest ached as he struggled. "Just knowing it is enough."

Her eyes glistened. She nodded slightly.

He didn't say anything.

"I promise," she whispered. "I won't tell anyone."

Gichyòbi took a deep breath and fought to keep his voice from cracking.

She inhaled.

He took her hands in his, the tiny little palms dwarfed by his fingers. He kissed them gently before looking into her eyes. "I'm sorry. I am going to hurt you too. At a very specific point in your life, I'm going to hurt you more than anything you have ever felt before."

A whimper tore his heart apart.

He rushed forward, kissing her fingertips again. "I promise, I will only hurt you once, but I must do it."

"W-Why?"

Tears ran down his cheeks.

She tugged on her hands and he let her pull away. "Why, Papa? Why would you do that?"

Gichyòbi struggled to say anything. He felt like he was on the edge of a cliff, about to say something that would ruin everything.

Kidóri's words rippled through the stone. (Do you need me in there? I'm in the hall behind you.)

He smiled. (Not yet, my love.)

She gave a word of encouragement and nothing else.

He focused on his crying daughter. "Do you like it when I take you through the stone?"

"Y-Yes?"

"Or when mama makes all those flowers bloom at once? Remember when she made the blue petals for your birthday?"

Nifaópi wiped her nose. She gave him a smile. "Yes."

"That's magic. Clan magic."

"I know what magic is, Papa," she said rolling her eyes.

"What about my brother? What about him?"

Her smile faded. "He's...."

"Go on. He's not here, he's not to hear anything you say in this moment. I promise." Gichyòbi tensed as he waited for the answer he knew would be coming.

"He's not very good, Papa. He keeps hurting himself when he walks through stone. That's why he fills out all the forms and and stuff."

"So, would say his magic was strong?"

Nifaópi cringed as if Gichyòbi was about to yell at her.

He stroked her cheek. "I'm not going to yell, I'm trying to explain why I have to hurt you."

"But you didn't hurt him, right?"

Gichyòbi felt the memories jam into his stomach, cutting deep. "No, but that's because I told him the secret. My papa and my mama did also because they were worried about him. Many people told him and he listened."

He let out a long, shuddering breath. His brother had forgiven him for his role, but even years later, it still hurt. He forced himself to focus on his daughter. "No one told me, which is why I'm so strong. I didn't know the secret. Your mama didn't know it either."

She frowned. "I-I don't understand, Papa. How can a secret make someone strong? How can hurting someone give you magic?"

"Are you sure you want to know?"

Nifaópi hesitated. She sniffed and looked around, then dragged his mug closer to him. For a moment, he thought she was going to drink from it but she only toyed with the rim. "Why?"

Gichyòbi sighed. His chest ached and his heart hurt. He shook his head for a moment, trying to steel himself.

Kidóri rescued him by coming into the room, her bare feet whispering across the stone. "You remember that time when you and your brother fell off the wall and into the water basin?"

Nifaópi nodded and cringed.

"What happened?" Kidóri came up to slip her arms around Gichyòbi and their daughter.

"I started to drown. I kept trying to grab the edge but my fingers were slipping. I broke my fingers. There was water everywhere and I couldn't stop it from going into my nose."

"Did you think you were going to die?"

"Y-Yes, Mama."

Kidóri wiped the tears from her eyes. "Its moments like that when magic finally comes. When you are grabbing at the wall, trying to rescue your brother, and there is water in your lungs. That feeling, that horrible feeling like you aren't going to make it? That's when the clan spirits can finally hear you."

Gichyòbi added his own words. "The more terrified you are, the most scared, the more you are convinced you are going to die, the more powerful the connection and the stronger your magic."

"But I didn't get magic then. Neither of us did. I thought I was going to die."

Her mother answered, "Last year your mind and body weren't ready yet. It happen about the time when you are struggling not to cry or lash out at your brother, when you start to grow hair along your southern fields, and your breasts get bigger. That will be the point, many years in the future."

Nifaópi buried her face into her mother's arm. "So if I almost die when I'm older, I get magic?"

When said that way, Gichyòbi couldn't help but feel her words as a knife into his stomach. He knew it was coming, he thought about it like any parent did. He bowed his head. "Yes. If you knew you were going to be okay, you wouldn't be as terrified and the spirits won't be able to talk to you."

"Then why do you have to hurt me, Papa?" His daughter's voice was muffled from being pressed against her mother's arm.

Kidóri kissed Nifaópi on top of her head. "Because he loves you."

Nifaópi pulled away. "He's going to kill me, Mama! Papa is going to hurt me! Why? Why would he do that!?"

"Oh, my diamond," Gichyòbi said as he hugged her tightly. Tears burned his eyes as he cursed the world they grew up. "I have to do this."

"W-Why?"

"To keep you safe."

Nifaópi stopped for a moment. "What?" He could almost see her struggling to understand.

Gichyòbi closed his eyes tightly, squeezing out the tears. He was risking everything to let her know, but he kept thinking about how much pain Rutejìmo had gone through. "We do it before the desert does it for us. If we don't, you might be in the middle of the sands when it happens. You might be poisoned, drowned, or suffocating and we might l-lose you." His voice cracked.

Nifaópi shook her head.

Kidóri continued where Gichyòbi faltered. She looked into his eyes as she spoke in a low, strained voice. "The desert will making it happen, I promise you that. However, if we do it ourselves, your clan will be close by to make sure you survive no matter what happens."

With that, the secret had been broken.

Gichyòbi squeezed her waist. Through the ground, he sent a quiet word. (I hate this so much.)

"I-I won't drown?"

Gichyòbi shook his head. "No, I will do everything in my power to make sure you are safe. No matter in the world is worth losing you or your brother. When it's over, I won't ever hurt you again. But until that point, I need you to believe you are going to die otherwise you won't hear the magic."

In the back of his head, memories of his own rite rose up. Even now, decades later, he could still taste the foul air as he pounded desperately on the inner walls of a coffin made of stone. The scratches were still on his fists, if he looked. He had nearly suffocated to death in that tomb, not knowing that his own mother was watching only inches away. He had no doubt she was feeling the same thing he was experiencing in the moment.

Nifaópi toyed with his mug. Tears ran down her cheeks as she rocked it back and forth.

Kidóri started to take it away but then stopped. She sighed and leaned into Gichyòbi. (I hope we are doing the right thing.)

(Me too, my love.)

Nifaópi sniffed once and then began to bawl.

Gichyòbi's heart ripped in half. He opened his mouth but couldn't find words that would help. There was nothing he could say.

His daughter reached out to grab both of them, pulling their bodies close as she sobbed into the space between their arms.

Kidóri and Gichyòbi held their daughter tightly as the sobs wracked her body.

His back began to ache almost immediately as he had to crouch down to hold them but he didn't care. All the pain in the world was nothing to the sorrow of seeing his daughter in distress. It was part of being a parent, even the monster the desert would force him to become.

(I hate that magic requires so much pain.)

He nodded silently.

(Why does the sun-damned sand make us do this?) Kidóri asked. (To Nifaópi? To Rutejìmo? To our entire family?)

(Because if we don't, the desert mother will do it herself. The sands are far crueler than anything we will do to her. Would you rather put your children in her hands?)

She kissed Nifaópi. (Never.)

Their daughter's tears began to slowly subside.

Kidóri shifted her hand to pull her off the counter.

Nifaópi buried her face into her mother's breast. Her cries were softer. It sounded like she would be sleeping soon.

Gichyòbi pried his arm free. (Think she'll remember this?)

(I hope not.) Kidóri looked at him with concern. (You know what will happen if she does?)

(Then I will have to hurt her even more.) He frowned. (We were wrong for telling her?)

Kidóri kissed their daughter's head. (No, she has to know that we love her. Even if it remains in the back of her head, I can't have her ever think that she isn't anything less than our diamond in the sands.)

Gichyòbi stroked Nifaópi's hair, running his fingers along her curls. After a moment, he pulled away. "I love you, Ópi. With all my heart, I will always love you."

(Leave the rest of the dishes, come to bed.) Kidóri carried Nifaópi out of the kitchen.

Gichyòbi sighed and looked at the remaining mess. Reaching out, he grabbed his mug. He didn't have a taste for it anymore. Carefully, he poured it down the drain.

The desert demanded he hurt his children and he would obey. The consequences were too much to bear. But he refused to do it again. He would hurt his daughter only once.

Then he would spend the rest of his days telling her how much he loved her.
