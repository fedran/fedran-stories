---
title: Losing It Brick by Brick
date: 2019-11-04
summary: >
  Despite winning the fight against the baron, Gertrude and Eustas Ridor lost when the legal system stole their home away. Almost penniless and broken, they head to the nearest city in hopes of recovering their lives.
---

> Signs of the second Kormar Civil War came from the edges when rural barons and and knights began to carve out private domains. --- Faston Cargril, *Brother versus Sister: How Kormar Fell*

Gertrude sat on the carriage seat, clinging to the suitcase on her lap and her head bowed low enough her brow touched the cracked leather. She felt old and broke, as if time had accelerated in the last few months until the weight of age bore down on her like a wagon full of bricks.

The carriage turned on its rails and she didn't have the strength avoid bumping into Eustas, her husband. His wiry body caught her. He sighed, pushing her away and back into a sitting position. "Sorry," he muttered before focusing his attention to the stack of papers in his hand.

Slowly, she looked over at him. Her left eye made it difficult to focus on him, but her mind filled in the details that time had hazed over. The old man who sat next to her blurred into the sixteen year old farmer who proposed to her from on top of a cow. He managed to stay on long enough for her to agree before the annoyed cow kicked him into a pile of dung.

She smiled.

He glanced at her.

At the sight of his hollow eyes, her smile faded.

He shook his head. "No, please don't. I need that smile right now."

Eustas leaned over and kissed her. "At least we have the three of us."

Fighting a wave of sadness, she kissed him back. She didn't have the words to say anything else. Unwittingly, she glanced down at the papers in his hand and what little joy their embrace had given faded instantly.

Eustas had their eviction papers and a scrip for the property they were forced to sell. A pittance of just over five thousand golden crowns for five generations of her family's quarry, countless memories, and her entire life. She was born in the fields and she planned on dying there.

He sighed and folded it over. "Nothing we can do. I... I thought...."

Gertrude leaned her head on his shoulder. They both had so much hope after their fight over the quarry, but they didn't realize that the baron's wife was the one pulling the strings and that bitch had already started the eviction proceedings.

A young man in a dark blue uniformed sat across from them. "Hello, Mother."

He leaned back and hooked one knee over the arm of his bench. He had a short-billed hat with the lower half of a brass mask perched above the bill.

Next to her, Eustas tensed. They only had one child and the young man wasn't it.

She reached out and patted his hand, partially to cover his fist but also to stop him from doing anything.

The men who delivered the eviction notice had been wearing the same uniforms but their masks were in place to obscure their identity. She remembered the leader, he had the same build as the man speaking to her now. Without a mask, though, he looked familiar.

"I saw you hook up the car to the train back in Bricksetter's Haven. I thought you were getting out of the quarry business for good." He gave a little smile.

Eustas cleared his throat to say something stupid.

Gertrude interrupted him by squeezing her hand over his and speaking up. "Last load to sell."

It was a lie, the three tons of bricks on the carriage weren't for sale. They were the last of their home, their belongings. The only thing they had left.

Eustas's jaw ground together for a moment.

"Pity," the young man said without a hint of sadness. "My mum always said as long as the Ridors were around, Bricksetter's was the safest place to grow up. I guess with you leaving town, there isn't much reason for anyone to stay."

"Your mum?" Gertrude frowned as she tried to reorganize the boy in front of her.

His smile grew wider. "Shel."

Stunned, she stared at him for a moment before she could speak. "You're Shelis's son? Oh my blessed couple, I haven't seen you in nine years! I thought you left town, Ralnak!"

Ralnak swung his foot on the other side of the arm. His black boots tapped against the metal bench. "After mum died, I had to get out and see the world. I did and I'm glad." His face twisted in a scowl. "Then I missed watching my dad marry that Hork cow."

Gertrude's stomach twisted and she felt her heart break a little. Ralnak's father had married a wonderful Hidanork woman who had immigrated a decade before. She couldn't imagine how the smiling young man in front of her could have ever see a negative side to her. "H-Have you seen your father?"

Ralnak's smile grew wider. "I served papers to him this morning. That look on his face was priceless."

Eustas's arm shook with anger.

Ralnak's gaze turned to him and then back. "I should have done it a week ago, that was the date on the papers, but why should he have so much time. I gave him two days, that's enough. Tomorrow, I'm going back with my squad to make sure he vacated."

"If he doesn't?" Gertrude asked quietly.

Ralnak sat up sharply. His boots thumped against the carriage floor as he leaned forward. "Then I'm going to kick him and that cow out by sword or rope."

"But your---"

"My family?" Ralnak shook his head angrily and then sat up. "Why would I care about them? They are dirt, nothing more than sheep to be sheared. Let them rot. I'd rather see the two of them in a gutter than be in my life."

He stood up sharply, slapping his hand against a long knife on his side. "You better be careful yourself. Say the wrong thing and you might be up worse."

Gertrude looked up, tears in her eyes and dread filling her.

"The world is changing and if you aren't working with the baroness, you are going to be finding yourself at the end of a rope before you know it."

With a mocking smile, he strode away.

As soon as the sound of his boots faded away, Eustas turned to her. His eyes were shimmering. "What happened to him? How could he... how could he do that to his own father?"

"I-I don't know. I don't understand anything right now."

"Mother, we can't stay in Gajour. That's still in the baron's lands. We might need to keep on this train until we get closer to the capital."

She tightened her grip on his hand as she struggled to think through the growing despair. She had never gone more than a sixty miles from home. Gajour was their plan, the largest city near the rural community of Bricksetter's. But they couldn't leave their friends behind either. "What about Karl and Maril? If he's served them papers, they can't empty the farm in time. They'll lose everything."

Neither mentioned what would happen if they were at the farm when Ralnak came back. She was sure that Ralnak probably told them just to jab a knife into her ribs. Going back would just be rushing into his trap.

"We have to go back," they said at once.

Eustas smiled and reached up, his bony fingers shaking from the effort. He cupped her chin as she smiled back. After a moment, he said, "I love you."

She sniffed. "We have to."

"I know, Mother. I just... hoped there would be something bright in our futures. Settle down, live our lives, be happy."

Her knuckles ached from age as she wrapped her hands around his. A tear ran down her cheek as she smiled at him. "I will never be happy if we just run away. None of us will be: you, me, or Baby. The Couple will see we are taken care of, if not in this life, then the next."

"Blessed be."

They stared into each other's eyes, the moment stretching. But it was interrupted with a new fire. They had to plan and get ready. They broke as one and looked down at the papers in his hand.

Eustas sighed. "We should be getting in just past noon. There will be a bank or money changer that can break the script. I'll start with that."

Relieved that they had a focus, she picked up her bag of knitting and pulled out her notebook. "It will take about three hours to get the bricks shipped back north. I'm thinking a fifty crown bribe would get that to two hours and we can take the Elegant Express back north. Another hundred crown will get them to stop at Bricksetter's for ten minutes."

He looked at her sharply. "We're riding Baby?"

"No, he can't carry us that long."

"Good because I don't think I can keep Baby moving that long."

"And I don't have enough fire to keep him moving either." She smiled. "Not even in our younger days. No, Old Man Sal has that wagon by the train station, remember the one you used to buy flowers from?"

When Eustas nodded, she continued. "His granddaughter has been talking about selling and moving to Gajour. And she used to run all the way to Karl's pies. She'll sell it."

"Another two hundred for her?"

"She's going to have to go much further than Gajour. Give her five hundred, she's going to need it. Maybe head north to Saberal or Tonifew."

With her tongue sticking out of her mouth in concentration, Gertrude started making notes in her book. They weren't going to have a lot of time if they were going to beat Ralnak back to his parents to muster a defense. Baby was good for a stand-up fight but terrible for a running battle.

An hour later, the train pulled into the station and the Ridors had a plan. Gertrude hobbled with her cane off the train and headed straight back toward the tail of the train. Eustas walked with her as he scanned the street for a money changer's signage.

The city seemed somber, more so than usual. The steam vehicles belched clouds as they rumbled past. Horses spooked and shied away before going about their own path. It felt like there was some invisible beast in the city, waiting to suck out their last breath.

Despite their need to hurry, neither could move quickly. It felt like forever by the time they reached the final car: a flat carriage with a heavy tarp on it. Three tons of honey-colored bricks stood in the center with rusted iron bars next to them. There were other strange shapes.

Somehow, seeing Baby gave her a second wind.

"There's a money changer," Eustas said. He stopped and turned toward her.

She kissed him automatically. "Be safe, father. Don't let them scam you."

He smiled, his lips curling up and his glazed eyes almost shining. "I won't. We're doing the Couple's work, things will---"

"Attention everyone!" Ralnak announced.

She turned with a sinking feeling in her gut.

The young man stood with a dozen other uniformed others, all of them in their twenties. With their uniforms, they looked like an army. They were, in a way, just the baroness's personal army instead of Kormar's infantry. She noticed most had their hands on their sword hilts while a few had drawn their weapons.

"What is this fool doing?" muttered Eustas.

Ralnak gestured grandly to the people standing between him and the train: travelers carrying luggage, workers with packages to deliver, and migrants looking for new homes. No one who had money would take the train, they would have had private carriages. He held up his hands and smirked. "I have reliable reports that there are smugglers on this train."

"Oh, for the Couple's sake," muttered Gertrude.

"He's going to rob us." Eustas let out an exasperated sigh. "That little shit is actually going to rob the train. He knows we have a scrip for a lot of money."

"By the order of Baroness Strol, the rightful leader of these lands, I am ordered to inspect all packages for stolen scrip, money and valuables. If you are in possession of forgeries, your ill-created goods will be confiscated!"

"That little shit," they both whispered at the same time.

Gertrude glanced up at the pile of bricks and then back at the arm. "Eustas?" she whispered.

"Yes, Mother?" He kept his voice low.

"Wake up Baby."

He stiffened. He looked at the bricks and iron. "Baby isn't that fast. They are going to see him waking up."

Gertrude kissed him again. Her heart ached from her nervousness and her bladder felt like it was about to explode. "I'll distract him."

"He needs you too."

"He will have his mother." She had no clue how she would get back but she couldn't let Ralnak continue. She reached down and pulled the pile of papers from his pocket while carefully leaving the scrip behind. With a loud sigh, she made a point of shoving it into her knitting bag before turning around.

Ralnak was watching, smirking as he toyed with his knife.

She strode toward him, raising her voice. "What is wrong with you?"

"What do you mean, Mother?" He reached out in a friendly gesture, almost to pull her into a hug. His smile didn't reach his eyes.

Gertrude batted his hand away.

His face twisted in a scowl.

Behind him, the rest of his squad started to draw weapons.

"You are acting like an immature ass!" she started, raising her voice as much as her throat could handle. She wanted to look back to see what Eustas was doing but couldn't afford to risk her ruse. "You know these people aren't smuggling."

His lip started to curved back like an angry dog.

She hiked the strap further up on her shoulder, made sure her knitting was safe, and then used her now freed hand to slap him with all her might.

The impact with his cheek burned her knuckles and palms, pain that barely registered over her anger.

His head snapped to the side.

Everyone froze.

Slowly he looked back at her, a glare in his eyes and her print across his face. "That was stupid, Mother. I was willing to giving you a chance, but now you're going to pay like the others."

On the edge of her hearing, she heard the scrape of brick on brick. It was a faint sound but she had heard it most of her life, there was no way she could miss it over the other sounds still drifting down the streets.

He started to tense and she thought he could hear it too. She spoke loudly, raising her hand to slap him again. "I'm not your mother, you ungrateful bastard! You abandoned your family when---"

Ralnak punched her. No open-hand slaps, just a closed fist right into her stomach.

The air rushed out of her lungs. She couldn't tell if something broke but her entire body felt like nothing more than shattered bricks as the blow threw her back to the ground. Her hands pawed at the air but she couldn't stop before she landed on the ground with a crunch.

Something tore in her wrist. Sharp agony ripped through her senses as she let out a scream.

"Looks like we found our smuggler, boys!" Ralnak announced.

Gertrude looked back to see him stalking toward her knitting bag.

She turned back to the train carriage and her husband. Eustas stood with one hand reaching up. His gnarled fingers held one of the bricks tightly as the entire pile shuddered like leaves in a breeze.

With tears in his eyes, he held up his hand.

She couldn't move.

She had to.

Gasping, she forced herself to her knees.

"No, don't go anywhere, Mother," Ralnak said with a grin. He grabbed her bag and shoved his hand in. Then, he swore as he yanked his hand one, one of her knitting needles stuck in his palm. "Damn the gods!"

Gertrude tried to put weight on her left wrist. When it folded underneath her with a crunch, she let out a sob. Then she forced the rest of her weight on her right wrist as she crawled toward the train. The ground tore at her knees and ripped at her skirt.

Ralnak's boots slammed into the ground next to her. His hand grabbed her by her shoulder, the fingers digging into the aching joint. With more strength than she could resist, he yanked her to her feet and spun her around and away from the train.

Shaking violently, he shoved his face into hers. "Where is the scrip? I know you got some for that shit hole of yours!"

There was a ripple of noise. The sound of bricks scraping grew louder.

He held up his other hand, it had her knitting needle in it. There was a droplet of his blood still on it. "Now, either you give me the damn money you got for your quarry or I'm going to shove this needle in your eye!"

A shadow covered both of them and the air grew colder.

Gertrude felt a shimmer of hope through the pain and fear.

Ralnak drew back the needle. "Now, give me the---"

A ton of honey-yellow bricks landed on his head, smashing his skull and and crushing his body in a spray of blood. It slammed into the ground with brutal force. It narrowly avoided her toes as it drove a foot into the ground before stopping with a crack of stone.

Suffocating silence filled the air as the ground rumbled from the impact.

Seconds later, more bricks landed around her. They drove into the ground with rapid speed around her, tracing out two circles. More bricks landed on top of the first circle, forming two thick column-like feet.

With tears in her eyes from the pain, she lifted her foot and stepped onto a platform that had formed from the dropping bricks. Then another step as she crawled up into the shape being formed. More bricks landed around her, forming a pot-like belly perched on top of the columns.

Iron bars and pipes speared into the shape, narrowly avoiding cracking the bricks. Gauges and valves on the bars slid into place as if they had been designed for the shape being formed.

The bricks around her shuddered once and then snapped together, clinging to each other as if they had been glued together.

Panting, she leaned back as more iron and bricks formed a large, boiler-like shape in front of her. There was no wood or fuel, not even a fire rune inside it.

It didn't need one.

Gertrude smiled grimly and reached out for the boiler with both hands.

Her broken wrist screamed out in agony.

She sobbed and tried again, but it was too much. Her fingers shook as she struggled to do her part.

"Let me," Eustas said as he crawled in next to her. Sweat glistened on his brow. He knelt next to her, his body filling the space near the boiler. "I won't let a shit like him hurt my wife and live."

She let out a sob of relief.

He cradled her broken wrist and helped lifted her hands to the boiler. Broken bones scraped as they pushed until her palms were tight against the brick.

Tears ran down her cheeks.

His beard tickled her ear. "Wake up our Baby."

She closed her eyes and brought up the memories of her childhood, the suffocating heat when she helped fire the bricks, the comforting warmth of heated stones in her blankets at night, even the first night she had made love to Eustas on the cooling oven. Forty-seven years of marriage came rushing through her heart and all the pain faded away to leave her smiling.

Searing heat blasted her face, ignited by her happiness. Liquid fire, bright red as molten rock, poured into the boiler before spreading out into channels formed by Eustas's bricks. It filled the arms and pulsed with power as the cracks between the bricks glowed brightly.

Gauges snapped to life as the pressure rapidly gathered in the pipes. The boiler screamed as a final brick slammed into place, closing off the system except for the molten rock that poured out from spigots. It filled molds around her before rapidly cooling into smaller, dense blocks of stone.

Eustas kissed her cheek and crawled up into a cockpit-like shape in Baby's head. His feet smacked down on angled bricks as he braced himself. He groaned as he twisted a few valves and looked out through a grill. "Brace for attack."

She shoved her hand against the side of the chamber.

Baby shuddered violently as something struck it.

"Mother? Give me a line of reds to the right arm." Eustas stared out at the grill. His body shuddered as Baby lifted one leg with the scrape of brick and iron. The powerful shape tilted to the side before the foot slammed down again.

Gertrude focused her power on pipes and veins that lead to the right arm. Molten stone poured through the arm and into molds shaped by the other bricks. She could feel the heat in her mind as she kept it cool enough to avoid scorching the molds but hot enough to flow like water. As soon as the stone filled each one, she withdrew the heat to rapidly solidify each one into bricks.

Then Eustas's power took over. With the force of his will, he lifted each brick out of its mold and sent it down the empty chambers toward the ends of the arm.

She started to refill the molds for the next shot.

"Firing reds," he said. There was a shuddering bang as the first brick launched out. "I see some of these shits have shields. Start shaping blues for the left, Mother." He lurched to the right as he pulled on two levers.

She could picture as Baby's arm lifted up and then pointed down at the attacking men. Then the scrape of stone on stone as the next brick fired out of Baby's palm.

There was screaming.

Eustas fired again.

One of the screams ended abruptly.

More shots began to burst out of Baby's arm as fast as Gertrude could shape more bricks. Splitting her power, she began to shape heavier, more dense bricks for the other arm. All the pain went away as she lost herself in the complex dance of molten stone.

It wasn't going to end with this fight. They had to stop the baroness. She wasn't going to stop with stealing their homes and robbing the poor. Strol was going to never stop.

Gertrude knew Eustas would agree, they couldn't leave this fight. With a rapid fire blast of their brick cannon, the last of the Ridors went to war: Gertrude, Eustas, and Baby.
