---
title: Curse of the Blood Corn
date: 2019-10-30
summary: >
  Korl and his best friend, Pasir, were a sure win for the year's contest at the corn festival. However, just as they were pulling into the lead, they stumble upon a blood corn that could ruin everything.
---

> The red corn is fed on blood spilled in guilt and anger. --- *Tales of Horror From the Gepaul Countryside*

Korl took a step forward and lifted both of his hands. The tumble of corn stalks piled in front of him shuddered twice and then lifted into the air. With a twist of his fingers, husks and corn-silk were peeled off the cobs in a explosion of green and yellow. The smell of fresh earth and the dusty smell of ripe corn washed over him.

He drew his hands apart and the shucked corn separated from the chaff. He drew one hand back and the corn lined up to stream to his left. He didn't have to look back to know it would be streaming a few hundred feet to a wagon waiting behind them.

The silk flew in a separate stream to another waiting wagon while the stalks and husks were throwing to the side into a long pile being formed by the harvest.

He lowered his hand as a tremor of exhaustion rippled along his arms. They had been working for close to three hours without a break but they were almost to the end of the field.

Ahead of him, Pasir steadily drew his scythe through the stalks. The massive blade easily cut through near the base. The final sweep of the blade dragged the corn in a wide circle to deposit behind him as he drew back, took another step forward, and sliced through it again. Unlike Korl, Pasir didn't stop or hesitate. His talent was endurance, the ability to keep swinging the blade without his muscles burning or his back protesting. Not even his shirt was soaked with sweat.

Korl stepped forward and raised his hands, lifting and separating the corn to keep up.

Pasir grunted and sliced through another few feet of corn. "How are you doing back there?"

"Keeping up, keeping up."

Pasir turned slightly and grinned, his dark brown beard almost obscuring his lips. He nodded with his chin behind them. "What about the others?"

First team to the end of the village field got a cask of stout for the winter. Korl looked behind but there was only a narrow aisle between the corn stalks that went back almost a quarter mile. He could see the flash of blades in the distance.

Korl said, "No one is even close. We're going to win again this year."

Then his eyes focused on the woman walking toward them. She had on a bright blue dress and carried a basket in her arms. It was Rian, Pasir's wife. A memory rose up of Rian's legs wrapped around Korl's hips. He could almost smell her perfume in the air.

Blanching, he forced the smile from his lips and turned back. "Your wife is coming, take a break?"

Pasir hefted his scythe. "Keep going, Old Man. We cut until she shows up."

Korl answered by lifting the stalks ahead of him and began to separate them. His eyes glazed over as he used his mind and magic to send the harvest back to the wagon. It was a steady stream of white and yellow, the corn for the town's harvest would ensure everyone had enough to eat for the coming winter.

There was a flash of red.

He frowned and turned around. The stream of corn froze as did the silk, husks, and everything else. About twenty feet away, he spotted a blood-red cob hovering in the air. With one hand, he beckoned for it and it separated from the hovering line and sailed back.

Korl relaxed and waiting for the cob to land in his hand. He half expected to see rust or a diseased cob, but it looked perfectly normal except for being crimson. He flipped it over and then again.

"What's that?" asked Pasir.

"This looks strange."

Pasir finally stopped swinging his scythe. He flipped it over to the blade hung over his head and then slammed the haft down to bury it into the ground. It swayed for a moment and then steadied. He wiped his perpetually sunburned hands on his overalls before walking over. "Let me see that."

Korl handed it over just as Rian came up.

"Hello, boys. Need a break?"

The smell of freshly baked bread brought a rumble to Korl's stomach. His eyes rose to meet her gaze as he held out his hand.

Rian smiled sweetly at him, then glanced over at Pasir.

He was still looking at the red cob.

Her eyes came back and her grin turned more seductive. She parted her lips and stuck out her tongue for a moment.

His manhood twitched. He knew what to do with that mouth.

"It's a blood cob," Pasir said.

Korl snapped his head back and noticed Rian did the same. A faint blush burned his cheeks but he knew it would be hidden by his farmer's tan.

Pasir hefted it for a moment and then shook his head sadly. He tossed it to the side. "You know the story, only the guilty find them." His voice was low, a rumble of disinterested knowledge.

Korl's face whitened. He glanced at Rian who looked back in fear. Then he realized he was being obvious and snapped his head back. "Do you believe it?"

Pasir shrugged. He held out his hand for the basket. "Doesn't matter, does it? I didn't find it. Hope you aren't guilty of anything, I heard that no one survives the day they find it." He gave Korl a broad smile. "You still feeling bad about stealing Farmer Jon's apples?"

Korl tried not to think about his hands on the insides of Rian's thighs. "That was thirty-five years ago and you were the one who hopped the fence."

Pasir hesitated for a moment and then shrugged. "Nope, don't feel guilty at all. Those were damn good apples."

"Yeah, they were," Rian said quietly. She set down the basket. "I-I... need to tell the others that you've filled the wagon."

Korl flinched as she hiked up her dress from her ankles and hurried away. He turned slightly to watch her wide hips and wonderful ass for a few yards before he remembered the blood corn and turned back.

"Wonder what that is about?" asked Pasir. He knelt next to the basket and fetched a large skin filled with stout, two loaves of bread wrapped in a towel, and a hunk of Farmer Tail's sharp cheese. He broke the cheese in half and handed it over to Korl.

Streaks of mud had smeared along the cheese but Korl took it anyway. He wiped it off on a clean spot of his own shirt before taking a bite.

"Think we could hurry up? I want to win this contest and get going on the second. I'm betting we can get three lines done before the others catch up."

The cheese choked Korl and he gulped hard to ease it down his throat. "Good idea, just let me get another two bites and I'm ready to go."

Pasir stood up. He gathered his scythe and began to cut through the stalks.

Swallowing as fast as he could, Korl hurried up and finished eating. He hefted the blood corn for a moment and then sent it to the side, tossing it in the pile with the rest of the stalks and husks. Trying to not think about it, he focused on lifting, separating, and sending the various parts to their respective piles.

With the fall sun beating down on them, they worked in silence. The steady rhythm of step, separate, step, let the minutes pass into a haze. It was almost relaxing except for a faint burn in his shoulders and a twitch in his fingers. Using magic to lift corn wasn't as exhausting as doing it himself but it still took energy to send the harvest corn down to the wagons.

Then he realized Pasir's strokes were coming fast and harder. The scythe blade had steadily accelerated, swinging in a wide swoop that ended with a rattle of some piece. The sharp end sparkled in the light as Pasir drew back and slammed it through another few feet of stalks and dirt.

Korl thought about the blood corn. He was guilty of something, Rian. They had been fucking each other on the sly for about three months. It was mostly stolen moments in the barn or while Pasir was running a load into the village, but she even occasionally stopped by his house on her errands for a roll on the bed, some conversation, and then to hurry out the back door.

Pasir's strokes continued to grow faster. They sounded harder, more violent.

Korl gulped and concentrated on his duties but his eyes kept focusing on the blade as it swung back toward him before stopping with a thud. He skipped a step to keep some space between them and continued on. He just needed to get through the day, maybe not do anything with Rian for a week or so until things calmed down.

Step, slash, thud. Pasir moved with relentless force. He drove into the stalks and slashed through them. They cascaded to the ground behind him and Korl found himself struggling to keep up.

The scythe suddenly stopped at the end of the stroke, the force of the movement stopping with almost a shudder of the air.

Korl froze, his heart beating faster as sweat prickled down his neck.

"It's Rian, isn't it?"

Korl gulped.

Pasir turned, his brown eyes almost burning. "That's what you're guilty about, isn't it? You're sleeping with my wife."

To be honest, Korl and Rian had never slept together. They were usually too busy talking or fucking to do anything as mundane as sleep.

The tip of the scythe drew back but Pasir was still facing Korl.

Sweat burned Korl's eyes as he reached out for the stalks at his feet. They quivered as he felt them. He didn't know what Pasir was going to do but there wasn't much that could stop the scythe blade.

"How could you do this? We've been friends since we were t-three." Pasir's voice cracked with his emotions.

Korl couldn't lie, not to Pasir. They were friends. He felt tears burning his eyes as he held up his hands. "It wasn't what I meant to do, it just---"

The scythe whistled as Pasir swung it with a grunt.

Panicked, Korl threw up the stalks at his feet. The ones to block the blade were easily sliced through but more of them caught the blunt haft of the instrument and slowed it down. Despite preparing for it, he wasn't expecting the sheer strength and power as the curved blade came around toward his side.

The tip sliced into his upper arm before the corn stopped the strike.

A gout of blood sprayed to the ground, painting some forgotten husks red with crimson.

Agony exploded from the wound as the blade twisted and ripped open muscle.

Pasir snarled, his knuckles white around his scythe. "You can't have my wife!"

Korl used his right hand to reach out. His magic caught on the corn and stalks still lined up next to their lane. Before Pasir could try to pull back, Korl yanked his palm forward and the cobs ripped out of their husks. Fueled by his magic, they whistled as they slammed into Pasir, each one shoving him back a few inches.

More corn ripped out of the stalks, peppering him with a stream of heavy weight.

The scythe blade ripped out of his arm. He felt the shock as more blood sprayed across the ground.

Pasir staggered. His face was bruised and bloody from the telekinetically thrown corn. He got a tighter grip on his scythe. "You bastard!"

Korl ignored the searing hot liquid pouring down his left side to rip more of the corn from the field and throw it into a heavy stream toward his chest. He wanted to say something but using magic was too much for him. He couldn't afford to let his impromptu defenses down.

Pasir staggered back, the impacts of the corn shaking his body violently as he stumbled. Then, he suddenly tensed and slammed his feet down.

Corn cobs bounced off his face and chest, the sharp edges tearing at his skin and ripping at his beard. Crimson dripped down his face as he pulled back his lip with a snarl. "You can't steal her from me!"

He launched himself off the ground, the scythe flashing in the air.

Spotting an opportunity, Korl yanked his hand up. The corn stalks on the ground shot up like spears, slamming into Pasir and tossing him higher into the air. He gasped and forced out the words. "Steal her!? I don't want her!"

Pasir hit the ground hard. The scythe rolled from his hand, thudding loudly on the blood-flecked dirt. His mouth opened up but no sound or air came out of it. He closed it with a snap and flailed at the ground.

Korl ripped more corn from the stalks around him. They swirled in the air above him before he brought his fist up. Then, the cobs formed into a massive hammer, one that would crush the life out of Pasir. He staggered up and looked into the other man's eyes.

When he saw tears, he stopped.

"Do it!" snarled Pasir. "Do it or I will kill you."

Korl couldn't. Pasir was his friend. Too many decades had bound them together. His fist shook as he stared down. It would be so easy to end it, to crush Pasir and end the threat.

He was being honest that he didn't want Rian. Not as a wife. She was fun, she had so much energy, but it wasn't forever for him.

"End it." Blood flecked Pasir's lips and beard.

Korl shook his head as he struggled with the words. "D-D... Do you know what she talks about?"

"How your shaft feels---"

"You!" bellowed Korl. "She talks about you!"

Pasir opened his mouth to say something but Korl spoke quickly. "She talks about how she misses you! She talks about how there is no more passion in your life. How much she wants to just... talk to you."

Korl gasped as his voice grew softer. "Rian misses the old you."

Pasir's lips tightened but his body didn't lose any of its tension.

Korl stepped back and released the corn. It rained down around them but he made sure not a single cob landed on his friend. "That's how all this started. You lost your love."

"And you want her."

"No, I mean... she's a great...." No, that was not the thing to say. Korl gulped and remembered how she looked a few days ago as they spoke under the millio tree. "When was the last time you just sat down and talked to her?"

Pasir pushed himself into a sitting position. "Every night! Every night I tell her I love her. Twice even!"

Pain radiated from Korl's arm. He clutched and his palm smacked against the hot blood. He felt the urge to run away and lash out at the same time, it felt like a rope under tension. "No, not just the words. Show it."

Pasir ground his teeth together.

"She still loves walking in the fog, you remember that?"

"Yeah, of course. That's how we met."

"When did you do that?" Korl staggered, he was having trouble remaining standing.

Pasir's face paled and he closed his mouth.

Korl gasped and looked around. He couldn't seem to stop the blood no matter how much he pressed down. "What about making love?"

"Two nights ago."

"Ring her bell?"

"Of course," Pasir said as he folded his arms over his chest. His teeth ground against each other.

Korl sighed and sank to his knees. "Did you take more than sixty seconds?"

Pasir looked guilty.

"Then you didn't ring that bell. I know because she came to visit me after you rolled over and fell asleep. Three times that night and I made sure she rang that bell every time. She takes a while, you know that. It takes time to do it properly. When did you make a point of making sure she came first?"

He already know why Pasir looked even guiltier.

"That's what she talks about. Not just the sex, not just the talking. She missed the passion when you two were oblivious twenty-somethings getting in trouble because you were kissing in the village square. I remember distracting the mayor while you two sneaked out of town." Korl gave a weak smile.

Pasir shoved himself to his feet. Blood dripped from his hand as he limped closer.

Korl tensed, waiting for his friend to pick up the scythe.

Pasir yanked open the basket and pulled out the cloth wrapped around the bread. He folded it over and came over to wrap it around Korl's arm.

Korl panted, his world was still swaying. He looked up with dizzy vision. "I'm sorry for Rian, I really am. I just... she needed someone and I hate to see her cry, you know that. I was wrong there."

Thick fingers tightened the bandage. It was already soaked with blood.

With a sigh, Pasir shook his head. "I'll apologize tomorrow."

"No, don't do that. She never believes you. Just change. Show her love, damn it. Make her a meal or take her on a walk. She's been talking about checking out the stalls in the village. Just... stroll around and let her dream about the things she could buy without telling her you can't afford it. Or, just ring that bell of hers, loud and clear."

"Easier said than done. I may last for hours." Pasir gestured to his crotch. "But he doesn't."

He sighed and finished tying the bandage around Korl's arm.

"Then learn to adapt. Maybe take your time to taste that apple."

Pasir pulled a face.

Korl chuckled dryly and shook his head. A wave of dizziness took him and he almost fell over. "If you can enjoy a stout, you can learn to enjoy some cider. You know that. Just get on your knees and make her feel like the village queen. If you are lucky, she'll do the same."

He wanted to say something more but his tongue felt sticky.

Pasir shook his head. "I'm sorry."

Korl tried to smile but he felt weak. He slumped to the side, his cheek smacking against Pasir's arm. He looked down to see blood pooling on the dirt. It was already soaking into the ground. "Yeah, I guess that red corn was cursed, wasn't it?"

Pasir slipped his arm underneath Korl and then stood up, picking him off the ground. He swung Korl over his shoulder.

Korl threw up, splattering cheese and bread against Pasir's side.

"Come on." Pasir grunted as he stomped toward the wagons. His feet crushed the cobs that had dropped from the air when their fight had started. "We both need more time and I'm not going to let my friend die before that happens."

Korl smiled as much as he could as he drifted toward darkness.
