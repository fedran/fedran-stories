---
title: Brilliant Luck
date: 2020-04-01
summary: >
  Jacom's daughter had been missing for two days. He had been searching for her with no luck. But then a stroke of luck came when his son returned home.
---

> The forces of magic are impossible to understand, they guide mages to actions that make no sense to those untouched by arcane powers. --- Kildoril Masonar

Jacom limped and leaned against his walking stick as he staggered up the walk toward his house. It was a small, one room cabin. He had hope there were lights and cheering inside but the windows were pitch black. No one was home, no neighbors waiting with news.

"Damn," he whispered and wiped at the sudden tears in his eyes. His wrist throbbed from where he fell down in the ravine. The bandages over the deep cuts from the sharp rocks were soaked with blood. He clutched his stick and continued closer to the dark house.

"Just go in. Just go in and get some food."

What he wanted to do was crawl into his bed. He had been searching the woods for close to a day and exhaustion burned in the back of his eyes and in all of his joints.

He managed to get up the two steps to the front door, but as he opened it, a wave of dizziness slammed into him. With a low groan, he slumped against the side of the frame and slid down. Knuckles smacked against the wooden planks and the entire house shuddered from a flicker of magic that radiated from his touch.

"Damn the gods," he whispered in a hoarse voice. He reached out in to the dark room. Wood twisted and creaked. One of the wooden planks on the floor buckled underneath his knee and lifted him up. He gripped the frame as he pushed himself to his feet.

"Just get food," he muttered and shuffled into the room. His ability to warp and manipulate wood had many uses, but finding light in a dark room was not one of them.

He patted the table. "Where is that lamp?"

A bright yellow glow blossomed through the front door. It shone across the room with the brilliance of sunlight. Dark shadows of trees painted themselves across the back walls.

Jacom's hope rose. He turned and shielded his eyes with his hands. When he saw the point of light moving, he smiled. "Gar."

The light came from a young man as he walked up the path. His entire body was radiating the golden light, it seemed to come from everywhere and nowhere at the same time.

Jacom groaned as he considered returning to the door. His entire body hurt and he wasn't sure he was able to move.

Instead, he waited as his son came strolling up.

Gardim was in his early twenties. He had grown a short beard since Jacom had last seen him. Skipping up the stairs, he stepped into the house and then stopped. "Why are you still up, Dad? It's past midnight."

"Oh, Gar. Thank the couple." Jacom limped over and held out his hands. "Thank the couple!"

The smile dropped from his son's face. "What's wrong. Where is Glin?" The light flashed around him.

Jacom stopped and lowered his hands. "I-I don't know. She was two days ago, but then she got really sick and started getting feverish. I went to get Mal for a fever down but when I got home, she was gone. The only thing I saw was her blankets by the door and her sheets on the front lawn."

The light flashed around his son. "Anyone helping you?"

"Almost the entire village."

"Even Old Man Saber?" Saber and Jacom had many fights over the years, mostly over Glin's mother.

Jacom nodded.

Gardim dropped his backpack on the table. "You look like shit."

"I've been looking since yesterday morning. Just let me get some food."

"And then you're going to lie down."

"No!" snapped Jacom. "That's my daughter out there and I will not abandon her."

"Look at you, Dad. You are limping, covered in scratches, and your eyes are bloodshot. You are in no condition to be wandering the woods."

"That's my daughter!"

"She's my sister!" snapped Gardim. The light grew brighter until it was blinding. He grabbed Jacom by the shoulders and pushed him back toward his bed. "Now sit down."

Jacom sat down heavily on his bed. He blinked at the sudden fury from his son. Gardim had never spoke out like that before, it was something Jacom didn't think he would ever do.

Gardim stepped over to his bag and pulled out two, heavily carved rods of bone.

Surprised and confused, Jacom could only stare.

"I've learned a lot at school, Dad. I'll make you proud." He smiled grimly before he held out the rod in front of him.

Jacom opened his mouth but then closed it when multicolored light blossomed from the rod. It shone through the carvings, painting the walls, floor, and ceiling with strange symbols. Motes of energy rose up from the room and swirled around him.

Gardim frowned in concentration. He raised the rod for a moment and then released it.

Instead of falling, the rod hovered in the air. Gardim waved the hands in front of him and the motes of light swirled after like leaving in a still lake. He gestured again, then took a half step back before pointing toward the door. "Go."

The bright points of light suddenly shot out of the door as they blossomed into brilliance. Like the light when Gardim approached, each one lit up the forest as bright as day.

Jacom stared in shock. His son had never been able to do that before.

Gardim grabbed a chair and dragged it toward the bed. "Hunter sprites. They'll spiral through the forest and look for anyone wandering around. If she's close, they'll find her."

He sighed. "No reason to search the woods ourselves until they get back. You can rest."

"What if they find her?"

Gardim gestured to Jacom's pillow. "I'll wake you, I promise. You need to sleep now. You're exhausted."

"You've changed."

"It's only been a year, Dad." Gardim dug into his bag and pulled out a notebook. He propped up his arm with his leg and wrote down something in it.

For a moment, Jacom was reminded of Gardim as a young boy writing in his journal. He wrote down everything those days and there was something comforting in seeing an old habit.

He sighed, feeling old. "A year is a long time."

Gardim looked up and smiled, two images overlaying in Jacom's exhausted vision: one of his little boy, and one of the man who sat in front of him.

Jacom shook his head. Gardim was right. He didn't bother cleaning up or even grabbing something to eat. He rested his dirty head on his pillow. "Promise me."

Looking up from his journal, Gardim smiled grimly. "I promise, when you wake up, Glin will be back home."

Jacom's eyes fluttered. He didn't want to go to sleep. He forced his eyes open. "Why did you come home?"

"Been thinking about it for a week. Just... missed the old place and I thought you'd like company. Mom's anniversary is tomorrow and I know you like to have us here when you visit her grave. I thought I'd surprise you."

Jacom smiled. "Lucky for me you showed up."

"I assume Saber and Glin's mom are out there?"

"Yes---"

Gardim held up a hand. "Ah, I just found them."

Jacom closed his eyes for a moment. "Are they okay?"

Looking up, Gardim stared at the ceiling for a moment and then nodded. "He's holding her hand. It looks like she was crying."

Jacom sighed. "Saber is a good man."

"He's an asshole."

"Yes, but a good asshole."

Exhaustion caught Jacom. He closed his eyes for a second. When he woke up, it was morning. The light was streaming through the windows, bright and natural.

Voices drifted through the door as heavy footsteps came up the stairs.

Jacom managed to sit up when the door opened and Gardim came staggering in while carrying Glin in his arms. He headed straight for her bed.

"Glin!" gasped Jacom. "What happened? Is she okay?"

"Fairies caught her," came the strained reply. Gardim set his sister down on the bed.

Saber, an old man with a ragged beard, stood in the frame. Behind him, Jacom could see Glin's mother, Hilan, peering over his shoulder. They were never married, but Jacom was proud to raise Glin as his daughter.

Gardim straightened. He looked exhausted. Turning on his heels, he strode over to grab his rod and journal. Shoving both in his back, he hiked it over his shoulder.

"W-Where are you going?"

When Garim looked back, there was a look of fury on his face. "I have some fairies to burn."

As his son left, Jacom realized how lucky he was to have a good son like Garim. He was also startled about how much his boy had grown in only a year.
