---
title: Offers of Blood and Candy
date: 2019-10-10
contentWarning: "Some themes that appear in this story: depression, graphic death, and suicidal idealization."
summary: >
  Gris was hungry. The creature only came out a few times over the year, but when it did, it hunted the nearby villages for fresh blood and breath.
---

> Death hasn't passed until the last of the snow melts from the ground and the survivors breathe warm air. --- Bil Kraus, *The Origin of Blood and Candy*

The snow was just beginning to melt off the piles of cow shit and Gris was ready to feed. The creature was a foot tall ball of white spikes with a pair of large eyes and six smaller ones. It walked on spindly legs, each of the eight, yard-long segmented limb came down to delicately balance on the fading chips of ice and still-frozen dirt.

Dinner was just ahead, a small village that Gris had called home for at least fifteen years. With the moon only a sliver of light above it, the creature skipped past the first three hours and headed for the fourth. Last year, when spring had just started, someone in the first house had tried to poison it. The creature wasn't in the mood for puking out the only meal it would have for a year.

The sweet smell of blood brought a tinny rumble from its belly. Creeping forward, Gris made its way up to the front door where the corpse of some small mammal had been smeared across the wood. The sickly sweet smell of the fresh blood brought another tinny growl and Gris pressed its mouth up to the still warm door and lapped at it.

It only took a moment to break its fast and sate the edges of its hunger. Then Gris stepped away, wiped some of the droplets of blood from its spikes, and then headed to the next house. It's stomach rumbled happily as Gris digested his small meal.

The next home, a newer one wit a freshly milled door presented no delicious treats across the wood. Gris sniffed at it and then licked at it. When it tasted only bitter stains, it pulled back with a frown.

Gris's stomach rumbled, the tinny sound almost like little bells. It was already hungry, blood never stated it for long.

The creature licked the door again, hoping to find something sweet to drink. There was nothing but the sound of someone snoring inside suggested there was food inside. The hunger rose up, it needed to feed now.

Gris tested the door. Locked. However, the window next to the door wasn't as secured and it could easily push up the pane of glass to slip into the inch-wide gap. When its long legs touched the ground, it pulled itself into the house and let the window close with a thump.

Somewhere in the house, the snoring stopping suddenly.

It didn't matter, Gris could smell the sweetness. It walked across the wooden floor, the tips of its leg making tiny tapping noises as it headed for the smells of the only living being inside.

A whimper rose up from a shivering blanket.

Gris's stomach rumbled again.

The whimper turned into a gasp. Whoever was underneath the blanket slapped a hand over their mouth.

The creature's hunger pushed it forward. It crawled up on the bed, its weight pushing down the edges of the blanket as it positioned itself over the shivering lump.

Gris waited. It looked at the blanket, trying to find the source of the sweet smell of blood and fear but it couldn't see any way to get inside the amorphous shape.

The whimpers and heavy breathing grew louder.

The creature waited, fear always revealed itself if it waited long enough. It just had to freeze until the sound of the first scream. Then there would be something to drink.

Sweaty fingers reached for the edge of the blanket.

Even as hunger peaked inside, Gris waited patiently as the occupant pulled back and looked up. The human's eyes grew wide as the scent of sweat and fear poured out of it's body. "By the Couple... no---!"

Gris jumping and grabbed the human with all of its legs.

"---help me!"

The human's screams ended when Gris plastered its mouth over the humans and sucked at the sweet air. Soon, the taste of blood flooded its stomach and Gris drank with all its might until its victim's struggles grew weak. It wasn't until the human's arms fell back and no more sweetness flooded into Gris's mouth that the creature release and step back.

It was sated again, but it would never last.

Content, Gris squeezed out of the window and headed to the next house. It had blood on the door but it wasn't quite hungry enough and walked past the meal.

The hunger returned soon enough and Gris headed toward the next house. There was a sweet smell surrounding the door but it wasn't blood. Curious, Gris crawled over, lowering its spiky body near the ground as it came up to a small bowl next to a burned-down candle. Inside were little lumps of... something.

Gris reached it and grabbed one, bringing it to its mouth.

It was delicious.

Gris tasted the sticky offering.

Whatever the food was, it wasn't blood but it was tastier than anything it had tasted before. Gris's spikes fluttered as it danced for a moment. Then it dove into the bowl and scored three more pieces to shove into its mouth. The sticky substance clung to the roof of its mouth and filled its stomach.

More full than it had ever been, Gris started to continue its journey but then went back for another piece. Cradling it one leg, it limped through town as it passed blood-streaked door after blood-streaked door. Despite the wonderful smells of fresh blood, its hunger didn't rise up.

Gris finished walking through the village and was about to head back with the tinny bell of its hunger finally woke again. It ate the piece it had been carrying and then headed back for more.

This time, it made it only halfway through the village before the pangs were too much. Turning, it walked to the front door and tasted it for blood. There was old stuff, from the year before, but nothing recent.

The hunger grew too much to move to the next house. Gris tested the door and the windows but they were locked tight. However there was a gap a few inches tall underneath the front door. Squeezing down, Gris shoved one leg into the gap and then a second, pulling itself until its body squished almost flat and the spikes left gouges in the wood until it could pull itself free.

The human was sitting in front of the fire. It was a male, Gris thought, and appeared to be awake. The human also smelled of blood and sweet air.

Straightening, Gris tapped across the floor.

The human didn't seem frighten. He reached over to pick up a cup of something. He sloshed it around and then set it back down with a sigh.

Gris crawled up on the chair behind the human. The fabric indented from its weight but the human didn't even look up.

Instead of screaming in fear, the human only spread out his hands and looked at Gris. "Go ahead."

Gris cocked its head. It braced four legs on the couch and swung again to land in the human's lap and on its hands. Looking up, it tensed as it waited for the exhalation that always began the scream. It knew there was fear in there, there always was.

Red-rimmed eyes stared back at Gris. They were empty and hollow, as if someone had sucked out all of the life out of them. Only a pulse quivered through the slack limbs.

Gris blinked one of its large eye and then slowly worked through the others. Where is the screaming? The fear? The blood?

The human sighed. "Just get over with."

Even though the smell of blood was close, Gris's hunger gave way to a sudden sense of curiosity. It looked around at the room. There were human things scattered around but none of them made any sense to the creature. It was sure none of them were weapons. It turned back and looked at the human. When no screams came, it reached up and poked the human in the face with one leg.

The human let out a strange sobbing noise. Gris had heard them before, but never a single one that felt like the world bore down on the nearly mindless creature.

Gris blinked again and gave another tentative pokes.

"Just kill me, that's what you do, it's it? Steal my blood? Steal my breath? Just... I'm done. I'm done living."

Confused, Gris pulled back and crawled on the floor. There was something not right about this human. Maybe the human needed something sweet?

It turned and headed toward the door, squeezing through the opening and out into the cold air of the earliest spring. It knew where it was going, straight for the bucket with the new sweet offerings.

It took a while to shove the contents of the bucket underneath the door. Then Gris followed to carry the pieces to the ground in front of the human.

Red-rimmed, dead eyes stared at Gris as it piled up the offerings. There was confusion almost visible in the darkness, just a hint of something that Gris recognized but still no fear to drink.

Gris reached down and grabbed three pieces of the offerings. Climbing up on the human's lap, it carefully put one piece down on the still outstretched palm.

"Candy? What is this for?"

Candy. That seemed like a nice name for the offering. It wasn't blood but it tasted sweeter. Gris sat down on the human's other hand and shoved a piece of candy into its mouth. At least it wouldn't be hungry while it waited for the human to get better again.
