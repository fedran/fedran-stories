---
status: alpha
availability: public
author: D. Moonfire
title: The Cros Gambit
format: Story
character: Cros #de Goslin
summary: >
  Anyone who follows crashball knows that the The Cros Gambit was the epic play that not only won a tournament but also changed how the game would forever be played. The Gambit was the fatal play of Cros de Goslin, he gave his life to win the scoring ball.
genres: [Action, Sports, Tragedy]
---

> There is no greater thrill than seeing the brutal violence on the field of a crashball game. --- Jacim do Kasin, *The Legendary Game*

Cros switched the rubber ball to his left hand as he ducked under the guard's fist. Tucking his head in, he threw himself against the ground and rolled out of range before the guard could spin around and attack again. The slick grass cut at his face, one of the only few exposed parts of his body, before he was once again on his feet.

He felt a rush of air against the back of his neck of a near strike. Cringing, he charged forward and hoped his momentum would keep him safe. His ribs already ached from the last time he tried that, but the skirmisher that caught him had already been sidelined and was out of the game.

A deafening roar of a thousand fans followed him. It echoed against the stands and shook the ground. The fast he ran, the louder it would get until his ears ached from the pain.

Cros ran faster, heart pounding as he prepared himself for the final opponent standing between him and the castle goal: Pennir Stam.

Unlike most players who wore heavy armor, Pennir chose to wear only padded cloth armor over her slender body. She also refused to wear a helmet, which only drove the fans wild whenever someone got close.

If someone did manage to tackle her, hundreds of pounds of muscle and leather would have snapped every bone in her body. But, in the five years since she started playing for the Infinite Razors, only one man had ever laid a hand on her. In retaliation, she had broken every bone in his body and sent him to an early grave.

Half a dozen spheres of mud and rock spun around her body. Each one was the size of a large fist and sailed smoothly in response to every movement she made. The last time he got close to the goal, she had hit him with two of her spheres. The mud had turned to solid rock at the point of impact with bone-crushing force that dented his armor and tossed him across the field. The two skirmishers behind him were not as lucky. Both were sidelined and the Razors got a ten point bonus for the incapacitation.

This time, he didn't have anyone behind him to take the blows and there was no time to dodge. He had to get his ball into the goal behind Pennir, but it would be almost impossible with her spheres blocking the rod-wide opening between the two poles.

Cros steeled himself for Pennir's attack and bore down, sprinting at the remaining two chains before he reached the sentinel. His boots tore up grass and dirt, the spikes along his soles tugging at his exhausted muscles. As he did, he dug into his flagging strength and gathered up the shreds of his energy. Lightning crackled along his armor, tracing along the metal ridges and jumping from the rivets. It scorched the air around him, suffocating and burning.

Pennir unfolded her arms and smiled. She casually held out her hands, formed her hands into fists, and pulled up. The ground buckled underneath her, the grass tearing open as the earth boiled up like black magma. Before he could take another step, the liquid burst out of the ground and formed another sphere of spinning mud and pebbles.

He drew on his own power, gathering the lighting in his hand. The energy crackled around his knuckles as a brilliant ball formed between his fingers.

The roar of the crowd grew louder until his ears rang out from the sound.

Cros spotted an opening to Pennir's left, a gap between the orbit of her spheres and the arm of a second guard. It was a narrow opening that would get him a castle goal. If he missed, it would either sail into the tower goal next to the castle or bounce off the wooden pole that separated the two.

Thankfully, the other guard was occupied by one of Cros' players, Tamir. Tamir was the best skirmisher Cros knew. He was using one of his water whips to harry the guard, striking fast and hard with glittering magic at the same time he threw punches with left hand.

Tamir's opponent was blocking and trying to land punches himself, but his gauntlets kept bouncing off the ribbons of water that swirled around Tamir.

Blood dripped from both of their gauntlets, but Cros couldn't identify the source.

His skin prickled just as a flash of movement rushed toward him. He jerked back as one of Pennir's sphere screamed past him. The sphere shattered into shards that bounced off his armor. He swore as he realized he had let his attention drift. When he focused on Pennir, he saw that he was only a rod away from her; less than a second of sprinting.

Cros swung his hand, the length of his arm crackling with lighting. He caught the second sphere with the back of his hand. Before it could crack his bone, he let the electrical power explode from his skin and shattered the rock in a burst of brilliance.

He swung his other hand out, spreading his fingers and threw a blast of lighting. Blinding white crackled from his hand, catching the next two spheres in mid-flight and pulverizing them.

A fifth sphere shot out of the cloud of dust and mud, blurring on the edge of his vision as it aimed straight for his face.

With a swear, Cros realized that she wouldn't give him enough time to aim for the narrow opening for the tower. He spun and threw the rubber ball with all his might toward the left tower goal. As he came around, he braced his hands before him and caught the stone sphere inches away from his chest.

The impact blasted him back, plucking him from the ground as the pressure crushed his chest and shoulders. The force of the sphere carried him almost a chain down the field before he hit the ground. Hard-packed ground tore at his back, ripping open his leather armor as the force carried him a few more rods before he slumped into the ground.

He gasped for breath, the world spinning around him and sparks sliding across his vision. His right arm shook with the effort to hold the crumbling sphere in his hand.

At first, he couldn't hear anything over the rushing in his ears but then he realized it was the roar of the crowd. He blinked and let it wash over him, the words slowly sharpening past the ringing.

"Hammer! Hammer! Hammer!"

One of the announcers, a perky woman that Cros used to date, screamed out almost incomprehensibly with her magically-enhanced voice. It was terrible during their knock-down screaming fits but perfect for yelling over a thousand fans cheering a goal.

He caught only a few words but "tower goal" was all that mattered. He smiled even as he groaned. His ten points would put them only sixty behind the Razors. It would still take a miracle to win the game, but he already ensured that they would be celebrating his name for weeks to come.

One of the Hammer's guards knelt down next to Cros. "Almost made it back to our goal, Cros. Next time, catch two and tower yourself."

Cros groaned and reached up with his right arm, wincing as his shoulder popped and a bone in his upper arm ground together. Pulling back, he switched to his left hand and caught an offered hand. "Did it look good as I hope, Bankol?"

The guard chuckled and pulled him up. "Yeah, got me hard watching you backhand that cow's balls."

Cros leaned against Bankol and inspected his right hand. The leather had scorched off his hand, exposing blackened skin underneath. He tried to relax his hand, but his fingers refused to move. A sharp pain ripped up his shoulder and he swayed for a moment.

He looked up to see Tamir coming toward him. The narrow visor gave only a hint of the concerned look on his face. "Cros, can you stay in?"

Cros glanced at the sidelines. Most of the Hammers were off-field, probably in the infirmary. The league's healers would have their hands full repairing shattered bones, cracked ribs, and ruptured ears. He reached up and pressed a hand against his own ear. When he pulled back, there was blood.

Glancing down the field, he focused on the massive hourglass that marked the time left in the game. He guessed there was only a minute left, enough for one more play. The score over the glass reminded him that all the time in the game couldn't reverse the score: Hammers 93, Razors 159.

He turned away. "We can't win, can we?"

Bankol shook his head. "Can't slap that cow far enough to get to the castle. Even if we did, that's only fifty points. We'd have to either get a submission---"

Tamir held up his hand, his gauntlet had been ripped open to reveal a long wound bleeding profusely. Streamers of water dripped from his skin, formed by his talent. It mixed with the blood before splashing from his body in a red-tinged puddle. "Four submissions. We need seventy points if we're going to beat them."

Bankol glared at Tamir. "Thanks, Mom. That hand good enough to jerk me off too?"

Tamir twisted his hand and flicked his thumb at Bankol.

Cros started to smile and then winced as his shoulder twinged. He rested his palm against it and felt the heat soaking through the armor. It could be bleeding, but he would have to strip off his armor to find out.

When he looked up, both Tamir and Bankol were looking at him. "What?"

"You're the captain," said Tamir.

"Want to bend over and let Pennir shove one of those rocks up our asses?" asked Bankol with a snort. He rubbed a bruise on the side of his face. His large body looked like a wall of stone, not unlike his power to solidify his skin. He crunched his fist into his palm. Dust and blood drizzled out from the impact.

Cros shook his head. "Not for this game. We need the win if we're going to go after the Spears next."

Bankol growled. "Damn the Couple, I want to snap that Spears' leg and shove it through his skull for what he did to Jeber."

Tamir shook his head ruefully, a pained smile on his face. He glanced back at the rest of the team and Cross followed his gaze.

The other players were all injured and broken, beaten to their limits and exhausted. The two hour game was almost over and Cros could tell they had already lost. They were just waiting out the last minutes before they could flee back to the team's healers. Two of them were inching toward the sidelines, as if hoping to just give up now.

Cros sighed and gestured for Tamir and Bankol to follow. They headed back to the others. "All right, Hammers, we have a minute to make a decision."

He watched their faces as he spoke. "We can give up right now and head home."

Guilt washed over their faces, barely visible through the mud, blood, and exhaustion.

"Yeah, I thought so." He wanted to give up just as much as them. He could picture his wife sprawled out on her bed, nothing but a warm blanket and an ice-cold lager between her legs.

Cros looked back at the clock and score. He sighed and turned back. "Damn the Couple. I'd rather go home a winner. My wife could use another trophy."

Bankol chuckled. "I'd rather go back to your wife with a trophy, but how do you plan on getting four submissions and a castle out of this?"

Cros chuckled and shook his head. "Not a submission, a sacrifice. That's five points. If I can take out three others---"

Tamir held up his hand. "And a castle goal, don't forget that."

Cros glared at him. "---and a castle goal, then we can win this thing. If I can take out more of them, then we could probably get away with a tower."

"Do you actually have that much power in you?" asked Tamir. "Your range isn't that impressive, maybe a rod at most. And you've been shocking everyone pretty hard for the last hour."

Cross looked across the field. At the far end, the Razors were having a huddle themselves. All of them stood with heads bowed except for Pennir who remained in front of the castle goal. They didn't need to include her in any strategy, she knew her role on the field: stop anyone from reaching the goal.

He sighed. "I don't know, it's risky."

Tamir chuckled and held up his hand, water pooling. "Would this help? Remember when we had that brawl in the rain a few years ago? You shocked everyone around you."

Sweat dripped down Cros' face, but he couldn't wipe it away without pulling off his helmet. He glanced at Pennir who watched the field with a bored expression. "Do you think you can reach her? They are going to protecting her with everything they have."

"No, I don't have the range either." Tamir's shoulders slumped. "Damn it."

Bankol patted Cros on the shoulder as he stood up. "I'll get you in range, I promise."

The large man's body grew hard and rigid, the skin turning as solid as rock. His voice cracked as he straightened up, increasing a few inches in height.

Cros looked at his friend and nodded. "This is going to hurt."

"Well," Tamir sighed, "yes."

"Okay," Cros turned back to the others. "Okay, here's the play. Ground feint. All of you hold back and we'll plow a path to the castle. If there is anyone standing, just hammer them hard and fast."

Cros shook and he pretended it was the rush of battle and not fear. Gulping, he looked at Tamir and Bankol. "Ready?"

Ahead of them, one chain from the Hammer's goal, the referee set down the blood-stained rubber ball. Since the Hammers were losing, the ball started at their end of the field.

On the far side, the Infinite Razors arranged themselves into a classical Iron Wall formation. The triangular arrangement was designed to slow down opponents from reaching the goal, a stalling maneuver for a team that already knew they won.

He smiled. They thought they won. He strolled toward the ball. Tamir and Bankol joined him. He heard the others spreading out, preparing their own parts of the final play.

"After this, can I have a chance at both of your wives?" Bankol asked as he set himself, his body cracking with every movement. Dust dripped from his joints, pouring out of the cracks of his armor. He grinned. "Or either of your daughters?"

Tamir glared at him. "Would you like to survive this? Because if you mention Namor one more time, I'll sacrifice you myself."

Bankol shrugged. "Five points is five points. Besides, three against us and they are going to hit us with all they have. One of us is already fucked." He chuckled as he gestured to Pennir. "And I don't plan on taking her balls up my ass."

Cros chuckled. He knelt down next to the ball and shoved down, putting more of his weight until the ball sank into the ground. When he charged forward, he hoped they would miss that he wasn't holding the ball.

Taking a deep breath, he summoned his power. Energy crackled along his body, tracing the silver-traced edges of his armor. It arced to Tamir and Bankol, the subtle differences in their magics resonating with each other.

A sparkling mist surrounded them as Tamir pulled on his own power. Liquid rushed along his body, coating his skin in a sheen. It pooled underneath his body but the liquid didn't soak into the ground. Instead, it rippled and rose against gravity, dripping up to hover around his shoulders in crystalline ribbons.

Cros took a deep breath. Light flickered around him, dancing on the edges of his vision. Inside, he felt the source of his power, a core that ached from his exhaustion. If he pushed too hard, it would become a blinding pain. By then, he hoped to see the ball cross the goal.

"Ready?" He yelled.

Tamir and Bankol crouched down, their armored fists grinding into the ground. Blood, sweat, and water soaked through the ruined grass. There would be more of it by the time the day was done.

"Rock 4." It was a fast count, the next number would set them off.

His breathing grew deep and purposeful. He locked eyes with Pennir, almost a furlong away and standing with her arms across her chest. It looked so far away from his position on the ground, but he would reach her if it was the last thing he did.

"Stone!"

It took all of his willpower not to charge as he bellowed. A heartbeat after Tamir and Bankol sprinted toward the Razors, he dug his feet into the ground, crushed the ball hard, and then threw himself into racing after them. He clutched his arm to his chest as if he still held the ball.

It was five chains, half the field, to the Razors. It looked like a long distance, but they easily closed it in a matter of seconds.

Bankol surged ahead, the earth shaking underneath him as he let out a terrible roar and tucked his head down. Stone dust poured out of him as he thudded along and his footsteps left imprints on the ground.

Behind him, Tamir chased after Bankol, slow enough that the distance stretched out to a rod before Bankol struck the front ranks of the Razors.

Bankol's impact blasted the skirmisher who tried to stop him. The crunch of flesh against stone shot out across the fields, despite the heavy armor designed to protect both of them. A body flew back, arms and legs windmilling before the skirmisher slammed into one of the rear guards. Blood burst out of the visor and poured out from the helmet.

The Razor's guard tossed his own skirmisher aside. With a roar, he tucked his shoulder down before coming up with an uppercut that caught Bankol under his chin. When the gauntlet struck the stone man, a brilliant light burst from the impact.

Bankol shot straight up in the air as if he weighed nothing. A concussion blast exploded from the guard's fist. It knocked into the other Razor's before striking Tamir. A heartbeat later, it punched Cros in the chest.

Tamir ducked underneath the guard who attacked Bankol and sprinted forward. His feet tore at the ground as he managed to slip around another guard and race out from the first ranks of the Razors.

Cros followed behind him, but the ranks tightened up and he found himself charging toward a solid wall of flesh and leather. A haze of power crackled along the air, sparks of energy rising up from the defenders. One of his opponents burst into flames while another grew solid like Bankol.

Bankol's body slammed into Cros' right. The impact shook the ground as the man's shoulder snapped and bounced away. There was no blood, just rock dust and a dead expression on the guard's face.

Cros looked away, a sudden tear in his eye.

Between the narrow gap of bodies, he could see Tamir racing toward Pennir.

Pennir uncrossed her arms and held out her hands, her face deadpanned except for a slight smile. Globs of mud ripped themselves from the ground and spun around her as they splattered everything around her.

The streamers of water around Tamir streaked forward, slithering through the air like snakes.

Cros had to look away as he crashed into the ranks of guards. The two men he hit were powerful and heavy, one of them had increased his weight and the other had become metal. His ribs and arm burst into pain as he smashed against their immovable bodies. Stars streaked across his vision, blinding him briefly.

With a snarl, he let his power go. Arcs of electricity became bolts of lighting. It tore through both men in front of him.

The one with the increased weight screamed out in agony as his armor scorched and burst open. Underneath, the flesh blackened instantly.

The metal guard was unaffected by Cros' magic, but the bolts arced out from him into another guard who was. The screams turned into a high-pitched wail as the man folded in half as every muscle in his body tightened. There were muted crunches as the spasms snapped bone.

A skirmisher rushed at Cros' side. He jumped high into the air, his body hazy with power, and then brought his foot down on Cros' right arm. The impact drove Cros to his knees as a loud crack resonated from his body. A shard of bone ripped out of the bottom of his armor as the arm snapped from the impact.

Fighting agony and a sudden despair, Cros knew that he couldn't past the guards. He looked up just as the first of three spheres slammed into Tamir's chest.

The impact shoved the skirmisher back three feet in a shower of blood.

The second and third slammed into Tamir, picking him off the ground and throwing him back. A streamer of blood and water trailed after him.

Cros caught movement on his left side. He turned and backhanded the Razor's champion. Electricity surged through his body and into the larger man. It traced black lines across the champion's face, melting flesh and exposing a wide expanse of bone that quickly cracked from the heat.

An impact caught his other hand, this time in the leg. His attack against the champion sputtered as he looked back. His body was tilting to the side and he couldn't stop it. His leather armor kept his leg together, but he could feel blood and bone grinding against the inside.

He growled as despair crushed him. He couldn't clear a path, he couldn't even make it to Pennir.

Cros managed to catch the ground on his good knee. The impact drove agony through him and he felt broken bones grinding as he bounced. His chin struck his head and more sparks filled his vision.

When he looked up, he had a clear view of Tamir and Pennir.

The sentinel had just thrown her hand back and the last two spheres spinning around her shot back, floating in the gap of the castle goal. Stepping forward, she threw both of her hands forward.

The spheres streaked forward, blurring with speed and leaving behind ripples in the air. The first one caught Tamir in the chest with a muted thump. The second caught Tamir in the throat, the speed hitting so fast there was nothing but a splash of red.

For a moment, Cros thought Pennir's sphere continue past Tamir's body, but then he realized it was Tamir's helmet spiraling back toward him, leaving behind a spiral of crimson blood as it flew back.

His eyes focused on Tamir's headless corpse as it landed only a rod away. His body bounced before settling to the ground.

Cros glanced at Pennir who stood with a smug smile of satisfaction on her face.

Rage boiled out of Cros. He sank into it as he glared at the sentinel and let the energy flow. Lightning bolts shot out from him and punched into the bodies of his attackers.

The man who broke his leg tried to pull back, his scream turning into a high pitched wail as his armor vaporized and lightning blackened the suddenly exposed flesh.

The metal guard's body began to melt from the heat, his transformed body becoming white-hot in an instant.

Around him, the grass ignited into flames and the ground shriveled.

Cros sank into his anger and despair, pulling on his reserves and gripping with all his might. He surged up on one foot with a scream. More lighting crawled out of him, spreading out in a sphere of electricity.

It caught his opponents, even the ones trying to back away. The smell of burning hair and skin filled the air, the stench of it swirling through the sharp taste of ozone that filled his nostrils.

Blood dripped from his ears, nose, and eyes as he stretched out with his might. The bolts scraped against the ground, leaving deep gouges as they reached the sides of the fields and cut into the rest of the Razors.

The screams were inaudible over the crackling.

Pennir's smile froze on her face and she stepped back. He could see her fear from where he was stuck.

Twisting as much as he could, Cros pulled back his one good arm and then shoved it forward. The momentum pulled the lighting arcing around him into a single bolt that crackled down the field. It arced along the water and blood of Tamir's body, stretching further than Cros had ever electrocuted before.

Pennir ripped mud from the ground, but then the lightning caught her in the chest. There was a burst of light and then it continued forward through the ruins of her body to tear into the furthest end of the castle goal.

The magical shield that surrounded the bystanders from the goal shimmered into existence, but the lighting blasted through it. He watched in horror as the arc tore into the fans for terrifying milliseconds before he could pull back on the power.

But, the magic refused to stop flowing. More bolts tore out of him, crackling as they shot out into the crowds. Stone blackened, wood burst into flames, and the screams rose into a high-pitched cries.

Cros' heart exploded in his chest. He felt it shatter ribs with a wet explosion of heat and agony.

Utter silence filled his ears, the ringing from years of playing suddenly gone. He looked across the devastation of the field and felt shame and despair spreading out.

He hit the ground with both knees, the broken leg only slowing him down a fraction of a second before he continued forward. As he swung forward, he saw nothing but a smoking field and corpses scattered in front of him. His momentum carried him forward and he pitched face-first into the ground.

Cros hit the ground with a smile, only a single thought echoing in his head in the final moments of silence. He had won the game. Five points for each person he downed plus fifteen for their own sacrifices.

He may have lost everything, including his life, but at least he won the game.

# About

Samus de Goslin is the brother of Cros de Goslin and has written this fictional version of the Gambit statements from the Hammer players, fans, and reports at the final game. He is currently living at the Cros de Goslin estates while managing his brother's final will.
