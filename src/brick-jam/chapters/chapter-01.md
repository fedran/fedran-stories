---
title: Brick Jam
format: Flash
genres: [Action]
summary: >
  Rarely did battle go well for Gertude in her Baby.
---

"Babe, I lost two!"

Gertrude wiped the sweat from her face and crawled around the boiler that kept their baby moving. The air shimmered and blurred her vision; her age didn't help. Peering down the arm, she saw that one of the local bricks they had sourced had cracked and jammed a gear.

Baby shuddered and the smell of fire magic choked her.

Swearing, Gertrude grabbed a pipe and banged it around to dislodge the brick.

"Love?"

"Almost... there! Clear on two!"
