---
title: Confessions Among Friends
date: 2020-03-28
summary: >
  Teril always looked forward to playing games with his friends in the park. To his surprise and joy, his old friend Rotal was there after a winter apart.
---

> One thing that was hard to explain is exactly how many soldiers had died during the marches war between Kormar-Gepaul. --- Pilanor da Tusar, *Border Conflicts Throughout History*

Teril shuffled along the park path. He didn't have the strength to lift his feet too high nor the agility to recover if he moved too fast and fell. He had to focus on planning his path to avoid rough spots and make sure each step was solid before moving to the next.

It was a far cry from years ago when he marched fifteen leagues a day, fought off warriors, and even managed to take on two war mages with nothing more than a sword and a mirror. Those days were decades behind him now.

Now, the only thing he had to remember the battle was an old notebook in his pocket with his old company insignia engraved on it and a dozen medals gathering dust at home.

His destination was a set of stone tables arranged around a fountain. Each one had four seats arrange around them a grid engraved on top. It was perfect for pick-me-up card games or a quick round of asail.

During the day, the various retirees would gather to pass the time. Most of them were waiting for the day to end but a depressing number of them were stalling until the end.

Teril was one of the latter. He had no family left alive and the small amount of crowns that he got from his pension were barely enough to survive on. All of his income went into buying food and paying for his apartment. His daily games were the only joy he had left.

It took him almost a half hour to reach the tables. There were others already sitting at half of the tables. He had played with half of them, but there was a face he hadn't seen for a few months. Shuffling closer, he held up his hand. "Surprised to see you, Rot."

Rotral looked up from his book. He smiled broadly, revealing his two remaining teeth. "Ter! You're a sight for old eyes."

The other man was a broad-shouldered but squat man. He had all the signs of being military, from the way he stood and the phrases he said. Teril didn't know which military though. They had been playing games together for three years and Rotral kept his past close to his chest just like Teril did.

"Mind if I join you?"

"I was hoping you'd come up. You were always my favorite opponent. Basir or asail?"

Teril sat down on the opposite chair with a groan. Asail was his favorite game and he usually two out of three against Rotral. "Asail, please."

"Need to win a game or two, huh? Rough winter?"

"More than you can imagine."

Rotral pulled out a flask and set it down. After a second, he pushed it closer.

Teril glanced at it, curious why Rotral was making a big deal. Instead of the beaten metal one that Rotral had been spinning on for years, it was a fresh one with a new leather case and a Gepaul military crest for the 19th Invaders.

Throat suddenly dry, Teril sat heavily as he stared at it. He knew of the 19th, they were the ones who crossed over the border in the middle of the night to assassinate high-profile targets or to kill soldiers in foxholes. They were bloody, brutal, and terribly efficient. More than once, Teril had seen their handiwork in piles of corpses and puddles of blood. "T-The 19th, right?"

Teril was in the 162nd Bastions. His job was to defend the same soldiers against the 19th. For two years, he fought against the 19th and the rest of the Gepaul invaders during the border wars.

Sweat prickled his brow as he settled down, unsure of what he should being doing. The wars were decades ago. He looked around nervously.

Rotral grunted as he pulled out the stacking disks used to play asail. His red joints popped as he set up the board with practiced skill. "I spent the winter back home, visiting my grandchildren." He smiled. "I have a great-grandson now with a daughter on the way. Four generations under one roof, that's probably the best gift I could get these days. All those smiles."

He finished setting up the board. "They want me to move back to Gepaul though. Even have a lovely little cottage near a beautiful lake."

Teril reached out for the three dice. "Are you moving?" He felt like his tongue was stumbling over the words.

Rotral shook his head. He gestured toward the board to indicate Teril to go first. "No. My wife's family is still in town. They've taken good care of me for many years and I'm not going to just get up and move because my daughters want me to return home. Besides, I like Old Mam's pies." With a wink, he picked up the flask and took a swig.

Teril's chest ached. His hand shook as he picked up the dice. Each of the dies only had three numbers: zero, one, and two. The three dice totaled four.

Looking at the row of seven stacks of three disks high, he picked the outer ones and shifted two from separate piles and moved them forward.

"What about you, Ter? How was your winter?"

Teril didn't know how to respond. He played with the dice for a moment before handing them over. "Lonely. Spent most of my time in the veteran halls."

"Sorry, old friend. It's hard not having family."

It sounded like Rotral was truly apologetic. The old memories were rising up, of the times Teril had come upon a murdered solider with their throat cut or stabbed in the back. There were nightmares, long forgotten over the years, and they threatened to rise up.

Teril's hand trembled. He pressed it against his thigh to stop it. He focused on the flask, trying to deal with the sudden storm of emotions.

Rotral rolled his dice. Getting a three, he moved an entire stack on the same side that Teril had moved his stacks. "It's a strange thing about memories, you know. I went home and I remembered all these things that I had forgotten since I was a young man."

Teril stared at the dice pushed toward him.

"When I left home, I was going to be a hero. I was going to be the one who ended that damned war of ours. Save the country and everything." Rotral sighed.

Teril was eighteen when he joined the Kormar army. He walked into the meeting hall with plans of being the subject of stories and ballads. A brave warrior against the hordes of Gepaul threatening to invade.

Tears burned in his eyes. Instead of being heroic, what he experience was long months of training, brutal marches, and battles. He spent much of his time knee-deep in the blood and mud on the failed missions. For all the others, he spent much of his time in the infirmary recovering from one life-threatening injury after the other.

"I-I know the feeling." Teril managed to stammer. He scooped up the dice and let them roll off his hands, the bones bouncing around a few times before coming up with one.

He moved one of the single disks onto a stack to make three again.

Rotral picked up the side and rolled them in his palm. "Did you ever figure it out that all of it was bullshit? All the training, all the patriotism?"

Teril look at him in surprise.

Rolling the dice across the table, it came out three. Rotal made no effort to move his stack. His eyes focused on Teril. "They tell those children they are going to be the hero to get them to march in lines. They promise them stories and ballads as they send you off to die. The rich and powerful were never on the front lines dying, but they sure as hell are willing to tell us what to do."

He shook his head. "After a while, I hated hearing those damn speeches about how we were going to win. In just a day, a week, a month. It never stopped. Those speeches. Before every mission. Be proud, get excited, then go out and die."

With a sigh, he lifted one off three stacks and set it down all three of them in a single pile. He used his fingernails to push the dice toward Teril. "You know, I've done terrible things during the war, right?"

Memories tore through Teril's memories. He looked at the dice, half afraid to touch them. "I know. I've seen it."

"I figured. Bastion?"

"162nd."

"Out of Jain?" Jain was a large city near the Gepaul border. Teril was stationed there for almost the entire war. He could still remember the smell of his barracks.

"I liked Jain," Rotal said. "It had the prettiest churches at night. I used to watch the flames at night. You remember the big one to the Divine Couple? With those massive lions in the front."

Teril remembered the church. He prayed there almost every day. At first it was to be the hero and see glory. Near the end, it was just the same prayer: for the war to be over.

He looked at Rotal. "Why are you bring this up?"

Rotal's eyes were shimmering. He picked up the flask and rolled it in his hand. "My family were so proud of me. They kept going on about how much of a hero I was. The problem is that all I could think about was the men I killed. Two years of slaughtering, two years of blood, and mud, and blades. Two years of tears and horror. And in the result...?"

Teril sighed. "Not a single border changed. All those people died and nothing really changed."

Rotal wiped his face with the back of his hand. "My family sees a hero and I see a murderer. I see blood on my hands every time I look in the mirror. There is nothing to be proud about, you know that."

Shaking his head, Teril couldn't speak. He looked down at the piece and then up to his friend.

"We were on the opposite sides, Ter. I killed the men and women you were trying to protect. We probably even fought against each other, I'm not sure."

Teril let out a shuddering breath.

"When I got out," Rotal said slowly, "I couldn't take it anymore. I had to leave. So I crossed over the border and wandered around. I met my Rosie right over there," he pointed to a gazebo, "and fell in love. I stayed because I had friends here. Real friends who didn't see me as this glorious hero but as what I had become, a man."

Silence.

Teril couldn't figure out what to say. He picked up the dice and played his turn. Once he moved his pieces, he handed the dice back for Rotral's turn. Neither man said anything for a long time as the game pieces slowly crawled toward each other.

Finally, the white and black pieces were next to each other. A border war made of bone and stone.

Teril sighed. "Why are you telling me this?"

Holding the dice up, Rotal stared at him for a moment. "You know I look forward to these games, right?"

As much as he was unsure, Teril knew that he did too. He got up in the morning and shuffled his way just to spend time with Rotal and the others. It was the only reason he got out of bed sometimes. Other days, he was the reason someone else got out of the house. He let out a chuckled. "Same with me."

Rotal picked up the flask. "I hated this thing as soon as I got it. But, as I was thinking about melting it down, I realized that we were both dancing around who we really were. Ever since I met you, I've had a feeling we've met before, back when we were enemies, but I don't want those days again."

"Why didn't you just remain silent?"

Rotal looked older for a moment. Then he looked around for a moment before he held it over the side of the table. He frowned for a moment.

The leather case of the flask began to smoke. It blackened and charred as the metal underneath it turned hotter.

Memories crashed into Teril. He remembered a fight against one of the invaders. They were both equal in skill with the sword, at least until his opponent had grabbed Teril's weapon and melted it in a matter of seconds.

The same flames as before engulfed the metal flask. The drink inside, probably whiskey, flared as it was vaporized. Moments later, the metal became molten and poured from Rotal's fingers.

They had fought against each other. It was a fight that had put Teril in the infirmary for close to three weeks with burns. He had lost five good men that night.

He remembered how angry he was at the time. He had failed.

As an old man, though, it seemed so pointless. So many had died for nothing and all the prayers couldn't bring them back.

Rotal sighed and shook his hand clean. "I'd rather be honest with you." He looked up with tears in his eyes. "I'd rather give you the choice of walking away instead having you find out at some horrible time, like my funeral."

He stopped and looked at Teril. "I'll leave if you ask, Ter. I promise you. I'd rather honestly lose you as a friend than anything else."

Teril struggled with his thoughts for a moment. Then he shook his head. He reached into his pocket and pulled out his notebook with his own company's insignia. Setting it down, he pushed it over. "Burn that for me too."

"Are you sure?"

"We are not the men who fought in that old warehouse. If you are willing to set aside your past, then I see no other action than to do the same for you."

Rotal's eyes widened.

"Neither of us can walk easily much less whip out a sword and beat each other. All I want to do is play a game." He looked down at the board game. Asail was about wars, the march of soldiers against each other. Basir wasn't much better, another strategy game about heroes on the battlefield.

He shook his head and set the notebook on the stacks of disks. "I'd rather spend time with a friend that relive those days ever again."

Rotal picked up the notebook. When Teril nodded, he held it over the spot where he had melted the flask. The leather smoked for a moment before it burst into flames. Fueled by the magical fire, it was ash in a matter of seconds.

Both men looked down at the board.

Teril had lost all desire to play their game.

"Shit," muttered Rotal.

"Cards?" asked Teril.

"Yeah, a nice game of kailen sounds good right now. At least that is based on growing gardens. You bring your cards?"

"Right here, old friend." To his relief, it sounded right to call Rotal a friend still. "Right here in my pocket."

Then, two old men put away their war game and their pasts to find a new game to play together.
