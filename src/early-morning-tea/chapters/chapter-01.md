---
title: Early Morning Tea
date: 2019-11-10
summary: >
  Markor sits on his front porch, watching the morning fog as he waits for his wife to return from her first night with another man.
---

> Adultery tears a family apart with blood and lies. --- Casor da Robins, *Codified Rules of a Holy Life*

It was one of the mornings where nothing was in a hurry to get anywhere. Even the fog clung to the ground, swirling around but refusing to relent under the lazy sun that peeked out from the clouds. The breeze that blew across Markor's porch had a hint of ice in it, the sharp bite that brought the smell of burning leaves and rain.

He smiled and sat down heavily on a padded bench. Hoarfrost crunched underneath him but the leather cushion quickly warmed as he settled back. He sniffed at his mug of tea, enjoying the steam that fogged out from the top.

It was still too hot to drink. Setting it down on the arm, he looked out at the fog toward the front gate.

The path in their yard could barely be seen through the shifting mists, but after years of living there, he could see it clearly in his mind. He traced out the path to the gate and down the street.

It was only a quarter mile to Garl's house. Pirlin and Markor had walked to his house many times over the years. Markor had a lot of fond memories of carrying dishes for potlucks or stopping to admire the woods on the route.

Those were happy days then, when he walked hand-in-hand with Pirlin and they talked about having children, her research, and his sculptures.

They had a good life together.

Markor sighed and tested his tea.

Still too hot.

He settled back and toyed with the rim of his mug, enjoying the contract of the hot liquid and the cool air. It was a bittersweet from memories that still stung from when he came home to hearing her cries of passion from the front porch.

His heart screamed for him not to open the door but he couldn't turn away. He had to open the door and see Pirlin and Garl fucking on the couch.

He sighed and shook his head. It had been two weeks and he couldn't help but feel as if she had stabbed him in the chest.

To distract himself, he reached out for his tea mug but he knew it was still too hot. Clenching his fingers together, he settled back down. His breath fogged in front of him.

It wasn't her cheating that hurt, it was finding out that it had been months since they had started. Months of her walking to his house alone, sneaking along the foot trails, and not saying anything.

Markor leaned forward and pulled a blanket from a nearby chest. He shook it twice before draping it across his lap. Reaching over, he took his mug and nestled it into his lap to let the stinging warmth flood through his fingers and his thighs.

He leaned back and exhaled, a cloud forming from his breath. He took a deep breath and exhaled again.

The front gate creaked open.

He looked to see Pirlin walking down the path, her heavy coat fluttering in the mist. She had a flush to her cheeks and her hair was messily across her shoulders. Even from his position, he could tell that she had cleaned herself off before leaving.

Somehow, that made him feel better.

She came up and stopped in front of him. Slowly, she crossed her arm over her stomach and looked to the front door. "I'm... um... home."

He looked at her as he struggled for a moment. He took a deep breath and lifted his blanket. He nodded to the spot next to him. "Come on. It's cold out there.""

She sniffed and tucked a damp strand of her hair behind her ear. Carefully stepping around the blankets near his feet, she sank down into the bench.

At the warmth of her body, he reached out and draped his arm over her shoulder. With a gentle tug, he pulled her close and tugged the blanket over both of them. "Did you have fun?"

She tensed briefly but then said, "Yes. Thank you."

A waft of Garl's house teased his nostrils. He chuckle. "Better than ripping your dress on that animal trail. Or that time he said he strained his foot?"

She chuckled and leaned her head on his shoulder. Her hair was icy hold but he didn't care. "By the Couple, he's a terrible liar. He was in that clashball game the next morning."

Markor kissed the top of the head. "I didn't notice."

She tilted her head up to kiss his lips. "You were just happy he won the game against Basel Grove."

"It was a good game. I won a hundred silver crowns."

"I remember, you took us out to dinner. We had steak." Pirlin picked up his cup of tea. "May I visit him again tomorrow night?"

He deftly stole his tea back. It was much cooler than before, almost perfect. "Tomorrow is fine but tonight you're mine. I've already started the roast and we need to finish our puzzle."

She smiled broadly. "Thank you."

They sat in silence for a moment. Then Pirlin slipped out of the blanket and went around for the door. "You are my love, you know that?"

He nodded and then gestured inside. "Your tea is on the counter. Your favorite, blueberry and lemon."

Markor watched her head in. When the door latched behind her, he sipped at his tea. It was just the right temperature, warm but not too hot. He drank deep for a moment before letting it rest in his lap.

The memories of her cheating still stung but he knew they would fade soon enough. Garl was his best friend and a good man. He would treat her just like Markor strived himself. Together, they would give her what she needed and she would do the same for them. It was just part of growing old together.

"All you had to do was ask," he said to himself.
