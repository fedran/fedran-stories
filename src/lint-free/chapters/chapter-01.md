---
title: Lint-Free
format: Flash
genre: [Horror]
summary: >
  Polsindor was content to prepare lenses for his family's telescope. The last thing he wanted was an interruption.
---

Polsindor polished the lens with wide circles. The cloth was lintless and would only be used once before tossed aside, all to make sure the Lookfar could gaze into the sky during the night.

When he heard a thud and a scrape, he carefully pulled the cloth away before looking around.

The chamber was just turning red from the sunset. No one should be up.

He crawled down the ladder and walked around. When he saw glowing red eyes, he stopped.

The creature's claws flashed out of the shadows.
