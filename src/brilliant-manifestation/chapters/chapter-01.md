---
title: Brilliant Manifestation
format: Flash
genres: [Slice of Life]
summary: >
  Jacom was exhausted and ready to be home. He wasn't expecting to be surprised only feet from his front door.
---

Jacom trudged along the trail. It had been nine hours since he started off in the morning. It didn't matter if he was only a few hundred feet away, his exhaustion sapped what excitement he had.

An explosion of light burst from one of the bushes.

He stopped, dropping his hand to his sword.

His daughter screamed from the other side.

"Sorry." His son who came out of the bushes, glowing brightly.

A smile cracked Jacom's exhaustion. His son had manifested magic. Light mage, a rare power.
