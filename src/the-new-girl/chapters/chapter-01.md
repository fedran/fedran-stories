---
status: alpha
title: The New Girl
summary: "Staple was more interested in playing than learning the family business. At least until the old man and his new apprentice came to recharge the fire cores that drove the family's machines."
---

> The crux of the modern age of industry is the fire core: an iron chamber with a permanent rune of fire etched into the bottom. Only requiring the occasional recharge by a mage of the appropriate resonance, the shield around the rune prevents it from interacting with other magic. --- *Kings of Industry*

Clouds of oil smoke poured out from a cracked manifold, choking Staple's breath as he dangled his hand between two heated pipes while trying to reach for his wrench. Searing heat burned his shoulder and he strained to keep the canvas cloth away from the blackened fixtures. His fingers flicked at the wrench, swearing under his breath as his efforts only brought it closer to the edge of a particularity difficult coil. Below, he knew it would fall over two yard before landing on the super-heated reservoir that housed the engine's power source, a rune of fire that kept the water in the pipes constantly steaming. If that happened, either his wrench would melt or he'd have to explain to his mother how he lost yet another wrench to her infernal machine.

The same device threatened now to burn his shoulder. He flicked at the wrench a few more times in frustration and only managed to push it closer to the edge. Above him, the oil smoke grew thicker. Black and choking, it tickled the back of his throat.

Glancing up, he swore even louder. Glaring at his errant wrench, he took a deep breath and steeled himself against stupidity. Already hearing his mother's lecture, he shoved his arm further between the two pipes, slapping his shoulder against the pipes. The canvas and leather outfit he wore protected him for a few microseconds, then agony cut through his senses. He let out a scream from the pain as he grabbed at his wrench. It slipped from his fingers, skittering toward the abyss. He grabbed at it again, watching as it danced along the edge. With desperate dexterity, he jammed his finger into the holding ring at the end of the wrench. Triumphantly, he yanked it up. The catwalk creaked with his weight and he fingered his shoulder, wincing at the pain, and watched steam rising up from the oil-streaked leather.

"Mama is going to kill me."

Hefting his wrench, he let it dance along his callused palm for a few moments until the metal cooled down enough for him to hold it firmly. Impatiently, he slammed the wrench head underneath the cut-off valve and screwed it down, cutting the flow of oil smoke from the ruptured pipe.

Staple considered checking the time, but already knew he was running out of it. Being a mechanic was a fast job, at least when you had pressure diverted across a dozen different pipes and more junctions than he wanted to think about. Moving quickly, he jumped over to the ruptured section and began to unscrew the securing bolts. Steam and oil smoke poured out of the junction, burning his fingers, but he didn't have much time. He could imagine the pressure building up in the pipe and in a few minutes, it would probably explode and cost more crowds than his mother probably had at the moment.

Fear drove him as he loosened the last of the retaining bolts and yanked the smoldering pipe from its place. He carelessly dropped it on the catwalk and felt the vibrations rattle along the entire length. Grabbing the replacement which hung from a convenient handle on another pipe, he slipped it out of the cloth mesh and jammed it into place.

"Sablen! Where are you!?"

Ignoring the shrill voice of his mother muted by the surrounding pipes, he continued to twist bolts frantically into place. The pipe under his hand began to rattle, one of the many warning signs of too much pressure on the manifolds. Somewhere else, he heard a high-pitched whistling sound. Cursing under his breath, he systematically tightened each bolt. Even a single loose one could inflict costly damages.

The conduits around him shook more violently as he felt tiny quakes rippled through the mass of steel and iron around him. He grunted as he twisted the last bolt into place, then sprinted to the valve. Jamming it into place, he twisted it hard and felt the shudder of pressure flowing through the rusted valve. Watching the replaced section carefully, he was relieved to see not even a single wisp escaping the tight seal. The catwalk shook with the power as the steam purged the oil smoke from the pipes and then quieted down into the constant thrum of the steam-powered engine.

Wiping sweat from his brow, he gave himself a breather. The hot air teased his lungs, but it was the same air he breathed for most of his fifteen years. Chuckling at a job well done, he trotted down the catwalk to the next cutoff he needed to release before the pressure built up to dangerous levels.

As he rounded the corner, he saw his mother twisting the very valve he was aiming for. She wore a similar outfit as him, leather and canvas trousers. On him, it was a burned outfit in constant need of repair but, somehow, on his mother, it was neat and almost elegant. She gave the valve a hard twist and then clicked her tongue at the smear of grease that darkened her fingers. Without looking around, she ran her fingers through her dark hair and the stain disappeared.

Staple chuckled as he padded forward, "And yet you yell at me for wiping it on my pants."

His mother jumped, then glare at him even as she shoved her hand into her pocket. "I know better than to leave the C-series valves on divert for more than a few minutes."

He blushed, barely felt in the heated air, and ducked his head. "Dropped my wrench."

The catwalk shook lightly as she walked up to him. He looked up into her greenish-brown eyes, shadowed by the pipes above them. He watched as her eyes focus on his shoulder and he sighed dramatically.

"I know, but I was in a hurry."

She clicked her tongue again, then reached up. He flinched, then relaxed as he saw a hurt expression flash across her eyes. Her fingers grabbed his shirt and began to unbutton it quickly. A few quick flips and she had his protective outfit away from his shoulder. At the sight of his blackened wound, he groaned.

Her eyes slid back to lock on his gaze, the disapproving glare of a mother. "Sablen Discel, you had better be careful, I won't lose you like your father."

Staple---known to his mother as Sablen---looked away with annoyance, "Father died in a...," realizing he was going into forbidden conversations, Staple let his voice trail off instead of digging himself into a deeper whole. A flash of pain burned in the back of her eyes, then it was her turn to look away. It was one of those conversations he never entered and one she hoped would never come.

He felt a surge of guilt and took a deep breath. "At least, I got it fixed."

She favored him with a sad smile, then brightened with the change of topics. She nodded back to the valve. "Next time, be a bit more careful. You could have bypassed to the F-series manifold, then over to the second reservoir. That would have given you almost a quarter bell longer."

"The second is cracked."

She gave him a glare, "And this morning I told you I fixed it."

Surprise, he stared at her. "You did?"

His mother rolled her eyes, "Yes, this morning."

"This... morning?"

"Yes, do you remember having eggs this morning?"

"Um..."

She chuckled dryly and rolled her eyes. She wrapped her arms around him for a tight squeeze. "Hurry up and finish the rest of the valves. Master Berafin is suppose to be here in less than a bell. Some big announcement."

Staple shook his head, "I hope he isn't increasing our recharge rate again, I don't think we can afford another ten percent."

"We can't even afford a two percent increase, little troll. So, go make sure we don't have any pipes exploding soon, clean up the damaged section I bet you left behind, and meet back at the front office."

"Yes, mama."

Kissing her on the cheek, he spun on his heels and padded back the way he came. Relaxed, it took him about a minute to climb back up the ladders among the oil-drenched steam and hop across chasms with invisible heat rolling up into the air. Sweat speckled his brow by the time he reached the site of his repairs. The ruptured segment remained where he left it. Snatching the loosing webbing, he wrapped it up, slung it over his back, and continued on his way without losing more than a few seconds.

Grabbing the next ladder, he quickly slid down it before hoping across another gap to a second catwalk. A few short steps and he made his way to the steel door that sealed off the engine from the rest of the building. Kicking it open, Staple breathed in the scents of wood being cut and burning wood.

Outside of the engine was his mother's lumber mill. Situated a half mile outside of Sougan, it represented the family's good and poor fortune, a good clean living with more than enough hard work to keep Staple out of trouble. Or, at least that is what his mother always said.

Kicking the door shut, he listened for the loud click of the latch. Not hearing it, he kicked the door harder until the click of the door set back into place. Spinning on his heel, he jogged between a row of two massive saws, the spinning places easily two yards over his head. A high-pitched whine deafened him as he watched a steady stream of logs being shoved through the murderous blade. His eyes ignored the wood in favor of the many pipes and joints that connected each saw to the bewildering array cobbled together junctions and manifolds that reached high up into the ceiling and into the engined he maintained. He slowed down as he caught a sight of steam rising from the depths of one of the saws. Setting down the ruptured section, he peered inside where water collected around a joint. A tiny puff of steam escaped with each powerful shudder as the massive saw blade crashed into another log.

Standing up, he looked around before spotting the foreman. "Mud!"

The foreman, a man with an obvious barbarian ancestry with his broad shoulders and large forehead, looked up from his dark tea in confusion. He turned around twice before he spotted Staple. Rolling his eyes in mockery of his mother, the large man padded over to Staple. Even he had to speak loudly over the sound of the saw blades.

"What's up!?"

The teenager poked his finger at the joint, "Joint is going! As soon as we shut it down, have someone weld it up!"

Mud nodded curtly and wrote something down on a clipboard he always kept with him. His tea balanced on the very edge of it, spirals of wood shavings already bobbing in the dark liquid. Mud finished and set down his pencil to take a deep drink, swallowing wood and tea with equal ease.

Staple grinned, bowed in the din, and continued along his way.

The din faded at the far end of the mill, where the main offices mixed in with the smaller rooms they called home. Breakfast was served just outside the door, inside the mill, along with the rest of the employees who spent their lives within a few kiloyards of the mill. Staple flung the ruined section of pipe into a pile with many others and then head inside the office.

As the door closed behind him, the constant noise faded into a low rumble through the floors.

His mother beat him there, working on some papers for a moment. She didn't even look up at his entrance before speaking. "Go clean up."

"Already did."

Her eyes never left the paper. "Do it again."

A dramatic sigh and he climbed up the ladder to the second floor. It took him almost a half bell to scrape away the oil, soot, and grime that came from working hours inside the engine. He dabbed some ointment on his burn, the reddish stain matched many other ones that covered his body. Staple promised himself he would be more careful, even as he thought about he would just get burned again in a few more hours.

When he came down, his mother had visitors. The first was the curved form of Master Berafin, Lem to his mother when she wasn't being formal. His age reached well over fifty and Staple always thought his nose would make a perfect hook to hang something, if Lem would just stand on his head. His wrinkled hands always reached for him, squeezing Staple's shoulder whenever the old man came close. It wasn't uncomfortable, it was just... annoying.

As Staple's feet hit the ground, the old man bowed to him.

"Young Master Discel."

Staple blushed and looked down. From Lem, it was an empty title, but it was part of the formality that permeated the old man's manner. He ducked his head slightly, muttering back.

"Master Berafin."

Master Berafin gestured to a chair next to his mother's desk. It was ragged and splintered, but it was Staple's. The teen didn't pull his eyes away from the ground as he slipped into it, growing silent to figure out where the conversation headed. The old man grunted once.

"Where was I? Oh, the academy. It is a great honor, of course. Being accepted as one of the professors there would be an excellent capstone for my education, my profession, and my family."

Staple's mother spoke respectfully on her own, "I have no doubt, your skill has blessed our family for two generations."

The old man nodded in appreciation for the compliment.

"Living in Bell Tower, I feel, will cause problems with my services to the Discel Family."

Staple looked up, feeling words rising up past his lips. One look from his mother silenced him, but he felt frustration filling him as he stared at the old man. Lem at least looked sad as he spoke.

"I've been finding it harder to come out this far from Sougan every month. Living in Bell Tower will add a quarter day to my trip and significantly cut into my research at the Academy."

The teenager glanced over at his mother, seeing fear and frustration mirrored in her own gaze. Her eyes were fixated on the older man, but her knuckles were white and a tic along her neck twitched.

Slowly, she spoke. "I see."

Lem sighed, "I have taken care of your family too long, Alab. I have no intent of just abandoning you to some mage who would force you to retune your engines and soak you for what money you have left."

Tears were sparkling in his mother's eyes. "W---We can't afford another increase."

The old man nodded once. "I know. I've seen your books. By my best estimate, I would have to increase my fee by thirty percent to make up for the loss of energy and time on my part."

Thirty percent. That would push the lumber mill into the red and utterly destroy it. Staple wanted to lash out, but he remained silence, listening. He dug his fingernails into his palm, though, to feel something in the face of his future.

His mother slowly lowered her head, "I can't... you know that, Master, I can't keep the mill open, even if I switched to wood burning."

Lem reached out and patted her hand. "I have a proposal."

Staple's mother looked up, a furrow creasing her face. Her eyes flickered to the side and Staple followed it, surprised to find another person standing right behind Lem. It was a girl, a teenager about his age or maybe a little older. She had a dark blue dress on. He felt his body growing hot as he stared at her, his eyes moving from the narrow waist to the swells of her breasts, then up to an angular face and starling blue eyes that matched her dress. The gaze flickered to look at him and then away, dismissing him with a mere movement. His mouth opened slightly in surprise, then in shock as the blush continued to rise in his cheeks.

Lem held out his hand and the girl stepped forward, setting her left hand lightly in his.

"This is Madienn Osteram. She has been my apprentice for two years now and I think she is ready for take on some more productive duties."

Madien bowed, her hand still resting in Lem's.

Staple wiped a sudden sweat from his brow, his eyes unable to tear away from the new girl.

His mother looked over Madien for a moment, then back at Lem. She started to speak, then closed her mouth.

Lem chuckled, "Half what you pay now. A fifty percent reduction in fees."

Staple's jaw dropped in surprise. A fifty percent reduction in their largest expense would give his family a chance to pull ahead, to gather up some savings for the coming winter and maybe even improve the mill. Still gaping, he looked at his mother. Her face remained tight, but he could see the same calculations flashing in her eyes.

When she spoke, it was soft and steady. "You already know the answer, don't you?"

Lem nodded again, "Yes. She is young and I think she understands the merit of a long-term relationship with clients. If you are willing, I have no doubt she would continue to build on our relationship long after Sablen's children take over the mill."

Staple blushed at the thought, glancing up at her. He saw just a flicker of her own gaze at him, then an icy glare that faded as quickly as her eyes returning back to look at his mother. He looked at his mother, then Lem, then back at his mother, somehow avoiding looking at the girl in fear of another glare.

His mother sighed, "What about the attunement? I can't afford to have the mill burn down."

Lem chuckled and Staple could imagine he was relieved.

"I will cover any feedback from the attunement. That will be for the cost of getting a new rune inscribed and attuned to Madien, no expense to you."

"But the mill can't---"

"It would be out of commission for about a month, that much I can't help with."

Staple's mother thought about it for a moment, then looked at Staple. The teen felt suddenly in the center of his engine, the heat flickering at his skin. He thought about the books, the accounting and math he struggled to understand, but knew where they stood. He nodded slowly, peeking over at Madien as he did.

A ghost of a smile crossed his mother's lips, then she returned her look back to Lem. "Please, Master Berafin, I would be honored to take your apprentice as your replacement."

It was as if a subtle tension strum along Staple's back. A knot formed, despite the savings and potential, it just felt so wrong. The man he knew his entire life, the old mage who came to recharge the engine every month, was suddenly going to be gone. And in his place....

His eyes slid toward the girl. As pretty as she was, she was an apprentice. There were a thousand things that could go wrong with charging runes. The worst would destroy everything. At best, she would be only half as talented as Berafin.

The old man nodded once and then grinned. Pulling an envelope from the jacket hanging on the back of the chair, he handed it over to Staple's mother. "Let me get Madien started with the attunement and I'll be back to talk about the contract. I won't even charge for my services today."

"Thank you, Master Berafin."

His mother focused on Staple, "Sablen, please go with Master Berafin and Mistress Os-Osteram and take them to the power chamber?"

He wanted to resist, but the look she gave him allowed no back talking. Standing up, he followed the old man and his apprentice outside. He paused at the door, then looked back. "I'll tell Mud on our way, mama."

She nodded, tears sparkling on her eyelashes. Staple took a double-look, then started to come back, but she waved him away.

"G-Go on. Tell Mud and give him a few minutes."

Staple followed Master Berafin and Madien, stopping long enough to tell Mud to stop feeding logs into the saws and patch the pipe he found earlier. Mud glanced over at the new girl with a raised eyebrow, but Staple shrugged him off.

"I'll explain later."

Mud gave a soft sigh and followed after Berafin and Madien. Silently, he followed as they walked along the soot-covered catwalks and down ladders, leading closer to the heated core of the engine.

It took them close to five minutes of walking, the heat rising up around them until sweat stung along Staple's brow. From his sullen state, he noticed that sweat also dripped down Madien's neck and he occupied himself by watching it dribble down her neck.

The power room represented the very center of the mill's engine, a furnace powered by magic and steam. A heavy steel cube, two yards across, with a heavy door on one side. A massive turn wheel dominated the center of of the door and Staple could see the edges of the handle steaming slightly. From experience, he knew touching the handle when the engine ran would result in burns that took months to heal properly. He shivered at the memory of the one time he accidentally fell against the door when he was ten.

His eyes focused above the room, at the steel and iron chimney that rose up from the power room. Hundreds of pipes, many of them thicker than his thigh, punched through the chimney, then stretched out to other parts of the mill. Those pipes contained the water and steam that fueled the saws, circulated the water, and otherwise ran everything inside the mill. Higher pipes, where the hot air from the furnace cooled down to safer levels, would pump water for showers and sinks and to heat the homes surrounding the mill in the winter.

Staple grinned at the idea of maybe having enough money in winter to visit Sougan, but it faded as Berafin stopped in front of the steel door. In his thoughts, he didn't notice Madien dropping back until she stood at least ten yards away from the door. Staple looked at her in surprise, then turned to Berafin as the old mage began to work.

Holding out his hand toward the handle, he drew letters in the air. In the heat, the characters shimmered like flames for a moment. Staple watched in rapture, wondering what trick of fate let Berafin use magic in that manner. It looked easy to perform a task critical for the entire mill's livelihood.

Behind them, Madien suddenly coughed loudly and rubbed her throat.

Berafin drew for almost a minute before letting his hand drop down.

Staple closed his eyes and leaned against the railing of the catwalk, feeling something change deep inside the engine he knew his entire life.

It started softly, a hissing noise faded away, barely heard through the quarter-yard thick walls of the power room. Anticipation flooded through him as the furnace stopped on Berafin's command. The chugging and gurgling of water rose up as he heard steam pumping through the engine with a powerful rhythm, like a heartbeat. It began to slow with the heat no longer fueling the massive steel heart. Groans shuddered through the metal, shaking the catwalk as the massive saws came to a halt outside in the main room of the mill. A shiver coursed down his spine as they stood there for a half bell, waiting for everything to come to a slow halt. It ended when the last of the steam finally cooled down, dripping down into the lower bowels of the engine.

Staple opened his eyes slowly, enjoying the sudden and profound silence. His eyes focused on Madien's. The blue eyes were locked on his, a mixture of confusion, concept, and... something else inside them. As his eyes focused on hers, an icy glare rose up before she quickly looked away. Her hands tightened into fists as she stared fixedly at the door and cleared her throat loudly.

A flush rose up on Staple's cheeks and he looked away himself. He let his gaze focus on the pipes, droplets forming on the outer edges as the air cooled around them. He let his gaze return back to the door as Berafin began to draw again, his letters shimmering in the air as he drew the final seal before opening the door.

While Staple couldn't perform magic, he could trace the images of the cooling spell Berafin used in his mind.

Madien coughed again as Berafin finished and then blew the runes toward the door. The warm blue letters shimmered as they flew toward the metal, then slapped against it. Steam rose up from the door as it cooled down faster than it could naturally. The one time they didn't charge the furnace in time, it took close to a day to cool down enough for Staple's mother to open it. With magic, it took only ten minutes.

Staple grew impatient when Berafin finally gestured for him. Padding forward, he danced his fingers in front of the handle for a moment before grabbing it tightly. With a grunt, he twisted it, putting his weight and strength behind it. At first, it refused to move, then it squeaked loudly before lurching open. Staple almost fell but caught himself, standing up quickly. His eyes flashed toward Madien then away with a blush.

"Hurry up, boy, I want to get back out of this heat."

He bowed his head and pulled open the door. A blast of heat shoved the door open as he had to brace himself as the heat stung his eyes. The furnace burned cleanly, but the heat would peel back skin instantly if he wasn't careful.

Berafin tugged the door open and entered the furnace, ignoring the heat.

Staple turned to watch him as he entered the small steel room.

The fire rune that drove the furnace filled the floor of the furnace. It spread out like a flower, with a dense and complicated center and thousands of lines carved into the steel. Flickers of orange and red ran along the lines, following the edges with the pulsations of power and spiraled in toward the center. Staple shivered at the sight of it, something far more complicated than he could comprehend, but so critical to his family. Between recharges, that is.

Berafin knelt down with a groan and pressed his finger against the center. A surge of energy flashed through the rune. In the past, the mage would concentrate as the motes of energy would reverse themselves, power flowing from the center to fill up all the darkness. This time, his efforts accelerated the crawl of energy toward the center, draining it. Staple watched in fascination.

Minutes later, Berafin looked up and spoke in a soft whisper. "Madien."

When he repeated himself, Staple turned to look at the girl and nodded. Immediately, he flushed because he didn't have authority over her. Embarassed, he glanced away.

She hesitated, then walked the remaining distance to enter the furnace. Smoothly, she knelt down next to the old man and pressed her finger against the center herself.

Berafin pulled back his finger and groan. "It's inert. Now, remember go slow and attune to it. You've done this before, but nothing this large. If you have any problems, just leave and we'll try again."

Her eyes were fixed on the rune. "Yes, master."

Berafin struggled to return to his feet.

Staple pulled himself away from the door to help him, careful to reach over the rune instead of standing on it.

The old man took his hand gratefully and stepped out. He looked back into the room, at the teenage girl kneeling in the center, then back at Staple. "Stay here in case she has problems."

Staple ducked his head. "Yes, Master Berafin." He wasn't sure what he could do though, he didn't have the magic needed to power a rune.

A chuckle and a squeeze of his shoulder and the old man left.

Staple ignored him as he peered inside the furnace.

Madien frowned as she concentrated, her other hand resting on her thigh as she peered at the rune. Sweat dripped down her face, following the line of her throat and soaking into her dress.

The teenager wondered why anyone would wear a suit in this heat, but he kept to himself.

A flicker of energy surged through the rune and he jumped at the suddenness of it. It stretched out along the rune, then sizzles along one crack.

Madien mouthed something and the energy faded instantly. She sighed and closed her eyes. After a moment, the energy spread out from the center again, much slower than before but still faster than Berafin ever did it. It filled more of the intricate petals of the magical rune, but it began to sizzle again. She withdrew the energy rapidly and leaned back, keeping her finger set on the center.

"Too fast." He didn't know why he spoke up, but they came out before he could stop it.

Her head snapped up, the icy glare cutting to the bone. "What do you know!?"

Staple blushed, "I don't, but I've seen Berafin do this for ten years. You are doing it way too fast."

Another glare and she closed her eyes to focus. Energy flickered along her finger, the center of the rune glowing brightly before energy began to spread out.

Staple held his breath as he watched it oozing along the cracks, a single pulse of energy tracing the lines of the rune. Flickers of heat rose up from the steel carving, but it began to sizzle loudly only a few inches from the center.

"Slower?" she asked with a sharp tone.

Her tone made him blush with embarrassment and he turned away. His stomach twisted like he was sick and he leaned against the door.

Her breathing echoed out of the tiny room and he felt more than heard her trying again.

In his head, he counted the seconds before a sharp sizzle and a whispered swear.

A shiver trickled down his spine as he thought of the horror stories, of rune attunement that went bad and exploded, taking out the mage who charged it and everyone around it. His fingers clutched to the warm steel door.

Behind him, a sizzle was followed by a steady stream of whispered curses.

On her next failure, he finally turned around, his eyes sullen as he watched a tiny puff of smoke rising up from the rune.

Still swearing, she wiped the sweat from her brow and tried again. It came too fast, sizzling around one of the corners. This time, a tiny shiver vibrated through the steel as flickers of energy flashed along the entire rune.

"Still too fast."

She snapped back, "Shut up!"

He felt emotions rising up inside him. With a frustrated sigh, he released the door and pointed at the rune.

"Look, I've seen him do this. It should take a few seconds to get to that first ring."

"I know what I'm doing!"

Rage rose up, but he didn't move as she tried again. The pulses of energy raced along the tracery, slower than before but way too fast than what he remembered. He started to open his mouth, then closed it. With a disgusted sigh, he stepped out of the room and backed away from the furnace. At the next sizzle and a pop, he turned to leave.

"Um, S---Sablen?"

He stopped, considering the effort to keep on walking.

"Listen, please?"

Biting his lip, he sighed deeply and turned around. Slowly, he walked back along the catwalk to the furnace and peered in. Madien knelt there, looking as uncomfortable as himself.

"I'm," her eyes looked away from him, "I'm sorry."

He wanted to smack her, to tell her he was right, but Staple just stood there, clenching his fist. Madien returned her gaze.

"How slow?"

Hesitating, Staple knelt down on the edge of the rune, careful not to touch it and pointed from the center to the rune to one of the curves.

"It always took, about..." he struggled with how to describe how long. He remembered Berafin's humming. Unsure, he hummed it softly, some strange music. On that first higher pitched noise of the song, he tapped the air above the point. "About that that long."

When he looked up, her gaze caught him and he felt a lurch in his stomach. He started to draw himself back up, when she pressed a hand against his hand to hold him. An electric spark shot through his hand and he snatched it back, then blushed at the flash of hurt in her eyes.

"Sorry."

Madien looked down, "S---Show me again?"

Butterflies fluttered in his stomach, but he held his finger over the center. Taking a deep breath, he hummed louder, following a remembered pulse of energy as it stretched out across one line of the tracery. He followed it for a few bars past the junction before letting it fade.

Madien gasped, "Was that Discort's Concerto, the first movement?"

Staple had no idea so he shrugged.

Madien thought for a moment, then closed her eyes. Her lips moved as energy shivered through the air, then she began to whisper a song, a soft seductive sound. It wasn't Berafin's humming but it was the same song, spoken in a language he didn't know. The energy spread out from her finger, slow and steady as it stretched along the line, a mote of power filling the tracery and stretching out in all directions. It followed the lines, further than ever before. The tiny furnace room filled like the lines, but with Madien's sad song.

Staple felt a tear forming in his eyes without knowing why.

Then a pop and a sizzle.

He groaned. "I'm sorry."

Madien looked up with a smile, "No, no. This is good. Most of the runes that Berafin makes now are based on the Grane's orchestras. He builds his runes with music," she grinned and took a deep breath, "I can do this."

She looked so happy as she closed her eyes. The song welled him from her throat as she sang, the energy pouring out along the lines of the rune. It came strong and powerful, not surging or pulsating.

Staple watched as it stretched out along the lines, single lines of bright orange.

The light filled the furnace room as she continued to sing, louder and more confidently. He had never heard the words to it before, nor would he imagine the sound of her singing. A flush rose in his cheek and he crawled back, stepping up as the butterflies surged inside him.

He was still blushing when she finished a half bell later. Shaking slightly, she stood up and staggered out of the room.

"Um... Sablen? I need to," she gestured to the door. He jumped and shoved it shut. She watched as he jammed it into place, then twisted the handle until it latched into place. Together, they stood at the door for a moment until a muffled boom shook the engine. Heat seared up through the chimney as the rune ignited into smokeless flame.

Next to him, Madien sighed, "It worked."

Staple nodded, started to turn away, then stopped. His eyes caught the sight of her dress, the very edge it caught in the door. "Um, your dress."

"What?"

She looked down, then gasped. The door groaned as the heat built up quickly inside. Steam rising up from the pipes as a series of ping noises filled the engine.

Madien tugged at her dress, but it refused to slip out from the tightly sealed door. "I need to turn it off, I can't get out."

His mouth refused to open, to explain how fast the furnace heated up. Instead of babbling or stammering, he knelt down and wrapped his dirty fingers around the delicate fabric. She gasped in outrage as he tugged once, then yanked it hard. Fabric tore loudly as he ripped it free. He looked down at the stained blue fabric in shock. His eyes caught the first hints of flame around the fabric still in the door. Dropping the fabric, he stood up and looked at her, trying to explain.

Madien slapped him. "You bast---"

Her voice silenced as the cloth still in the door burst into flames from the magical heat inside.

Flushed and frustrated, Staple snarled at her and her surprised expression and stalked down the catwalk. A moment later, he heard her racing to keep up. He didn't slow down until she grabbed his elbow.

"Sablen, I'm sorry. I didn't know."

He stopped, not looking at her, "I know, but it heats up really fast once it activated."

"I'm sorry, Sablen. I didn't think, this was my mother's dress."

Staple didn't know what to say, so he just grunted. He gestured along the catwalk.

"Come on, mama will make us lunch."

"Thank you, Sablen."

"Staple."

Her voice rose up surprise, "What?"

"People here call me Staple. Only mama and Berafin call me Sablen."

"Oh."

He looked at her. Her blue eyes were focused on thought for a moment, then she gave him a hesitant smile. To his surprise, she reached up and kissed his cheek.

"Thank you, Staple."

Suddenly, the new girl didn't seem so bad.
