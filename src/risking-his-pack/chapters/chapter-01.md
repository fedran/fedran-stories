---
status: wip
title: Risking His Pack
availability: private
---

The worst part of being a slave is hearing the punishments. Salcid sat on the end of his bed with his head in his hand and his triangular ears pressed flat against his head. No matter how hard he kept his ears against his skull or pressed his hands over them, the crack of the whip still made him jerk.

It didn't matter if he looked up or not. His windowless room was two yards by one, just enough for a bed with a drawer underneath it. The thin wooden wall, not enough to keep back the cold but sealed enough to lock in the stench, didn't shield him from the crack of the whip or the high-pitched yelp of his nephew.

He sighed and tried to think of a better life but failed. He had been a slave his entire life, working at the Germudrir Mill since he was a pup. Now, twenty-one years later, he was still at the mill with no future besides long hours, freezing winters, and dying from some accident. None of the dalpre lived to old age at the mill; the oldest was only thirty-four.

Dalpre. He silently cursed his body between the cracks of the whip. Just because he had the ears and tail of a dog, he was less than human, a slave with no hope. The rest of the Germudrir Pack was like him, humanoid canines. It was some mage's cruel joke of combining a human with animal traits to make the perfect slave race. He was born a slave and would die one.

His nephew yelped louder. The cracks of the whip stopped but Salcid didn't lift his head. The last ten would use a heavier one, one that designed to cut through flesh and leave him bleeding. Salcid still had the scars when he was whipped many years before, it ached whenever the rain came.

Salcid closed his eyes tightly and tried not to listen to the men walking outside. In his mind, he could identify them by the sound of their breath, their gait as they shuffled across the hard-packed ground, and even the chuckles as they watched a poor dalpre get beaten.

His curse, his manifestion of power, made the voices crueler. He knew they were telling the truth when they talked about beating other dalpres for the fun of it or when they mocked Poil, his newphew. The guards at the mill were cruel men used to abusing the dalpres who couldn't fight back.

Salcid didn't have fight in him. He was a thinker, not a fighter. He understood the mechanics of the saw blades and helped maintain the water chutes and wooden gears that kept them spinning as they cut down the thick trees into planks. Foram, the mill owner, didn't send Salcid to learn how to use the mechanisms, he just randomly picked Salcid out when the last mechanic had died when a catwalk had collapsed on him.

A crack of the heavy whip shot through Salcid's room. He jerked violently as if he had been the one struck. He didn't know why Poil had ran off like he did, but the brutal beating would ensure no one would ever try to escape again.

Salcid clamped his hands over his ears and prayed he couldn't hear the next blow. It was futile, but the act of praying helped with the agony in hearing his own family being beaten.
