---
status: wip
title: Risking His Pack
availability: private
---

Later that evening, the Pack had dinner under the watchful eyes of the guards. Three long tables had been set out in the open yard between the dalpre barracks, the barn, and the main house. It was early evening, a crisp one where the fall air left his breath fogging in the light from the lanterns that surrounded him.

Poil sat in the middle table, bandages covering his back. His tail, a black one with a white tip, was curled under the bench as he tried to eat with his left hand. His right was bandaged from finger to elbow.

His mother and sister sat on both sides of him, helping him as much as they could. Poil resisted, yanking away from either of them. He was always a stubborn pup and even worse as a young man.

Salcid focused on the man across from Poil. Lonmir was the oldest of the pack and the Alpha. A dour man with drooping ears and a lazy eye. He was talking quietly but sternly to Poil, who only nodded at the appropriate times.

"Sal?"

Salcid looked up and then to the side. When he saw that it was Mamgum sitting next to him, his heart beat faster and his ears perked up.

Mamgum smiled at him and leaned into him, the side of her breast pressing against his shoulder. She was short and stocky, but it was an easy strength that let her pick up as much as the men whens he put her mind to it. "You are lost tonight."

He took a deep breath, filtering out everything but her scent. When the musky heat rolled across his senses, his smile grew brighter and he leaned into her. "Sorry, too many things in my head."

"Chasing Poil in circles?"

Salcid nodded and looked back at other injuried dalpre. Salcid's own scars ached at the sight of the bloody furrows that the whip had cut into the younger man's back. He remmebered the pain and agony too well, there was no escaping it.

"Do you know why he ran?"

Salcid shook his head. "To escape?"

"No," Mamgum lowered her voice and leaned closer to him, "for freedom."

It was hard to concentrate with her so close. He held his breath and bowed his head until their muzzles brushed against each other. "Freedom?" he asked in a whisper.

"He said that there was a notice on Foram's desk. Something about the dalpre being freed."

Salcid's pulse sped up. "At the mill."

"No," whispered Mamgum, her brown eyes shinging, "everywhere. The world has freed us. But then Foram chased him out. He was going into Rock River's postal office to make sure, they'd have a notice if it was true."

"T-That can't be true," said Salcid as his voice cracked.

"I know, but what if...? What if we finally have a chance?" Salcid could smell the excitement rising from her. It was a familiar smell to him, one that he enjoyed more than once when they spent the night in each other's room.

Salcid glanced up at the windows of the main house. Inside, Foram was having his dinner and working the books. The mill was reasonabily profitable and one of the primary sources of exports for the entire area. After a second, Salcid shook his head. "No, it can't be. They would tell us."

Mamgum scoffed. "Foram would tell us? He's give up the slaves he's had since his daddy's daddy's time?"

"Someone would tell us. The *bortim's* men, if anything."

"*Bortim* Lurkulan hasn't been to the mill since we were both pups in nappies." Mamgum sighed and pulled away. "I thought you said you had hope."

Salcid glanced at his newphew. "It's hard to keep it when we're stuck here."
