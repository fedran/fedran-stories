---
title: Judging the Boyfriend
character: Milner
date: 2020-11-08
summary: &summary >
  Milner's daughter had brought her next boyfriend to visit and he was dreading vetting her next choice. He was a navy man, which was good, but he had an interest in the new mechanical devices that Milner had no interest in exploring.
teaser: *summary
---

> The church and society are offended at the unmarried father. --- Kasor de Gorik, *Where Church and Country Meet*

Milner let out an annoyed sigh as he shrugged his black jacket on and tugged it down. It was his favorite Pinar-Caus, a custom-fitted jacket that he had bought when he turned fifty. He wore it for special occasions that required a bit of somber attitude like funerals, reading of wills, and meeting one of his daughter's boyfriends.

The scuff of fabric reminded of the last time he had worn the jacket. He looked down at the cuff where there was still a few smudges of dirt.

A pang of sadness darkened his thoughts. He had last worn the jacket when he buried Grail, his wife. Even though it had been eleven years, he remembered what it felt like to stand next to her grave holding hands with his two daughters. With shaking hand, he scraped at the dust until it tumbled to the ground.

Eleven years is a long time. The pain had faded but the emptiness remained. He wished he could say that he missed her ever day but it would be a lie. He had his daughters, his job, and all the other severed threads of his life that he had to gather up to fill in the space. It was only moments when he stumbled onto a reminder that the sorrow returned.

A rumbling noise filled the air. It was a rhythmic thumping and scraping. He could feel it through the ground, the beat of one of those horrid machines that had been plaguing the street. Clicking his tongue with disapproval, he leaned over to peer out the window.

His townhouse sat on a quiet street. The cobblestones were shimmering with the morning sun baking off the last of the moisture. He watched a black carriage roll past. He could see the small green flag fluttering off the back side.

"The Kagils," he muttered. He wasn't fond of the new couple who moved in a few houses down. After Victor Kagil's parents had given the new couple a sizable windfall for their marriage, the newlywed couple had moved in with all the grace of the newly rich. That means extravagant parties for each of the holidays and daily rides around town a carriages, not to mention new dresses every week.

It was just the appearance of wealth, though. They didn't have enough to own the carriage outright and rented it from one of the local families. Milner could appreciate the effort. It was a game they all played when they first weaseled their way into Society. Grail and he had done the same thing forty years ago, right down to renting the carriage to parade their position like an ill-fitting outfits.

In a few more years, the Kagils will have settled down and he could start to be friends. They just needed a bit more maturing before they were ready to properly join society instead of merely visiting it.

He rubbed the cuff absentmindedly before he realized the rhythm thumping had continued to get louder and lower in tone. He could heart it echoing off the fronts of the townhouses. As the sound took on a more immediate, sharper timbre, his curiosity plummeted into despair. Whatever monstrosity approach was coming down his street.

Then he saw it. It was a four-wheeled vehicle rolling over the cobblestones. A large chimney belched out a white cloud of steam from where it stretched out of a rat's nest of pipes that clung to the back of the car like a tumor. It looked unbalanced except for a large, bronze horse head that had been mounted on the front. He couldn't see the faces of the two people in the seat of the horror but he recognized his youngest daughter's favorite hat.

He leaned against the windowsill. "Don't stop, Noril. Don't stop."

The monstrosity pulled in front of his townhouse. It belched out a loud cloud of steam that obscured his vision.

"Shit," muttered Milner. "Come on, Nor, a mechanic? I raised you better. I'm tired of you bringing these guys over."

The steam dissipated. He watched the young man with his daughter come around and help her out of the car. Noril glanced up with a smile and then lead him lead her away.

Milner sighed and tugged on his cuff. "Better meet the new boyfriend." Turning around, he shook his head and headed toward the stairs.

By the time he reached the bottom, Noril had come into the front door. She wore one of her good afternoon dresses, the one with the blue flowers along the edges. Her eyes lit up when she focused on him and then she threw out her hands. "Good afternoon, daddy!"

"My baby," he said affectionately while bracing himself for the impact.

She slammed into him and squeezed him tightly. "Oh, I missed you!"

"You saw me three days ago," he said with a smile. Over her shoulder, he got his first look at her companion.

It was a young man, maybe in his early twenties, with a slender build and a tiny goatee. He had a flop of a hair over his forehead with a swoop that reached down to almost, but not quite, blocking his vision. The young man reached up to scratch under his chin and Milner caught sight of a naval tattoo along the back of his hand.

Milner made a double take. Apparently his daughter's tastes had somehow shifted drastically over the last few months. Previously, she went for broad-shouldered men with barrel chests. As much as he hated to admit it, most of her previous suitors were not unlike talking to mirror copies of himself.

The new suitor was in a different category entirely and one that dredged up Milner's past before his days with Grail. He grunted and then returned his attention to his daughter. "I missed you too."

Noril pulled away with a grin. "Daddy, I'd love for you to meet Kril."

Kril smiled broadly, his white teeth flashing. He held out his sun-tanned hand. "Good day, sir."

Milner shook it firmly. "Navy boy?"

"Yes, sir. Currently posted here at the administration desks."

"That's a solid job." Miler was surprised, usually the soldiers at desks didn't have mechanical vehicles. They were expensive and dangerous. He glanced down to where they were still holding hands. Kril had a soft but firm grip and Milner couldn't help but remember his first boyfriend. With a blush, he pulled his hand back before the memories caused problems. "Surprised you have one of those monstrosities."

Kril's eyes sharpened. "Oh, I wouldn't say it was a monstrosity, sir."

"What would you call it?"

"Daddy...." Noril said in a low voice. "Be nice."

Kril shrugged. "I would say it was my pride and joy. My brother and I have been working on it two years now." He smiled slyly. "I'm quite adept at handling it, though it can occasionally buck if I go too fast."

There was something in Kril's tone that brought a flutter of heat across Milner's chest. He had heard that tone before, but it was playful and teasing, not the type of voice one would use with the father of someone one had been wooing.

He glanced at his daughter.

Noril looked up, dropping a sly smile of her own. She blinked a few times and then pointed to the door. "You should look at it."

"What are you talking---?"

"Go on, Daddy." She said and gently pushed him toward the door. "Trust me."

Milner shook his head. As much as he trusted his daughter, she was up to something. At least this time she wasn't trying to find some eligible widow looking for love. He shook his head and stepped away from the stairs. "Come on, Kril, amaze me."

"Of course, sir." Kril stepped out of the house and held open the door. Together, they headed down to the vehicle.

The young man smelled of spiced perfume. He was easily a head shorter than Miler but he moved with an easy grace of someone who spent many years on a small boat. Milner reorganized the gait and sway from his days in the navy.

"From Tarsan?"

"No, a native here in Gepaul but right at the Tarsan border. I'd be a da Kisner on one side and a Disrobin on the other."

"I bet that made family gatherings painful."

"No," Kril said with a grin. "The Kisners don't acknowledge we exist anymore on account of the entire civil war and such. But, occasionally I'd get to meet a cousin over a few pints."

The vehicle was the monstrosity that Milner expected but he had never gotten close enough to see any details. They had only been on the streets for a few years but they had already left a foul impression. Milner's frown came back but he held it in check for Noril's sake.

Kril gestured to the back first. "Have you seen one of these before?"

The top half looked like an ant's nest of pipes with turns and twists going in all directions. There were gauges of some sort measuring things Milner didn't understand. Brass rivets and joints glistened with water that dripped down onto a tank he could barely see inside the guts of the machine.

The lower half had more structure with coils of pipes lined up neatly in lines. They seemed to circle around the iron block. When he peered in between the gaps, he saw that they continued to spiral in on itself, creating a vortex of metal that also connected to the tank and a flickering core of heat inside.

"I'll admit, we tried a couple of designs to get this far. Everything is still pretty chaotic. I mean, the entire principle of iron shielding is pretty well known but it is hard to find something aesthetic that is functional at the same time."

Miler didn't really understand. He felt confused and lost, as if the world had moved beyond him. With a grunt, he pointed to the neater lines at the bottom. "I like this part better."

Kril came around until they were standing side-by-side. Then he crouched down and waved his hand over the part that Milner referenced. "Oh, this is my favorite part too. There is something about the simple lines and elegance. It reminds me of the formations during training."

Miler smiled. He remembered the same neat lines of ships as they were anchored in the bay. There was something majestic about the potential of the fleet, the structure and discipline. He had spent twenty years in the navy, falling in love with both the service and its members more than a few times before leaving.

Kril looked up, his smaller body easily dwarfed by Milner's presence. "I... we started with this. It ended up being a lot more expensive and heavier than we planned."

Milner smiled grimly.

"I like discipline. I like when I have to get things in order."

It was a quiet phrase, but it felt like there was more to it. Milner's face grew hotter. He glanced up at the nest of iron and realized he had leaned too close to the heated coils. With a grunt, he stood up.

Kril didn't move. He tilted his head up but said nothing.

For a moment, Milner looked down at the young, beautiful man squatting in front of him. Old memories and fantasies rose up, awaking thoughts that had been quiet for a long time. His manhood twitched at the more erotic memories that came. With a blush, Milner stepped back while clearing his throat.

Kril stood up slowly. His eyes lingered on Milner for a long time before he gestured to the bronze horse in front of the vehicle. "My brother found this from an old parade. It was rusted and corroded, but more importantly, it was heavy."

Clearing his throat again, Milner circled around until the bronze horse stood between the two of them. "And it gives your... car the hope of catching up to one of the real things?"

Kril grinned. "Oh, I don't have a problem with catching up."

"Right," Milner said. "That's why all of these vehicles putter down the street barely faster than I can walk."

"Well, city law says that I can't exceed three miles an hour inside the limits."

Milner frowned and shook his head. "What? No, that can't be right. There isn't anything like that."

"Vehicular Acceptance Act, section five."

As much as Milner wanted to disagree, there was a confidence in the young man's voice. It was clearly something Kril knew better than Milner; after many years on the sea, Milner learned to listen to experts. "It... can really go faster?"

Kril leaned over to rest his elbow on the horse's rigid mane. "I can go faster than the *GNS Longshank.*"

At hearing the name of his old ship, Milner straightened. "The *Longhsank* was the fastest ship in the fleet."

"Only when you had a firm hand on the wheel."

Milner, like everyone else, had a magical talent. His was to make ships cut through water faster. It made the *Longshank* famous, but all the speed couldn't he could have mustered wasn't enough to outrun the mage-created tsunami that struck it. He had gotten two medals that day and a promotion that enabled Grail and him to move into the neighborhood.

He smiled broadly. "That is mighty bold words for young man like you."

Kril smiled and shrugged. "True though. I would proudly race my stallion against your *Longshank* if I could."

Foolhardy pride rose up. "Good thing we are in city limits."

"We don't have to be."

The simple words stopped Milner's smile. There was something in Kril's tone, a challenge but also encouragement. A thrill of something unknown. He gulped and wondered if he had something bitten off more than he could chew. He also wondered why he was blushing again or twisting his fingers together.

Clearing his throat, he gestured to the house. "W-We shouldn't be out here long. Noril will be---"

His words were interrupted when the front door opened. Noril bounded out, a wide grin plastered to her face. "I'm heading over to the Kagils for lunch!"

Without waiting for a response, she walked past Kril and hurried down the street.

Milner looked at her with confusion. She seemed more enthusiastic than normal, not to mention coming out at the most inopportune time of the conversation. He looked back at Kril. "Was she listening... to us?"

Kril patted the bronze horse. "Want a ride?"

Still confused Milner looked at the padded bench. The leather looked recently cared for, soft like a well-used saddle but not broken. It appeared to be more comfortable than any horse that he had ever ridden on. "Is it safe?"

"I promise, I will never hurt you."

Milner sighed and looked at the seat and back to Kril.

Kril slowly straightened up and strolled over to seat. He pulled himself up and sat down, his small body leaving plenty of room for Milner. Then he patted the seat. "Come on, sir. Why don't you ride my boat and see how fast we can go?"

Milner looked down the street to where his daughter was entering the Kagils's townhouse. They looked like good friends as the three embraced before closing the door behind them. Then he glanced at his house, picturing himself upstairs in this room looking down. "Yeah, why not?"

He wasn't as graceful as Kril getting into the seat, but he was surprised how comfortable it was. He shifted slightly and looked around. A iron ring dangled off the side and he sneaked his hand down to clutch it tightly.

Between their bodies, he spotted an iron lever had been set in the center of the deck. It looked like one of the control rods for a smaller ship. He reached out for it.

"Oh, let me handle that. It takes a bit to get used to controlling the speed."

Milner chuckled nervously and dropped his hand to the seat between them. "I feel strange."

"I'll start slow." Kril's tone brought a flutter of excitement coursing through Milner. He looked around for something else to grip but couldn't. With a shuddering breath, he tightened his fingers over the edge of the seat.

"Just take a deep breath, the first moment is always slow but then hits with a jolt." Kril pressed on something with his foot and pressed down with a grunt.

Metal scraped against metal and then a wave of heat rose along Milner's back. It felt like a comforting heat from a bath but quickly grew hotter until it felt like sitting with his back to flames.

Gauges across the front of the vehicle began to rise up. Kril started to explain them but none of the words really made sense. Milner didn't know why pressure would be important or what a "ratio" was but the young man spoke with a confidence that was comforting.

"Get ready, the pressure is ready and I'm about ready to start."

Milner held his breath and gripped the seat and ring tighter.

Kril grabbed the control rod and pushed it forward.

At first, nothing happened.

Milner was about to relax when the vehicle lurched forward. He let out a yelp and clamped tightly.

The vehicle shook violently as it rolled across the cobblestones, picking up speed. Kril deftly turned a second control rod on his other side and the front wheels twisted to the side. The car followed and then swung back with his control.

Milner was in a vehicle. An actual car.

His heart pounded with surprise and fear and excitement. They were going slower than he could walk but it felt like they were racing across the horizon. He stared in shock as the wheels picked up speed and soon they were bouncing and shaking down the street.

"It's a bit rough around here," Kril said. "Cobblestones are hard on the wheels."

Milner nodded sharply, not trusting his voice. It felt like his heart was beating a thousand times a second as he watched his surrounding roll back. It was like being on a ship again, watching the world sail by.

Kril turned at the street and headed for the city limits.

After a few blocks, Milner found himself relaxing slightly. He let out his breath.

Kril grinned at him. "Exciting, isn't it?"

"Y-Yeah."

"It's going to take us a few minutes to get outside of the city. Then I can go faster."

"N-No too fast," Milner gasped. He tightened his grip on the ring.

"I'll go as fast as you want me too. I promise... sir."

The strange tone, almost seductive and promising, added to the rush of being on his first car. Milner had to discretely adjust his sudden hardness before settling into place.

As they drove, Kril pointed out various signs for vehicles: speed limit reminders, one-way streets, streets for horses or pedestrians only. Milner hadn't paid attention to any of them before, but he could see a wealth of information had been slowly added to the roads over the years.

Soon, townhouses made way for homes with yards. By the time the distances between the houses grew and the roads turned into gravel, Milner had gotten used to the movement of Kril's vehicle. The gravel felt easier on the car and the sensation of sailing across the water increased.

"Want me to go faster?"

Milner gulped and looked down at the speed control rod. It was a simple thing, push forward to go faster, pull back to slow down. The heavy weight of the car kept it tight to the ground.

Kril reached down and pulled Milner's hand up.

An electric surge rippled along Milner's nerves. He was helpless as the young man wrapped his fingers around the speed. Then, the soft palm pressed against the back of his hand. "Here. This way, you can dictate how fast we are going."

"I-I---"

"Don't worry, I'll guide you. Like this." Kril pushed his hand firmly forward with surprising strength. The rod shifted underneath Milner's hand, inching forward as the vehicle accelerated.

Exhilaration rose with the speed, bringing a pang of memories and joy. He smiled as he gripped the stick tighter, enjoying the firm hang on his. Cautiously, he pushed it forward himself.

"That's it, just as fast as you want."

Milner looked at the road ahead. They were on a straight line of gravel for leagues. During the fall, it would be crowded with harvest coming into town but now it was just a ripple of heat off the gravel and no one else. He smiled and pushed the vehicle faster.

Underneath him, the rumble of the engine didn't bang against the fronts of buildings. Instead, it seemed to gather between his legs and shook up his spine. He could almost sink into it, enjoying how he felt it in his chest more than heard it.

It added to the heady sensation of his close contact with Kril. The young man had, in such a short time, brought back memories of old lovers and friends, of the days Marlin had long thought past. He glanced at the younger man.

Kril was smiling as he focused on the road. It was the same look the spotters had on the ship as they peered across the horizon looking for danger. It was the same look at Marlin's first lover had as they spent long hours talking away the night while watching the surrounding ocean. The young man's eyes flickered toward him.

Marlin looked away with a flush on his cheeks. He must be reading the signs wrong. He was in his sixties, with a daughter Kril's age. No, he had a daughter who was currently being wooed by the young man. He shook his head. It was just his imagination.

He shifted in place to adjust his hardness. At least it would be a lovely fantasy.

"Want to go faster, sir?"

Somehow the way Kril said "sir" brought the thrill back. Marlin tried to push the bar forward but it wouldn't move. He pulled back and pushed, but it only stopped on something. "I can't."

Kril leaned over until their shoulders touched. "Are you sure? I recall you had special talents. Your daughter talked about them all the time, Longshank."

Marlin glanced back at the engine. He didn't know if he could use magic around the core of heat inside the engine. If he pushed too hard, the feedback between him and the delicate device would cause it to fracture and then explode.

"It's safe. That's what all that iron does, it makes it safe."

It was hard to believe him but Milner had heard that was the reasonable the mechanical devices were possible.

"Trust me... sir."

It was the final "sir" that pushed him over. Marlin couldn't help but enjoy the thrill of the almost playful respect. He reached out, not with his hands but with his feelings. The sensation of wind rippled along his senses, a swirl not unlike the ocean currents.

Marlin knew how to talk to the ocean. He reached ahead and parted the current of air, guiding them to separate just a little sooner than they would have split on the horse's head. It only took a little effort but then the car lurched forward and the wind blew stronger across his face.

Kril stroked his hands over the back of Marlin's. It was teasing and playful, not to mention seductive.

Marlin fought with his urges as they raced down the road. They were far away from everyone except for the occasional farm house and grain silo. They were quite alone and he found it harder not to think about forbidden thoughts.

He had to be misinterpreting Kril's intentions. He had to be, the young man was with his daughter. It wouldn't be appropriate to ask, but it was hard enough with the thrill of racing down the road and the affection touches that Kril had been giving him.

Kril lifted his hand away. "You got this?" he asked over the rush of wind.

Marlin nodded.

"Good."

The older man focused on the road, at least until he felt Kril's hand resting on his thigh. The electric touch made it almost impossible to concentrate and Marlin had to strain to keep his magic splitting the currents and keeping the speed up.

Kril didn't make any maneuver other to to rest his hand on Marlin's thigh but that was enough. It was intimate and without hesitation. He knew exactly what he was doing.

Marlin glanced at him.

Kril smiled and leaned closer, turning his body so he could get close to Marlin's ear. His hand moved an inch higher up. "Just so you know, Noril and I are just friends, sir."

Milner lost control over his magic. A shudder of air slammed into the car but it continued to rush forward. Slowly, he pulled back on the speed control until the wind died down.

"I swear, we've been friends for years but it was never more than that." Kril chuckled. "In fact, she found out that I have a certain fondness for broad-shouldered bears with a hint of silver and got this idea that you'd like to... ride with me."

Milner squirmed with the sudden hardness between his legs and a flush across his skin. He pulled back on the speed, slowing the car down until it was ambling down the road.

Kril steered with his other hand to bring it to the side of the road. Then with a firm grip, he reached up and tugged the speed down to its furthest setting. With his foot, he pushed something down and there was a scraping noise.

The vehicle came to a halt. Even though it was no longer moving, Milner felt as if his body was still rumbling with the engine. It reminded him of the first few days after disembarking a ship. He panted softly, trying to grab onto the spinning thoughts in his head.

"Sir?"

Milner shivered at the soft voice. He looked over at him. "How... how did you know?"

Kril smiled. "Is she wrong?"

Milner blushed. "It has been a very long time for me. I'm so much older than you."

Their faced inched closer to each other.

The younger man grinned. His hand slid higher up on  "I find that one of the best parts. That and calling you 'sir.'"

"I like that."

Kril leaned closer. "Permission to kiss you, sir?"

Milner answered by reaching up and kissing the young man. It was delicate and tender. The very touch stole his breath away and left him shaking with a growing need and desire. He moaned into the embrace.

Hands caught his thigh as Kril lifted himself from his seat.

Milner reached out and guided the younger man into his lap. It took a moment for their bodies to mesh together, lips to lips and hardness to hardness. It was everything Milner hadn't forgotten. He moaned into their kiss as he slipped one arm around Kril's waist and held him tight.

Reaching out for balance, he wrapped his fingers around the nearby bar.

Kril broke the kiss. "No, that would be bad. Here, sir, let me."

The little word brought another thrill.

The younger man reached down and guided Milner's free hand to the back of Kril's neck. "Now you can decide how fast or slow you want to go," he said with a whisper and a grin. "Or how low you want me to go."

With a moan of need, Milner pulled Kril back in for another kiss.
