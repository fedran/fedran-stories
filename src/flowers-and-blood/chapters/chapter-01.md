---
title: Flowers and Blood
format: Flash
genres: [Action, Drama]
summary: >
  Bleeding to death, sometimes the only healing one can get is a useless power that had no place in battle.
---

Clutching his side, Dinmor tried to take a few more steps but couldn't. He collapsed to the ground in agony, blood pouring out through his fingers. Hissing, he wished that magical healing wasn't a fantasy and he wouldn't die alone in the middle of a field.

He couldn't die yet. His brother was in danger. With a groan, he dredged up the last of his magic to create flowers. Roses, dandelions. It didn't matter. He shoved them all into the wound to stop the blood.
