---
title: Later
summary: &summary >
  After another long night of sex and drugs, Farimon once again throws himself into his research into resonance. However, as he goes to clean up the mess from the previous night, he discovers something new.
---

> After centuries of research into magic by thousands of mages, there are no more secrets to discover. --- Dean da Coister

It was late afternoon when he woke again. The din of traffic outside of his window pounded against his ears, adding to the burn in his stomach. Too many hours of drinking and smoking had left him unable to wake without feeling his stomach in his throat.

With a groan, he flipped on his stomach to ease the pressure and reached out for Margaret. His hand found nothing but empty mattress and cold blankets.

He cracked open one eye and peered across the room. The wall of pictures had two new ones resting on the floor. One was the three of them, Farimon, Margaret, and Robert, dancing together on the floor. The second was one of Robert's own sketches of Farimon and Margaret wrapped in each other's embrace. It was set at the Golden Rose Brewery, judging from the casks of wine they were using as an impromptu bed.

Farimon frowned. He only had a vague recollection of dancing with Robert and none of the casks. But, that wasn't much different than the rest of his life lately. He crawled out of bed and slipped to the floor, sinking against the icy wood to clear his thoughts.

He noticed he managed to avoid throwing up on his clothes. His new jacket had been torn along the back and the knees of his trousers were stained in mud and wine.

He shook his head and staggered to his feet. "Mar?"

His bare feet scuffed on the ground as he slumped toward the railing. The strength left him and he sank to his knees again, clutching the bars for support. The metal felt good against his skin as it pushed away the fog of drugs and drinking.

Margaret was on the ground floor, sprawled out across the painted lines. A puddle of vomit spread out from her face, but he could see she was still breathing. Her dress rode up her thighs, catching on the sharp edges of one of Farimon's experiments.

He looked away from her and at the floor around her. The mess was hopeless. The debris covered almost every inch in a wide variety of wood, metal, and exotic materials. Hundreds of thousands crowns worth of failure.

He looked at the center of the circle. The glowing piece of paper mocked him. It was the simplest thing in the world, protect a strip of paper from the presence of another mage. For the last two years, he couldn't even do that.

Gripping the railing, he pulled himself to his feet and started toward the stairs. Two nights of parties had not given him any new ideas, but maybe cleaning up the mess would. He couldn't give up, not when he was so close.

Farimon was halfway down the stairs before he realized that Margaret was sprawled across the lines and the paper was untouched. He froze in mid-step, his eyes growing wider.

Shaking, he looked at her. She was crossing two circles with her arm outstretched between elegantly forged bars of a silver cage and a heavy metal arm used to brace a stone pillar. She was almost six feet closer than she ever had before.

He knuckled the sleep from his eyes as he focused on the glowing piece of paper. It wasn't his imagination, it was still there.

"M-Mar?" he gasped.

She didn't answer.

He slid down the rest of the stairs and plowed through the mess. Something cut his shin, but he barely noticed. "Mar! Mar!"

His knees splashed in the puddle of drying vomit as he grabbed for her. "It isn't shorting! Mar, look at it!"

Farimon glanced over to prove that he wasn't imagining things.

There was a mote of bright purple rising from the paper.

"No, no, no!" he yelled. He dropped Margaret and sobbed. "No, don't burn up. Not now, I just---"

The spark of light faded and the rune stopped flickering.

"---figured... it out?" Farimon frowned as he looked at the paper and then back at Margaret.

She was face down in the mess, her head half buried by a metal mirror that he used to concentrate sunlight. Her face was twisted in a scowl as she struggled to wake up. "F-Far?"

Farimon watched both of them as she pushed herself up. Her dress stuck to the ground and she had to jerk to peel herself off. As soon as her head lifted above the debris of experiments, sparks rose from both her and the paper.

He gasped and pushed her down. He felt her jolt with the impact with the floor but his eyes were locked on the paper.

The sparks faded away. Panting, he beamed as he looked over her. Her slender body was flailing at the ground, shoving hunks of metal as she tried to find purchase. But, there were no sparks rising up toward his rune.

"Mar!" He scooped her up and pulled her close, not caring about the sharpness of vomit or her ruined dress. "That's it! I found it! Thank you! Thank you!"

Her arms were limp for a long moment but then she wrapped them around him.

Eight yards away, the rune exploded. The impact struck both of them, searing their skins and blackening their ruined clothes.

It didn't matter that he wasn't sure why it didn't respond. He saw it was possible and it had something to do with the carcasses of his failed experiments. He had a start, now he had to track it down.

"Thank you!"
