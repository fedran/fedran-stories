---
title: Lost in Time
summary: &summary >
  After hours of work, Farimon thinks he finally solved the problems of resonance. But, when he goes to test it with Margaret, things go horribly wrong.
---

> The final days of the Crystal Age were lived in a drugged stupor where the rich used others for their playthings: for sex, for violence, for anything to stave off the stagnate feeling in their bones. --- *Crystal Goblin Goblets* (Act 1, Scene 1)

When Farimon looked up again, it was dark. He didn't remember the day passing but losing the light didn't surprise him either. He glanced out the window and tried to guess if it was early or late evening.

When nothing revealed itself, he turned back to his lab. The only light came from the rune in the center of his studio. The steady blue light cast long shadows across the room, turning the shelves and walls into a speckled pattern.

With a groan, he looked down at his work. The wooden cage was completed. He had carved vines along each of the bars and polished the wood with a rasp. The latch was a simple hook but he had a spot for a more formal lock if it worked. Overall, it was an elegant piece of work.

"Margaret?"

No one answered.

He stood up. Hours of sitting in one position suddenly came back to him with aching joints, a rock-hard bladder, and a rumbling in his stomach. A wave of dizziness washed over him and he gripped the table until it passed.

As soon as he could balance again, he decided to handle the pressing concerns in order of importance; he headed for shared bathroom outside of the studio.

A half hour later, he came wandering back into the studio with a block of cheese in one hand and a bottle of wine in the other. Both were poor vintages, but there would be finer food when he managed to rouse Margaret for the night's celebration.

"Mar?"

When she didn't answer, he headed back to the bed on the second floor. He found her sprawled out on the blankets. She wore her robe, which meant that she had woken up at least once, but the only sign that she had done anything was the fresh pipe in her hand. The ashes were still glowing and a wisp of sweet-smelling smoke curled up from the bowl.

Farimon knelt down by her head. "Mar? Mar?"

For a moment, he wondered if she would wake up. There were enough drugs clawing through their veins to kill them.

He sighed and pulled the pipe from her hand. His close presence to her caused a throbbing in the joints in his fingers and his stomach began to bubble again. He fought it down. Her presence was worth the discomfort.

"No," she murmured and tightened her grip. "Mine."

He let out his breath. "Hey. Can you get up?"

Margaret lifted her head and peered at him with blood-shot eyes. He watched as her pupils contracted and she focused on him. With a groan, she blinked slowly, one eye and then the other. Her cracked lips moved for a moment before the whisper escaped her throat. "Did you get it?"

Farimon smiled. "Yeah, Mar, I just finished."

She pushed up, her robe splitting open. "Really?" The sharpness had come back into her voice, brimming with instant excitement. Her smile stretched across her face until dimples appeared on both sides of her pale lips.

He couldn't help but enjoy her expression. "Yes," he said, "and it's beautiful."

Margaret scrambled to her feet even as she spoke. "Let me see!" Her robe caught him in the face, plastering against his nose until she rushed past him.

Farimon stood up and held out his hand. "It isn't in place, hold on!"

There was a wild race to the bottom of the stairs. She beat him, but stopped just outside the outermost circle, her bare feet inches away from the bright yellow paint. It was her circle, measured after hours of experimentation and more than a few nights of drinking. Unlike many people, her aura was steady as long as there was a certain amount of drugs in her system. It made her the perfect measuring stick for his experiments, and his lifestyle.

Panting, he gave her a quick kiss on her sweat-slicked cheek before padding across the circles. The rune didn't respond to him because it was his magic that imbued the page. But, if she stepped any closer, the rune would short and then probably explode into flames.

He got the wooden cage and brought it back. As he did, his breathing grew deeper and nervous flutters ran through his stomach. He didn't know if it would work. Even an inch would be something, a foot would make him better than a thousand years of mages and experts.

His throat dry, he stopped at the rune and picked up the glowing piece of paper. Blue light shone through his fingers as he set it down in the cage and latched it shut.

"It will work," breathed Margaret. She shook at the edge of the circle, one hand clutching her pipe until her knuckles were white. Her robe continued to flutter around her, revealing flashes of her naked body with every shift of movement.

Farimon fought the doubt and forced a smile on his face. "It will work."

He set the cage down in the center and held still.

"Back away."

"It will work," he said more emphatically.

"What if it blows up?"

"I...." He fought a sudden fear that rose up in his throat. "What if I ruin it? If I stay here, it will work."

He looked back at her, pleading.

Margaret held out her free hand. "Please, I don't want you hurt. If it works with you close, then it will work with you over here."

It took all of his willpower to walk backwards to her. He didn't take his eyes off the wooden cage, his chance of a lifetime. It had to work, he knew it. There were a thousand other experiments surrounding him, all failures, but he had to believe this one would work.

She caught him in a hug, her body pressed against his back. He shivered at her touch, the familiar ache growing in his joints and the sweat of their bodies. Her breasts caressed his naked back just as her pipe rested against his chest.

The curls of smoke rose around him and he breathed in deep, enjoying the flush of euphoria that followed it. He took another breath and held it in, feeling it seeping through his veins.

"Ready?" she whispered behind him. The bowl of her pipe brushed against his skin, sending a bolt of discomfort from the heat.

He nodded, not trusting his words.

When she moved, he moved with her. They took a step. Only enough to pass over the yellow line.

Farimon's eyes never left the cage, watching for any signs of shorting or resonance.

Nothing happened. His heart beat faster as he held his breath. "It work---"

A mote of magenta rose off the rune. The startling contrast of bright purple interrupted the blue as it drifted toward her.

The tension left his knees and he sank against her body before hitting the ground. "No," he whispered, "No."

The smell of ozone filled the air, a sharp bitterness that rose up from the paper and the woman behind him. Small embers of energy, mostly bright purple, drifted from her body as others lifted from the paper. They sailed toward each other, speeding up with every passing heartbeat. The only flicker came as the motes passed through the bars of the cage.

"Damn their eyes," snapped Farimon. He couldn't tear his eyes away from the rune as it flickered rapidly. More magenta embers rose up, tracing the lines of the rune as it pulsed. The flashes grew faster and brighter, the contrast of light and dark blinding in the studio.

He lifted his hand, hating how the flashes were so fast that it looked like his body was teleporting instead of moving. He looked past at the paper despite the brilliant light hurting his eyes.

With a sharp pop, the paper and cage exploded. A wave punched into his stomach, shoving him back a few inches. It continued on to knock over a shelf before thudding loudly against the brick walls and the windows, but the wire-filled glass panes held.

His ears popped from the explosion. He gasped and let out a long shuddering, gasp. "Damn their eyes," he said into the darkness.

Margaret sank down behind him and pulled him into a tight hug. "Next time."

"It's always next time. A hundred different ways and I haven't figured it out. I know I can."

She kissed the back of his head.

"Damn it, why haven't I figured this out!?" Farimon's voice echoed through the darkness.

In the distance, a bell began to ring. Even as he fought the tears in his eyes, he counted out the ringing. Nine bells, it was a few hours to midnight.

"Come on, Far, we're late for the party."

"No," he said. "No, I can figure this out."

She tugged on him. "Come on, we have to go."

"Why?"

"Because it's Robert's party."

Robert de Monsar, Farimon's patron and source of his money. The older man was well past fifty and burning away the last years of his life with drugs, sex, and throwing money at a fool who thought he could change a thousand years of dogma.

"Come on," encouraged Margaret. "You have to get pretty for him. Both of us do." Her voice was soft and cheerful, as if she wasn't haunted by the failed experiment.

He jerked free of her grip. "He can wait."

"No, he can't. You already know he's getting anxious for..." Her voice trailed off as he glared at her. She sighed and crouched down. Her proximity felt like nails scraping against his skin. "Look, love, there is a time for this and a time for Robert. And we need to make Robert happy if you want him to keep paying for your experiments."

Sullenly, he let her pull him to his feet. He didn't mind visiting Robert, the sex and drugs pushed back the pain of Margaret's resonance. But, it was hard to pull away from one of his failures. He wanted to fix it, to obsess about the problem until the world burned away or he answered the impossible question.

With a sigh, he headed toward the burning rune.

"Far, don't do that. We don't have time."

"Just let me create a new one. And then I'll get ready."

She said nothing. He listened to her bare feet on the stairs as she went to find a clean dress. In the back of his mind, he promised he would ask Robert for more clothes for both of them.

Numbly, he swept the remains of the paper and the wooden cage into a corner before finding a fresh piece of paper. With his finger, he gathered power and traced out another rune. His fingernail glowed with the same blue light as he felt the energy coursing through his veins and into the paper. The glowing line left behind flickered with his presence.

When he pulled back, there was once again a steady blue light shining from a piece of paper. He carried it to the center of the circles and set it down on the still heated stone.

He backed away and stared at it. A few wisps of smoke rose up from the stone around it, framing his obsession. He gripped the shelf, wracking his mind for another way of fighting the resonance. It was possible, he knew it was.

Nothing new came up. Just his old failed experiments flashing across his mind in rapid succession. He couldn't escape them any more than he could find the answer. His fountain of imagination had dried up and he was helpless to do anything other than try pointless experiments endlessly.

"Damn their eyes," he whispered. His grip on the shelf tightened. "Damn their eyes!" With a snarl, he yanked the shelf over. It teetered for a heartbeat until he pulled harder. Screws, hunks of metal, and broken shards of pottery tumbled around him as he tossed it over.

"Damn all of the god's eyes!" He shoved his way through the scattered and scorched hunks of metal on the floor and grabbed the next shelf. With a grunt, he pulled it over. The sound of heavy metal and hunks of wood crashing to the ground gave him a brief moment of satisfaction. But, when the echoes faded, the frustration and despair rose up again. He stormed to the next and pulled it over. Without waiting for the crashing to silence, he hurried over to the next one, and then another.

He managed to get across the room before his anger fled. He panted for breath as he surveyed the mess.

The paper still shone in the middle, but now the room was cast in countless shadows that stretched up the walls. It looked like a battlefield, or at least the images of war he had seen in pictures and paintings.

Trembling, he looked up at the second floor.

Margaret stood at the railing, tracks of tears running down her cheeks.

Farimon felt sorrow gripping his heart. He wanted to sink down and cry, or find something to smoke and retreat from everything. He forced his breath out and then bowed his head. "I made a mess."

"We'll clean up," she whispered. "Once we get back."

He sighed and stepped over a pile of knives. He would try again, but after he had a few days of sex and drugs and parties to clear his mind.
