---
title: Sour Taste
summary: &summary >
  Farimon wakes up from a night of partying with his companion, Margaret. Despite having a hangover and hungry, his obsession with resonance drags him into investigating the nature of how mages respond painfully to each other's presence.
---

> All magic has a resonance, a signature or frequency of energy. Rarely does it work harmoniously with other energies. The resulting feedback frequently causes pain from mild itching to piercing headaches. --- *The Inescapable Truths of Magic*

Farimon woke up with a sour taste in the back of his throat and a stomach that refused to settle. He groaned, clutched himself, and forced his body over a pile of pillows. The pressure against his gut helped with the gurgling and he let out a sigh of relief.

His arm slipped off the pile and smacked against Margaret, his companion of the last few months. The back of his arm slid along her sweat-soaked breast and he pulled away before she woke up.

With the inclines of the mattress and her body heat to orient himself, he found the closest edge of the bed and rolled away from her. When one leg dangled over the edge, he blindly grabbed the end table and forced himself up. The sharp edge dug into his palm as he managed to lever himself into a sitting position.

It was fall, right at the edge when the winter storms loomed along the horizon. The windows along the north side rattled from a blustery wind.

Rivulets of sweat ran down his chest and back, prickling in the coolness. He grunted and forced one eye open, peering through the haze until his eyes focused on a wall of pictures. Sketches and watercolors of him with the nouveau riche of Boxna Xas, a town that five years ago was nothing but a backwater destination for the dredges of Tarsan high society. Now that the town had a reputation of being the place to be for the sharp edge of magical research, the quality of society had increased. At least the money flowing through the town did.

His attention slipped away from the wall of pictures and to the railing over the rest of his studio workshop. With another groan, he forced himself out of bed and over a pile of vomit-covered clothes. He spotted his own black button-down suit, a Martin Grover original, and Margaret's slinky white Talstoy. Both were ruined, but that was typical of their nightly parties.

At the railing, he took a deep breath and drank in the familiar smells of old wood, grease, and metal. Except for a small kitchen area and the bedroom, the rest of the two-floor studio had been converted into a laboratory of sorts. Over the last few years, he had bolted shelves to the stone walls and then promptly stacked them with thousands of cast off, abandoned, and failed experiments.

He focused on the crux of his experiments, a single piece of paper on the floor in the center of his studio. He had imbued it with a simple light rune. The blue light shone weakly in the light from the windows, but at night it would fill the room with a blue glow.

Farimon had painted ten concentric circles on the floor around it, though the two innermost circles only a memory with the floor scorched and pitted from frequent explosions. The outer circle was the freshest, painted seven months ago, soon after he met Margaret.

The circles mapped out the resonance of the rune, the invisible force that surrounded every mage and artifact. For two mages with incompatible auras, the disharmony created sharp pains and discomfort. For Farimon, Margaret's presence caused his joints to itch, his throat to dry, and his stomach to turn sour. And she sweated profusely no matter how cold it was.

Unlike living beings, artifacts responded to resonance differently. Instead of experiencing pain or discomfort, the damage from resonance affects the physical form of the magical device, weakening it and eventually cracking it. When too much damage was inflicted, the artifact was destroyed as all the energy is expended in an instant. In the rune's case, it caused a two-yard radius explosion that scorched the floor. For larger devices, the damage could destroy a build or set a city on fire.

He glanced down at his scarred side. The burn had faded over the years, but he still remembered when his brother's rapier had exploded when Farimon picked it up only a few weeks after Farimon's powers manifested. The month of recovery set him on his path to prevent resonance from ever hurting anyone else.

"Well, it won't solve itself." He cleared his throat and started down the stairs to the ground floor. One bare foot slapped against the worn wood, the other still had his sock on it.  The contrast of fabric and icy wood was startling. It also helped clear the fog from his mind.

He headed for his workbench and the latest experiment, a wooden cage carved from exotic woods from the southern jungles. The stack of almost white planks cost him a pretty crown, but it wasn't exactly his money.

Ignoring the pressure in his bladder, he sat down on his stool and picked up his carving tools. He could have built a simple box in a matter of hours, but there was more to crafting than just making something simple. It had to be something that the rich would want to buy. Something beautiful and functional; a light rune was pointless if no one could see it.

He sighed and settled down, working on carving out the last side of the cage. It was going to be a long afternoon, but if he could finally solve the riddle of resonance, then nothing else would matter.
