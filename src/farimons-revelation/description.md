---
title: Description
---

**He will usher a new age of society with his discovery.**

History would paint Farimon Ganasir-Bilen as the shining Father of Industry, the clean-cut man who single-handedly discovered the secrets of resonance and found a way of create an artifact that was no longer subject to the destructive power of feedback.

However, the reality was far different that what history wrote in its pages. Farimon was a research mage, but he split his days struggling to find some secret and his nights as a plaything for the rich and powerful. His live-in companion, Margaret, was both his partner in pleasures and his crux as her very presence caused the physical pain of feedback in both of them.
