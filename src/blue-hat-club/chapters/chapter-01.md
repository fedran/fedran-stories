---
title: Blue Hat Club
summary: >
  On the third day of every month, the Blue Hat Club meets up in a cafe in the corner of town. It is a chance of the city's gay men to chat, talk, and socialize without worrying about others. Today, they have an unexpected guest.
date: 2020-01-30
---

> We should be ashamed of our tolerance of homosexuality that we've allowed in our once high and glorious society. --- Finagel da Josin

Pael checked the pale blue, bowler hat on his head before he came around the corner to Miss Hathin's Cafe. The comfort of making sure the curved rim slid along his fingertips smoothly always soothed his nerves before he stepped into the view of others. It was a vanity thing, he told himself, but it didn't matter. He was the elected leader this year and it was up to him to look the part.

The cafe was at the end of a street, right where the cobblestones stopped at a wrought iron fence. On the other side was Blomdering Park, one of the prettier flower parks in the city. It also had plenty of foot trails and paving stone paths that wandered through the trees and wild flowers and sculptures.

He smiled to himself. It had been a while since he walked hand-in-hand with some lovely fairy and whispered sweet nothings into a tilted ear. Of course, he was a few years past being a fairy, a slender young man who fancied other men. No, as he started the final lengths of his twenties, he was firmly becoming more of an elf than a fairy. His paunch was getting just a bit snug inside his waistcoat and there was a bit more sag in his buttocks. But, that is the nature of growing up.

Pael threaded his way past the wooden chairs and straightened a few of the tables before heading inside the cafe. "Good morning, my lovely Hathin!" he announced.

Hathin dea Pinnir---a cheerful spinster who made the most glorious of teas and cupcakes---looked up from her counter. Her smile brightened the room as she lifted up a tray of still steaming cups. "You are right on time."

"Of course. I must be."

She cocked her head. "Should I remind you of last week?"

He frowned for a moment and then smiled. "That didn't count. We didn't do anything, he just wanted to talk."

"And you came in with last week's suit on. Not to mention the jelly on your hat?"

He gave a sly grin. "It was the only thing I had at his house."

"You're just lucky his wife---" Her voice stopped as she looked to the side and behind Pael.

He turned as an older man in an old black suit, worn shoes, and an almost bald head came shuffling into the cafe. He had an ornate cane that squeaked on the brick floor as he set it down and tried to push the door open enough to squeeze one drooping shoulder past.

Pael spun on his heels and strode over. "May I help you, Father?"

The older man grunted and nodded.

Reaching out, Pael opened the door slowly and then pinned it in place with his foot while holding out his hand.

The older man looked at it for a moment and then up at him. He had the cloudy gaze of a man who had seen miles. He shook his head and grunted again. "'an do it, young one," he muttered.

Pael felt a sting of annoyance but pushed it aside to keep a smile on his face. As soon as the older man was clear, he gestured toward one of the tables. "I'll ask Miss Hathin to see you."

Another grunt.

He headed back to the counter. "It's for you."

She sighed and reached up to pull her light brown hair out of her face and into a pony tail. "Bad timing though," she said with a whisper. "You got more blues heading in."

He turned to see a small cluster of men heading in. All of them wore blue hats of some sort, though a "hat" was pushing the limit of Kabel's scarf. However, Kabel was new to the club and didn't have the money for a proper head covering. "Thanks, Love. I'll try to keep it down for the black but do you think he'll be trouble?"

She was already gone, sweeping around her counter with a swish of her ass and a swirl of her perfume.

Pael envied her for a moment. She had accepted her life as a spinster and had even flourished in it. Besides the occasional lover, she seemed happy to deal with a bunch of fairies, elves, and trolls that invaded her little corner store once a month.

At least no one painted her front door telling her to burn.

He shook his head, put on a smile, and headed toward the group entering the room. "Lovelies!" he said with all the cheer he could muster.

They greeted him enthusiastically with hugs, kisses for the cheeks, and gossip from across the town. He fluttered around, guiding everyone to their seats and collecting orders.

Kabel was one of the first to sit down and it took Pael a moment to get alone with him. Squatting down next to the table, he looked at the large man. "You okay, Kab?"

Tugging on his blue scarf, Kabel nodded but his eyes were moving around the room. "Just feel a bit uncomfortable today."

"It's okay, we aren't going to bite."

"I know," Kable started as he toyed with his scarf. "I just, you all look so pretty and I'm... not." He ended with a sigh. He finally tugged off the blue knit and crunched it up on the table.

"Not all of us are pretty, that isn't what this is about."

"I'm fat. A troll."

"Trolls are beautiful," Pael said with a smile. "Fairy, elf, troll, and dwarf. Each one has something wonderful about them. Every smile is worth treasuring. Every kin is worth saving. Every one of us is worth loving. We may not like the same things and we may not stand the same way, but we will always be brothers when we wear blue."

The last few words were echoed by the surrounding men in blue hats. It was the motto of the Blue Hat Club and they started every meeting saying almost the same thing.

Kabel looked around, his eyes shimmering. "But everyone else is so...."

His voice trailed off. Pael noticed that he was pulling on his scarf. Almost everyone else wore hats but he didn't have one. After a moment, he leaned back to fish his wallet out of his pocket. Discretely, he pulled out a hundred jems and folded them in his palm. Replacing his wallet, he leaned back. "You know Gan's?"

The immediate clenching of the scarf and the widening of eyes told Pael he was right. Kabel was partially uncomfortable because he had to wear a makeshift blue hat instead of a proper one. With a smile, he took Kabel's hand and pressed the bills into his the larger man's palm. "After the meeting, why don't you get yourself a lovely new hat?"

"I can't...." Kabel's eyes widened.

Pael grinned and stood up.

"I can't wait to see what you pick for yourself," he said as he withdrew his hand, leaving the money behind.

Before Kabel could say anything else, Pael stepped back and rejoined the crowds. There were about thirty men packed in the cafe. Most of them were chatting and most of the usual cliques were already forming in the corners.

To his surprise, the old man in black remained in his seat, sipping his tea and looking uncomfortable. Confused, Pael looked over at the counter where Hathin was dealing with the neat and tidy line of customers.

Their eyes met and she pointed at the old man. "Yours," she mouthed.

"Mine?" he responded silently.

When she nodded, Pael shrugged and walked over the to the table. Despite the press of people, there was an empty space around the old man. He was an intruder, a black blot in the field of blue. Pael stopped by the opposite chair and then changed his mind. Shifting his hand to the chair next to the old man, he leaned over. "Mind if I sit here?"

A grunt, barely heard of the chatter.

Pael kept his smile on his face as he sat down. He saw a different sort of discomfort on the old man's face, probably too much noise and crowd, not to mention being surrounded by strangers who were all very excitedly interacting. Pael could almost remember the first day he showed up at the cafe, back when Taner was leading the group.

He slipped the blue bowler off his head and sat it down. The pale blue was almost like a gumdrop on the white table. "Hello."

"Um, the lady said you were in charge?"

Pael chuckled. "In charge is difficult concept with these guys. My name is Pael da Kasin, I happen to be the one who runs these meetings. The third day of every month, right before noon."

Someone laughed and the old man cringed.

After a second, he held out his shaking hand. "My name is Largol."

No family name, how interesting, but the first didn't seem like an alias. It didn't really matter, people were called what they wanted to be at the cafe.

Pael took the firm grip and shook it before releasing it. "Welcome to the Blue Hat Club. As you can tell, most of us wear hats and they are blue."

Largol smiled uncomfortably. "It's very... obvious. In my day, drawing this much attention usually got you tossed into the military or chased out of town." His eyes grew haunted. "They didn't give you any warning either.  Just one night, you wake up in a carriage with your own family holding you at sword point."

Pael frowned. "I'm sorry."

Largol looked at the door. "That was a long time ago. I used to live here in Forest High. A long time ago. It's hard to believe you are out in the open."

"Well, some of us are. That's why we meet here in the corner though. Some of us use fake names, otherwise are more in the open. The family elders still don't like us, but if we don't cause trouble or draw attention to ourselves, they are usually willing to look the other way."

"The blue hats aren't drawing attention?"

Pael grinned broadly. "You would think that, wouldn't you? No, the hats just give a hint of something going on so they know to avoid us. Plus," he said with a wink, "most of us only wear them one day a month. It's a chance to be ourselves, even for a few hours."

"Is that what that elf and fairy thing is about? Pretending to be something?"

Pael struggled for a moment, his thoughts reminding him that he was shifting himself. "No, it was just a term. You know all the slurs, rag, bucket, sails."

Largo's face twisted in a scowl.

"So Taner pushed us to see ourselves as something beautiful."

"Trolls?" There was a look of disbelieve on Largol's face.

Unable to keep the smile on his lips, Pael smiled. "Oh, trolls are... beautiful. Large, muscles, willing to hold you tight. Most of them have those beards that I just can't get enough. Same with dwarves, fairies, and elves. There are other names some of us use, mostly from mythology. Just... a little special, a little more than our day-to-day lives. Well, like men who wear blue hats on the third day of every month."

Largol smiled. "I wish I had that."

"You are welcome to join us? We won't judge if you do."

The old man gestured to Pael's hat. "I got nothing blue."

"You don't need it. We'll welcome you simply because you asked to join us."

Largol looked nervous for a moment. He rubbed his hands together, his knuckles seemed to be swollen and red. "What if he doesn't want me."

Pael wondered if Largo meant "he" or "they," it seemed rather specific if he was addressing someone specific. Why wouldn't they accept anyone here?

"I had to leave. I didn't have a choice." Largol's eyes were growing wetter. "I kept trying to come back and then I was scared."

Something clicked for Pael, a memory of his early days at the club. Taner had lost his lover decades ago, long before the first club was created. But that loss never stopped haunting him. Pael looked curiously at Largol for a moment. Was Largol Taner's love? He said, "Do you know why Taner started this club?"

There it was, a slight jump and renewed focus.

Pael's skin grew tight as he tried to remember the exact words, the way Taner stood in front of everyone with tears in his eyes. It was an old memory for him, twelve years since the founder of the club stopped coming, but it was those moments that drove Pael to commit himself. "We stand tall to let the ones who are lost know their home. We leave our doors wide open, day or night, if they can return. We give our hearts every day to those who cannot be found."

One of the older members patted Pael's shoulder as he passed. The poem used to be how they ended their meetings.

Largol sniffed and wiped a tear from his eyes. "That sounds like one of Tan's poems. He always had a flair for the dramatics."

"He's still in town, you know."

Largol's eyes widened and he inhaled. "H-He is?"

"About twenty minutes away."

"I... no, I can't. It's been too long. He won't..."

Pael reached down and took Largol's hands gently. "Do you want me to ask?"

Largol sniffed and looked like he was on the edge of crying. "It's been forty-seven years."

"He hasn't forgotten, trust me."

"Will he really?"

"He will."

Largol let out a soft sob. He pulled his hand away to pat his pockets.

Pael handed him a handkerchief.

After a few minutes of crying, Largol finally nodded hesitantly. "I'm afraid he'll say no."

"Then I will stay here, okay?"

Largol pointed to the rest of the cafe. "Don't you have some fairies to lead?"

Pael looked over at the others. It was a few minutes past the beginning of the meeting. He looked back and then smiled. "I will be waiting over... there," he said as he pointed to the front of the room.

"T-Thank you."

Hathin was approaching as Pael got up. He leaned over. "He's on me."

"Little old for you," she said with a smirk.

"Behave. Also, can you get a message over to Tan? Tell him Largol is here at the cafe."

Hathin stopped for a moment. "Largol... him?"

Pael nodded.

She squealed quietly for a moment and then rushed over to the older man.

Shaking his head, Pael headed to the front of the room. "Good afternoon, my lovely blues!"

"Good afternoon!" came the cheerful chorus.

"Shall we start?" Pael announced as he reached the front and turned around. "We have a busy day and I think a bunch of you are looking forward to a little stroll through the flowers."

After a cheer in response, he started with the motto of the Blue Hat Club. After the first few words, everyone joined in.

"Fairy, elf, troll, and dwarf. Each one has something wonderful about them. Every smile is worth treasuring. Every kin is worth saving. Every one of us is worth loving. We may not like the same things and we may not stand the same way, but we will always be brothers when we wear blue."

As much as he wanted to see what would happen to Largol, he couldn't with his attention on the meeting. Soon, he was lost in announcements, schedules, and coming up with a theme for the masked ball at the end of the month. He loved how everyone worked together, not always peacefully, but in the end, they had each other.

The hour stretched until it finally broke. He decided to end the meeting in the old manner, the way Taner used to do it.

"Now, this is a little different, but today I was reminded that we are all here for different reasons. Some of us come to look for new friends."

He smiled at Kabel who beamed back.

"Some of us are here with our loved ones. That includes you, Farol and Dank."

The couple lifted their tea cups above their heads and there was a round of cheering.

Moving through the front window caught Pael's attention. It was an old man hurrying forward, moving with tiny little steps as he tried to smooth down his hair. His face was red, probably from moving faster than he had in days. Pael smiled at the sight of Taner being flustered but this his voice caught as he tried to speak. "And s-some were... are here because they were hoping to find someone they lost."

It was hard to breathe.

Outside, Taner toyed with the door but not quite opening it. Then he shoved his hand into his pocket and pulled out a deep blue, crushed fedora. With shaking hands, he jammed it on his head backwards.

Pael smiled and opened his mouth but the words wouldn't come out. He was crying and wasn't sure why.

Largol looked up and then turned back. HIs eyes widened.

Hathin muttered, "Oh, for the Divine Couple's sake," before she stomped through the gathered men toward the front door. They parted to the side and left Pael a clear view of the front door.

Largol was struggling to his feet.

Hathin reached the door and pushed it open. "Get in here, you old man!"

Taner opened his mouth. He looked frightened and terrified.

She held up her hand. "To your right," she said in a whisper that fooled no one.

The rest of the cafe grew quiet as the others realized there was something going on. They turned and looked curiously at the two old men who were approaching each other.

Pael choked back a sob of his own. Then, he cleared his throat. "Today, I think we should end in one of Taner's poems."

He had to wipe his tears before he spoke. "We stand tall to let the ones who are lost know their home."

Taner took a few steps into the cafe and then halted. He was a few feet taller than Largo, a former troll as he used to say it. His skin was weathered and wrinkled, his hair almost completely white. There was no question there was hope in his eyes as he stared at Largo.

"We leave our doors wide open, day or night, if they can return."

Largo stumbled forward. He gripped his cane tightly as he limped closer. "I'm sorry," he choked, his rasping words loud in the deathly quiet cafe.

Taner caught up with him in a hug. There were tears in his eyes as he held his lost lover tight. "Welcome home," was the only thing he said.

Pael took a deep breath and finished his words, "We give our hearts every day to those who cannot be found. We wait in hopes that we can be together once again."
