---
status: alpha
title: Renewed Faith
availability: subscriber-1
summary: >
  While writing forbidden words, Lutier is caught and threaten with blasphemy.
---

> When asked why he decided to leave the Divine Couple, Lutier simply stated, "it was time." --- *The Bitter Divorce of the Holy Father*

That night, close to midnight, Lutier staggered down the hall to his cell. He smelled of water and soap, the icy air plucking at the moisture that still clung to his skin. He was exhausted beyond reason and his eyes kept drooping with the desire for sleep.

"I'm sorry," he whispered to himself, "no writing tonight."

Even as he said it, he knew he would write. There was so much pain in his heart from losing Jartim that he had to put them down in words. It was the only way he could find peace to sleep.

As he walked, he could hear whispered prayers and snoring from the other cells. There were quiet gasps and moans from a few other's rooms. Everyone dealt with grief in different ways, some did it alone and others sought company in their brothers and sisters in the cold night.

His cell door creaked as he entered it. Turning around, he closed and locked it before heading for his bed. With one aching hand, he picked up the corner of his mattress.

He reached for a book that wasn't there. His heart skipped a beat as he stared down at its place. His fountain pen had been moved also. Ink stained the boards underneath the mattress and the black splotch soaked into the sheets above it.

Lutier closed his eyes. Someone else knew about the book besides Jartim. He even knew who: Marion. He knew the words on the page would enrage her, it was the thing she hated the most about him.

He set down the mattress and tried to calm his rapidly beating heart. He had many options, but couldn't think of any. He knew that she would fulfill her promise if news of his book got out. He had written it as a jdacku, a holy book, but not one for the Divine Couple. For a priest of the Couple to scribe it, it was blasphemy, but one that gave Lutier his faith.

With a shuddering breath, he set down the mattress and left his cell. She would probably be reading it in her cell. He headed down the hall and knocked on her door. When she didn't answer, he knocked again.

A few minutes passed and Lutier turned away. Despair clawed at him as he tried to imagine where she would have taken the book. He headed toward the main hall, the one he avoided after the funeral.

Marion sat at the end of the table with her back to the fireplace. A fire burned brightly in it, lighting up the plain wooden tables. It glinted off the murals and decorations of the faith: the staff, the cup, and the chain.

His book was in front of her, splaying out and exposed. One hand was balled in a fist to hold the corner down as she used her other hand to flip through the pages. Even from the opposite end of the table, he could see her eyes flashing as she scanned each page.

"Blasphemy," Marion said in a low, cracked voice. It was the voice of grief and anger.

Lutier started toward her, walking along the bench that he had eaten at for almost his entire life.

"You have turned your back on the Holy Wife and written her out of your childish fantasy."

He wanted to explain the joy he felt in writing it, the focus on the masculine, the perfection of the male form. The stories were the ones in his heart, burned clean by years of writing his ideas out. They were his faith, the one he believed in now.

Marion turned the page. As she read it, her fist tightened until her knuckles turned white. She shook as she reached the bottom of the page and shifted her gaze to the next one. "I knew about this, but I never thought you'd betray us so completely with this... this blasphemy. These vile words."

Lutier stopped a few feet from her. "Jartim---"

Her head snapped up. "Jartim should have never given this to you!" Her shrill voice echoed against the walls. The anger and rage almost burned the air around her. "These are not the divine words!"

"These are my words."

Marion jumped to her feet. The page in her hand was torn free and Lutier felt a bit of himself scream out in agony. Balling it up, she brandished it in front of him. "You are not worthy to write these cursed words! These are not the words of love, of faith!"

"They are," Lutier insisted. He fought the urge to lash out, reminding himself of the very stories he wrote in the book.

"This is sacrilege!" With a snarl, she turned and threw the balled paper into the fire.

Lutier gasped and surged forward, trying to stop her.

Marion knocked his hand away and tore a handful of pages from the book. Shoving him aside, she threw them into the fire. The fluttering paper quickly caught aflame and danced in the ashed. "I will not allow you to desecrate my church with your poisonous words!"

He grabbed at the book, but she knocked him aside.

With a scream, she turned and threw the entire tome into the fire.

Lutier didn't have time to think. He dove into the fire, ignoring the agony that burst along his skin. He clutched at the book. Embers seared at his skin and the stench of burning flesh filled his nostrils around the choking heat that tore at his lungs. He grabbed the book, shaking as he tried to pull it out.

The heat and light made it hard and he slammed against the searing hot stone of the hearth. It took him two more tries before he staggered out of the fireplace, holding the still burning book to his chest. Flames licked at his cassock and he fought to remain conscious with agony coursing along his veins.

He beat the flames from the book first, flailing at Marion's arms when she tried to stop him.

His burning clothes continued to lick at his skin, blackening flesh and leaving him sick. He left his book long enough to grab a bucket of water and dumped it over his head. The icy liquid almost overwhelmed him as it poured down over his burns and across his ruined clothes.

Lutier shook his head to clear it and saw that Marion struggled to pick up his book. Desperate to save it, he lashed out with the only thing in his hand.

The bucket caught the side of Marion's head and threw her aside. She flew back a few feet before slamming against the hearthstone with a sickening crunch. Her eyes rolled up in her head as she slid down, leaving a smear of blood against the rough stones.

Lutier shook as he stood there, staring at her body. He had to strain to focus past the tears in his eyes, but when he saw her chest rising and falling in labored breaths, he let out a sob of relief.

And then the realization of his situation struck him. If she lived, he would be excommunicated from the church. He knew the words were blasphemy but they were still his words, his faith.

Blood pooled underneath her, soaking into her yellow cassock.

He glanced at the book. It was a third gone, the edges blackened from the flames. Years of his writing destroyed in a matter of moments.

He took a breath and then another. He had written the stories. Stories of the Holy Father and how he took care of the Wife. Of the bravery and intelligence of the masculine over the feminine.

Lutier closed his eyes for a long moment and then opened them. Ignoring his book for a moment, he knelt down and pulled Marion from near the fire. Ripping his own cassock open and then into strips, he bound her head quickly.

Marion's eyes fluttered open and babble slipped out of her mouth. It was incomprehensible and tortured, filled with pain and sorrow.

"I'm sorry," he said and meant it.

As soon as he was sure she wouldn't die immediately, he grabbed the burnt book and rushed back to the cells. There was still a light under one of the doors and he pounded on it. "Open up!"

The sister inside opened it after only a few seconds, her shapeless tunic hanging off her shoulders. She took one look at Lutier and gasped. "You're burned!"

"Mother Marion needs help!"

"You need---"

"Help the mother! She's in the great hall. She hurt---" He froze for a heartbeat, unsure of what to say. Finally, he finished. "She is hurt badly and needs help. Please! Please help her!"

It was the right thing to do, to make sure Marion was safe. He wrote something in his book about the same thing.

But Lutier couldn't stay. When Marion woke up or when they found the book, it was over for him. He had to run. He glanced down the hall at his cell. There were other brothers and sisters peeking out of their rooms. Some of them were already dressed. There was no way Lutier could get to his cell to change clothes without looking suspicious.

He had to run.

With a deep breath, he yelled out. "Mother Marion is hurt in the great hall!"

They came out of their rooms and rushed down the hall. He let them push him along, dragging him toward the great hall and away from his cell. As soon as he could, he slipped aside and headed straight for the front doors with his book pressed to his chest.

It was icy cold outside and he had nowhere to go. But he couldn't stay there. If he had any faith at all, it would carry him to safety.
