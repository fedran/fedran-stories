---
status: alpha
title: Renewed Faith
availability: subscriber-1
summary: >
  Punished and charged with with recovering all of the books before he could resume writing, Lutier finds himself lashing out at his fellow priests. However, he gets an unexpected visitor with hope.
---

> While Lutier was the man who lead to the creation of a religion without the mother, there were others who quietly enabled him from the shadows. --- *The Bitter Divorce of the Holy Father*

Lutier slammed the door of his cell and threw himself on his cot. The wooden frame creaked with his weight. He rolled over on his back and tried to calm his rapidly beating heart and the fury that pulsed his thoughts.

It was the third fight in less than a week. This one had started as a debate over one of the scriptures that Lutier didn't care for. It detailed some of the events of the Holy Wife in her aspect as the Guiding Mother. He avoided the Wife's scriptures whenever possible, but it was either the overly hot summer air or the hours hauling garbage that caused him to join in.

Tempers and voices had risen rapidly until Marion and Jartim broke it up.

Lutier sighed and draped one arm over his face to block out the light streaming in from the window, bringing with it the sweltering heat of summer. He was already hot from hauling and the increased temperature only made each passing second worse.

A knock at the door interrupted his thoughts.

Sitting up, he pulled his sweaty tunic from his chest. "Enter."

The door creaked open and Jartim slipped in. He closed the door behind him. "Lut."

"Father."

"Hard day?"

Lutier nodded and then gestured for Jartim to take the stool by a narrow table near the door.

Jartim sat down. "That's been happening a while now, you know."

"I'm sorry, father. I just... I don't know why. I try to avoid those."

"How long has it been since you wrote?"

Lutier jerked at the question. He glanced at a wooden calendar hanging on the wall but didn't need to read it to know the answer. "Four months, seven days."

A sad smile. "How long before you get to start again?"

Lutier's thoughts darkened. "Still waiting for two more books to be retrieved. Both are in private collections, one by Tiosuf dea Forilid and the other anonymously by someone in Gepaul."

"I'm sorry, my son. There are times when our tenant of waiting for resolution before the punishment can be frustrating, but this is but the Couple's will. I know this is hard but you must be patient."

Lutier clutched his blankets. He wanted to write, ached to have the pen in his hand. He tried to promise that he would copy everything exactly, but Marion refused. It didn't matter if it was a *jdacku*, a scripture, or even a verse, he wasn't allowed to write anything.

"But, the greatest faith comes from struggles. You know that one, right?"

"Yes, father."

"Oh, don't give me that sullen tone, Lut."

Lutier nodded. "I'm sorry. I've just been on edge lately. For most of my life, I've been here at the abbey, scribing books and writing. It was my first job and had been for sixteen years. And then... I don't know what happened, but I made a mistake. It doesn't make it any easier to accept my punishment."

"Like when the Husband came to the Wife and begged for forgiveness?"

Lutier pulled a face. "I don't like that one."

"No, no, you wouldn't." Jartim chuckled. "But, it is applicable, isn't it?"

"Mother Marion wouldn't accept my forgiveness."

"Maybe she would accept mine instead?"

Lutier looked up, confused.

Jartim stood up and opened the door long enough to pick up a bag and drag it into the room. He hefted it and handed it over to Lutier.

When Lutier took it, he knew exactly what it was: a tome. He opened the bag to find a red-leather bound book with a blessed paper stripped around it. It was an empty jdacku, blessed by the priests and ready to take the words of the Divine Couple. Normally, the jdacku were orange, to represent the blending of the red masculine and the yellow feminine, but this one matched the red of the Holy Husband.

He felt a surge of joy welling inside him. With trembling hands, he ran his fingers over the plain cover and along the paper that sealed it. It already had paper loosely set inside the covers; when it was finished, it would be bound together and blessed once again.

"Write, Lut. In here, in your room."

Lutier looked up, tears threatening his eyes. "What do I copy?"

Jartim shook his head and smiled. "You have stories in your head. I see them in the images you made, when your mind is wandering. Write those."

"But, Marion says they were blasphemy."

"You honor the Divine Couple with your skills regardless of the topic. Write what is in your heart and get them on paper. We can look at it when the book is done and figure out if your heart is still with us." Jartim stood up. "Put everything on the page, my son, your heart and your faith. Your words will tell the story when you are done."

Lutier clutched the book to his chest. "T-Thank you, father."

Jartim stood up and came over to Lutier with his arms open.

Lutier stood up, not willing to let go of the book, but Jartim hugged both of them.

"Have faith, Lut. I believe in you."

Jartim left Lutier alone, closing the door behind him with one last warning. "Lock the door when you write, hide the book until you are ready."

Lutier locked the door and set down the book on his table. He let out a long gasp as he caressed the book and then broke the blessed seal. A few minutes later, he was on the first page with his fountain pen ready.

He wasn't sure what to write. He had spent his entire life being told to copy something exactly. He didn't know if he had any stories in him, but then he remembered that he had drawn something. He just needed to write.

The pen set down on the page, a splatter of ink.

Lutier started with a story that started like the scripture did but he already knew how it was going to change.

***

It was the third day of Dobmahin when Jartim died at the age of seventy-eight. Lutier was one of the three men who dug the grave, breaking the frozen earth with fire magic and a pair of shovels. The back-breaking work left him aching from shoulder to heels, but it was the last gift he could give a man who gave him his life back.

Now, they stood at the end of the day as Marion spoke the last of her prayers over Jartim's body. It was a cold and windy day. The sharp edge of wind buffeted past his cassock and his teeth chattered. Only the promise of hot coffee inside kept most of the priests and priestesses standing place as the ritual droned on.

Lutier retreated into his thoughts, knowing the prayer by heart. He regretted not working faster on the book Jartim gave him six years ago. He was almost done, only thirty or so more pages before it was completely filled. Thousands of illustrations, hundreds of hours drawing and writing. It was Lutier's proudest work and only one other man had known it existed.

The book had renewed hope for Lutier and he found a renewed faith in the Holy Husband in it. When he wasn't forced to include the Wife in every scene, illustration, and page, he found it nestled deep in his heart. The Wife was still there, she had to be for his version of the tales, but his book was only about the Husband. When he wrote, joy filled him as more of the ideas flowed out. He loved filling each page with neat writing and detailed drawings.

One more week and he would have been done. He had just finished binding the pages into the book, working late in the night with needle, thread, and glue. If Jartim survived only a little longer, he would have seen it completed.

A tear froze against Lutier's cheek. Why couldn't Jartim held on for a little longer?

Marion finished her prayer. It would be one of the last ones as a high priestess of the abbey. Only married couples were allowed to run every church and abbey, a couple because no man or woman was able to do it on their own. She bowed her head as tears splashed down; Jartim had been her husband for more years than Lutier had lived.

One by one, the brothers and sisters of the abbey walked to her, got a quick blessing from Marion, and then headed into the abbey to warm up.

Lutier was near the end and he hesitated when it was his turn. His shoes crunched in the snow as he walked up to her and held out his hands to grasp hers.

Marion look at him and the ever-present anger remained in her gaze. She didn't take his hand. Instead, she leaned closer and whispered, her breath fogging against his face. "If it is the last thing I do as the mother of this abbey, I will see you removed if not executed for blasphemy."

One sentence and then she turned away from him.

Lutier looked down at his hands, stained with ink from his nightly scribing. He knew that she was just lashing out in grief, they had scriptures about it, but it didn't make the pain any less. He loved Jartim as his father, maybe as much as she loved him as her husband.

He bowed his head. "Thank you, mother."

She jerked at his words.

Lutier turned and headed to the door. Instead of going inside, he picked up the shovel and waited for the others to go inside. He had one more thing to do for his father.
