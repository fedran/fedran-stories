---
status: alpha
title: Renewed Faith
availability: subscriber-1
summary: >
  Brother Lutier had been manually copying manuscripts from memory for years. However, as his attitude toward the Divine Couple had changed, in specific his disenchantment with the Blessed Mother, the books he transcribe were deviating from the originals.
---

> The origins of the Lutian Church are mired in history, but it is well-documented that it originated from the worship of the Divine Couple. --- *The Bitter Divorce of the Holy Father*

Brother Lutier managed to keep himself awake by propping his head up on his right wrist. His sagging cheek against his sticky palm prevented his jaw from slipping off and striking the table. He yawned, but managed to keep his pen smoothly writing along the neat line that bisected the page.

Above his fountain pen, there was a solid block of text copied from memory; below, cream-colored space ready to fill in. Even though there were no letters on it, he knew exactly where the letters would go and where he needed to leave space for the illustrations. He imagined the images in great detail, drawing them with his mental pen even as he finished the line with a small flourish and moved to the next.

"Lut. Lutier." It was Father Jartim, the head priest at the abbey. The older man's voice was a low rumble that spoke of annoyance. Lutier had heard the tone many times in the decades and it never bode well for him.

Lutier looked up without moving his head from his hand.

Jartim was past his seventies with short, close-cropped hair. He wore the traditional cassock. The deep red color was almost black in the basement room, but Lutier knew the shade by heart. "Posture," said the older man.

Blinking slowly, Lutier glanced back down at the page. He was still writing and the line of text remained as straight as if he was watching his work. He blinked again and looked back up.

Jartim sighed and pulled out a stool from an empty desk and sat down heavily on it. The seat creaked from his movement. "Lut, please. I need to talk about some things."

Lutier groaned and levered himself up, his hand still writing neatly as he did.

"Please stop writing."

Reluctantly, Lutier, picked up the pen and held it over the small dish used to catch drippings. He wiped it twice against a sponge to clean the tip and then carefully pulled it apart, making sure every piece was in its place. The fountain pen was expensive and Lutier's only possession besides his clothes.

Jartim remained quiet as Lutier finished. As soon as Lutier stopped, he cleared his throat. "There have been problems with your copies."

Lutier tensed. "There is nothing wrong. My lettering is perfect."

"Yes, your lettering is without equal and most of your illustrations are flawless as well."

Lutier sat up straighter. "Most? I made no mistakes."

Jartim leaned over and picked up the top of a stack of leather-bound books.

Lutier hadn't notice the pile before, but he suspected they were the copies he had been making for the last few months. After he finished his stack of pages, they would be bound by the other brothers in the abbey.

The older man thumbed along the top where there were dozens of pieces of paper sticking out. Finding one, he cracked open the book, rotated it, and set it against the edge of Lutier's desk.

Lutier glanced at the page, his eyes looking for flaws in the text.

"The illustration," came the prompt.

There were many illustrations on the page. Like most of the holy books, it had drawings covering every inch that didn't have a letter: there were tiny figures sitting in the line of text to indicate paragraphs, larger ones in the margins, and even a large spot in the bottom of the page for a more detailed scene.

Lutier didn't remember the drawings, he had copied thousands over the days, but he spotted nothing wrong with any of them. They were perfect. He looked up and said so.

Jartim gestured to the desk. "Where is the original?"

It took Lutier a moment to remember where he put it. He had memorized the entire book years ago and drew from memory. It gave him more room to rest his elbow while he worked. With a groan, he picked up the stack of loose pages he had been illustrating and set them aside. Then, he reached under his desk and pulled out the dusty original. He had forgotten how heavy it was and almost dropped it on the ground.

With a grunt, he started to set it down, but then realized his pen was in the way.

Jartim moved the pen case out of the way so Lutier could set it down.

With a dramatic sigh, Lutier flipped through the book until the two were at the same page. He compared the two using one finger above each one as he inspected the tiny images and margin notes for differences.

"At the bottom. The big one."

Lutier glared briefly at him and focused his attention on the large illustration on the bottom. It took him a moment to realize there was something missing.

Like most illustrations of the Divine Couple, a man and a woman dominated the picture. The page focused on events before the marriage, so neither of them wore a marriage bracelet. The couple were running away from a pack of wolf-like creatures with too-large teeth and massive paws.

On Lutier's copy, he had drawn the man perfectly but skipped the woman entirely. In her place was a couple of birds and nothing else. He had even continued the ground, effectively erasing the woman entirely. To his surprise, he had matched the style of the illustration perfectly.

"Notice anything missing, Lut?"

An icy cold ran through Lutier's body. He stared at the two pages, identical except for one omission. He knew the man he had drawn on the page, the details were perfect in shape, form, and detail.

He knew the missing woman didn't belong there. In the story, she was the one who disturbed the pack. If it wasn't for her, then the Divine Father would have made friends with the creatures. Lutier knew the stories by heart after writing them for so many years, but he never thought he would have missed anything as critical in the illustration.

Without a word, he flipped to the next bookmarked page. This one was of the Holy Husband in a pit. The original had the Wife reaching down for him; it was the Husband's time to make the mistake and the story showed how the Wife had turned around to rescue him. In Lutier's image, there were roots and vines that snaked down, handholds for the Husband. At the top was one of the wolf creatures.

He flipped to the next bookmarked page and then to the one after that. Each illustration continued a different story than the text, one where the Husband made friends with the various creatures.

Lutier also noticed that he had been replacing the female figures between the paragraphs and in the margins. It was a subtle change, but there were many of them once it was pointed out to him.

Slowly, Lutier looked up. "How long?"

"About a year, but we aren't sure. We had a copy of one of your books from the beginning of last winter. There were no mistakes. But we spotted a change in the one you finished a few weeks later. You are fast and capable scribe, so we think about forty books need to be retrieved and reviewed."

Lutier tensed but said nothing.

"The earlier ones had only a few changes, mostly in the paragraph marks, but that one," Jartim gestured to the one on the desk, "has the most alterations."

Lutier sighed and closed both books. He didn't look up, he could already picture the quiet fury in the old man's face. Jartim didn't turn purple with rage but the muscles in his neck tightened and his lips pressed into a thin line.

"This is unacceptable, Lut. We can't have a *cesnunspe jdacku* that doesn't have the Holy Wife in it. Can we?"

Lutier shook his head. "No, sir."

Jartim sighed. "This isn't good, Lut. We have to recall every book you've made to inspect them. Do you know how that will make this abbey look? The strike against our reputation could take decades to recover from."

"Sorry, sir." Lutier struggled to figure out if he was really sorry. The images on the page felt right. He knew the stories and tales. The Holy Husband would be far better without the Wife.

The door to the basement slammed open and the sound of stomping feet filled the air. Both Lutier and Jartim rolled their eyes and gave each other uneasy smiles as the approaching person burst into the room. It was the high priestess, Marion. She was a short woman with a scowl etched into her wrinkled face. Her bright yellow cassock was frayed on the edge and the hem by her right hand was stained from breakfast.

"Damn it, Lutier!" she started before she finished entering the room. "What is wrong with you! There are two damn...."

Her voice trailed off as she glared at Jartim through a pair of thick-rimmed glasses. "You said you'd wait."

Jartim shrugged. "I wanted a quiet word first before you started screaming."

"I'm screaming because he blasphemed!"

Lutier flinched.

"He made a mistake, my wife." Like all high priests and priestesses, Marion and Jartim were married. It was a tense relationship, picked by a council instead of founded with love, but they had fulfilled their duties to the church and abbey for many years.

"Don't give me that crap, Jartim! He desecrated the *jdacku* and you know it. Our reputation as scribes will never recover." Marion stepped forward, face red and hands balled into fists.

Jartim stood up. He was a foot taller than his wife. "It will recover. We'll just get---"

"You are too damn tolerant over his mistakes. He should be kicked out! Now!"

"The Holy Husband made many mistakes himself."

"Don't you dare use that!"

"Why not?" Jartim gestured to the books. "We know that both the Husband and the Wife are flawed. They made mistakes, they came back. Forgiveness is one of the five virtues, my dear."

Marion's face darkened even more. She glared at Lutier who flinched at the fury in her expression.

Lutier knew he was wrong, though he didn't remember consciously changing the drawings. The quiet part in his head wasn't ashamed for it either. The stories were in his head, the images that he drew came from his heart, not from malice or hatred. He bowed his head and waited.

She grumbled in the back of her throat, a grunting noise that Lutier always hated. "Will you accept my petition to have Brother Lutier removed?"

"Not from this abbey and not from this religion. Baring this one mistake---"

"A mistake made repeated over a year!"

"---he has been one of this abbey's greatest assets."

Silence plunged into the room. Lutier didn't dare look up as he waited.

"Fine," Marion broke it with a snap, "will you accept that he should be punished."

"Of course, my wife."

"No more scribing for ten years."

Lutier's breath caught in his throat. He clutched his short table for balance as tears swam in his eyes.

"Half a year after all of the books are accounted for is more than sufficient." Jartim's voice never wavered.

"One year."

"One year."

Lutier held on the table and nodded. "Yes, father."

Marion growled.

He quickly amended himself. "... and mother."
