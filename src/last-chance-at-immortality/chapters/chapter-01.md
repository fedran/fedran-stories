---
title: Last Chance at Immortality
contentWarning: >
  Some themes that appear in this story: body horror, death of children, death of named and unnamed characters, graphic injuries, and isolation."
summary: >
  Gadoral was an inventor of death. His enchanted and mechanical devices were famed for slaughtering gladiators and murdering rebels. But, after years of brutality, he had lost everything and his final hours were coming to an end.
date: 2020-01-24
---

> The true character of a man is revealed when Death holds their hand.  --- Sorgat Hakil-Mas

Gadoral groaned as he limped down the stairs. His knees ached and he could feel the muscles in his back spasm from the effort. He had to focus on his cane, to make sure it landed firmly on each step in case he slipped and fell. He couldn't take another tumble, not after losing his left hand from a festering wound.

Keeping his workshop in the basement seemed like a good idea when he was twenty-four. It was out of the way and he didn't have to worry about his children stumbling into charged crystals or sharp blades.

They were gone now, one dead and one fled. He was a terrible father to both of them but after thirty years, he still wasn't sure if seeing the light fading from his daughter's eyes when she was crushed to death or his son's swearing as he left everything behind to escape.

It didn't matter, both haunted his nights along with the thousands other memories of death, destruction, and despair.

He had a miserable life creating killing machines.

Gadoral almost missed a step. He pawed at the railing with his missing hand, the ragged stump of his arm scraping against the splintered wood. Only a luck catch of his cane caught him from falling.

Forcing himself to focus, he hobbled down the remaining fifteen stairs and to the stone floor of his basement. The icy ground was a relief, even through his slippers.

Trying not to think about the painful trip back up, he hoped his bladder would hold and headed to the back storage room. The dust-covered wine bottles towered over him. They were a testament of the days when warlords paid him thousands of jems for their pitiful games and petty wars.

In the furthest aisle of wine long since soured, he grabbed one of the few bottles wiped clean. It didn't move.

He frowned and tried again, balancing on his cane as he bore all of his weight down on the bottle.

It finally shifted. Somewhere behind the shelves, gears and rods rotated. There was a rumble and a creak and then the bottle jumped his hand.

He slipped off it and fell forward, smacking his face dangerously close to the end of a bottle. The impact bruised the bone over his eye. Gasping, he grasped for something to pull himself up. His hands only caught bottles that slipped out of their little hooks, spilling to the floor in cracks of glass and splashes of soured wine.

Gadoral slid down, his knees failing. In desperation, he jammed his hand into the curved wood that held a broken bottle. Glass dug into his skin but it stopped his plummet long enough for him to regain his composure and grab a safer spot.

Crimson dripped off the glass and he regarded it with a sinking sensation. He may not be able to make it back up the stairs.

With a grunt, he carefully stepped over the broken bottles and leaned against the railing for balance. It took a moment for his blood-soaked hands to grab his handkerchief from his pocket. The white fabric turned red in seconds.

Gadoral wrapped the fabric over his injuries and tied it as tightly as he could. The sight of the torn flesh sickened him, but he had seen worse over the years. It just meant that he only had one more chance to succeed.

With death's fingers wrapped around his heart, he reached up and grabbed the lever bottle. His wet fingers slipped but he managed to catch on the end and bore down with all his weight.

The gears finished turning and a stone door swung open to reveal a small workshop hidden inside.  It was filled with swords, cutting blades, and hammers. Thousands of gears hung from hooks along with pipes, tubing, and other devices. His magical supplies were on the far wall, meticulously categorized in a manner he no longer remembered. In the center, covered in a thick layer of dust, was his old anvil. Between it and the cold furnace, he had crafted so many machines over the years.

Yet, for all his skill, he was still going to die.

Gadoral stepped over the shattered bottles and carefully stepped inside. He had to lean on the anvil to turn around. The back of the door had four iron rods that would seal it in place.

He still remembered the day he designed them. At the time, what he was doing was illegal and he imagined sealing himself in his workshop to survive a siege. There was enough materials around him that he could built the ultimate suit of armor to fight his way out.

Back then, he didn't think about silly things like food, water, or an actual place to sleep. It was a good thing that the standoff never happened, they would have found his corpse when they finally broke in.

Gadoral smiled grimly and hobbled over to the door. He pulled the lever that closed it. As soon as it settled into place, he pulled the other ones that would cause the four iron rods to slide into place, locking the door.

He couldn't remember why he wanted to go downstairs in the first place but now he was committed. If he didn't do anything, he was going to die. There wasn't much to work with, he didn't have the strength to forge anything nor the ability to even hammer anything into place. Whatever he did would require the materials he had stored over the years.

Gadoral started with a suit of armor. Heavily riveted, it was going to be basis for a blade-festooned suit of armor. It was green from the base coating of paint but he never got around to the final layers of glossy black.

Too late now.

He managed to find a pair of old welding wands. A tiny inscription on the end of a metal stick was good for melting metal together but they didn't last long, only a few seconds at most.

He put them aside and began to work on the innards. As he gathered supplies and items and cobbled them together, he could feel death's fingers digging deeper into his heart. Blood smeared across everything as he summoned the last of his strength to screw the mechanisms that would move the arms and legs, a collection of runes and crystals in the helm and gauntlets to give it senses, and even iron ribbing from an old corset to shield it from resonance. Each step brought back more memories though he was working haphazardly.

Gadoral woke up with a start and realized that he had passed out. His chest ached and all the joints on his side were burning. He flailed helplessly until he could pull himself to his knees.

There was a puddle of blood smeared across the ground.

Patting himself, he found that he had cut open the side of his head. He had no idea how long he was out but the blood had started to set.

Dizzy and exhausted, he threw himself back into work. He only had a vague idea of what he was building. It was obviously a killing machine, that was the only thing he knew how to build, but the innards defied his cursory inspection. His gut feeling said that it would work as an artifact but his fading logic insisted that it was doomed to failure.

Gadoral didn't figure it out until he woke up the third time from passing out. Sick with hunger and thirst, he reached for the final part: a large crystal that a telepath said was used to create a *kotim*, a store of memories and personalities that the vomen used when they traveled away from their home isle.

The purpose of his final efforts became obvious. He was trying to create a shell for himself, a body that wasn't broken and bleeding. A second chance for an old man who had made so many mistakes.

The only problem was the crystal itself. He had tried to use the kotim crystal for years, but never managed to get it to do anything. He couldn't create an independent golem or a mechanical suit. There was nothing to drive it, nothing that happened when he integrated it into his design. It did nothing for the decades he tried to use it until he finally tossed it aside for the hard reliability of magic and steam.

There was one thing he had never tried, to put his own mind into the crystal. Maybe that was what he needed. He was desperate enough to try it.

As Gadoral stared at the crystal, he wished he could remember how to speak to say something to comfort him. It had been months since anyone had visited him. He paid someone to delivery food but they never stayed and spoke.

The world grew darker around him. He swayed and reached for the table, only to remember that he had lost his arm.

His head caught the side of the table. Black stars exploded across his vision as he slammed into the ground. The muted crack of bone gave him only a few moments warning before sharp pains exploded from his injuries.

Gadoral slumped back, smacking his head against the stone ground. He was so close.

His eyes fluttered.

No, he wasn't. It was a foolish idea of a dying old man. He had ruined so many lives, now it was time for him to let Death finish his collection.

The world went dark.

No, he couldn't give up.

He forced his eyes open. The stench of blood and feces was strong in the room but he didn't care. His blurry vision focused on the almost assembled armor on the table next to him. It took him a moment to realize his left eye was filled with blood, the reddish haze coloring everything.

Gadoral groaned as he reached up. He still had the kotim crystal in his hand. One sharp edge dug into his palm and blood caked along the wound. He tightened his grip to prevent it from opening up and used his entire fist to catch on a shelf and pull himself up.

Agony ripped across his senses but he let out a strangled yell as he forced himself up to his knees and then hauled his body up against the table. Panting with agony, he shoved the bloody crystal into the chest of the armor until it seated into place.

Using the last of the welding wands, he shoved his hand in and channeled his fading energy into sealing it into place. His mind grew hazy but he chewed on his tongue to use the pain to keep him focused.

When he finished, he could barely breathe. Blood covered everything, soaking into the green armor and puddling into the gaps between machine and magic.

Gadoral froze. He didn't know what to do next. He wasn't a telepath, he wasn't sure how to activate it.

His heart skipped a beat, a brief moment where there was nothing but agonizing silence in his chest.

He groaned and concentrated on the sharp edges in his palm. He couldn't think of anything other than his devices. Each memory rose up, crystal clear as if the years were no longer blurring them. He focused on one memory and then the other, remembering how his daughter died, the horror of losing his son because of his poor choices, and the countless battles that were waged using his instruments of destruction.

Gadoral woke up in a puddle of blood. He tried to pull himself up, but only sharp pain rewarded his efforts. It took all of his effort to turn his head to see the exposed bone of his arm sticking out of the bloody wound.

He was done for.

Metal creaked.

With his dying effort, he turned his head up to see the suit of green metal and dried blood stretch and reach out. It set down one foot and then the other, angling itself unnaturally to slide off the table to land heavily on the ground.

There was the briefest of hope in Gadoral's aching chest.

The armor rotated its head until it was facing forward again. Arms twisted and moved, first stretching and then coming down. It looked down with black holes for eyes. It looked like Death himself.

Gadoral felt no fear as the armor knelt down. There was a crunch as it landed on his leg. The weight shattered bone.

His vision blurred in agony and his mouth opened helplessly but no sound came out.

The armor leaned over him, silent except for the creak of metal and the rhythmic thudding of mechanisms inside. Magic sparked and hissed inside the shell, the hollow sound almost sounding like a voice. "You. Are. Me."

Gadoral looked up, his vision barely able to take in the armor looming over him.

"Memories. Thoughts. You gave. I take. I remember." The voice faded in and out but he couldn't tell if it was his dying body or the armor's voice. It could also be the imagination of his death throes.

He smiled. He wasn't sure, but he thought he succeeded.

The armor's gauntlet slammed into his chest, shattering bone and crushing his heart. The stone underneath cracked from the force of the blow. "You. Done."

The smile froze on Gadoral's face.

Success or failure, it didn't matter anymore.

Death had collected.
