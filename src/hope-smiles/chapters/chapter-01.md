---
title: Hope Smiles
format: Flash
genres: [Slice of Life]
summary: >
  Hapin pined for a human woman. It didn't matter that he was a slave and a dalpre, he could dream.  
---

He couldn't stop staring at her, he didn't want to stop. Even when he let his ears flop over his eyes, blurring them with the long hairs, he couldn't help but watch her backside as she strode down the lines of freshly planted seeds.

It wouldn't work, he told himself. He was a dalpre and she was human. A slave and a free woman. It never would work out.

He still could dream, weeding and planting.

She looked back and their eyes met.

She smiled and slowly turned away.
