---
status: wip
title: Night Arrows
availability: private
---

Kiks leaned against the flagpole on top of the guard tower and fought the urge to yawn. She was nine hours into her shift and she couldn't wait for the last hour to pass and the sun to rise. Then, she could trudge back to the barracks to sleep while everyone else toiled during the day.

She smiled at the thought of a warm bed and falling asleep to the sound of

Before she realized it was coming, a yawn slammed into her. It cracked her jaw from the effort but it did little to push aside the exhaustion that hazed her thoughts. She blinked and struggled to open her eyes again; she wasn't going to get caught sleeping on patrol like the last solider did. It took a month for the poor *pabilni* to stop limping and he was still cleaning out the latrines.

She blinked and focused on the horizon. Even though it was night, she wasn't blind. Thanks to her talent, she saw the world around her as a living painting: swirls of green and brown paint fluttered along the trees that lined the roads leading up to the tower, an angry slashes of red marked the flags of the other guard towers, and a thick swirl of colors for the nearby cities and towns. Even the wind was visible as brushstrokes, she could tell the speed and direction of the individual puffs and whirls.

Kiks tigthened her grip on her bow and turned slowly while keeping her shoulder pinned against the flag pole. She spotted movement along the trees. Curious, she concentrated and the image redrew itself in a large image, painting out a brown-feathered owl sailing through the trees.

She relaxed and the painted image peeled back until she was once again watching the landscape surrounding her. The vibrant colors were more than she what she could see during the day and she wished the painted image would never fade.

"How are you doing up here, Kiks?" It was Dimar, the other *pabilni* assigned to the tower. He finished crawling up the ladder and stood up. His bow bobbed with his movements, tapping lightly on the rungs before he stepped away.

In the dark, she saw him as a painting that highlighted his broad shoulders and muscular form. His eyes were dark and intense, almost black. The leather of his uniform creaked as he straightened and swung his bow off his shoulder and held it lightly in his left hand.

Even though they were both archers, their bows were very different from each other. Dimar's was short and thick, with a pull that would strain a horse. He used it to fire thick arrows capable of punching through armor and even the side of a wagon.

Kiks', on the other hand, was a long bow with almost as strong of a pull but designed for distance and accuracy instead of power. Tiny runes glowed faintly along the wood and a matching set lined each of the narrow-headed arrows that filled her quiver.
She nodded to him. "I'm here," she said in a low voice.

Dimar turned toward her and bowed with just a hint of mocking. He couldn't see in the dark like her, but he had a supernatural hearing that rivaled her own abilities at night. It made both archers well-suited toward night patrol.

She yawned again and turned back to look around.

"See anything?"

"No, hear anything of note?"

Dimar cocked his head. "The other *pabilni* downstairs is snoring. I threw a blanket on her and closed the door so Lakton wouldn't find her. Not that he would, he's in the middle with a screaming fight with his wife."

"Which one got caught sleeping around? Him or his wife?"

"Their eldest son with the *cijatna's* two daughters."

Kiks cringed. "Ouch, Lakton is going to be in a foul mood.

Dimar sighed and tapped the edge of the platform. "Just another day in the corps, right?"

She chuckled. "Only thirty-nine more to go."

"Can't believe you want to go ten rounds here."

"No where---" Kiks yawned again. "---else to go. I don't have a family or a home anymore, only the corps."

"You could sleep---"

"No," snapped Kiks.

POINT

with his wife."

She rolled her eyes. "Lakton is always fighting with her. The man sleeps around enough for the entire *rekiho*. Of course, so does his wife, so I guess it's only fair."

"I guess. Heading back down?"

She shrugged and looked back across the surrounding land, scanning for movement.

Dimar stepped up on the edge of the platform and sat down with his feet dangling over the edge. "I hate these quiet nights. The only thing
"The only thing fun I can hear is Laston






but it was far different than Kiks. Where she had a tall bow with a strong pull, he had a squat bow capable of firing powerful arrows

but when she concentrated on it, she could tell that it was just an owl sailing through the air. She relaxed and the painted image peeled back until she could see


other guard towers, and she could

night as a moving painting. The flutter of leaves along the road leading up to the

see darkness. Instead, it was as if the world had been painted in stark colors.

Months ago, when she was first assigned the night

Months ago, when she was first assigned the night patrol, it sounded like an easy duty. All she had to do was stand on top of a tower and watch the horizon.


scanned the horizon.


When the yawn came, there was nothing Kiks could do to stop it. She exhaled and let it take over. Her jaw popped as she opened it wide and drew in a deep breath of air into her lungs. She held it there for a moment and then released it.

The fresh air did little to push back the exhaustion that hazed her thoughts. She was coming up to the end of her ten hour shift

Kiks felt the yawn coming and tried to hold it back. Her grip on her bow tightened until her knuckles popped, but even the discomfort wasn't enough to prevent it. She leaned into her yawn, pressing her shoulder against the warm stone of the guard post as she did.

The fresh air did little to push back the exhaustion that plucked at her senses. It had been a long post on guard duty and she still had another hour before she could crawl into her cot and get a full night's sleep.
