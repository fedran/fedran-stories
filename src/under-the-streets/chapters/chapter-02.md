---
title: Later
summary: >
  With the young ones rescued, Wathin comes to Mudd asking for forgiveness. Or at least not to report the illegal event to the chief of the guards.
---

> One of the unwritten laws of guards and criminals is that the fight ends before family. To include a child is to risk more than death, it is to open up an unstoppable flood of destruction. --- *Queen of the Sewers* (Act 3, Scene 4)

A half hour later, Mudd watched the red carriage holding Tam and Sarah head toward the nearest temple. The girl would survive, but only because of the yellow-green healing that surrounded her body. Her attackers had pierced her shoulder and stomach, the gut wound would have killed her sooner than later. The sewage probably poisoned her and he wouldn't be surprise if she was scarred for life.

Minir sobbed against Nerin's shoulder as the compact woman held her tight. The silver badge sparkled in the light as Nerin rocked back and forth while whispering to her. Minir's fur covered most of her body, but the cream-color was almost invisible underneath the thick layer of muck. Her stained mittens hung off her wrists, exposing her tiny claws.

Mudd glanced down at his own clothes. They were ruined with the stench of sewage wafting around him. His gloves were ripped from helping Tam pull out the block before it crushed the two girls.

He shook his head and looked around. When he spotted a flickering light from around one corner, he headed toward it.

Wathin sat on a barrel, his head bowed low. His spear rested against the wall behind him, scorching the plaster.

Mudd dragged a box and sat down.

"When are you going to tell the chief?" asked Wathin.

"About what?"

"You already figured it out, haven't you? You said you can't help it."

Mudd shrugged and peeled off his gloves. He grabbed a bag from his side and shoved them inside. His clothes would join them as soon as he got home; the long ride home would be agony for him. Even though he wasn't on duty, the idea that he would taint evidence haunted every waking moment.

"Captain?" asked Wathin.

"Which one of the Matrid attacked Minir and Sarah?"

Wathin's shoulder slumped. "They were after Minir. The Djan brothers would never attack my family. They aren't that stupid."

The Djan brothers, a set of triplets that worked as enforcers for the Matrid Cartel, one of the organized crime groups in the city.

"You are working on a Matrid case right now, aren't you?"

Wathin nodded. "I just made the connection two days ago. They made a threat, as usual, but I didn't think they would go so far."

"A friend of a cousin doesn't quite offer the same shield as family, does it?"

Head snapping up, Wathin glared at Mudd. "They were going to make an example of her! In front of my own cousin! I had to do something."

"Like killing them without a warrant?"

Wathin's face purpled. "There was no time to ask! I didn't expect Sarah to jump in front of the blade."

"And the Djan brothers?"

"Two of them survived."

Mudd sighed. "And the third?"

Wathin said nothing for a long time. Then, he looked away. "I can show you the corpse. It's about a quarter mile away, in one of the riverside warehouses. I understand if you bring me in."

The mage-captain picked up his spear and set it down a few feet away. It was a symbolic gesture, Mudd already knew if Wathin attacked, there wasn't anything Mudd could do to stop it.

Shaking his head, Mudd stood up. "There isn't a lot I can do about that, Wathin. You just made you and your family a target. The Djan brothers won't forgive you from killing one of their kin. You remember what happened to Taliknir when he accidentally shot the fourth."

A nod.

"You may need to send Sarah and her friend away from town."

"I know," said Wathin in a low voice. "I can take them with me when I leave."

Mudd looked at him. "Why?"

"You, of course." Wathin gestured to Mudd. "I knew that you'd figure it out sooner or later, but it was my cousin. She's only ten. She's such a bright girl."

He sniffed. "Like her mother, actually."

"Then you better convict the Matrid before the Djan brothers hurt anyone else. And make sure it sticks this time, which means following rules and procedures."

Wathin gasped. "You aren't going to tell the Chief?"

Mudd shook his head.

"Why?"

"Family is important to you." Mudd started to put his hands into his pockets, but then felt the sewage still sticking to the fabric. Pulling a face, he crossed his arms and started back down the alley.

"Mudd? Thank you."

"Just make sure there is an anonymous tip about the corpse. I'll talk to Able about the medical examination. There are very few people in this town who use a blade weapon that electrocutes someone, but he'll figure out what to report that won't incriminate you."

"I'll owe you one," called Wathin.

Mudd stopped and looked back. "No, you don't. You are guard and so am I. But you still have a family left and I intend to keep it that way."
