---
status: alpha
format: Story
credits:
  alphaReader:
    - Emily June Street
title: Under the Streets
genres: [Mystery]
summary: >
  When Mage-Captain Wathin asks Mudd to help with an investigation off the books, Mudd agreed without question. Though, he wondered what was going on and what secrets Wathin had been withholding from him.
---

> 23. All long-term relationships (greater than two months) including singular and plural marriages must be researched by Internal Affairs. The form IA-23-T must be filed within two weeks of the two month annviersary.
 --- *Bylaws of the Great City of Rougan*

Mudd Fourier's held up his lantern above his head and peered down the sewer tunnel before him. The two yard diameter tunnel was lined with ancient brick with a hundred years worth of sewage caked over the ridges. Frost rimmed the outer edges of the thick river that ran down the middle; fortunately the cold made his nose numb and shielded him from the full brunt of the stench that permeated the walls.

Behind him, Mage-Captain Wathin splashed down the center of the tunnel, splattering sewage in all directions as he approached. Every step closer sent flares of discomfort along Mudd's joints, magnifying the pains of fifty years. The bitter smell of Wathin's weapon and the scorched scent of burnt sewage choked Mudd and blurred his vision.

Mudd's lips tightened into a thin line before he spoke. "Keep back, please."

"Then why are you stopping?" Wathin had a southern accent, a gift from his immigrant parents despite him being a native of Rougan. The rough sound grew more pronounced with the mage-captain's emotions.

"I'm trying to find a spot to safely walk."

"Oh, for the Couple's sake! Just grab your balls and dive in! It's just a pair of boots!"

Unperturbed, Mudd glanced at the captain. "Are we in that much of a hurry?"

Wathin was a taller man, thin with a black shock of hair and an angular nose. His eyes flashed in the light of his spear, a weapon with a massive blade. It crackled with lighting, sending brilliant arcs of power spearing toward Mudd and scorching the ground around both men.

The other mage-captain sighed and gestured with his spear for Mudd to continue. "Yes, move."

"It would be easier to understand the rush, if you told me what I'm looking for, Mage-Captain."

"Mudd, just pretend it's one of your crime scenes."

Mudd straightened with a flash of annoyance. He tugged on his black jacket, wishing he brought one of the guard's cloaks at least shield himself from Wathin's splashing. "I rarely go into a scene without knowing the crime. And I do not rush investigations without good reason."

Wathin's jaw tightened. "Fine, you're looking for two girls."

"And...?"

"No. That's all you're getting."

Mudd fought down his emotions and turned back. "One would think if you dragged me out of my house at the first bell, you'd be more forthcoming with information that would help me resolve this quickly. Not to mention," he glanced back, "help me understand why you didn't involve the rest of your district's guards in the process."

Wathin let out a frustrated groan. "Stop probing, Mudd! Please! Just find them."

Turning back, Mudd spotted a dry spot along the river of sewage and stepped on it. He held the lantern as he continued to pick his way along the tunnel, his eyes looking for signs of humans passing.

He spotted a fresh mark on the side of the tunnel when the mud reflected light from the lantern. Slowing down, he peered at and then to the ground. The mark on the brick was the edge of a small hand, the edges were fuzzy and smeared almost a foot before lifting. Tiny caresses of threads and a lack of individual fingers told Mudd that she was wearing knitted mittens.

Below, he spotted a small footprint with an obvious hole in the sole and a pair of laces trailing behind. Judging from the height, the person who made the marks was only four feet tall or so. A young girl.

Mudd tried to spot signs of the other girl, but couldn't. He stepped back, careful to avoid the wet spots, and continued along the tunnel.

Along the way, he saw more of the prints along the bricks. The consistent height compared to the footprints confirmed his observations. It was a shorter girl, probably only ten or so years old. There was no sign of the other girl, which worried him.

Behind Mudd, Mage-Captain Wathin muttered to himself as he splashed through the sewage. There was no elegance in the way the mage-captain moved through the sewer tunnel, no attempt to avoid the deep puddles of feces and garbage. Nor did the cursing captain keep back enough to avoid splashing the back of Mudd's legs or keeping the resonance from burning along Mudd's joints.

For the third time since entering the tunnels, Mudd reconsidered the benefits of wearing the city guard's cloak. At least he could have burned it after tromping through the sewer with a man who thought scorching a crime scene was an effective way of solving a crime.

Mudd caught sight of an unusual shadow. Holding the lantern higher, he stepped toward it before he realized it was just a shred of paper with too much sentiment to be recent. He turned and returned down the tunnel.

Wathin called out, "Have you seen anything else? Was it something?"

Mudd looked over his shoulder at the other captain. Unlike Mudd, Wathin wore the uniform of the captains with the impeccable grace of the city guard: long red cloak that swirled in the sewage; polished boots that would have been black except for the feces and grime that coated them; even his captain's badge, a gold bracelet with a ruby, dripped with fetid slime from where Wathin tripped into a deep sinkhole because he wasn't paying attention. Despite the mess, Wathin walked with a straight back and carried his spear horizontally as he walked.

"Mudd?" Wathin glared at him, the light of his eyes reflecting the brightness from his spear.

Mudd turned away. He didn't have all the clues to figure out either puzzle: who he was looking for and why Wathin needed him. He suspected that one of the girls was one of Wathin's relatives, which would indicate the press to handle it, but it didn't explain the secrecy. The guards always put a show of force on a missing relative case, one did not mess with the family of the guards.

He also knew that Wathin didn't have children himself. Dredging back through his memories, he remembered the guard talking about his aunt moving into town. It was a year ago, but Mudd rarely forgot details, and there was mention of a young girl.

He looked back over the sewer and continued along, rubbing his bald head for a moment before catching sight of a few blue threads caught on a brick along the right side. He slowed down as he approached and pulled out one of his evidence bags.

"This isn't a crime, Mudd."

Mudd refolded the bag and put it back into his pocket. He used his kidskin gloves to pluck the threads from the wall. It was a local wool judging from the texture and the color came from the Riversbend District, the distinctive blue pattern in the fibers identified the mechanical devices used to stir the ink.

He glanced at Wathin while still rubbing the wool between his fingers. Wathin lived in the Riversbend, but the quality of the fibers were typically associated with a lower class than the mage-captain or his family would have lived.

Looking around, he spotted more fibers a few yards down the sewer pipe. Carefully, he looked for footsteps but the river of grime pouring down the center had obliterated any tracks. A few splatters along the sides, the edges still drying gave an indication that someone had walked through in the last few hours. The bubbling of half-frozen sewage lapped against the old brick walls.

A piercing headache blossomed across the back of his mind. He looked over his shoulder. Wathin had inched forward until they were only a few yards away.

"You know," Mudd started, "you should have asked Able to help you. He lives down here, you know. He knows these tunnels better than anyone else."

Wathin frowned, the bottom corners of his lips disappearing into his black beard. The hair on top of his head was plastered to his head. "I don't like working with Able. He makes me feel uncomfortable."

"We have the same resonance, you know."

Wathin's hand tightened on the spear and the crackling grew louder. His eyes flashed down the sewer pipe and then back to Mudd. "It isn't his aura and you know it. That man is creepy."

Mudd shrugged.

"Every damn time he walked into the room, he's always asking what I ate just in case I die before he sees me next."

"He is the medical examiner and investigating digestion rates is a rather new field. He has written a number of pieces on it, if you recall."

"He's a freak."

Mudd felt a prickle of annoyance. "He's my friend."

"Does he do that to you? Ask you every single thing you ate and drank?"

"No." Mudd stood up and continued down the sewer. Knowing what to look for, he kept his attention focused on both the dry patches to walk along and the blue threads that marked the girl's trail. They staggered up and down a few feet from each other, with the size of the patches growing larger during the lower sections; she had been limping while using the wall for balance.

The threads were tightly woven but he spotted a few stained fingerprints near one corner. He stopped to look at the stains. Shit but no blood. She must have tripped. He caught a few more fibers and tugged them free with his fingertips. The fabric must have torn because he could see a fingerprint on the wall.

"Why do you get special treatment?"

Mudd looked at the mage-captain for a long moment, pretending to consider his words as he inspected the man. Sweat dappled Wathin's brow and he was looking around. He gripped his spear tightly, though the weapon would be useless in the confines of the sewer. It was obvious to Mudd that Wathin was nervous to enter anywhere that he couldn't use his weapon. Between his insistence that Mudd work in the middle of the night and without backup indicated that it was something critical to him personally instead of the guard.

When Wathin started to look up, Mudd knew that he had to speak. "Because I document everything I eat and leave the report in his mail box. That way, he doesn't have to ask and I can give precise times, quantities, and temperatures. Then if I die in the line of duty---"

Wathin scoffed. His opinion that forensic mages was well-known across the guards.

"---then he can further his research. It may save someone's life."

"There is something unnatural about it."

Spotting an opportunity, Mudd turned back and casually spoke. "Not unlike being woken up at the first bell by a fellow mage-captain to go walking through a sewer without a case file or assignment---"

Wathin's lips pressed into a thin line. His spear brightened with his thoughts, the small arcs of lighting tracing the length of his leg and burning through the sewage he stood in.

"---or even an explanation of why," finished Mudd.

Wathin's grip creaked on his spear. "Please, don't solve this."

"It's what I do, Mage-Captain." Mudd stepped over a freshly-soaked doll and continued down the path.

They walked in silence for a few minutes, Mudd focusing on the wall instead of his fellow captain. He spotted some footsteps, but the girl was mostly walking in the center of the sewage line. Only one foot came near the edge, the occasional footprint coming up along the drier edge. With her limping, he couldn't imagine why she would be not walking along the edge.

A memory rose in his thought. Turning around, he started backtracking. As he passed Wathin, the feeling of discomfort rose into a sharp pain that filled his head and caused sparks to float across his vision.

"We've already been that way!" snapped the mage-captain.

"I missed a clue."

"Clue? This isn't an investigation!"

Mudd hesitated and looked back. "It isn't? Because I don't really know what's going on. Unless you want to explain it."

Wathin growled and his spear brightened. "I can't."

"Then I missed a clue. It will only be a minute."

When Wathin didn't stop him, Mudd returned to the doll he had stepped over. In the last few seconds, it had soaked up more of the sewage and shifted a few yards down the pipe. He picked it up and inspected it. It was an expertly made doll of a dog girl, a dalpre. One of the large ears was torn off. He pressed his finger inside the rags that stuffed the doll, the sewage had only soaked in about an inch. On the other side, he spotted blood soaked into the fabric; it was still red.

Mudd's skin tingled. There was something serious going on but he wasn't sure if Wathin knew yet. He frowned as he inspected the blood marks. He could see where a girl's thumb and fingers marked the fabric. It was a right hand, the same hand that caught fibers in the wall. Frowning, he recalled the prints against the wall and the bloody marks on the doll. Same side of the body, but the hand against the wall wasn't bloody.

And then he knew why the girl was limping and walking down the center. The other girl was injured and leaning against her. The discomfort burned across Mudd's mind. He straightened up as Wathin approached, his eyes locked on the doll in Mudd's hand.

Mudd sighed. "You don't have children, Mage-Captain. Is this a niece?"

"Cousin," whispered Wathin. "Sarah is her name."

"And her friend?"

Wathin jerked and his face paled. "Minir. A dalpre from down the street. She's a cat-girl."

Mudd handed over the doll and turned it over so the bloody fingerprints could be seen. "Why is Sarah bleeding? And why didn't you ask others for help?"

"She's... I can't tell you."

Wathin's spear pulsed brightly for a moment, lighting up the sewer.

Mudd sighed and handed Wathin the doll. "Trust is a powerful sword, Mage-Captain."

Wathin's jaw tightened, but the mage-captain said nothing.

Annoyed, Mudd hurried back down the sewer.

"Mudd?" Wathin called after him.

Mudd turned back. "We have a girl who is bleeding and hiding in the sewer with her friend. What you did or why you are doing it is less important than finding those two, isn't it? The temperature is going to drop soon and they won't survive long."

Without waiting for Wathin's response, Mudd turned and continued along his way. He held his lantern up to catch as much as possible, following the patterns of threads, footsteps, and the subtle signs of two girls walking along the sewer. Now that he knew what to look for, he spotted splashes of the second girl trudging down the middle of the sewer.

At a junction, he noticed marks of both of their feet around a ladder leading up, but no mud on the rungs. They had left it alone and continued down the path. He wondered why they weren't escaping the tunnels.

A few dozen yards later, he noticed that the second girl's, Sarah's, footsteps were growing more ragged, becoming streaks instead of steps. Blood splatters marked the stone between the footsteps.

It wasn't much longer when the splatters became larger splashes and then a thin river of crimson that ran into the sewage. He saw it coating the debris flowing in the sewage and streaks of it still traveling along the river.

As he ran along the sewer, he wondered why Wathin or the girls weren't calling for each other. His mind reflexively followed the piece of the puzzle as he came up to another junction. He circled the openings for a moment before catching the blood trail. The print on the wall was bloody now, the fibers of the girl's mitten soaked in blood.

There was also a pair of circular marks on the threshold, soaked in blood and grime. Next to them, he saw Minir's shoes marked the edge. The curved shape showed that she had her weight along the front of her toes, which meant that she was crouching down to probably help Sarah stand.

"Problem," he swore.

"Where?" snapped Wathin, coming up. His spear crackled loudly as he straightened it in the junction.

Mudd jumped into the pipe and raced along his, his boots splashing the sides. He was ruining the scene, but he didn't think it mattered anymore. There was too much blood and there was a life at stake. Coming up to another junction, he spun once, found the right pipe and ran down it.

When he saw a bloody smear against the wall, he could picture Sarah leaning against the bricks. Fresh blood coursed along the crumbling mortar, the crimson almost invisible in the dim light cast by his lantern. It was also fresh and dripping, they were only a few minutes behind them. "Call for the healer! They're close and Sarah's hurt badly."

The resonance pressure faded as Wathin stopped splashing behind him.

Mudd continued allow, running until his lungs ached. He didn't care about the sewage on his boots, he was going to burn them when he was done anyways. He jumped down a short cracked brick and then dove into another junction. Coming up, he spun around as he inspected the three other entrances for any sign of the girls.

There were no bloody marks on the rocks nor could he find wet mud or threads. He circled again, to verify what he saw, but when he didn't find any signs, he focused his attention on the junction itself.

There was blood leading into it, but the thick river of sewage flowing through it made it difficult to follow the girls. He lifted his gaze and spotted a rusted metal ladder leaning into the darkness of a tunnel. A few flakes of snow drifted down from the holes in the sewer cover.

The last yard of the ladder had been rusted away, but there was a stone block underneath it. There used to be a second block under that, but now the sewer bubbled underneath it.

Mudd focused his attention on the block. A print caught his attention and he inched forward and then up. Taking a breath, he set his foot on the block, but then snatched it away as it shifted underneath him.

He stepped back and frowned. He couldn't see anywhere the girls could have gone unless they backtracked. Shaking his head, he started back down the tunnel to look for another clue.

When a line of crimson flowed past him, he stopped. Turning slowly, he followed it with his gaze. It streamed from the block, but not the top. Instead, the fresh blood came from the small gap between the block and sewage below.

He cocked his head and headed over there. He winced as he drove his hand into the icy sewage but he needed to balance as he almost shoved his face into the fetid liquid and peered through the gap.

A pair of glittering eyes looked back, the shimmering green betraying the little girl's true nature. Minir wasn't human, she was a dalpre, a half-animal hybrid with a human. Her whimper rose up in a high-pitched purr; she was a feline, he guessed. Focusing, he could see her triangular ears sticking out of the side of her hair and some whiskers.

When she pulled back further into the darkness, a whiff of fresh blood wafted past Mudd.

Mudd gave her his best smile. "I'm Mage-Captain Mudd Fourier."

Minir whimpered and turned away.

Mudd lifted his other hand above the block. A sphere of bright purple formed in his palm. It speared into the sewage, turning parts of it brilliant white while others darkened. Carefully, he lowered it until the light shone underneath the blood.

Bloody prints glowed bright as he saw the liquid outlining both girls' bodies. Minir had some of it across her face and shoulder. Sarah's, on the other hand, was flowing freely from a wound in her side. Every time Minir shifted, more blood poured out.

Mudd sighed. "Please, I need to help Sarah. She's bleeding badly."

"I-I can't." Minir had a northern Kormar accent. It had the rawness of a recent immigrant. "They hurt her. The men with the swords came and t-tried to take us away. They had black... black hands and a spider on their faces. And then the scary... monster with a spear came."

Mudd froze and sweat prickled. He knew the tattoo, it was one of the Matrid Cartel, a crime family that Wathin was currently investigating. "The man with the spear hurt Sarah?"

"No, but there was blood... and I saw them fall and then I... Sarah got hurt. And then... And I saw... I saw..." Minir burst into tears, her voice cracking.

Footsteps splashed down the sewer pipe toward him. Mudd pulled back and stood up, holding up his hand as Wathin approached.

"Mudd---"

Mudd shushed him.

Wathin's face twisted into a growl. "What---"

Underneath the block, Minir cried out and the block over her head shifted as she crawled further underneath it.

"Be quiet," snapped Mudd. "And send the healer down here now. Sarah is bleeding but I don't know if I can get Minir to release her. See if Nerin can come."

"The negotiator?"

"She's off duty but lives a few blocks to the north. Her date canceled just as we were leaving for the day, so she should be home. She's good with children. Did you find a healer?"

"Yes, Tam is coming."

"Good, tell him to hurry."

A sharp pain throbbed in Mudd's neck. The pressure built up but passed quickly. It was Tam, he knew the resonance.

Across from him, Wathin started scratching his neck. The arcs from his spear began to curl up toward the ceiling as they traced the healer's path above him.

"That's Tam. Get him down here and stay out of sight. Minir saw you kill one of the Matrid and seeing your damn spear will not make this easier. Now go, Mage-Captain."

Wathin's face paled. He nodded once and turned around, reversing his grip on his spear to hold is backwards. The point never wavered before he ran back to the last opening.

Mudd turned back. He couldn't heal or do anything else, but a comforting voice may be what the girl needed until Nerin could arrive.

Taking a deep breath, he shoved his hand under the block in case it felt and leaned into the mud until it slopped up against his shoulder. Seeing Minir fully, he smiled into the cat-girl's eyes. "Don't move, little one, I have to keep you both safe at all costs."
