---
title: Spring Planting
format: Flash
genres: [Slice of Life]
summary: >
  It was planting season. Jin and her brother were doing what they did well, they were setting down seeds.
---

Jin knelt down along the furrow that her brother turned up. Her fur rippled in the cool breeze, bringing the promise of a spring rain.

Her puffy tail quivered as she plucked a single seed from her bag and set it down in the moist soil. As she did, tiny curls of magic seeped into the seed and it began to crack. A white root peaked out and quested for the earth.

Shifting down a few links, she plucked another seed and did the same. By the third seed, the first had sprouted.
