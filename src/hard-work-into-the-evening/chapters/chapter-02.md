---
title: Finishing Up
date: 2020-02-17
summary: >
  After hours of work, Tsubàyo realizes that he may have made a friend.
---

> There are rare times when the clan disagrees with their spirit. --- *The Nature of Spirits and Their Clans*

Before he knew it, they were near the end of the field. With a start, he looked back to see the neat lines of plants sticking out in straight lines. It smelled of rich soil and fish guts.

"See?" she said as she leaned against the wagon. "Everything goes faster when you are talking."

"I don't remember eating dinner."

"It was short."

He shook his head. Somehow, he had just had the most enjoyable day since he arrived at the Seven Villages and he wasn't even sure it was real. Bemused, he helped her finish the field.

Just as they were cleaning up with the remaining water, he felt a horse approaching. It was Zukejùfa which meant Kamùji was approaching. The muscles in his back tightened. Every encounter with the Pabinkúe clan leader left Tsubàyo feeling like he was one step from being executed.

"What's wrong?"

"Kamùji is coming."

She shoved her hands into the bucket and then wiped them on her trousers. "He always had terrible timing. What is he going to come out of?"

Fimúchi referenced Pabinkúe power to travel between shadows and appear in unexpected places. He closed his eyes and felt around him, exploring the world between the shadows and the minds of the horses.

He gestured to a boulder.

Fimúchi turned just as a shadow bulged out of it. It stretched unnaturally to the side, then peeled back to reveal a pitch black horse being ridden by a man in his forties.

Kamùji had a deep black beard with streaks of silver in it. His eyes were steady but clouded over. He had two pairs of swords on him, one set on his hips and another with a reverse sheath on his back. Power crackled along his body as he turned his mount toward them.

Zukejùfa pawed the ground and sent out emphatic greeting to Tsubàyo who returned it respectfully.

Kamùji let out a grunt before he turned to Fimúchi. "You were supposed to work the northern Kabayo Fields tonight."

"Oh," Fimúchi said in a sweet voice that Tsubàyo had only heard when she was mocking someone in her stories. "I must have gotten my schedule mixed up with Gegía's. Silly me, I'll have a word with my elder at the next schedule meeting."

With another grunt, Kamùji waved his hand sharply. "You know damn well I was keeping her away from Pekochìo. They spent half their time kissing and touching and not getting their job done."

Fimúchi shrugged and wiped her hands on one of the now dry clothes on the wagon. "Pity. Did they finish the field?"

A moment hesitation. "Yes."

"Then I guess nothing was wrong, Papa."

Tsubàyo froze, his stomach twisting in a sudden pain. He had no idea that Fimúchi was Kamùji's daughter. Suddenly, he wanted to be anywhere else.

"They were fucking when I found them!"

"Well," Fimúchi said, her voice growing steady, "I hope you left them alone and let them finish. If they did their job, they can have fun. Can't you see they are in love?"

Kamùji glanced at Tsubàyo who stepped back. Then he turned back to his daughter. "You were not supposed to be here."

Fimúchi started to fold the cloth on the wagon. "I was curious about the newest Pabinkúe. Since the schedule gave me the opportunity, I decided to swap stories."

"That is not your decision to make. Pabinkúe should have never had selected him."

"Oh? Last I checked, I was Rojikinòmi. We and the Pabinkúe have been together for centuries and I have no trouble learning the character of our newest ally."

The scowled deepened. "You are being nothing more than an insolent brat."

"I'm a woman who can make her own decisions, Papa. You may still think I'm your little girl but that time has passed. I'm allow to make my own decisions as long as Rojikinòmi---not Pabinkúe---decides it is for the greater good."

He opened his mouth.

She interrupted by pointing at him sharply. "You are Rojikinòmi lands, remember that."

"And you are protected by the Pabinkúe!"

Tsubàyo felt completely divorced in the conversation while bristling as the subject. He wanted to lash out, but couldn't. Instead he remained still and listened.

The ground underneath all of them shifted slightly. The freshly planted seedlings twisted and turned until their leaves were facing Kamùji. Tsubàyo gulped as he looked around, it felt like the ground itself was breathing underneath him.

He looked around with his lips pressed into a tight line and his knuckles white as they balled into fists. Then, he relaxed them. "I don't trust him, my daughter."

"Then you might consider including him in the conversation," she said with a smile that never reached her eyes.

Tsubàyo grew flushed as Kamùji looked sharply at him.

"Do you have anything to say, Boy?"

Tsubàyo's muscles tensed. The Shimusògo always called him "boy" when they were insulting him. A familiar anger rose up.

The four horses pawed at the ground.

Kamùji's eyes widened and he looked down. Then up. There was just a hint of fear in his eyes but a lot more growing anger.

A warm hand caught his. He looked to see that Fimúchi had grasped his fist. With all his willpower, he relaxed his fingers. "I'm sorry, Great Pabinkue Kamùji."

"You should head home, Boy."

Tsubàyo bowed while he kept his anger in check.

"I'll see you tomorrow," Fimúchi said cheerfully.

Tsubàyo looked at her in shock.

Kamùji sputtered. "What? No! How!"

"Oh," she said dismissively. "The problem with us farmers is that we are always messing up the schedule. You never know which of us Rojikinòmi is going to show up to what field. Right? Besides, I want to hear more of his stories. I enjoyed them."

She squeezed Tsubàyo's hand before she stepped away. Both men watched as she gathered up the two work horses and lead them away.

Tsubàyo reached out with his mind and said goodbye to Jibòpu and Gokunàe.

They sent partings back before lugging the now empty wagon down the street.

"That girl always fights to get her way," muttered Kamùji.

Tsubàyo decided to keep his mouth shut.

"I could put you on sentry duty for the rest of your life to keep you away from her."

He tensed, the anger starting to show in his clenched fist.

"But it wouldn't matter, would it." Kamùji muttered. "Like a moon-damned weed, she would find some way to spend time with you until she gets what she needs."

Surprised, Tsubàyo turned and looked at his new clan leader. "Great Pabinkue Kamùji?"

Kamùji shook his head before he looked down at Tsubàyo. "You killed a good woman, Boy."

"I know."

"I don't know why Pabinkúe choose you but I suspect I'm going to find out sooner or later. That doesn't mean me or the rest of the clan is going to accept you with open arms. Not tomorrow, maybe not ever. You are a stranger and a murderer, don't you forget it."

Tsubàyo nodded. "I know. I should have never stolen that horse." Or killed the woman riding it. It was a moment of anger and desire that drove him and he could never get the blood off his hands for that one moment in his life.

Kamùji sighed. "Keep to your schedule. Let her measure out your character. Maybe she can see the good that I can't."

With a grunt, Kamùji turned and headed back for the boulder. "If you hurt her, though, I will have your organs ripped out of your stomach and stretched across this field for fertilizer."

Before Tsubàyo could respond, Kamùji and Zukejùfa plunged into the boulder and were gone.

Tsubàyo shivered at the threat and then turned to look across the field he had helped plant. Stories about Fimúchi growing up danced across his mind and he smiled. She showed him a world of peace and prosperity, two things he had never had when he was growing up. His clan may hate him, but he could see that there were some good things in his new home.

Buoyed by the hope for the future, he mounted Ryachuikùo and headed home.

By the time he reached the stable, it was getting close to morning. The old woman was still there. She sat on her customary spot on a hale bay, smoking a pipe and staring at the moon. When he stopped in front of her, she slid off and held out her hand.

Tsubàyo dismounted and bowed to her. "Good evening, Great Pabinkue Maporéku," he said and tensed for the insulting reply.

She only grunted as she took the horse into the stable. He started to follow but she stopped and held up her hand. "Let this old lady take care of Great Pabinkue Ryachuikùo. You take that basket and go home."

He looked where she pointed. There was a basket on the hay bale. Curious, he peered inside it. The smell of roasted meat, sharp cheese, and fresh bread rose up. It was warm, probably the hottest meal he had since he arrived. He sniffed again and let a smile paint itself across his face.

"Thank you."

"Thank Fimúchi, not me." The old woman lead Ryachuikùo a few steps and then stopped. Then, she turned and let out a long sigh. "Have a good night, Great Pabinkue Tsubàyo."

Stunned, Tsubàyo couldn't say anything. Maporéku had not said a pleasant word toward him since he arrived. He took the basket and left without another word.

At home, he went inside and set down the basket on the table. It clinked against something. Curious, he peered around to where someone had brought some flowers in a vase. His black board had been moved from the wall to the table. On it, there was a new item written under the next night's sentry duty.

"Dinner with Fimúchi."
