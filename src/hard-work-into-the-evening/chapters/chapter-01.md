---
title: Assigned Duties
date: 2020-02-17
summary: >
  The early days of Tsubàyo in the Pabinkúe clan were hard. He was still seen as a murderer of a cherished warrior and an outsider from the northern clans, one that was raised as a day clan. But there were some willing to look past him to see the man he would become.
when:
  start: 1471/6/25 MTR 20::61
characters:
  primary:
    - Tsubàyo
  secondary:
    - Rojikinòmi
    - Gachyorábi # Horse
    - Maporéku # Old Lady
    - Pabinkúe # Spirit
    - Jibòpu # Horse
    - Gokunàe # Horse
    - Fimúchi
  referenced:
    - Chimípu
    - Mikáryo
    - Karawàbi
    - Pidòhu
    - Rutejìmo
locations:
  primary:
    - Seven Villages
    - Karabi Fields
  referenced:
    - Shimusogo Valley
organizations:
  secondary:
    - Pabinkúe
    - Rojikinòmi
  referenced:
    - Shimusògo
---

> The Seven Villages is a prosperous cluster of night clans that line the Tikotāi River. They provide much of the food for the surrounding areas... while they lasted. --- Gobifuchi Nagòmi, *When the Horses Left*

It was early evening and the sun's last light was painted across the cloudless sky. To Tsubàyo, the dark purple smears looked more like bruises and his skin crawled with the still-fresh memories of his arrival to the village. He shook his head to clear the sour memory and then stifled a sudden yawn.

The coffee in his hand was still too hot to drink. He swirled it around in the heavy clay mug to cool it down. It was far too late in the evening and his body wasn't used to sleeping during the day to wake up with sunset. Though, the anticipation of the moon rise made it easier to crawl out of bed.

He turned slightly to look at the cottage that the Rojikinòmi had given him. Unlike the cave that he had grown up in, his new place was made from wooden beams. The edges of the rooms were straight and sharp, unlike the slightly curved surfaces that he had seen for seventeen years. He wasn't entirely sure how he felt about it, but he had decided it felt warmer.

His eyes looked at the bare wooden walls in the living room. When he was forced out of the Shimusògo, he didn't have a chance to take anything. All he had were the ripped and ragged clothes on his back and the bruises that covered most of his body. He had gotten rid of both on the long trip back to the Rojikinòmi villages. The cottage was a new life for him, one where he didn't know anyone and no one liked him.

A horse whined nearby.

He cocked his head and took a deep breath. Energy rippled underneath his skin, sluggish and cold without the moon above the horizon. He pushed hard and reached out for the horse with his senses.

After a moment, the warmth of the horse's mind blossomed inside his own and he smiled. <<Greetings, Pabinkue Tsubàyo,>> came the horse's thoughts, more emotions than actual words but the intent was clear.

He let his thoughts ripple along the connection, <<Evening, Great Pabinkue Gachyorábi.>>

The horse's joy rose and he felt it flooding his thoughts. While the Pabinkúe clan didn't seem to appreciate his presence, the clan's horses were almost universally cheerful in their greetings.

Tsubàyo smiled and tested his coffee. It was cool enough to drink and he sipped at it. He still hadn't gotten used to the bitter liquid, but he couldn't fault it for pushing away the exhaustion of waking up late.

Suddenly, a thousand pleasures flooded his veins. He couldn't quite put a word on it but it felt like the rush of his heated battle with Chimípu, the thrill of balancing on the edge of a cliff, and the afterglow of an orgasm all at the same time.

As quickly as it filled him, it was gone.

He let out a soft moan and turned his head to where the first crest of the moon had risen over the horizon. The brilliant white sphere was the source of his power and he could feel it flooding his body. The connection between him and the horse sharpened.

With the influx of power, Tsubàyo could sense more horses around him, from the five mares still in the stables down the road to the two horses pulling a wagon a quarter mile away. Seventy-three equines within a mile of him and each one greeted him with a wave of joyful emotions.

Tsubàyo smiled to himself and send a greeting to each of them. He finished his coffee by the time he finished. Then he looked at a black board hanging by his front door. It had a schedule for his week. He grabbed a hunk of chalk and drew through yesterday's entry of patrolling the villages and tapped on today's duties: fieldwork at the Karabi Fields. He had an hour to get there.

He sighed with annoyance. The horses couldn't tell him where to find the fields. They understood landmarks, not signs. That meant he needed to talk to one of the Pabinkúe or Rojikinòmi. Grabbing his short sword and a pair of work gloves, he headed out without bothering locking the door behind him; there was nothing in the cottage that he cared about.

His destination was the stables at the northern end of the small village. His horse, Ryachuikùo, greeted him with a whiny and a wordless joy long before he finished walking up to the open doors leading inside.

"Hello?"

An old woman sitting on a bale of hay grunted. She spat at his feet. "Took you long enough, you ugly bastard."

The side of his face tightened and he fought the urge to scratch it. When he was a child, an accident with hot oil left the right side of his face scarred and disfigured. The marks covered his shoulder and down along his chest.

He gripped the dark blue short he wore and clenched it in his fist. With his jaw tight, he bowed. "Good evening, Great Pabinkue Maporéku. It is good to see you this evening," he finished with a lie.

"You're late, you miserable cock-sucking murderer."

The muscles in his chest tightened painfully. "The moon has only been up for a few minutes."

"You're still late," she said before spitting on the ground at his feet. She slid off the bale and hobbled into the stables. There was no love long between him and the Pabinkúe. Less than a month ago, he had killed one of their herd masters in a moment of passion. If it wasn't for Pabinkúe accepting him as one of her warriors, they would have killed him at least a dozen times.

Tsubàyo sighed and contemplated going into the stable after the old woman. He never knew what was expected for him in the new clan. A frown furrowed his brow. It felt like the same abusive crap at the Shimusògo, only at night.

Ryachuikùo came trotting out, his hair gleaming and a fresh blanket over his back. He radiated joy and happiness as he rushed over to Tsubàyo and butted against him.

Tsubàyo's sour feels faded away with the presence of his horse. He closed his eyes and buried his face against the warm body. <<I missed you. At least you don't hate me.>>

<<The Pabinkúe don't hate you.>> The horse rubbed against him, his tail whipping back and forth.

<<Feels like it.>>

<<It takes a while for new stallions to be accepted.>> Ryachuikùo's thoughts grew more intense and weighted, as if the entire Pabinkúe herd was speaking to him at once. The tones became more feminine. <<They will. I choose you and they will accept in time.>>

Tsubàyo hugged Ryachuikùo tightly. <<Thank you, Great Pabinkúe.>>

The presence of the clan spirit withdrew.

Encouraged, Tsubàyo pulled his face away to look at the scowling old woman. "Excuse me, Great Pabinkue Maporéku, will you please tell this humble man how to find the Karabi Fields?"

For a moment, he thought she was going to spit on him. Then she turned around. "North to the next village, then east at the town center. You'll find the signs."

He thanked her politely before mounting Ryachuikùo. It took almost no effort since rider and horse were in each other's thoughts. As soon as he could, he gave her a bow from the top of the horse and spurred Ryachuikùo to the north.

It was short ride, only forty minutes at most, before he came up to a field that had been freshly turned. The rich smells of moist earth were almost overwhelming. He had spent his entire life in the sandy regions around the Shimusogo Valley. He never imagined a valley could have rich life and crops before. Then again, he had never seen a river before he joined the Pabinkúe either.

He spotted a wagon a short distance ahead with two horses grazing near it. He sighed with growing dread, it was another chance for a stranger to tell him he was ugly and a murderer. However, this was his home now and he had his duties.

The two horses were Jibòpu and Gokunàe, both older stallions. Their thoughts were easy to pick up and they greeted him with the same joy and friendship that all of the Pabinkúe horses possessed.

As soon as he was at the wagon, he slid off. Thanking Ryachuikùo, he reached out and patted Jibòpu and Gokunàe with each hand.

"You can always tell a Pabinkúe," said a woman from behind him.

He jumped and spun around, a flush darkening his already dark cheeks.

She wasn't much older than him, with sorrel skin and dark green eyes. Her hair was pulled into neat rows of tight braids that went from her brow over her head and out of sight. She wore a yellow top that looked pale and ethereal in the fading light. On the bottom, she had dark trousers and sturdy-looking boots.

Tsubàyo gulped. "Sorry." He clenched his toes. They still were looking for boots that would fit him but his feet were heavily callused from spending an entire childhood walking barefoot on the sun-heated sand. He didn't need anything but he felt self-conscious among his new clan.

She smiled as she came up. "Don't be. It's a good thing. The Pabinkúe greet horses first, then remember there are people still around."

He felt a blush as he tried not to stare. She wasn't thin and wiry like Shimusògo women, but broad shouldered with large breasts and wide hips. She dug into a leather pouch resting on her thigh and pulled out a small strip of cloth and wiped her hands. Then she held it out. "I am Fimúchi and I speak for Rojikinòmi."

It took him a moment to take her hand. "I am Tsubàyo and I... guess I speak for Pabinkúe."

She smiled brilliantly. "Now that we got the formalities, I'm sure you'd rather call me Múchi being that Káryo was your introduction to our ways."

Mikáryo was a warrior of the Pabinkúe who hated formal names and always used the familiar ones. She was also the one who brought him to the villages.

He stammered for a moment. "No, Great Rojikinomi Fimúchi."

She arched one eyebrow. "Fimúchi then."

"As you wish, Great---"

"Fimúchi," she repeated as she tightened her grip. "My name is Fimúchi."

He winced. She had a powerful clasp and rough hands. "Tsubàyo."

Fimúchi released him and stepped back. Her eyes scanned him from toes to head and then she gave a slight smile. "You aren't what I expected."

He tensed and scratched the scars on the side of his face.

"Come on, ready to work?"

Tsubàyo said nothing and followed as she brought him to the wagon. It was loaded with trays covered in wet fabric. When she flipped over a corner, he could see it was hundreds of tiny plants in trays. He didn't recognize the type. Everything was dripping wet and he could see eight buckets of water covered with cloth near the front.

"Tonight is going to be long, tedious, but fairly simple. I'll plant the wheat seedlings and you try to keep up with me."

He frowned and glanced at her. She didn't look very fast.

Fimúchi smiled sweetly and picked up one of the tiny plants. With her other hand, she scooped up something that smelled like fish mixed in with other seeds and beans. Turning around, she carried them over to the furrow of freshly upturned earth and set the seedling down and patted her other hand around the base. He watched as she whispered a prayer over each one while pulling the dirt around the seedlings.

When she stood up, she returned to the wagon and grabbed another seedling and scoop. Giving him a nod, she planted both of them.

"Um," Tsubàyo said. "I take it keeping up with you doesn't mean going fast?"

"Of course not. Farming is slow and steady. However that wagon has a ton of water, plants, and supplies. It would be horrible if I had to keep walking back to it for this entire field, right?" She gestured to the dozen of acres in front of her.

Tsubàyo shook his head. He guessed shit work was part of every clan, be it cleaning off the lunch tables, taking out the garbage, or planting throughout the night.

She laughed, a low bellowing sound, but her smile was brilliant in the dim light. "Now you've got it. Come on, we get to take a break in four hours. I give your horses about an hour before they get bored and then you'll have a struggle on your hands."

<<That's because we can't eat the delicious treats on this wagon,>> thought Gokunàe.

He grinned.

True to her work, Fimúchi worked steadily. It would take her ten minutes to get down the furrow enough for him to move the wagons only a few feet and then he would have to stop.

It quickly grew dark, but neither of them stopped. One of the gifts both the Pabinkúe and Rojikinòmi had was their ability to see in the dark. The world took on a bluish-tinge and he found watching her alluring. Her top had become a surreal flash of dark and light, the patterns of what appeared to be straight yellow turned out to be designs of flowers and stars.

He realized he was staring and looked away, but that somehow made it worse. There was nothing to focus on except for the sound of her movement and the ripple of trees.

"Tell me about your birth clan?"

Tsubàyo scowled. "You don't want to hear about them."

"Oh?" she said as she straightened. She was sweaty from her effort and her shirt clung to her body. The smell of it wafted around him, sharp but not entirely unpleasant. "Want to hear about mine?"

"Why?"

"Because we have hours and it will pass a lot faster if we talk, don't you think?"

"I guess."

"Well, I was born here among the Seven Villages. In a few weeks, I'll be twenty-one." She was four years older than him. She walked up to the wagon.

Without thinking, he picked up one of the plants and scooped up some of the foul-smelling mixture. It looked like there were intestines and fish head in the mix. His stomach rolled but he turned and handed both to her.

With a smile, she took them and returned to the furrow. "My father was Pabinkúe but I never was good at riding horses. They were too delicate for me. But, you never know which spirit is going to call you so I spent my summers at the various clans in the area. There are twenty-one within a day's travel."

Tsubàyo was speaking before he realized it. "There are only four within walking distance of Shimusogo Valley."

"Oh, which ones?"

Before he knew it, they were trading stories about their childhood. He tried to gloss over the less enjoyable parts of his past, like him becoming friends with Karawàbi and bullying Pidòhu and Rutejìmo. A few memories rose up, of seeing Karawàbi's corpse with his throat cut, but he pushed them aside. Despite his effort, she managed to pry even his darker side out but didn't seem to take more than a passing interest. She didn't question or accuse him of anything, just took the story and gave another.
