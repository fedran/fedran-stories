---
status: alpha
title: Battle Mage in a Dress
format: Story
genres: [High Society, Romance]
summary: >
  Jenefer had little interest in the fancy balls of Tarsan society but her family had other plans. It didn't matter if she could use killing magic without a second thought, she had to attend.
---

> One of Tarsan's daughters could climb the highest mountain, fight off a dragon, and stop a war with an ice-cold dagger and the first question asked when she returned triumphantly would be of her husband. --- *The Rightful Place* (Act 2, Scene 4)

Jenefer da Rosenkir reached for one of the nearly identical spoons in front of her. For the third time in the last hour, she longed for the days of crouching in a low trench as the blast of combat spells roared above her.

"No, dear," said her mother in a surprisingly calm voice. "The one with the flowers along the stem."

Jenefer glanced at the spoons again, trying to identify which one had the rose. There were three that looked like they had flowers but her vision wouldn't focus on them enough for her to determine which had roses. She reached for one and then the other.

"A lady doesn't hesitate, dear."

She glanced up at her mother. The matron sat tall in her seat with only the faintest of curves of her spine to emphasize her breasts underneath the square collar and reveal her shapely neck. Jenefer wouldn't have known about either except for the hour-long lesson on how sit properly like a lady in hopes of attracting a desirable husband.

"Nor does she stare."

Jenefer inhaled.

"Or roll her eyes."

Fighting the urge to snap out, Jenefer turned to the spoons and delicately picked up the first one that looked like it had flowers. She held it up between her fingertips to ask a silent question.

Her mother's eyes flick up and Jenefer felt impaled by the sharpness of her gaze. A cold sweat prickled along her brow, soaking into the dress that squeezed her arms and chest. "Is that your answer?"

Jenefer set down the spoon.

"No," said her mother as she held up her small finger.

Jenefer stopped.

"Even if you are wrong, just continue. It is better to pick the wrong one than tell everyone you don't know what you are doing." Despite working with Jenefer for two days, her mother's voice remained calm and measured. Jenefer suspected her mother's stamina came from helping four other daughters approach High Society in their early teens.

Despite being the eldest daughter, Jenefer struggled with the thousand tiny details of how to walk, sit, eat, and even talk only months before she turned twenty. She frowned for only a second before remember her mother's disapproval. She took a deep breath and forced her face to relax, to mask her emotions behind a pleasant expression that took two days to master.

Her mother smiled. "Very good."

The small praise brought a smile to Jenefer's lips. It was the first she had heard in quite a few hours. The rush from the approval felt like the euphoria when she managed to best her army trainer in a wrestling match.

The smile grew wider. "Happy thoughts make happy faces. Though, turn your wrist slightly when you pick up your spoon."

Jenefer's amusement faded. Looking down, she focused on the scar tissue that marred her from thumb to wrist, and then up to her elbow. Even though it was hidden, a matching scar started at her bicep and cut clear up to her collarbone. Only the smallest of marks would have been visible if she wasn't going to be wearing a high-collar dress.

She turned her wrist slightly to hide her injury. She knew there were other, fainter scars that crisscrossed both of her arms, face, and thighs, but they would be easily masked over by makeup and a bit of magic. She tried to see some of them, but without her glasses, the blur around her arm made it hard to distinguish anything.

"The correct spoon, Love, is the one on the far left. You always eat from outside to inside."

"Then why not say the furthest?" asked Jenefer.

Her mother winced like she always did. Jenefer's voice was a rasp of sound, rough from years of bellowing on the fields and too many spells ravaging her throat. It wasn't close to the musical tones that Jenefer was supposed to have for the party.

Every time Jenefer spoke, she saw the despair flash across her mother's eyes. The scars could be hidden underneath illusions, her hair was lengthened using grafting magic, but there was little they could do for her voice. Nor could they do anything about her failing eyesight.

Her mother took only a moment later to take a deep breath. "If you are speaking to a potential suitor, you may miss one of the courses. You need to know which one to pick up."

"Situational awareness?"

"Yes, but with wine glasses instead of monsters. Though I'd still worry about eaten if you aren't careful." A sly smile curled her lip for only a heartbeat before it faded.

Jenefer relaxed a little. There were only a few times her mother had an analogy that matched both of their worlds. She nodded and took a few bites of the thick, meaty soup before setting the spoon down delicately on the plate underneath it. The metal only clinked faintly before she set it down.
