---
status: alpha
title: Figurines
summary: >
  Another case where Viola struggles to investigate a crime scene after the guards had destroyed most of the evidence.
---

> As a growing field, forensic investigations suffered with little interest or investment from street guards and the upper echelons of society. --- *The Rise of the Magical Investigator*

Viola sighs as she pushed her way past the broken door. Her boots crunched against splinters of wood and stone. The air stunk of fire magic and the sensation of walking into it sent her skin crawling. It was Mudd's magic, that much she could tell. Only his resonance set off the twinge in her back whenever she got close. She clenched her right hand tightly until her leather glove creaked from the tension.

The city guards never understood the need for evidence. They burst into the room, magic flying and swords flashing, without a concern for bringing up the criminals up in front of the courts.

For the third time that morning, Viola wondered if the street guards were actively trying to make their job harder. She was new to the district and this was only her second case with Mudd since she got out of training. The sheer destruction the other guards left behind was nothing compared to the college's descriptions of procedures or her own internship.

It took her a few careful steps into the destruction before she could identify the purpose of the front. It was a tailoring shop, judging from the shredded clothes and sodden mass of fabric samples. That would be Isir's spells, he was the water mage and somewhat of a creep when she first arrived. He was handsome enough, if you didn't mind a lazy eye that always dripped tears. He also had three wives, so there had to be some merit to the magic. But, at the moment, she just looked at his handiwork with a sinking feeling of despair.

Further into the destruction, a false stone wall had been blown into a hidden area of the block. According to the design for the building, it was suppose to be part of a warehouse on the other side of the block, but according to the investigating guard, someone managed to brick it off a few decades ago.

Little numbered cards sat in the destruction. Each number looked as neat as if it was printed one of those strange, magical printing machines over to Podarin City, but she knew it was just Mudd's handwriting.

"Don't ruin my crime scene," said Mudd from the darkness. Even the sound of his sharp voice sent a twinge down her back. His footsteps made no noise as he stepped out of the darkness, but the sense of magic scraping against her nerves flared up.

When she was a girl, she thought city guards were handsome men with chiseled chests coupled with an inability to keep their shirts on. And, in the college, it was true. But, Mudd was completely the opposite of her teenage dreams. He wasn't fat, but neither was he fit. Just a little belly and balding on the top. He wore gloves like hers, but they were dyed a purple that didn't brush off on anything else he touch.

She fought the urge of annoyance. "I know."

His eyes, a brilliant brown, stared fixedly into her eyes. The sensation of discomfort spread out along her back, itching along her spine and shoulders. "And yet, you are about to step on a curve of glass."

Viola gasped and looked down, a blush on her cheeks. Pulling her foot back, she looked down. It looked like a little glass figurine that had shattered. It also appeared to be insignificant, but Mudd didn't consider anything unimportant when it came to crime scenes.

She pulled a black board from her back and tapped it twice as she cast a short spell. The board hummed as it shed dust and lint. From the corner of her eye, she noticed Mudd flinching and scratching his wrist.

Setting the board on her knee with a practiced skill, she spread her hand over the glass and closed her eyes. Her palm tingled and she sank into the sensation. It was magical, but quickly fading. Whatever spell was in the glass had been ruined when the glass shattered.

"Check for magic."

The tension tightened her shoulders. "I am, captain."

She took her time to analyze the glass before she felt comfortable picking it up. She still used a pair of steel rods to do so. She held the board as she stood up and looked around, expecting to see Mudd standing over her.

To his surprise, he was in the corner of the room setting down cards. "What type?"

Her lip curled in a smile. "You don't know?"

He looked at her with a hard look. "No, you were investigating it."

The grin dropped. "It's a containment spell of some sort. Looks like it has a physical component along with a mental one. There was something alive in it once."

"Safe to touch?" He gestured down to something on the ground.

She carefully stepped over, still holding the board. Looking down, she saw an intact crystal figurine. It was a male guard of some sort. It was humming from both of their presences. She glanced at Mudd who stepped back.

The humming quieted considerably but didn't stop. It wasn't her magic but neither she nor the figurine were powerful enough to cause the glass to crack.

She held out her hand over the glass and sank into the sensations of magic. Since she knew what to look for, it only took a few seconds before she brushed against the consciousness inside. It felt like a feline and she shivered at the pitiful meow echoing across her thoughts.

"Are they safe?"

She reached down and picked it up to set it on the board.

Mudd clicked his tongue. "A spoken answer would be sufficient."

She fought the urge to snap at him. "They're safe. There is a mind inside them."

"That's a felony."

"Only if we find a human."

He nodded with approval and pulled a black board from his own bag. Tapping on it, he cleared it off and began to gather up the crystal figurines he identified with numbers.

Viola joined him, shivering as she touched each one. She could hear their cries in her head. Tears burned her eyes, but she forced herself to keep gathering them.

"Picking anything up yet?" His voice cracked, but when she looked over, his face was stoic. It betrayed no emotions that she felt boiling inside her.

"Nothing human. Couple of cats."

Mudd glared at her response. "How do you know they're cats?" He shook his head. "You can't make assumptions."

Her temper snapped. "How do you know they're not cats? If it looks like a cat and meows like a cat, it's a cat. These guys think like cats." She tapped the black board in her hand and the figurines danced on the table.

"And different from dogs, yeah?"

Her mouth closed with a snap. "Y-Yes?"

"Have you found a dog?"

The blush returned. "No, captain."

He gave another nod. "I'll look for the others further in. Could you come up with something I can use. If the magic is this fragile, we need to find something before someone ruins my crime scene."

Her jaw tightened as she stared at him.

Mudd headed into the darkness, his body sparkling with light. He left the black board behind him as the figurines hummed softly.

"Bastard," she muttered, but she set herself down next to the figurines. Spreading her hands out, she sank into the magic enchanting the glass trying to find some way of identifying them beyond the pitiful cries that echoed in her head.

Almost a half hour later, she was crying but she had a spell in her head. Next to her, she had written out the patterns of magic on a notebook. It was always hard to create a spell from another mage, usually it took months to refine it, but ever since she joined the city guards, she had been creating spells on the fly almost daily.

Mudd, who was probably five times her age, continued to push her but he also did the same, using magic skillfully despite the guard's attempt to ruin the crime scenes with their enthusiasm.

She went to set down the board and realized there was a box of food next to her. It was dense breads, sliced meat and cheeses, but it was the cloth underneath it that startled her. It was Mudd's personal lunch.

Viola looked up with surprise at Mudd who stood with another board of figurines. They were humming loudly and sparks rose in the air around them. She gasped and wiped the tears from her face before he noticed.

He didn't seem to notice as he set down the board and backed away. "You've been working for an hour, eat."

"I..." she gulped, "I got the spell." She wanted to show it off, it was something she couldn't have done a month ago.

He opened his mouth to say something, but then smiled. "If you show me, will you eat?"

She gave a nod, feeling like a little girl.

He nodded. "Show me, please?"

Viola picked up two figurines. "This is a cat and this is a dog. If you use that spell," she gestured to her notes with the dog, "they'll glow when they are next to something that triggers them."

"Glow?"

She closed her eyes and cast the spell. The magic rose around her, a tingle of power coupled with a flash of pain along her lower back. She heard Mudd stepping back and the pain subsided slightly from the interactions of their resonance.

When she opened her eyes, all the crystalline figures were glowing softly.

"How does it work?" His voice was almost a monotone, that meant his mind was spinning.

"The colors and brighten when they see something that would invoke an emotion from them."

She grabbed a piece of paper and drew a squirrel on it. On another page, she drew a stick figure. Folding each one, she waved the squirrel over the figurines. Two-thirds of them began to glow blue. She smiled with happiness, she wasn't absolutely sure it would work.

Waving the stick figure over the figurines, more of them began to glow blue. She remembered the canine sounds she heard as she analyzed the figurines and identified the colors.

"Just like the difference between red and blue. Besides, they're both normally like, uh, I dunno, a soft glow... until something lights ‘em up. But it takes different things to do it. Cats light up when they see some kind of small critter they can chase, but dogs only do it for people. In f.... Oh, wait! What's that now?"

In Mudd's hand was a figurine. It buzzed angrily from his presence but it also glowed yellow. Unlike the rest of them, which were male guards, the figurine in his hand was female dancer.

Setting down the figurine, Mudd stepped back. "I can't do this, mage---sergeant."

She looked at him, a tightness growing in her throat, and then back down. Gulping, she waved the squirrel over the female dancer. It didn't respond. She waved the other page across it and watched the yellow glow spread out from the figure.

Her breath came faster as she waved it again. "It won't change if you don't change your test. Try something else."

This time, Mudd's sharp words didn't annoy her. She grabbed a piece of paper, wondering what to drew. After a few seconds, she drew the city guard's seal. It was a sloppy version of it, but close enough.

When she waved it over, the yellow glow intensified into a bright red.

"What does it mean?" Mudd asked.

"I---I think it's a human."

And then she heard a little girl crying her head. She sobbed at the sound of it. A tear ran down her cheek before she wiped it away.

The corner of his lip curled up. "And that's the felony." He turned and walked back into the darkness. "I'm just glad they didn't completely ruin our crime scene."
