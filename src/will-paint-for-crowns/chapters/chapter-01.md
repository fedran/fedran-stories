---
title: Will Paint for Crowns
format: Flash
genres: [Slice of Life]
summary: >
  Furl didn't have anything but time until he died. So he spent his days painting in the air for a few crowns for dinner.
---

He was only a teenager, sitting in a niche between two brick columns. In one hand, he had a makeshift bowl of dung and paper. In the other... well, it was missing from the wrist down. The wound was infected, raw and red.

That didn't stop him from swirling it before his body. Glowing colors traced after the movement as he painted images.

A few stopped to watch but no one paid. They just passed on without a word.

He kept on painting the air, hoping for a few crowns for food.
