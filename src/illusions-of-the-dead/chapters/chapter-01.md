---
title: Illusions of the Dead
date: 2020-03-26
summary: >
  Kotin has the ability to craft an illusion of someone recently deceased. There is a price to his powers, the memories used to create the illusion are lost, but no limit to how many come to him asking for one last view of their loved ones.
---

> Better to have loved and forgotten, than to have the weight of memories around your neck. --- *Hober and Hiber* (Act 3, Scene 4)

"Please? I really need to see her again."

Kotin didn't need to look up to know that the young man had a pleading look. It was the same expression that everyone who asked him for a favor had. He considered ignoring the request, drowning his stout, and then slipping away.

He wouldn't. He never did.

With a sigh, he looked up. The young man was in his early twenties, with tears shimmering in his eyes and his fingers twisted together with nervousness. Emotions were laid bare in his brown eyes; everyone who had grieved had the same gaze.

Pressing one hand against the worn wooden table, he braced himself in case it got violent. Clearing his throat, he said, "It won't really be her. It's just an illusion."

"It doesn't really matter."

"You'll forget her faster if you do this. You don't get everything back when it's done."

Jorg pressed his hand to his chest. "I don't care. I need to see her. One more time, that's all I need. One more.""

Kotin hesitated then he held out his hand. "Then put your hand here."

Without hesitation, the young man pressed his palm to Kotin's. Grief made people do foolish things.

Memories burst through Kotin's thought, briefly drowning him with the raw experiences of another person. They flowed through his consciousness but he couldn't help pick up images and scenes that scraped at his thoughts.

He saw the young man, named Jorg apparently, kneeling in front of a young woman. He had a broken arm but still managed to hold up a woven bracelet of young love. "Please, be my wife."

Kotin didn't see the answer, it blew past but then he was standing surrounding by friends and family. The memory was laid over with fresher knowledge, of how Jorg's father would die in only a few short weeks or how Marim's sister would get pregnant in a year. At that moment, neither were thinking about that as they looked into each other's eyes and whispered the words of marriage.

Jorg let out a soft cry, reality intruding. Kotin only guessed at what the people who touched his hand felt. He could only imagine it felt like someone sucking all the memories out, forcing the grieving person to relive them one more time.

Marim's death started innocently enough, a little tickle in the back of her throat. Neither thought it was more than a summer flu. When she couldn't walk easily, she would sit on the front porch of their family's cabin so Jorg would see her as he came home from a long day of farming his grandmother's lands.

Another memory rushed past, of them making love one last time, a desperate need that they both craved. Kotin let it wash past him without focusing on the intimate time.

More memories, over a year after the first cough, as he held her hand every day and did everything he could to save her.

Jorg had come home early with flowers in his hand and new golden necklace for her. He saw her on the porch and let out a sigh of relief. He was terrified of the day that she wouldn't be there. Coming up the stairs, he realized something was wrong.

She wasn't moving. Her head was bowed down as she rested against a pillow, her skin pale and drawn in. There was no movement in her chest, no smile on her face, nothing but a still life.

The flowers fluttered from his fingers.

Next to Kotim, blue flames burst out silently. They beat with the cool air of illusion and death. It quickly outlined a shape of Marim in her youth.  Then blue flames filled in gaps, creating a dimension of shape and a hint of movement. Seconds later, her smile came into view and she made the little noise she always did when she woke up happy.

Jorg gasped. "M-Marim? Is that you."

Kotim let his magic transform the memories into a spark behind the shape. Little things that Jorg never called out but his memories caught, like the way Marim always tugged on her sleeve when she was nervous or the way she cocked her head just slightly when she was admiring him. Little things turned into bigger ones, teaching the illusion how to be Marim for just a little moment.

When her eyes fluttered, Kotim brought their hands together. Pressing Jorg's grip into hers, he looked up. "When you let go, she's gone. I can only ever do this once for someone and this is your chance. Please, don't waste it."

Jorg wasn't even paying Kotim the slightest bit of attention. He reached out with his other hand, "Oh, love."

Marim did the same, her fingers glowing with a faint blue, reached out to press his palm against her chin. "I missed you, Father Bear." It was their private little phrase for each other, back when they thought they were going to have children.

Kotim slipped his hand away and held himself still. Only once had the spell crumbled when he stepped back and he was terrified it would happen again. When Marim's form remained in place and the blue didn't turn to yellow, he pushed his chair back and stood up.

His stout was on the wrong side of the couple. He made a halfhearted attempt to grab it. When he couldn't without disturbing them, he shook his head and headed for the far side of the bar. He had to remain within a chain of her but that didn't mean he had to listen to their conversation.

Sitting down, he sighed and leaned.

The bartender, the owner of the public house, set down a fresh stout. He was a younger man, maybe five years older than Jorg. "You didn't charge him anything."

Kotim shrugged. "Why would I? He's already paid the price and no one in this town could afford what I should charge."

"So stay in the larger towns."

With another shrug, Kotim picked up the stout. It was a different type but it tasted good. "There is more to life than money. Look at them."

Jorg and Marim were sitting at the table, lost in each other's eyes, as they whispered. She was stroking his hair as he sobbed against her shoulder. They had been only parted for three months, but he knew how much pain would be healed by this chance.

"What monster would put a price on saying goodbye? He's going to forget her fast enough, I can't take anything else from that man."

He drank deeply from the mug as he thought about the one time his magic had failed. "I'm a monster enough as it is."
