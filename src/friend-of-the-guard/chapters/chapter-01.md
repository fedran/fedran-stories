---
status: alpha
title: Friend of the Guard
summary: "Life is hard for a lone guard in an old museum working the night shift. There is always a danger that someone will find an artifact worth stealing and there is nothing Shilah could do about it."
---

> No one will sand for blood will rise to a position of predominance, that is my promise. --- Kager Degas-Masail

Shilah dropped her pack on the table with a disgusted sigh and peered down at the log book half buried in food wrappers. Her hand gripped into a tight fist as she shook her head before sitting down heavily on the wooden stool.

"Bastard couldn't wait, could he?"

Her voice was smooth but still filled with frustration and annoyance. Her eyes, the color of summer mud, peered down the scrawl written in the small boxes until she focused on the last one: "Ben, checked out at the seventh bell, Shilah late."

She looked up and gazed out the tiny window of the warehouse to focus on the tower clock halfway across town. The flickering glow of mage light that burned behind the crystal face made it easy to read the front, just as bells across town began to ring out seven times.

Looking down, she picked up the pen and added an entry below Ben's: "Shilah, checked in at seventh bell, Ben missing."

She dropped the pen and stood up again, looking around her. The guard room was less a room and more of a space defined by missing boxes. On all sides of her, huge piles of crates formed the walls to her office. In the center of the claustrophobic area was a table, the foreman's during the day and the guard's during the night. A huge pile of food wrappers and garbage indicated Ben's presence; the second-shift guard was a slob and lazy, but there was nothing Shilah could do about it.

With another sigh, she shoveled the garbage into the can underneath the table and began to unpack her own supplies. From her pack came an elegantly carved wooden container, scenes of the forest were carved in intricate detail along it's length. It was obviously an incredible effort of someone's life and Shilah held it like it was the most precious treasure in the world. It sloshed with some thick liquid, probably soup from her mother. Setting it down carefully, she removed a large paper sack with her dinner and set it next to the wooden container. A few books came out, and then her short sword.

Shilah hefted the weapon for a moment, staring at the emblem of her security company that dominated the hilt: an eagle spread out over a building, Eagle's Rest Security Company. There was some writing underneath, but she had never managed to read them.

Buckling the weapon around her waist, she stood up and imagined herself, as if she was looking. She wasn't young, nor was she old. Her skin was a dark brown, not black and not cream but a pleasant shade of chocolate. Her dark skin was a sign of having desert blood in her but but her grandmothers had left the sands two generations ago. Now, after her entire life in Rougan, her accent was slight only from being at home and only her skin told of her ancestors. Her eyes imagined the rest of her, the dark blue uniform, with a button down shirt and loose---fitting trousers. A badge hung on her substantial right breast, a steel image of the eagle. Above that, a small cloth strip with her last name, "Calidoth" sewn in neat letters.

Her mental eye drew down, frowning at the slight weight she couldn't manage to keep off, specially during the summer days. Moving down, she remembered to straighten her belt and re-tighten the laces to her black, calf-high boots. The black leather was well used after five years of working with Eagle's Rest, but she was just as proud of them as the day she bought them.

Happy with her appearance, she opened her eyes and looked around. Huge piles of crates loomed over her, but she just causally looked them over, noticing a few of them moved and a few new ones were placed neatly on top.

Nodding to herself, she spoke softly into the silence, "Business is doing well here. Hopefully they'll keep the company around a few more seasons."

Double-checking her sword, Shilah stalked down one corridor of boxes, looking for trouble and doing her job.

A bell later, Shilah sat down on the frail stool with another sigh. Her eyes flickered from box to box, but she something kept bothering her. A sense of... wrongness kept tickling her spine and set her on edge. But, no matter how many times she walked around the warehouse, she couldn't identify the source of her discomfort. Her right hand reached back to brush the hairs on the back of her neck, which have been standing on end for the last half bell. At the same time, her other hand drifted down to her sword, as if the cool leather-wrapped hilt was some defense her growing anticipation of something wrong.

She barely settled down on the stool, when she sprung back up to her feet. The constant anticipation of something happening was too much for her relaxation and she prepared herself for another round of the warehouse.

Stalling, she walked around the guard area, peering down the corridors and into the dim light. Nothing sprung up and surprised her and she glared again at the table, as if it was the source of her problems.

Her expression melted as she saw the table. Her eyes looked for her carved wooden thermos but she couldn't find it.

It was gone.

The faint thought of ignoring her discomfort faded quickly as she realized someone stole one of her most prized possessions. Her hand wrapped around the hilt of her weapon as she looked around, trying to find the thief.

Nothing.

The papers on the desk were untouched, the food next to the thermos was still in its package. Her eyes narrowed as she padded forward; the faint prickles of fear began to grow inside her, causing her entire body to feel alive as she stopped next to the table. Her free hand, the one not on the sword, reached out to brush the wood where her thermos was. The wood was still warm; whomever took it did so recently.

Standing still, a faint scraping noise caught her attention and her head snapped up in that direction. Shadows hung silently and no one moved, but the noise was slowly fading. Padding in that direction, she cocked her neck to hear it easier. As she reached the edge of the boxes, where it lead into a shadow-filled corridor, she bent slightly, then rested against the wood. The faint smell of young wood brushed against her, but she was focused on listening to the fading noises of wood on stone. A frown graced her face as she tried to identify the noise, but it was too soft, too erratic for any human.

Deciding that the noise was fading too fast, she took a deep breath and jumped forward, into the corridor, with a yell. "Hah!"

Nothing.

Shadows stood still, ignoring her as the darkness of the wooden corridor stretched out before her. In the distance, she could hear the wind picking up, howling through the streets.

Frustration flickered through her mind until her eyes dropped to the ground and she spied a faint trail through the dust and dirt of the warehouse. It was erratic, like the noises of her thermos being dragged away.

Finding something, she padded forward, being careful to make as little sound as possible as she crept along the wooden boxes. After a few moments, she caught a flicker of movement

Stepping forward quickly, she peered into the murky darkness until she saw the outline of a rat, a rather large one, come into focus. It was shuffling along the ground in jerks and tugs, moving slowly for such a large creature. Behind it, her thermos was being dragged along by the rat, in the same slow jerks and tugs. Biting back another sigh, Shilah stepped crept forward, her hand reaching out for the thermos.

She barely brushed the heavily carved surface when the rat jumped and scampered off; her fingers cracked against the stone floor of the warehouse and she swore loudly. As the rat faded into the darkness, Shilah could see how the rat got it's tail caught in the intricate carvings as was attached to it as it bounced into the darkness.

This time sighing loudly, she sprinted after it, "Come back here, you stupid little...."

The rat led her on a quick chase, through winding halls of boxes through a few more spaces filled with papers. When she finally skidded to a halt at the far end of the warehouse, she was panting hard. Her thigh hurt from where the weapon was banging against her, but after so many seasons of training, she wasn't going to put it down. Sweat poured down her dark skin; she wiped it off before it could sting her eyes.

In front of her, the rat stood up.

It was looking at her, it's whiskers moving slightly as it sniffed the air. It's paws were holding some of the more detailed carvings of her thermos, more for balance then to pick it up.

Shilah glared at the rat, half surprised at it's appearance.

Darker than a moonless night, the rat was black except for two white-tipped ears and a tail as pale as moonlight. It's tiny claws were faint streaks of brightness in the dim light of the warehouse, but it's eyes drew her attention. They were the most startling color of green... and they didn't blink enough.

Shilah crept forward, her hand held out in front of her as she strained not to make a sudden noise or movement.

The rat stared at her, unblinking.

She was about to reach it when the sound of glass breaking broke her concentration. The rat jumped and ran off, her precious carvings bouncing after her. Soft murmurs of voices, talking in low tones, drifted through the cracks of the boxes and she realized someone was breaking into the warehouse.

Her sense of duty and her need to rescue the wooden container fought for a moment before she glared into the darkness. Whispering softly, she struggled for her belt. "You won this time, rat, but stay right here and we'll go another round."

When no thermos came rolling out of the darkness, she gently unsheathed her weapon, being as quiet as possible. Seasons of training and practice made her almost an expert in it, but every time she felt like a novice entering her first battle. Bittersweet memories of one of her mothers teaching her sword fighting drifted through her mind and she pushed them down.

Crouching down slightly, she crept closer to the boxes, pressing her ear against the cracks to listen to the thieves better.

The first voice was a nasal whisper, carrying easily through the silence of the warehouse. "Be quiet. There is probably a guard here."

A second voice, of a younger man, "Yeah, a woman. Desert scum, dark skin and all."

One of the thieves continued speaking in a curt whisper, "They may be, but they held off an entire army with that scary magic of theirs, so hope she doesn't find us."

"Don't worry. She's at the other end, I checked on her a few moments ago."

"Be quiet anyways."

As they talked, Shilah moved herself into position and peered at the rouges. There was three of them, all men. All three were wearing dark, shadowy clothes and wielding nasty-looking daggers. They were moving smooth as if they worked often together. One of them, the younger boy, didn't flow as well with the other two. He was moving ahead, being brash and confident as he gazed over the boxes.

Shilah stepped back, hiding behind a box and waited.

Less than a few breaths later, she saw the boy past her and she lunged forward, her weapon slamming into his side. His light leathers did nothing to stop the sharp weapon as it buried into gut.

He screamed in pain as he fumbled with his dagger, but the strength left him before he could do anything and he slumped to the ground, dying.

Feeling a surge of excitement and power, Shilah yanked hard on her sword and it slipped out of the young man's side with a wet slurping noise. One foot kicked hard at a flailing hand as she moved forward, toward the other two thieves.

They were already prepared, their weapons held in front of them with deadly competence.

As they met, Shilah danced to the right as one of the thieves slashed down with his dagger, the sharp edge aimed for her neck. It missed with a whistle of air and the thief swore and he jumped back, barely avoiding Shilah's horizontal slash.

He was just drawing his weapon up as Shilah's maneuver continued, her left fist swinging right after the sword to connect hard to his ear. She could see the pain flash in the thief's eyes, but he made no noise, not even a whimper.

His dagger snapped up at the last moment as she jerked back; the point missed her rib cage, but the blade cut through fabric and skin and left a line of burning pain along her side.

With practiced deftness, the thief reversed the motion of his attach and brought it down.

Shilah managed to throw her sword in the way and both blades rang out as they connected; she winced from the vibrations of the parry, but the need to keep moving pushed her forward. Her right knee came up hard between the thief's legs. He managed to dodge aside so it connected with his inner thigh, but she had enough power to throw him back slightly and pull her weapon into guard position.

The memory of the other thief reminded her of danger, and she stared at the thief's eyes as she listened. The thief was looking behind her, but when he didn't react to someone coming up, she lunged forward.

Her thrust missed as the thief dodged to the left and she barely had enough energy to step back to avoid the slash. Taking a risk, she snapped the weapon right, spun around with a low horizontal slash.

The thief wasn't ready to attack her exposed back and he managed to step into her blade as it came around her other side. The sharp weapon slammed hard against the thief's side, right above the hip and she felt it cut through leather. A spurt of blood splashed on her face and uniform, but she was already drawing back to attack again.

The thief was obviously in shock, but Shilah thrust her sword hard into his stomach, shoving up into his lungs before yanking it out. Her foot kicked the thief back as she spun around for the other attacker.

He was already dead.

The third thief, an older man, was pinned to a wooden crate, three daggers buried in his throat, shoulder and chest. The expression on his face was of surprise and fear. At his feet, two throwing axes had falled where he had dropped them, a mute testimony of her death, if the thief didn't die first.

The sense of someone watching flared through her nerves and Shilah spun around, ducking lower as she brought her weapon into guard position. Her chest heaved with her breaths, shuddering gasps that seemed to shake everything. A thin trail of blood dripped down the sharp weapon as she scanned the darkness for her next opponent.

It was the rat.

The tiny black rat was standing there, it's tail still caught in her wooden container. The unblinking green eyes flashed in the dim light, but Shilah was already looking around for another opponent, her blood pounded in her ears with the excitement and energy of the fight.

When she spied no more opponents, she let the point of her weapon drop. Pulling out a blood-stained cloth, she automatically wiped the blade and sheathed it.

The rat just watched.

Moving closer, she padded toward the rat. It watched with it's green eyes until she was almost in reach, then it jumped off and scampered into the darkness.

Screaming with annoyance, Shilah ran after it, surprised that the rat could move so quickly. Her body was already beginning to fall from the combat high and she found an effort to move her legs as the rat continued to bounce in front of her, her precious thermos skipping along the ground as the creature ran along the stone floor.

Even as she was running, her mind was mentally identifying where the bodies were for the report afterwards. There was always an incident report.

The rat stayed in front of her, easily dodging her grabs and jumping through narrow cracks in boxes, the thermos always being in the right angle to slip through without catching. By the time she ran around the crate, the rat managed to regain the lead her longer legs ate up. It also ignored her swears and curses as it bounced over boxes and dodged underneath shelves, but Shilah manage to keep up with it... barely.

As she skidded around one corner, the fire in front of her stunned her enough to slam into a wall of boxes. The rat forgotten, she stared at the burning crate for a long breath before racing back to grab a red blanket, one designed to fight fires. Ripping it off the wall, she sprinted back and threw it over the crate.

White magics sparkled around the blanket as the flame sputtered. Shilah, knowing the power of the blanket, pushed herself into the heat to beat the flames with the fire blanket. Fire seared her skin as she continued to put out the flames frantically.

Finally, the last flame gutted out with a hiss.

Shilah stepped back to wipe the soot from her face. Her hand, full of light burns and blistered, screamed at her and she dropped it to her side, unwilling to cause more pain to herself.

The smell of burnt wood assaulted her senses and Shilah back to the destroyed crate to inspect it. It was next to a many other ones, with the neat print of the shipping company written on the half-mutilated shipping order. If the fire was left to burn much longer, it would have ignited most of the warehouse. Inside the crate, she saw the remains of a few golden objects; the heat already destroyed the crate's contents.

Then, her adrenaline surge faded and she felt her knees struggling to collapse underneath her. Fighting back the lethargy, she set the torch aside and looked around.

No attacker burst out of the darkness and she shook her head. Finding a non-burnt crate to sit on, she sat down heavily and leaned against another crate. Her voice was filled with exhaustion as she spoke out into the darkness. "What is going on?"

As she expected, the charred remains of the crate didn't respond to her.

She continued speaking to it, though, as a focus for her thoughts. "First some thieves break in, then a fire starts in the middle of the warehouse. Something is strange and I don't like it."

A strange, feminine voice spoke out softly, dangerously close to her, "You're not suppose to like it. In fact, you were suppose to die from it."

Shilah jumped up with a yelp, her hand fumbling for her sword as she stepped away from the crate and spun around.

There was nothing there.

Her eyes scanned around the room as she managed to yank her sword from it's sheath, holding it with a shaky hand. Around her, the boxes loomed but there was no source of the voice.

Then, a flicker of movement caught her attention, a form in a shadow. As she moved herself into a defensive position, the female mage stepped out from the shadows.

She was short, a few handspans less than Shilah. Raven-black hair, complete with white streaks, cascaded down her shoulders and around the mage's small breasts. Her outfit was brief, little more than a long skirt and a tiny halter top, both jet black and trimmed with silver. In one hand, she was carrying a silver dagger with a black gem in the pommel; her other hand was empty but moving in a strange, almost hypnotic pattern.

Worried of magics being used, Shilah lunged at the woman, her weapon poised to slash down. It skidded off some field of energy, throwing silver sparks in a shower that avoided the mage and burned Shilah's leg. The effort of her blow slammed her into the same mystical shield and she slid down, momentarily dazed.

With a cruel chuckle, the woman gestured with her free hand and Shilah felt her sword rip out of her hand, thrown by some magical force. It skidded along the ground and buried itself into a wooden crate.

"We don't need that, now do we?"

The woman's voice was a whisper, playful and violent at the same time. She stepped back away from Shilah as the guard manage to pull herself to her feet, panting. For a brief moment, Shilah put her hands on her thighs to rest, but a blister forced her to jerk them away. Finding nothing to lean again, she stood up straight, hoping for a weapon.

The mage smiled, her lips quirking up slightly as Shilah felt her gaze inspect her more closely.

"My... what do we have here?"

The voice turned more appreciative and Shilah saw the gaze on the woman's face. It was the same look some of her mother's friends gave her when they didn't think she was watching. On this woman, though, the look held more open lust than she ever wanted to see in a woman. Feeling distinctly uncomfortable, Shilah glanced away from the look at her weapon, seeing it only a few steps away.

Forcing her eyes to look back into the dark eyes of the mage, she mentally prepared herself to lunge for the weapon. The mage's smile broadened as she stepped closer; Shilah noticed that she was wearing slippers on her feet that left the painted toes exposed. The silver polish matched the ones on the mage's fingers.

With a grunt, Shilah threw herself at her weapon, crouching down as her fingers wrapped around the hilt. With a scream of rage, she yanked the weapon out and spun around, aiming for the woman's neck.

Silver sparks exploded where the sword hit the mage's shield and Shilah felt the power of the magic rattle her arm and shoulder. The mage gestured again and the sword yanked back, against the wood. This time, Shilah manage to hold on to her weapon, but her shoulder protested as it felt like it was stapled to the wooden crate.

Another gesture as her other arm was thrown back, pinned against the crate with the same magical force. Shilah yanked at the resistance, but found both wrists pinned against the wall; as she pulled, silver energy sparkled brightly but didn't resist.

The mage stepped forward again, the same smile on her face, "Why don't you... just relax? I'm sure I could find something we both could enjoy."

Shilah thrashed for a moment, then glared at the woman, panting softly, "If it's me, I'm not interested."

The woman's voice was playful as she whispered quietly, "Pity."

Shilah ignored the comment and glared at the woman, "What do you want?"

The woman held up the dagger, "This. It was in a box a few lines over. I need it for a spell and I figured after the fire, no one would notice. But," her eyes glanced over Shilah again, "there may be something else I want."

Shaking her head, the guard struggled against her magical bounds. With her free hand, the female mage reached up to stroke Shilah's cheek; Shilah could feel the energy of the shield fade, but she was pinned helpless to attack. The woman smiled softly, "Come on. Do you really want to be a guard all your life? I could promise you... so much more. Money, power," she paused again meaningfully, "...sex."

Disgust and rage slammed into her and she tried to kick out. Her ankles encountered the same magical energy as a sparkle of silver exploded from below. The mage's soft smile dropped in a flash.

"What a waste. But I need the dagger more than I need you."

Shilah watched helplessly as the mage pulled back the dagger, ready to thrust it into her chest. But, before the dagger could swing down, three quick flashes interrupted the mage's gesture. Both the guard and mage looked down to see three, tiny black dagger sprouting from the woman's chest. Each one sparkled with a black, almost oily, glow before fading.

As the third dagger's glow faded, Shilah watched the life drain out of the mage. A puzzled expression crossed her face as she crumbled to the ground and the energy faded from Shilah's bounds.

Lurching forward, Shilah spun around, her sword ready to parry. Nothing attacked her and no daggers flashed in the air, but her eyes still scanned up.

Five crates up, a tiny woman, barely half her height, was standing with her hands on her hips. The newcomer was wearing a tight, form-fitting black outfit, except for two rounded white ears that sprouted out near the top. A white tail trailed down her leg and Shilah realized she was looking at a dalpre, one of the beast people, probably a mouse- or rat-woman.

A nagging suspicious crossed her mind and she looked down into the woman's left hand; the heavily carved wooden thermos confirmed it. With a sigh, she sheathed her weapon and stood up straighter. "So, did you do this," it was a statement as the events of the night started to make sense.

A nod and a soft giggle.

"You used me, didn't you?"

The mouse woman shrugged and sat down on the edge of the crate, her tiny hand playing with the edges of the thermos. When she spoke, it was a soft, high-pitched voice, "Lady Arahlos is known for her shield magics. Nothing short of an adept can break through them, which is why she wanted to come along. She apparently didn't trust her hirelings to identify the correct dagger. It was just a matter of making sure she decided to break in the day you were working."

Swirls of anger began to drift through Shilah as she realized she was set up. Memories flooded back of the rat, who just managed to keep ahead of her. With a sigh, she looked back up at the woman, "And how did you know she would be... attracted to me?"

"It was a guess."

Annoyance dripped off Shilah's voice, "If you were wrong?"

The mouse woman shrugged, "I wasn't. Her preferences are rather well-known. She has a thing for desert women. Once she saw you, I knew she would drop the shield to touch you."

There was a brief silence before Shilah asked another question, "Who are you?"

A smile that reached the unblinking green eyes, "Just a friend."

Feeling betrayed, frustrated, and shaky from the fights, Shilah decided to let it go and pointed to her thermos tiredly.

The mouse woman lifted it up and admired it for a moment. "Nice work."

Shilah nodded and watched as the woman tossed it high into the air. She had to step back to grab it, her sword dropping to the ground with a clang as she grabbed onto her precious container with both hands. When she looked back, the mouse woman was gone.

A few days later, everything was back to normal. Killing a mage and three thieves earned her no bonus, no special favor. Stopping a fire saved the company thousands of credits, but she got no special mention, no reward.

Just part of the job.

Shilah sighed and propped up her feet on the table as she leaned back on the stool. On the table, a cup of hot soup was slowly cooling. In one hand, she held a carving knife; the other held a new wood-covered thermos.

She began to carve a new image on a new thermos. One of a mouse woman.
