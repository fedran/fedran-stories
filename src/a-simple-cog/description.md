---
title: Description
---

For generations, Flarius had served the baron's family as a treasured servant. However, even his loyalty would be brought to the test when the youngest baron decides that a group of warriors were an army and the only way to deal with it was to use the terrible weapon his father had constructed.
