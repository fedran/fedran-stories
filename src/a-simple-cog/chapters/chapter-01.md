---
status: alpha
title: Arguments
summary: >
  When the baron insists that an army was set to destroy him, he decides to use the ultimate weapon his father had built years before. But his servant tries to stop him because he knew the weapon should never be used.
---

> The war between Kormar and Gepaul started many years before the first blade was unsheathed. --- *The War That Wasn't*

The door slammed hard into the plaster wall and knocked down a few errant portraits before swinging back just as violently. The door smacked the man following behind the baron right in the chest, staggering him back. Shaking his head, the older man managed to clear his head and burst through the door.

"Baron, I insist! Please, don't---"

"We have no choice, Flarius," the baron didn't look back as he spoke in his cultured Tarsan accent, practiced before a mirror since he was eight. The young baron stopped in a hallway and smacked one of the gas sconces. It swung down and a loud booming rang out. Flarius shuddered as the baron turned two others and the floor sank down into stairs leading below.

"B-But, my lord, your father said this was only for emergencies!"

The younger man's eyes flashed as he spun around.

"And what do you call an army heading inland?"

"You don't know---"

"I know what an army is when I see it," snapped the baron. Without giving the older man a chance to speak, he raced down the stairs.

Flarius hurried down the stairs as fast as his aging bones could move. He only made it half way before he heard light footsteps coming down the stairs after him. Looking up, he saw a familiar face of his personal maid, a gamine he picked up from the streets of Sorfil.

She made to help him down the stairs.

He waved her off. "No, Gisel, try to stop him. He can't ever turn it on."

"Yes, master."

She slipped away from him and raced after the baron, the tapping of her black slippers fading quickly. He swallowed hard, dreading the future and hurried down the stairs as fast as he could.

At the bottom, he came to a hallway lined with copper pipes. Heat boiled off every surface. His wool suit, perfectly appropriate for early fall, hung heavily on his shoulders as he hurried to the end. By the time he reached it, he had to lean against a brass railing to peer down the shaft into darkness. He grunted and tugged on the call chain. In the depths of the shaft, two gas lights glowed as an elevator rose for him. Still gaping for breath, he winced at the sight of a bit of Gisel's dress clinging to one of the many pipes.

"Be safe, girl."

The elevator rose up and Flarius' mouth dropped as he saw an armed guard standing on it. Armed with a saber and a new rifle, it took him a second to recognize the man.

"Jacob!?"

Jacob just nodded.

Panting, Flarius stepped on the elevator and Jacob flipped the switch. Half a century old hydraulics rumbled and the elevator dropped quickly. Flarius watched the endless pipes and gauges flash by. When he spoke, he tried to be as casual sounding as possible. It belayed the storm of his mind raging. "When did he buy it?"

"Three months ago."

Flarius frowned unhappily at the revelation. Around him, the heat cooled down but it was replaced with the sound of immense machinery rumbling deep in the caverns below the baron's mansion.

"Thirsty?"

The guard just grinned.

Flarius reached into his pocket.

Jacob stepped back, hand dropping to his saber.

The steward pulled out his flask. "It's peach brandy."

Flarius left it with Jacob and he hurried down a musty tunnel. As he entered the brass and iron control room, he realized the baron waited for him.

The young man grinned broadly. "Ready for history?"

The older man leaned against the railing. "Please don't do this."

"It is for the safety of Kormar."

"Your father said to use this only," he almost choked out the word, "when all of Kormar was at stake, not just your barony."

"How do you know this isn't a surprise attack by Gepaul?"

"You don't know who is coming. Gepaul are our allies, not enemies."

"Then Tarsan or somewhere else."

"You attack without knowing your opponent?"

The younger man's eyes narrowed. "I know it in my heart."

"Young hearts make many mistakes."

"And old ones don't make enough!"

Flarius reached out to stop him, but the baron triumphantly yanked on the final lever and activated the enigmatic machine. Pressure built up as the room began to turn around on massive gears. Fear and sweat dripped down the older man's back as he felt the caverns rumbling, dust and steam rising up everywhere.

Then, an explosion as springs snapped and bolts sheared apart. Steam tanks ruptured and hydraulics exploded in the darkness. Something shot out of the darkness and then Flarius only saw light.
