---
status: alpha
title: The Light
summary: >
  After Flarius tried to stop the baron, he was knocked out. When he woken up in the ruins of the weapon, he didn't know if it had been set off or something else had happened.
---

> Too many fools only knew how to activate a spell but have little regard for the consequences of their actions. --- *One Spell Too Many*

Flarius groaned as he opened his eyes. Water dripped from every surface as he tried to remember where he was. The hard metal floor dug into his back and Gisel knelt over him, blood dripping from her shoulder.

"What happened?"

She smiled sheepishly, "It broke down."

She help him to sit up and he looked around at the now silent control room. The metal pipes were twisted and smoke rose from control panels.

"Who knew that his father would have gotten anything wrong?"

She spoke up softly, "Um, begging the pardon, master, he didn't make no mistake."

He looked over to see her holding a simple cog between two bloody fingers. With a sigh, he pulled out a bit of white fabric from his pocket and wrapped her fingers. His fingers deftly removed the cog and he put it into his pocket. "I hope we did the right thing, girl."

"You promised his father."

He reached up with a soft smile, to rub his thumb against a smudge on her soft cheek. "An oath is an oath. Made over a lager in the middle of the night, but thank you for helping me keep it."
