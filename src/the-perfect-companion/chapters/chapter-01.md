---
title: The Perfect Companion
teaser: >
  Sinair had retired from a life of travels and adventures but now his father had intentions to marry him off. However, Sinair  had no interest in a wife or husband. He just wanted to live his life without relationships.
summary: >
  Sinair had retired from a life of travels and adventures but now his father had intentions to marry him off. However, Sinair  had no interest in a wife or husband. He just wanted to live his life without relationships.
date: 2020-09-22
---

> For a stunning example of true love, look no further than the marriage of Sinair and Xaris Paxar. Despite their class differences, they were together for over seventy years without even a hint of an affair or betrayal. --- Rumanis Caldur, *Grand Romances in the Years Before the War*

Sinair stood in the center of the room and struggled not to turn on every person who walked behind him. It would not be appropriate for the guest of honor to attack the guests, but that didn't stop the muscles in his neck from knotting painfully.

It was hard to resist jumping. Less than a month ago, an assassin had stepped out of an alley and stabbed him. The brief brawl ended poorly for his attacker but it had taken a month to recover from the nerve poison on the blade.

Being stuck in bed had given him plenty of time to think about all the times he had been stabbed, burned, drowned, and crushed. He was tired of tasting blood in his mouth or waking up to assassins. The countless fighting and near-death experiences haunted him; even when he was awake, he could see the flash of blades in the corner of his vision. His bones ached from being away from home for so many years.

Sinair shook his head and forced his attention back to his surroundings. He survived wars by paying attention. In many ways, he was still in a battle. It just wasn't one fought with knives. His eyes narrowed as he looked at the combatants.

His opponents were the young women wearing fancy dresses that milled around Sinair's father's great hall. Their leader was his father. The young women circled around him, trying to find their opportunity to bat their eyelashes and say the right thing to win his heart.

Well, at least at the beginning of the party they were trying to woo him. An hour into the party, most of the tension had fled and the young ladies were more relaxed. A few choice words had helped them to realize they were to enjoy themselves because they had no chance at stealing his heart.

One of the final young ladies stepped up. He remembered seeing her entry in Stonewait's, Beatris Kamp or Beat to her friends. Beatris was twenty-something with bright blond hair pulled into a complicated braid and laced with gemstones. With her makeup and outfit, it had probably taken her all morning to prepare to steal his heart in the few minutes.

Sinair nodded to her as she approached. "Good evening, Lady Kamp."

She fluttered a small, lace fan and smiled broadly. "Good evening to you, good Sir. Did you have a chance to see the beautiful string of clouds that were arranged over the manor? It looked a lot like these."

With a flip of her wrist, she rested the fan against her cleavage and right underneath a beaded necklace.

A casual opening speaking about the weather? That was chapter two of *A Lady's Guide to His Affection*. Chapter four talked about drawing attention to a woman's physical assets. No doubt, she would be using more suggestions from the book in the few minutes it would take for Sinair to convince her that she had no chance at stealing his heart.

He couldn't flat out say her flirtations were doomed to fail. There was no graceful way of interrupting her without risking an emotional response. While he despised it, decorum meant he couldn't have her crying in public.

Sinair took a deep breath. "Sadly, I wasn't able to see the clouds. When I arrived, the sun wasn't up and my father insisted on a dragging me into the wine cellar to have a long involved discussion about dogs and stags."

Beatris looked confused for a moment.

"The mayor? My father?" Sinair gestured to his father on the far side of the room.

"I know that, but dogs and stags? Aren't you a little old to know how babies are---" Her eyes widened. "Oh, I'm sorry, Sir. I presumed too much."

Sinair chuckled and held up his glass. "You have no idea.I'm forty-two but have never been in an intimate relationship. I have, however, been in three wars and countless fights. My joints ache. When I can sleep, I always do it in a corner on the floor because I've been stabbed in the back too many times." He was careful to keep his voice cheerful and modulated.

There was a brief haunted look in her eyes. A flash of a young woman realizing that she was talking to someone twice her age and was completely foreign to everything she had experienced in her life. He could almost taste the fear in the air as the grip on her wine glass tightened. "I... I...."

All the books in the world couldn't prepare a young woman how to face the sudden fear and despair. It was a cruel weapon to use, but Sinair needed to win the battle decisively to avoid her hopes being raised.

He caught her hand and brought it up to his lips. He kissed the back of her knuckles chastely. "How about I ask about you tomorrow but then get in enough trouble that will prevent me from ever meeting you again? That will let your parents and the rest of the gossip-trading thieves in this city know you are still a catch but gives both of us the opportunity to part with our dignities?"

"Why?" Beatris whispered. She tried to smile but faltered. "I thought...."

The muscles in his back tightened. He fought the urge to look at his father. "Just relax," he said in a calm voice. "It isn't you. I'm just not interested in marrying any woman. Even one as pretty as you."

She blinked but then a more honest smile crossed her face. "I have a brother and he likes to steal my dresses. He might be pretty enough."

Sinair chuckled and straightened. She had promise in the war of society. "Alas, my dear. My interests don't run toward men either."

The young woman pulled her hand back. The tension had faded into a surprising playfulness. "Are you sure? I've heard he's quite attractive."

He shook his head. "My desires travel neither toward marriage or sex. Man or women. It just isn't something I want. Never have, never will."

Beatris stepped closer, her yellow dress swirled around her. "Then why a bachelor ball in your name?"

He nodded his head back toward his father. "My father disagrees and is insists that I just haven't found the right girl to set me on fire. He's been has been trying to get me married for decades." He snorted with amusement. "I started going out on adventures and war just to avoid this very thing when I was your age. I just walked out and never came back."

She glanced around the room. "How many others have you given this warning tonight?"

"Tonight? You would make nineteen."

"There were only twenty of us who got invitations."

Sinair shrugged and grinned. "Imagine that. It's been a whole hour since the party started."

Her brown eyes turned back to him. "You really aren't ever getting married?"

"Not if I can help it."

Beatris curtsied. "Well then, good sir, I shall take my leave and anxiously await your kind words."

With a bow of his own, Sinair watched as she strode away. He straightened and smiled to himself. One more young lady and he could sneak out without his father rebuking him.

The crowds of people around him suddenly pulled away. The rapidity of the movements sent off a thousand warning bells in the back of his head. He lowered his hand down to his belt before he remembered a proper guest of honor didn't bring their weapons.

"Son, a word?"

Sinair's smile dropped from his face. He turned to his father who approached with a glass of wine in his hand. There wasn't another person with ten feet of them.

His father gestured toward Beatris who had rejoined her entourage. "What did you tell her?"

"She's a brilliant young lady who is very---"

"No, what did you say to chase her off? That conversation was too short and she too happy to be walking away."

For a moment, Sinair wished lying was an option but his father's magical talent was seeing lies for what they were. He sighed and shrugged again. "I told her I wasn't interested in her."

"Didn't I tell you that wasn't acceptable?" His father's tone came out tense and furious. He reached up and rubbed one of the medals on his chest; it was for the Battle of Kanailford.

"Yes but it is the truth. No matter how you wish otherwise, I have no desire for Beatris or any of the other ladies in this room."

His father growled. He used his left hand to poke Sinair in the chest. "Pick one. I don't care if you don't love her. Just find someone pretty to hang on your arm when you parade around town. And not some gold-digger who doesn't have the decency to hide when she cheats on you."

Sinair's shoulders throbbed.

"You are a Paxar and its about time you acted like one. Paxar are good men." He poked Sinair again. "We are proper, dignified, and we don't disgrace the Divine Couple or the blood in the soil by avoiding women!"

Sinair glanced around at the room. While no one appeared to be staring, he knew they were paying attention to every word and gesture. Gossip was the universal currency, one that was produced with every tense stand-off such as the one he had stumbled into.

The muscles in Sinair's shoulders tightened more, grinding down as he tried not to lose control of his emotions. Even in his forties, he hated arguing with his father who treated him as a teenager. "You said I had to come to this party, not that I had to choose a wife tonight. In fact, I'm pretty sure you made a point of not letting me know I was the guest of honor until it was too late."

"You damn well know what this party was about."

"Parading women half my age around in hopes that I fall in love with their breasts or ass? Twenty minutes or even an hour isn't enough time to get to know anyone."

His father reared back and glared. "Well, if you would tell me what you like, I'll pick girls with better assets. At this point, if you prefer a rooster instead of hens, then tell me and I'll find the prettiest rooster in a skirt!"

"How about letting the Blessed Mother guide me?"

"How about you just pick a damn wife!" hissed his father. His face darkened into red. "One month, Sinair. One month and I will make the choice for you."

Sinair smiled bitterly. "You can't force me into marriage, you know that."

"No, but I can ensure that any living son of mine is still married," came the hard reply.

He tensed as he heard the words. They were made in the heat of passion but it also revealed how far his father would go to ensure marriage and the family reputation. Twenty-eight years of killing monsters and being a hero was for naught, the same fight remained no matter when he came.

Sinair considered coming out of retirement. Though he was getting slow, a blade in the gut or poison in his veins was a better-looking alternative to hurting one of the ladies around him. They wanted marriage, they wanted the glamours lives that came from being the wife of a hero.

He shook his head and scanned the room. He still needed to find Lilat of Glauskar.

"What are you doing now?" snapped his father.

"I have one more to send off before I get out of this cursed party. I find the company---" He looked pointedly at his father. "---distasteful at the moment."

His father's face darkened. "Why don't you just leave like the spoiled ass you always were?"

"Because unlike you, father dear..." Sinair paused to keep his tone measured and set a face smile on his face. "... I believe in not hurting anyone. Being ignored by the guest of a party is rude and would give the impression that she is less than desirable. She doesn't deserve to get hurt because of me."

"Why do you care?"

"Because it's the right thing to do. It isn't their fault they were put into an impossible war."

His father grunted. "Always the hero, aren't you?"

"Yes, father." Sinair didn't look at his sire. Instead he focused on scanning the crowd. "That's why I waited so long to come back. It's hard to be a hero in the family's shadow."

His father's eyes narrowed.

Sinair stared back.

"The Glauskars were unable to come to the party. They didn't say why but the courier said it was a family emergency. Since you've already disgracefully poisoned this party, just avoid making a fuss when you sneak out."

Sinair bowed to his father, but not as deeply as the one for Beatris. "I don't make scenes."

"One month from today." His father held up a finger. "I swear on the family's blood, you will be engaged by then."

The "or else" was left unsaid.

Sinair nodded. "Good evening, father."

"One month."

It took Sinair some time unless he could escape the crowds and head out the main doors.

The front of the Paxar Manor was brightly lit with magical lanterns that cast a warm amber light across the paving stones and flower gardens before disappearing into the night. Only the entry circle, a ring large enough for fifty carriages, marked the darkness in a halo of yellow.

There were just over twenty carriages waiting along one side of the massive circle. A testimony to the fading reputation of his family. Decades ago, there were would be carriages around both the inner and outer rings of the circle plus more lining the two roads that came into the manor lands.

Sinair nodded to the door guards and then cut across a flower garden on his way to the end of the carriages. As he passed them, the drivers and guards of each of the families attending looked at him with a variety of emotions: respect, annoyance, and boredom. He gave a few sympathetic looks; drivers had to wait by the carriages for hours until their charges were ready to leave.

At the back of the line, with a number of spots left open, was his own carriage. His driver---a good friend name Xaris---leaned against the side with a book in one hand and a lit cigar in the other. Her slender body looked more like the hitching post, long and lanky.

He approached. "Ready to go?"

Xaris snuffed the cigar against the carriage and closed the paper-bound book with a snap. "It's before midnight. Manage to piss off your father already?"

"He's furious at me again." He groaned. "Why can he make me feel like a kid every damn time he opens his mouth?"

"Asshole never change." Xaris tucked her book into the pocket of her vest before reaching over to open the door. "Need the long way home to cool down?"

The long way. There were very few ways back into town where everyone else lived, but one route would take them near the marsh. It was quiet and a perfect place where both of them didn't have to pretend to be master and servant.

He nodded and got into the door.

"As you wish, Sir Knight." There was only a hint of sarcasm in her voice.

Sinair listened to Xaris as she unhitched her roan and then crawl up to her seat. He closed his eyes and let his mind drift back to the party, reviewing the words and interactions with the same scrutiny of a post-battle analysis.

Concentrating, he lost time until he felt the carriage roll to a stop.

"No one's watching," Xaris said quietly.

Relieved, Sinair pulled himself off the padded bench and let himself out the carriage. They were on a dark road, with no light except for single oil lantern hanging from a hook on the corner of his ride. He grabbed the railing and pulled himself. The leather creaked as it accepted his weight.

Xaris handed him a flask. "Bourbon from the coast," she said. As soon as he took it, she grabbed the reins and started the horses again.

Sinair took a small sip and winced at the burn. He never liked getting drunk when there was even a possibility of danger but a little alcohol wouldn't hurt his mood.

They drove for a short while.

He listened to the insects buzz and the frogs croak. It  reminded him of the long nights before a battle or those first evenings when he first set out. That was when he got to know Xaris, as they spent nights talking over a fire while sharing the same flask. He closed his eyes and smiled while he tried to get his thoughts in order.

When he was ready, he grunted. "I told the girls not to bother trying to woo me. It was an exercise in futility."

Xaris chuckled. "I bet that went well with your father."

"He threatened to have me assassinated if I wasn't engaged in a month."

"Assassinated? He actually said that?" She sounded surprised.

"No, he's too smart for that. He just said that any living son of his would be married properly."

Xaris took the flask back. She started to drink but then stopped with a hiss. Propping one boot on the bar, she tucked the reins under her armpit. Leaning away from him, she pulled her vest free and then dumped the book on the seat between them.

In the light, he saw it was a treatise on vehicular design. Xaris had been following the popular trend of mechanical devices. Her interests were in the races that had began to roar across the country but Sinair saw how they were only years way from turning a rush for speed into something more military-inclined.

She sat back down and took a long gulp from the flask.

He chuckled and leaned against the lantern pole. "He probably won't actually kill me, but he could make my life a living hell. Somehow, I don't think he's going to let my brother become the head of the family."

Xaris looked at him. "What? Not after your dear brother sent that gang of thugs after your sister?"

Sinair nodded. "The thing is, I honestly think father was more upset that Lak did such a poor job of trying to kill her more than the fact he tried in the first place. Pir is just as bad as the rest of us but Lak shouldn't have gotten others involved over that land deal."

"Well, your family does have issue."

Sinair chuckled. "They do, Xar. They do."

"How much longer before you can leave? I'll go with you, you know."

Sinair's amusement faded. "I was gone twenty years. You saw what was happening to me." The memory of the last seizure was still strong in his mind. It had left him weak just when he was recovering from the poison and set him back a month. "It's going to take me at least four or five years before I can leave this damn land without getting sick again."

He shook his head. "Blood and mud."

It was the family saying, a way of acknowledging that their power came from the land itself and living away gnawed at both the soul and body with each year apart.

Sinair rubbed his arm. He wouldn't have had a problem being poisoned if it was ten years earlier.

Xaris handed the flask back. "Going to have a sham marriage then? If you do, I'd ask you keep Beatris out of it. That girl deserves more than what you can give her."

He took the flask but didn't drink.

She looked at him and smiled. "Second cousin, father's side. She came by yesterday for hints on stealing your heart."

Sinair smiled. "What did you tell her?"

"Don't use any advice from a book. You always hate when you know when someone is blindly following advice."

"She did invoke *His Affection* a few times. However, of all the ladies I warned off, she was the most enjoyable to speak to. She recovered quickly and offered her brother in her stead."

It was Xaris's turn to smile. "Oh, Lar. If I had interest in men, he would be a catch. The problem is that he wants romance, passion, and plays. Epic poems of going to parties to look into his lover's eyes. Practicing his poses so they can paint him while dancing until the morning."

Sinair reached over and set the flask down between them, patting it to let Xaris know where it is. "Don't worry, if all goes well, there won't be anyone for either of us."

It was a joke between the two of them. Neither Sinair nor Xaris had any interest in romance or sex. It was one of the ties that kept them together as friends after she was hired as his driver when he was a teenager.

He saw a light in the distance, bobbing with the curves of the road ahead. Not wanting to draw attention to himself or his scandalous position on top of the carriage, he leaned back and waited.

Xaris hummed to herself as they drove past. When the other carriage came within light, she saluted them while passing. "Evening."

"Evening, Sir," came the gruff reply of a bearded driver.

Sinair shook his head with amusement. Other people frequently thought she was a man while driving.

Xaris spoke as soon as they were out of earshot. "Easy for you to laugh, but I'm in the same boat as you."

"Oh, your power-hungry father wants to get you married to ensure the bloodline stays within the lands?"

"No, worse. My baby-obsessed mother wants me to spawn."

He perked up. "Oh, your sister is pregnant again?"

"Nassin just had twins two days ago. Kloel announced her pregnancy yesterday over lunch."

"That makes nine for Nas?"

"Ten screaming little bastards. You would think she would marry one of her children's fathers but she's content to pop them out like fish spawn." Xaris's voice grew sardonic. "Between my five sisters, that makes twenty-four grand-children."

Sinair laughed. "And yet, you need to continue the family line. Oh, I remember waking up one day with her sitting on the chair across from me asking why I wasn't sleeping with you. Followed up by asking how many babies I planned on planting."

Xaris turned to him sharply. "When was this?"

"When my house burned down and I was sleeping on your sofa for a month."

"You mean when you didn't check for clothes for firebugs and one burned down your house. You know to always look for eggs when you go into those caves."

"Well, I do now." He started to say something but then an idea started to take root. There was a way for both of them to solve their problems.

"I don't remember you meeting her though," said Xaris.

"You were driving for Lak then. That was during his summer of adventures."

Xaris smiled broadly. "The bar-hopping year? Also known as the puking after two glasses of wine every blessed night?"

She bumped him with her shoulder. "At least you can hold your spirits."

He didn't answer as his mind began to work out ideas. Xaris has been a good friend for a long time. They shared mutual interests, but more importantly, they both had no interest in each other or anyone else. They had also been playing a role of master and servant for many years, how different would it be to pretend to be in love?

After some time, Xaris cleared her throat. "You haven't talked for a league. We're almost to town."

"Just thinking about things."

"Battles?"

He frowned. As much as he wanted to talk about it, it was also something that could risk their friendship. He wouldn't lie to her, but she also couldn't lie to his father. He sighed and shook his head. "No... yes. Maybe?"

"I have no doubt you'll win this fight."

Suddenly uncomfortable, he cleared his throat. He looked around until he remembered the flask. Picking it up, he drank more than he usually did before letting it rest on his thigh.

They drove the rest of the way to his townhouse in silence. The entire carriage rattled over the cobblestones and it was a steady drum to meet with the chorus of insects that faded.

When Xaris stopped the carriage, she gestured toward his front door. "Sir," she said in her servant's voice.

"Thanks, Xar."

"What time tomorrow?"

"No," he said. "Take the day off. I think I'm going to go over battle plans."

"Have a good evening then."

He crawled down and landed lightly on the cobblestones. Tucking his hands into his pocket, he headed into his house.

"I'm not in love you with you, that isn't going to change."

Sinair stopped, the muscles on his shoulders tensing. Turning around, he looked up to Xaris who leaned over the edge. He thought about his ideas and then nodded. "You've always been a good friend. That's all I've ever needed."

"Separate beds?"

He smiled and the tension in his shoulders finally relaxed. She had thought the same thing as him. "I think different rooms for sure. My mother moved to her townhouse about fifteen years after they were married. They haven't lived together since. That would seem reasonable for us."

Xaris looked up for a moment. "No public affection?"

"Only the two kisses required by church law."

"But I can still drive?"

"Drive, ride. Whatever you want to do, I'll support you. You followed me across the country while I was being a hero. Even as my employee, you could have always told me no. I will just tell everyone it's your turn. But what about your mother? I don't see us ever having kids and she may turn into a monster when she realizes that."

Xaris grinned. "You've been adventuring and I've seen you bloodied more than once. How about I just say the family seeds got cleaved too many times?"

He shrugged. "Why not?" He gestured down to his crotch. "I don't need these anyways."

She settled back. "Maybe convince your father to do something for Beat? She doesn't need all of this---" She gestured around her. "---in her life. Do you think you could arrange for her to head to the coast for one of those universities?"

His mind started to work out how to convince his father. After a few moments, he had an idea. With a nod, he smiled. "We can do that."

Xaris saluted him, finger finger against her brow. "Until tomorrow then?"

"Thank you, Xar."

She shrugged and picked up her reins. "You understand me. Even I know there is probably one person out there for each of us. I just happened to think we're perfect for each other." She said nothing for a few seconds but then smiled broadly. "I'll see you in the morning, Sin."

He nodded and watched her leave. Plans raced through his mind, but he finally saw the opening he needed.

"Thank you," he whispered before heading into his house.
