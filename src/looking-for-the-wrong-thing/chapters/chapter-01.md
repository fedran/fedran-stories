---
title: Arrival at the Camp
date: 2019-10-23
when:
  start: 4.4.1849 TSC 1.4
summary: >
  It was Yubanis's first Grand Moot, when families across the tribe clans gathered together to rekindle friendships, show off their prowess, and maybe find a brief romantic entanglement. There were just a few things that had to be done first.
constraints:
  - this.when.start before the-battle-of-the-snakes-betrayal.start + 2 months
  - opila.age is 17
  - opila.born before yubanis.born
  - opila.born before sophi.born
  - sophi.born before yubanis.born
  - tubocak.born after yubanis.born
characters:
  primary:
    - Yubanis
  secondary:
    - Opila
    - Sophi
    - Penilil
    - Goisay
    - Tubocak
  referenced:
    - Danisit (Epigraph)
    - Warin
    - Kailal (Penilil's Sister)
locations:
  primary:
    - The Grand Crossroads
topics:
  secondary:
    - Grand Moot
    - Bloodline Tattoos
---

> It's hard to imagine how the violent orgies of the Hidanork moots could have any purpose in life, but it does provide a genetic diversity among the so-called moot children. --- Danisit ho Disrobin

Yubanis patted the flanks of his horse and clicked his tongue.

The glassy-eyed mare leaned into him and sighed dramatically before casually stomping on his foot.

There was a crunch but he knew it was coming and managed not to flinch. He patted her a bit harder. "Come on, pretty lady, off my foot and get over there. I'll give you a good brushing while you have a snack."

The horse delicately lifted her foot and headed forward. Her harness creaked and the small wagon she had been hauling rolled over.

Yubanis pulled his aching foot away before she could roll the steel-rimmed wheel over him. He smile and thumped his foot on a rock until he felt the joints settle into place with muted clicks. There was a flash of pain but it quickly faded into a throbbing. Before the second wheel rolled past him, he couldn't even feel a twinge.

"Yuba, you need to stop letting that old hag do that," said his father as he led the second wagon behind him. "Before you know it, she's going to try that on me."

Yubanis chuckled and patted the mare's neck gently. "It's okay, old lady. You're just tired, aren't you? Dad's been mean and driving you all day long."

"Your dad doesn't recovery from injuries like you."

With a grin, Yubanis stroked the mare's muzzle. "Yes, he's being a cruel driver, isn't he?"

She exhaled hard before muscling her way over to a basket that Yubanis's younger sisters, Opila and Sophi, were pouring a bucket of grains into the bottom. Their reddish hair shone in the fall sunlight.

Opila giggled and patted the mare's muzzle. She was the youngest of the family, seventeen and past the cusp of becoming an adult on her own.

This was her first grand moot, just like Yubanis and Sophi. When they started the long journey a week ago, all three of them had gotten their adult tattoos. The intricate knot design that traced their collar bones, from one shoulder to another. His sisters' tattoos were red-edged with healing but his looked like he had them since birth thanks to his ability to recover quickly.

His father swore as his horse bumped into him. The younger mare tried to jam her head into the basket, but Yubanis's mare shoved back. The two equines jostled for a moment before they found a comfortable spot for both of them to eat.

From one of the wagons, Penilil came down. She stamped her feet to get their attention. "Tali and Yuba, get those harnesses off. Opil and Sofi, brush them down. Where is Tubo?"

Yubanis looked around. They were in a roped-off area about fifty feet by twenty. A brightly-painted post marked each corner. Theirs was painted red with a "North-102" written in white. More stakes marked the other lots, each one marked off into neat rectangles. Over half of them had arriving families, some with horses and others without. Everyone he could see had similar broad shoulders, rugged skin, and wind-scoured skin from traveling the steppes. Beyond that, there was a variety of brown, red, and blonde hairs and skin colors that went from perpetually red burn to almost black.

Turning back, he shrugged to Penilil. "I don't see him, Mom."

His mother held up her hands in exasperation and then let out a groan. "The whole reason your brother went ahead was so we didn't have to wait or wander around!"

Yubanis avoided shrugging or making other noise. When his mother was ranting, there was little he could do.

"Fine, Yuba, finish and then help me with pitching my tent. Girls, set up yours on the Yuba's wagon over there." She pointed to the far end of their lot.

Sophi looked startled. "We aren't setting up one in the middle?"

Frustration and annoyance fluttered past Penilil's face before she smiled broadly. "No, not here. This is the safest place to be so spreading out will be a lot more enjoyable for all of us. Besides, I don't want to listen to whoever your sisters bring to their cots this week."

Opila grinned even as her cheeks darkened with a blush. She had been excited about coming to the grand moot ever since the announcement came. Like Yubanis and Sophi, she was conceived during the celebrations. Not much was known about their fathers other than there was something about them that her mother found alluring.

Yubanis's father came up and rested a hand on Yubanis's shoulder. He chuckled as he leaned over and spoke not-so-quietly, "What she means is that she doesn't want you to hear her company tonight."

Penilil turned toward him and waved a finger. "I'm done with being pregnant, Goisay. Let the girls have their time." She turned slightly and looked around at the surrounding. "Besides, I don't have time for little boys who can't keep going all night."

She pointedly looked at her husband.

Goisay chuckled and then gestured to the side. "Well, if you need a real man---"

Her eyes narrowed.

"---I saw Warin's wagon when we were coming in." He finished with a grin. "He's still got a bit of steer left in him, from what I heard."

She rolled her eyes before focusing her attention on her children. "Finish up and have your father get you three into line to get marked. The only thing I want to bring back with us is memories and babies, not diseases."

His father laughed, a deep booming noise that added to the growing din from the surrounding families.

A choir of agreements rose up before everyone went about their chores. Yubanis finished taking care of his mare before he dragged the wagon to the far side. His sisters hung the canvas of their tents on each side, draping it from hooks near the top and pinning them in place with spikes. Normally they would create a single tent braces across both wagons for more protection and shelter.

Yubanis had just finished pitching the tents when Tubocak strolled up.

"Where have you been, Tubo?" snapped Penilil.

Tubocak didn't seem perturbed. Somehow he had managed to lose his shirt and walked around with his heavily-tattooed chest bare to the slightly cool air. "The line for the rune marker is about a quarter mile long. I'm betting you're looking at a three hour wait."

"Damn the woods! I told you could go ahead---!"

"However..." he interrupted with a drawl and a smirk. "One of the Crows is about to set up over there." He pointed in the opposite direction. "Five lanes over, line forms to the south. Probably a half hour wait."

Their mother closed her mouth with a snap. "Lead with that next time."

Tubocak shrugged. "More fun this way."

His mother took another look at him. "What happened to your shirt?"

"I saw a pretty Storm girl."

Penilil pointed at him accusingly. "You stay away from Storms and Chains! I don't care how pretty they are!"

Tubocak smirked.

Her fingertip tapped to his collar tattoo. All four of the siblings had the same right side, an intricate design of chains that stretched like fingers to the shoulder. It matched their mother's design. Each one had a different left side: Tubocak's was storm clouds and lighting; Yubanis had intertwined ram horns. The chain represented their mother's father while the other side was their biological father.

"No Storms!" she repeated.

Tubocak shrugged again and the smirk didn't leave his lips. "Your sister is at Red-South-23."

"Don't interrupt me when I'm telling you off."

Yubanis suspected Tubocak got away with being difficult since he was five years older and already established as a solid hunter for their little group. His deep connection to the tiag---the living pulse of the land---meant that he was also needed for their visits to their ancestral homes.

Yubanis thought his abilities gave him a swelled head.

Tubocak held up one hand. "Mom, I got to get the kids over to marking otherwise they are going to lose most of the early evening. Don't want to bring back any diseases, right?"

Penilil's jaw clamped shut. Then she smacked him lightly. "Stop being a dick then." She was fighting a smile.

He leaned over and kissed her check. "I love you. Don't worry, I'll make sure they're safe before I have fun. I'll see you for lunch tomorrow? Noon?"

She started to say something but stopped. Looking up, she stared at the sky which was already beginning to turn orange. "Fine. By noon, do you understand? Your asses on the ground or I'll hunt you down."

Yubanis shivered at the idea of his mother catching him.

When Tubocak agreed, she pointed to Yubanis, Sophi, and Opila. "All of you? Be back by noon."

Each one agreed with various degrees of excitement. Yubanis was the last, it was his first moot and he wasn't sure what to expect. The sudden freedom was daunting and a bit scary.

Her eyes focused on him but she spoke to all four. "This is your first moot and I'm sure you are anxious to get out. You have five days to have fun, relax, and explore. If you want to drink, drink. If you want to brawl---"

Her gaze turned to Tubocak who grinned back. "---then just try avoid breaking any bones or serious injuries. You also know the rules about mating: no means no, women provide the beds, the men provide babies. You know which families to avoid."

She paused to tap her collar. Then she took a deep breath and turned back to Yubanis. "It's okay if this isn't for you. The moot is about having fun. Even if fighting isn't your thing, it's okay. We will never---" Her eyes narrowed as she stared at Tubocak. "---tease anyone for that. Do you understand, Tubo?"

The smirk finally faded. "I won't tease him," he said quietly.

"You better not."

She gave each of them a kiss and then them on their way.

The four siblings headed to get a rune to protect them against disease, nausea, and discomfort. It was the perfect way to enjoy the moot when the main goals were freedom, fucking, and fighting.
