---
title: The Morning After
date: 2019-10-27
summary: >
  Coming home the next night, Yubanis finds out that he earned himself a prize that even his father or brother couldn't beat.
when:
  start: 4.4.1849 TSC 20.11
characters:
  primary:
    - Yubanis
  secondary:
    - Opila
    - Sophi
    - Penilil
    - Goisay
    - Tubocak
  referenced:
    - Hasar (Epigraph)
    - Ami
topics:
  secondary:
    - Bedding Gift
  referenced:
    - The Noble Nomads
    - The Grand Council of Tribes
---

> The gathering of tokens and trinkets is a ritual in itself, one that is important only for the length of the moot and then quickly forgotten as the Hidanork go their separate ways. Everything is forgotten but memories and those to be born. --- Hasar dea Xahos, *The Noble Nomads*

Yubanis felt like floating on air as he drifted down the crowded lanes between the camps. The press of people didn't bother him as much as before, mainly because half of them were cradling their heads or simply keeping their gazes down as they headed to the communal eating or bathing areas. Throughout, he saw various men comparing brass and silver trinkets with each other and the women teasing them cheerfully at the same time. Everyone was having quiet though muted fun.

Except for the children. They were running around screaming at the top of their lungs and avoiding the casual swipes made by hung-over adults.

His mother was standing at the end of one wagon, grilling meat over a fire rune. To his surprise, she wore her winter robe instead of her usual daily outfit. Normally she only wore it in the winter house, when they didn't have to leave the warmth. It looked comfortable, not functional like every other day of his life.

His sisters were sitting in their chairs around a small fire. Both wore their sleeping clothes. Sophi's hair had been done up in a braid but Opila's brown strands were tousled and haphazard. They smiled at each other as they sipped from steaming mugs. He didn't have to approach to know that they were drinking strong tea.

He approached and stepped over the rope. "Good morning!"

His mother glanced at him sharply. Then a slow smile crossed her face. "You look cheerful."

Yubanis blushed. "I am."

"Left behind some seeds?"

Tubocak hopped the rope from behind him. "Probably not as many as me. I left three saplings behind this year." He brandished three leather thongs wrapped around his wrist. They were small compared to the dozen brass and silver bracelets that covered him from wrist to elbow on each arms. Each one had various ancestor symbols on it and a few decorations of brass.

Penilil's eyes widened. "My, you were busy last night."

"Yeah, I thrashed a couple silvers in the ring. It was the greatest moment of my life." He mimed an uppercut before reaching past his mother to grab one of the steaks from the grill and yanking it free.

"Hey! I'm cooking those."

"You like it brown, I want it juicy."

"Brown is safe."

"My rune says juicy is good enough for today, mom."

She waved the fork she had been using to grill at him and then shook her head in amusement. Turning on her heel, she looked at Yubanis. "How would you like it? Cooked or nearly raw?"

"Brown is good, thank you."

"See," she told Tubocak, "some of us know how to eat. Now, pull up a chair by the fire and stop gnawing on that steak like a bear."

"Yes, mom." Tubocak winked at Yubanis as they both gathered up chairs for everyone and arranged them around the fire. When the wind shifted, smoke rolled over Yubanis who coughed.

Opila reached over and twirled her finger near the fire. The smoke suddenly gathered together and rose up a straight column that ignored the wind. She twirled a few more times before sitting back.

"Thank you, Opil."

She grinned and shifted to her other side. She looked like she was uncomfortable, not unlike himself. "Good night?"

He had to hesitate, unsure how to describe his time with Ami. "Yeah, it was a good night."

Tubocak puffed out his chest. "Not as good as mine!" he announced. Then he pulled off a pair of pouches of his own and set them down on the end of the wagon near their mother. Each one clinked from whatever was inside it.

"You call that an accomplishment?" Goisay said as he dropped a larger bag on top of Tubocak. It rattled loudly from the contents.

Tubocak's eyes narrowed. "And what did you do, old man?" He ended by running his hands along the bracelets that marked his winnings in the ring.

"A little of this, a little of that. Oh, and I got myself one of these." He held up a golden ring.

"Oh, a little heavy drinking for the old man? Delved into spirits before you had your ass handed to you? Or did you just guzzle some of that watery swill in the brass area?"

"This old man can drink you---"

Penilil interrupted them by shoving plates into both of their hands. "Stop stroking your dicks and eat. You can show off what good boys I have after the meal. I want to hear all about the night."

Blushing, Yubanis took his own smaller pouch and set it down next to his father's. It looked small and pathetic. Ami's thong sparkled in the light.

Penilil leaned over to kiss him. "I hope you have fun," she whispered.

"I did."

"That's good, I was...." Her voice trailed off.

He looked down to see her running her finger along Ami's band. The gold sparkled in the sunlight for a moment. Then she looked up at him, her eyes wide with surprise. "Gold?" she whispered.

"It was a good night," he whispered back even as his cheeks were burning.

She grinned broadly and kissed him again. "I'm proud of you, Boy."

Together they headed to their seats.

Goisay finished chewing his bite. "How about you, girls? Have fun?"

Both nodded, Opila more enthusiastically than her sister.

"Any grandchildren for the winter?"

"I hope so." Opila giggled. She shifted slightly and a blush colored her cheeks.

Tubocak gestured to Sophi with a greasy finger. "You made a good choice there. That guy could really fight."

Sophi blushed. "Pilos was very good."

"I'd like him on our side if we ever got into a fight," continued Tubocak. "I wonder if he's a walker or a wagoner?"

"River," said both Opila and Yubanis at the same time.

Their mother's head snapped up, her eyes widening even more. Slowly her lips parted as she glanced back at the wagon and then back again. Then she cocked her head as she stared at Yubanis. "Her?"

Yubanis nodded sheepishly. Then he looked up to see the rest of the family looking at him with confusion. He gulped and ducked down to work on his meal. His cheeks were burning and he wasn't sure if the pounding in his heart was from the memories or embarrassment.

After an uncomfortable silence, Tubocak broke it with a grunt. "So, ready to throw down and see who won the night?"

Goisay chuckled. "Why not? I like humiliating my boy."

Penilil stood up. "Hurry up, I need to go to the Council in about twenty minutes and all of you need to clean up."

She stepped near the wagon and casually picked up Yubanis's bag. "Session is until sundown, so volunteer and clean up. And you," she gestured to her husband, "clean up the camp."

The sisters laughed as they levered themselves out of the chairs and grabbed the rest of the bags and pouches. Goisay and Tubocak moved their chairs so they were all clustered around a small towel they used to lay on the ground.

"You first, old man."

Goisay smirked and emptied out his bag. There were silver and brass cards, chips used in dice games, two silver rings, and even a silver bracelet. It was an impressive standing, to say the least.

Tubocak had almost two dozen brass bracelets, five silver, and a handful of brass drinking rings. He also added the three thongs, markers for the women he had enjoyed that night. Two had ram horns and the third had a stylized heron but otherwise all three were plain. Even using the exchange of ten brass to one silver, he had one more silver than his father. "Yes! I beat you!"

Yubanis realized he had far more than both of them combined. On the way home and with Ami's help, he had exchanged seventy of his silver rings for gold ones which left him with a sizable but compact amount.

He hesitated as he held his pouch.

"Come on, Yuba. It's okay," Opila said resting her hand on his hand. "This is just for fun."

"No shame in not beating your brother," Tubocak said with a grin.

That pushed Yubanis to act. He carefully unwrapped Ami's thong and set it down on his corner. He trembled with the effort not to smile.

Goisay picked up the thong. "Oh my, apparently your brother shot for the clouds and scored well."

Opila took it from her father. "Mom, I thought you said we couldn't use silver."

"You can't," Goisay said. "You have to be a mother to use silver."

Then his face brightened and he chuckled.

"But there is gold there," Sophi said as she peered closer. "Who can use gold?"

Tubocak's smile froze, the muscles in his jaw tightening.

Their mother answered sharply. "It doesn't matter, let's see what is in the bag."

"Yeah," Tubocak said with a little bit of worry in his voice. "Let's see."

Yubanis opened the pouch and shook out the contents. The first was one of the gold rings.

"Root me sideways," said one of his sisters. "You drank as much as dad?"

Then the rest poured out, gold and silver rings spilling out into a pile. In the sun, the pile shone brightly. It was a rather small pile compared to the others but there wasn't even a single brass ring visible.

"Root me," swore everyone else, their voices shaking with surprise.

Tubocak sat down heavily, his face paler than usual. "H-How?" Then he shook his head and scrambled to his knees with a broad smile. "My brother!" he yelled as he yanked Yubanis into a tight hug.

The storm clouds that had formed by Tubocak's first question faded from Penilil's face. She smiled broadly and hugged both of them tightly before embracing her husband.

Goisay cleared his throat. There was a look of pride on his face. "Well, I guess two of us were humiliated, weren't we?"
