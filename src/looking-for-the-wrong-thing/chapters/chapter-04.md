---
title: Under the Table
date: 2019-10-26
summary: >
  Yubanis's regeneration and recovery magic easily bests the older men drinking. Though, there is more than golden rings waiting to reward him.
when:
  start: 4.4.1849 TSC 10.61
locations:
  primary:
    - The Grand Crossroads
characters:
  primary:
    - Yubanis
  secondary:
    - Ami
    - Zanno
  referenced:
    - Queen of Solitude (Epigraph)
topics:
  secondary:
    - Bedding Gift
  referenced:
    - Ring Mother
    - Hall Father
---

> To yearn for the older man, one who knew that the war of pleasure was not won with a single fight but through the battles that test one's edge. --- *Queen of Solitude* (Act 2, Scene 5)

Yubanis slammed down his glass. His body ached and he was starting to feel the affects of the spirits but he still had one person left sitting at the table. The others had passed out or staggered away a while, each leaving a pile of gold rings behind to mark their defeat. Now, he saw victory ahead and he was willing to try for it.

Zanno swayed as he gulped at the tiny glass. He paused and took a long time to find the bottom. He shook his head for a moment and then slammed it down hard. It teetered and almost fell. "Damn, still... first time...?"

Yubanis smiled and nodded. His eyes were blurry from exhaustion, not drunkenness. It was getting late and the din of the celebration outside had died down to only a few bursts of noise and cheers.

Zanno picked up one of the empty bottles and tried to pour it out. Only a drop came out. He muttered and swayed as he tried to grab another but failed. Slowing down, he shook his head and then clutched the table for balance. "No... no, I'm done. Damn it, I lost to... a boy."

With a rush, Yubanis realized he had just drunk twelve older men under the table. He smiled broadly, his heart beating faster. He had never done it before and the rush through his veins was intoxicating.

Zanno reached out. "You were a worthy opponent, Yuba. I'm... hon... honored to have lost to you."

His hand smacked the table and he sat down heavily. "Now... take care of my wife... while I take a... nap."

Zanno slid off his chair and landed on the ground with a thump. He groaned and rolled over on his back. Seconds later, he was snoring.

Ami set down her beer mug and clicked her tongue. "I'm impressed. I don't think I've seen anyone drink him under the table."

"Wife?"

Ami got up and headed for the edge of the drinking hall. She came back with two blankets, one she bunched underneath Zanno's head and the other over his bulk. "He's a good man and a good provider. We've been together almost thirty-six years. Even though he was born a walker, he took to the rivers well." She smiled and stroked his cheek.

Yubanis watched nervously. He wasn't sure what he was supposed to do. "Is he going to be okay there?"

Ami straightened. "Of course. The ring mothers and hall fathers will be around with a cart to bring him to the sleeping area. That way, someone is there to make sure he doesn't get hurt when he's sleeping and he gets something for the hangover when he wakes up. We plan for five years for these moots, everything from supplies to drunks to making sure there are enough latrines and paper to wipe asses."

She grinned and stretched, her back arching.

Yubanis's eyes took her in. She was nothing he had been looking for, but he found himself wanting to stay. Whatever had been happening that night felt more real than what he had seen around the fighting rings. He glanced at the pile of silver rings in front of them. There were a lot, more than he had ever seen before.

She yawned and then reached for her pony tail. It took her a moment to unwrap a sparkling leather thong that kept it bound. Pulling it free, she shook her head to the side until her hair spread out across her shoulders. It was a honey brown streaked with silver and gray.

His body grew tense.

"Come on, let's get your winnings wrapped up." Keeling down, she grabbed a pouch from her husband's belt. Together, they pushed all the rings into the bag and then tied it shut with the thong from her hair. He noticed that the leather strap had gold and silver charms along with a stylized snake and horseshoe on the end, the symbols matched the ones on her tattoos.

Yubanis finished and stood awkwardly. "Um, what do I do now?"

She held out her arm as if to take him by the waist. "Walk me to my barge?"

An intense longing filled his heart as he slipped his body next to hers. The warmth and softness enveloped him again and he felt happy as they made their way out of the now quiet drinking hall. His fingers stretched out to rest his little finger on her hip and his thumb along her side.

Ami made a soft noise of approval and then nestled closer, her body melding with his. He could feel her breast bumping against his side and his body grew warmer at the touch.

In the hours since he had started drinking, the celebration had quieted dramatically. No one was fighting anymore and there were no contests of strength. He spotted a few card games still going on, but it looked like everyone else had gone their separate ways for the night.

He frowned. "What happens to those who don't....?"

"Find someone to warm their bed at night? They usually go back to their camp." She chuckled. "If the camp is occupied? Well, there are usually enough beds to collapse without interrupting any fun. If not, there are always the rest tents like where they are going to be taking Zanno. The grandmothers and fathers watch over those, at least the ones who don't want to play."

Yubanis wondered what he would find back at the camp. But then his thoughts turned to the woman he was nestled again. Could she be interested in him? He wasn't looking for an older woman but he couldn't imagine there was anything about him that she would find interesting. Though, the idea appealed to him.

Ami chuckled. "Having trouble walking?"

He hesitated, trying to explain the sudden hardness.

She answered by reaching out and pressing her hand on his length. Without a word, she stepped in front of him and lifted her head until their lips were almost touching. "I would very much like if you joined me tonight, Yuba."

He inhaled sharply. Any words escaped his thoughts, driven out by the firm grip on his manhood and the press of her body against his own. He gulped and did the only thing he could imagine: he kissed her.
