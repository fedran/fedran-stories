---
title: Brass Bracelets
date: 2019-10-24
when:
  start: 4.4.1849 TSC 3.51
summary: >
  Yubanis finally got the rune to prevent disease and he was able to wander through the area looking for ways of showing off. It doesn't take him long before he finds the fighting rings.
characters:
  primary:
    - Yubanis
  secondary:
    - Tubocak
    - Dinkay
  referenced:
    - Dianois (Epigraph)
locations:
  primary:
    - The Grand Crossroads
topics:
  referenced:
    - The God Tree
---

> For a man, one purpose of the moots is to prove oneself. There are many opportunities from fighting in the rings, drinking contests, and trivia. --- Dianois dy Ladnia

Three hours later and the clouds were purple fingers stretched across the sky. Along the western side of the moot, there were streamers of colorful spray of light being thrown into the air. The pops and bangs of the celebrations punctuated the sound of laughter and talking coming from all directions.

In the time it had taken to get the rune, the camp had gotten a lot more crowded.

While there was an overwhelming variety of strangers around him, he couldn't help but notice that most of them had the same weathered skin that hours of exposure had created. The strangers, more than he had ever seen in his life, all spent their lives on horses, wagons, and foot traveling across the lands to their secret places, their family claims. They were all muscular, with deep wrinkles that faded only when they smiled.

At the moment, there were a lot of smiles.

"Good thing we only do this every twenty years," Tubocak said next to him. He was barely watching where he was going as he nodded and waved to the various women that were wandering in the opposite direction. He was strutting and getting all the attention he could use. "So many beautiful ladies."

Yubanis had to agree, but he did it silently. There were many pretty women there and the possibilities of finding a mate for the night hung over him, but he didn't have a clue where to start. He sighed and nodded; as much as he wanted, he didn't have his brother's gregarious nature or the raw physique to match.

Tubocak patted Yubanis on the shoulder. "Don't expect anything, brother."

"What?"

"If you are here just to prove that you are a better fighter or lover, it never works out. You get frustrated and then you'll do something stupid. The next thing you know, you are knee deep in a chasm of shit with a shovel." He pulled a face. "Trust me, there are fewer horrors than looking into the asshole of the God Tree to teach you to behave. Three days of cleaning up this---" He gestured to the people around them. "---and you will be praying for freedom and a new nose."

Tubocak spoke from personal experience, at least according to their father.

Yubanis glanced at a pair of women passing them. They wore leather tops with plunging necklines to show off their cleavages and their collar tattoos. He wanted to say something but he felt invisible as they only hesitated to look at Tubocak before moving on.

Tubocak winked back and then kept going. He was leading Yubanis toward the east side of the moot, where rumors had it that the younger crowds were setting up. It wasn't much different than the festivities throughout the moot, but the stakes were lower, enthusiasm was more important than skill, and the spirits had been watered down to keep everyone going well into the night.

They came up to the first of the fighting rings, a large circular area marked with wooden stakes and stone. Arranged in a square field, there were almost forty of them. Most of them were empty but a number already had small crowds watching one-on-one matches.

Tubocak whooped and rushed ahead to the nearest occupied one. "Dinkay!"

One of the brawlers, a bear-like man with a thick beard, turned around. "Tubo!? What---!"

The word was interrupted by a left cross by his opponent.

"---hold on!" With enthusiasm, Dinkay focused on his opponent and rained blows down, missing more than hitting but each impact drove his opponent closer to the edge. Sweat glistened on their bare muscles by the time Dinkay pinned the other fighter against the ropes. A few powerful blows later and he was the only one still standing.

With a whoop to match Tubocak's, he held out his hand to help the other man up. They hugged for a moment before stepping away.

The fighter who lost pulled off a brass bracelet and handed it over.

Dinkay slipped it on, adding to the two he already had. Then he headed over. "Tubo! It's been years!"

They hugged tightly, thumping each other's backs.

Tubocak beamed. "Ready to get your ass beat?"

"Think you can with those scrawny arms?"

Tubocak flexed his thick bicep and grinned.

"Come on, Squirrel." Dinkay stepped back. "Let me teach you a few lessons."

A younger child, maybe twelve, ran up and handed both Tubocak and Yubanis a brass bracelet each.

As Yubanis stared down at the heavy ring, his brother shoved his on and crawled into the fighting right. Yubanis sighed. He wasn't a very good fighter, he didn't have either the strength or aggression to keep up with his brother. His only saving grace was his regeneration, he could outlast almost anyone.

A pair of whoops startled him. He looked up to see his brother throwing himself at Dinkay. Both of them were competent fighters but Tubocak was obviously stronger and faster. His blows came hard and fast, slamming into Dinkay as they fought from one end of the ring to the other. Blood splattered on the ground from the attacks.

As if called by the noise, onlookers came streaming closer. There were people of all types: young and old, male and female. They were cheering for Tubocak and Dinkay, though only a few used their names. Most of them were there just to see two anonymous fighters beat each other.

Yubanis cringed at the press of people but kept his position near the ring. He focused on his brother as Tubocak finished with a flurry of blows against Dinkay's ribs, one hit after the other in rapid succession.

Dinkay dropped to his knee.

Tubocak started to bring his fist down but then saw his friend had yielded. He stepped back to spoil his blow. Then, with a grin, he stepped back to reach down with bloody knuckles and help his friend to his feet. "By the Tree, I missed you."

They hugged each other tight before Tubocak took one of Dinkay's bracelets to add to his wrist. Then he turned and headed to Yubanis.

Almost immediately, there a pair of younger women, one appearing in her twenties and the other in their thirties, pressed up against Yubanis but their eyes were focused on his brother. Yubanis got an eyeful of their breasts nestled in leather bodices as they thrust them forward.

Tubocak flirted for only a few seconds before interrupting. "I'm sorry. I'm just getting started and I'm hoping to get enough of these bracelets to trade in for a silver." It would take ten brass bracelets to get a silver, or he could move over to the higher-stake games where he could earn silver directly.

The two women cooed and promised to follow after them before stepping back.

Tubocak leaned over the rope. "I'm going to be here a while. Are you going to be okay on your own?"

Yubanis nodded. "Yes." He didn't really feel comfortable alone but he also knew a subtle request to be left alone.

"Good." Tubocak reached up and rested his hand on Yubanis's shoulder. "Do you remember where the tents are?"

Yubanis pointed to where "Red-North-102" had been drawn on his hand, right next to the faintly glowing rune that would prevent most diseases.

Tubocak sighed and pulled Yubanis into a hug. "Have fun. This only happens once every twenty years but that means you have many more no matter what..." His eyes lifted up and a smile crossed his face. "... happens here."

Yubanis followed his gaze to where a stunning woman was walking by. She had long brown hair with a blue streak in it and wore a matching leather top and skirt. She smiled at his brother and there was something that told Yubanis that they had been together before.

"... yeah, I need to... renew an acquaintance." Tubocak scrambled from the fighting right and trotted after the passing woman in blue leathers.

And then his brother was gone.

Yubanis looked around, his anxiety rising. The gathered crowd had already pulled apart to drift toward the other fighting rings. He felt alone despite the crowd.

"Want to fight?"

He looked up to see someone about his age, a young man with short hair and dark skin. He looked like had just gotten his collar tattoos also. A single bracelet hung on his wrist.

Yubanis looked down at his own. He might as well have fun and see if he could garner attention like his brother. Slipping it on, he gestured to the recently abandoned fighting ring. "I have to warn you, I'm not very good."
