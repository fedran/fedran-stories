---
title: Disappointment
date: 2019-10-25
when:
  start: 4.4.1849 TSC 5.28
summary: >
  After a series of losses, Yubanis takes a break from brawling. His regeneration abilities aren't able to keep up with the fighting. As he rests, one of the caretakers for the area starts talking to him.
locations:
  primary:
    - The Grand Crossroads
characters:
  primary:
    - Yubanis
  secondary:
    - Ami
    - Pilos
    - Sophi
    - Zanno
    - Tubocak
  referenced:
    - Koisay (Epigraph)
topics:
  secondary:
    - Ring Mother
---

> Ultimately, the competitions at the Hidanork moots is to prove oneself to potential mates in a situation where people from opposite ends of the country are encouraged to intermingle to avoid one race from dominating the landscape. --- Koisay fon Malice

Two hours later, it was obvious that Yubanis wasn't good at fighting but he had managed to get up to five bracelets before a series of defeats left him with two. He wiped the sweat from his brow as he crawled out of the fighting ring. Defeat always stung but being beaten twice in a row hurt even more. He sighed as he ducked underneath the rope from the fighting ring.

Looking back, he saw that his opponent was on the far side. He had a gaggle of people around him, congratulating him with hearty smacks and stolen kisses.

No one was around him.

He shook his head. He could have hoped to make ripples but it seemed like he was doomed to be stuck with only two bracelets and no attention.

"Are you okay?" He looked up to see a woman approaching him. She was curvy with the full breasts and wide hips. She looked like a mother with a bit of gray in her hair that followed the curve of her pony tail. Functional and authoritative even with her easy smile. He started to look down but then stopped. The front of her leather jerkin strained slightly, revealing the curves of her breasts and her snake and horseshoe markings around her collar.

On her belt was a white flag, a ring mother. They were the adults that understood the body more than others and they looked for signs of concussions or more serious injuries. It was one of the few authorities for the moot.

Yubanis nodded and held up his hands to show they were steady.

She stepped closer, the warmth of her body pushing back the fall night's chill. With a firm hand, she cupped his head firmly and stared into his eyes.

He stared back. When he exhaled, there was a bit of fog escaping his lips.

"You took some serious strikes to the head."

Yubanis frowned. That was a fight ago. His regeneration had already recovered from that. "It's okay, I'm pretty sturdy."

"Looks like it. Your knuckles aren't bleeding either."

He couldn't look away from her eyes. They were the color of polished glass, the dark amber that filled the windows of one of his family's winter home. The warmth of her body and the color of her eyes gave him a comforting feeling. "I'm resilient."

She smiled and looked away.

He found he could breathe again. His eyes took her in, from the wrinkles on her face to the deep-set red that darkened her skin. She had tattoos that delved far below her clothes, the recording of major events in her life inked across her body. He only had the one besides his collar tattoo.

She turned back with a smile. "Come on, you should eat now."

"I'm---"

"You've just gone ten rounds without a break." She had a mother's voice, commanding yet soft at the same time. There was no question he would be eating soon.

Then he heard her word again. Ten rounds? Was she paying attention to him? It didn't seem like a lot to him, he probably could go another ten with his magical talent.

He looked around to see if any of the younger ladies had been drawn toward him. When no one was giving him sideways glances or approaching, he sighed and turned away. "Yeah, might as well get something to eat."

She guided him to a nearby tent where they were roasting a pair of hogs. By the time they sat down at one of the wooden tables, he had a heaping plate of food and a large glass of watered-down beer.

"How are you with drinking spirits?"

Yubanis shrugged. "I don't get much on the road. It weighs too much."

She smirked. "None of us do, but it does make the nights go easier." She took a healthy swig of her own glass before setting it down. "Though, it's nice to have something other than fermented swill. Or... this stuff. It's a bit weak for me."

He sipped at his own glass. It tasted like water with a few hops waved over it. Even though he didn't have much, it wasn't much better than drinking lukewarm water.

"Don't worry, just eat a bit before heading back into the rings."

He obeyed her mom's voice with a smile though the idea of going back to the rings didn't really appeal to him. While he didn't have any injuries from the fight, he also wasn't remarkable. His fighting style was just to outlast his opponent. It wasn't flashy or sexy.

"By the way, my name is Ami." She didn't give a family name, but there were only casual relationships at the moot.

"Yubanis. I came with the wagoners from the south."

Ami smiled broadly. "The river runs through my veins. We came down in our barge to the docks on the north side."

"I never met anyone who wasn't on foot, horse, or wagon."

Her eyes sparkled. "Now you do. What's it like riding a wagon?"

That opened the floodgate and they began to talk about their own lives. The conversation meandered through their lives, bouncing from nights on the back of a wagon to storms near the ocean. Somehow it jumped over to her three boys and his sisters before finding more topics.

A series of whoops and a swell of cheers finally broke the moment.

Startled, he looked up and around at the fighting rings. There was a massive crowd around one of the larger rings. Everyone was jumping up and down as they yelled at the top of their lungs.

"Someone must have just earned a silver bracelet."

Yubanis tensed as he stared. He didn't feel any excitement for whoever won, but he had a feeling that he would be seeing his brother coming out sporting a silver ring.

Though, when the crowds did part around the victor, it wasn't his brother that he saw.

The winner was a powerful-looking man with a short-cropped beard and a hair chest covered in blood. He had a broad smile on his face but that probably had to do with Sophi riding his shoulders.

Yubanis's sister was cheering just as loud as everyone else. Half of her clothes were loose around her body as she rocked back and forth. Her thighs flexed to keep her balance as the victor trotted out.

"Oh, Pilos won? Good for him."

Yubanis glanced at Ami.

"My youngest son," she said with pride. "He's a good man. A little strong-headed but has a good heart. That girl is going to get a ride of her life tonight."

"That's my sister, Sophi," Yubanis said quietly.

Ami's eyebrow rose. "Small moot."

"It's her first."

"Don't worry, Pilos will take care of her. She will have good memories."

Yubanis stared at them as the two headed back toward his camp. He was happy for her, Pilos appeared to be everything she wanted when she talked about the moot.

Then he caught sight of his brother entering another fighting ring. Tubocak had both arms covered in rings and it look like he had already converted a few of them into silver bands. His hair was wild and his right eye puffy. It look like he had been fighting for hours.

His opponent had three silver bracelets of their own.

The fight look evenly matched but Yubanis realized he couldn't stay to watch it. "I... I need to go. I can't be near this fight."

Ami stood up. "Come on."

He hesitated. "Don't you have to stay?"

"Do you want me to?"

He didn't. He liked talking to her. She was comforting and kind, with stories he wanted to hear.

Ami came around the table and slipped her arm around his waist. She was soft and warm, the comforting sensation washing over him. "Come on."

They walked away as the cheer rose up.

"That was my brother."

"I take it he's a good fighter. He really should be in the silver fights instead of hanging around the brass."

"Best I know. A strong sense of the tiag also."

She didn't say anything.

After a moment, he sighed. "I'm not as good as him, am I?"

Her hesitation answered the question. "You are not very aggressive in the ring."

"Or strong or flashy."

She squeezed him with her arm, pulling his body tight to hers. He hesitated after a moment and then slipped his own arm around her. She smelled good, like flowers in the morning rain.

His thoughts darkened. He had hopes for tonight, to be amazing in the ring or at least good enough to be asked to mate. He wanted to feel the touch of a woman's hand, the comfort that he imagined was there.

Yubanis would never get it, not at the moot. He shook his head and stopped between two aisles.

Ami stopped with him, her eyes moving back and forth as she stared at him. "What's wrong?"

"I should go back. There isn't anything for me out here."

She didn't let go.

"I'm sorry, I---"

"How resilient?"

"What?" He stared at her in confusion.

"You got beaten on pretty well but don't show any signs of it. Healing? Regeneration?"

"Regeneration. Pretty fast, I can heal broken bones in a few seconds."

She smiled. "What about drinking? Ever get drunk?"

"No...? I haven't really tried though."

"Come on," she said with a sly smile. "I have an idea."

Curious, he followed as she lead him away from the brass area and toward the western side of the moot. It got quieter but he still saw fighting rings with older warriors. The blows being traded looked more brutal and skilled, precise hits instead of flailing around. He also saw the flash of magic in the strikes; the fighters were not holding back.

There were also other games going on: card and dice being the more obvious. Three men were debating something, it was passionate as they waved their hands at each other and somehow someone was keeping score on a chalkboard.

Ami lead him to a drinking hall and sat him down between two men old enough to be his grandfather. There were ten other men at the table, all of them with little glasses filled with amber liquid in front of them.

As one, they stared at him.

"Pour him a gold, Zanno," Ami said to a bearded man holding a bottle.

"Ami?"

"Go on."

Zanno shrugged and set down a small glass in front of Yubanis. It looked barely large than a cap for a bottle. When the other man poured in a glass, however, he could smell the sharp scent of a powerful drink. He had never had anything even close before. "You know the rules, Boy?"

"Yuba. No, sir."

"First person to leave the table, either falling off or stepping away, has to give a silver ring to everyone left. Same with the second and third. You get the idea. If you make it to the end, that's a lot of silver rings you can turn into gold."

Yubanis stared in shock. Gold rings? That was worth ten silver or a hundred brass. He was in one of the highest stake contest at the moot? He looked at Ami in shock. "I-I don't have any rings."

She winked. "I do. Show me what you can do."

Swallowing to ease his dry throat, he turned back.

Zanno nodded and then grinned at her. "Ami's covering the boy. Everyone good with that?"

A flurry of nods and grins.

Zanno focused on Yubanis. "Got the balls to play?"

What else did he have to lose? He nodded. "Yes."

"On three. One. Two."

Yubanis grabbed the glass with a shaking hand.

"Three!"

Everyone slammed back their drink. The spirits tore at Yubanis's throat, burning all the way down before pooling in his stomach. His vision swayed and he coughed violently.

The table burst into laughter.

Zanno slammed his glass down in front of him. The others followed suit. Yubanis gulped but set his glass down.

"Again?"

The burn was already fading from his gut. He smiled and nodded. "Yes, sir."

"You got some heft to that shaft of yours, don't you, Boy?" One of the grandfathers clapped Yubanis on the back while Zanno refilled the glasses.

By the time all the shots were full, the burn was gone. He was thirsty again and the spirits didn't seem so scary.

"One... two... three!"

The second shot went down much easier.
