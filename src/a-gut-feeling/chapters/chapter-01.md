---
title: A Gut Feeling
contentWarning: >
  Some themes that appear in this story: death of named characters, torture, and verbal abuse.
summary: >
  With her husband grievously injured, Gertrude went out to find someone who would be willing to look at his injuries. However, she ends up alone when an old enemies finds her.
date: 2020-01-26
---

> Violence begets violence. It doesn't matter who starts it, only that once begun, it will never end.  --- Borganil dea Rinfir

Gertrude walked close the brick wall along the alley as she made her way toward the main street. It was late evening and a few lanterns barely gave her enough to see; it would have been easier thirty years ago before her vision started to go. She traced the wall with her gloved hand to keep her balance while walking through the banks of snow. Occasionally she would step into a puddle of water and the icy water seeped into her ancient boots.

Behind her, the young man following her stumbled on some garbage. "Damn the Couple!" he hissed.

She turned and glared at him.

He didn't notice and kept stumbling forward.

"Tobias," she hissed.

With a gasp, he stopped. "What?"

"Quieter. The guards are still out and we still have a few blocks to go."

"I'm not really suited to this, Mother."

Gertrude smiled. It sounded better when he called her mother than all of the Baroness's men. They always managed to turn the term of endearment into an insult. "Do you still have your bag?"

He hefted it, she couldn't focus on him in the dim light but the shape looked right.

Gertrude didn't think Tobias had it in him to betray her, but she had been surprised before. She scratched a healing scab on her cheek for a moment---she had gotten too close to some shrapnel---and then favored him with a smile. "Just a few more blocks, then we'll be safe."

Tobias looked around nervously. "I'm not a doctor or an alchemist. I'm not sure I can do that much for your husband." He rubbed the wall and knocked off some clumps of ice.

She shrugged. "You are willing to help Eustas, which is more than anyone else in this town."

He cringed. Slowly, he looked back the way they had come, as if he was regretting his choice to come with her.

She couldn't help but feel guilty for pulling him out of the cheap bar that she had found him. After the Baroness shut down all the trains and outgoing wagons, she had abandoned hundreds in town without any offer to help. Everyone has scrambling for food and shelter and Gertrude's offer of two hundred crowns, everything in her pocket, was barely enough to get him into the cold.

There was a moment if she wondered if it would better just to surrender. It had been a week since the ambush and her meager attempts to keep Eustas were rapidly failing. He had a fever and the chills. She suspected his blood had gone bad but she didn't have the skill, spells, or alchemy to save him.

She turned and rested a hand on his shoulder. "I understand if you want to go back."

Tobias frowned and looked back again. "I'm just a journeyman veterinarian, I don't know how to handle humans."

His reluctance tore at her heart. She desperately wanted him to come, but she couldn't force him. She gave it one more attempt. "When I grew up on the farm, vets were some of the most adaptable people I knew. I know you can do it."

He sniff. "What if your husband dies? I couldn't live with myself."

Her throat grew tight and dry. She had been thinking about it ever since they decided to fight the Baroness. "I... you would have tried, that's all that matters."

Tobias didn't say anything for a moment. Then he reached out to know more ice from the brick wall next to him. "I don't... I...." His voice trailed off. "Okay, I'll try."

He didn't sound convincing.

Gertrude looked around. "Do you want me to see if there is anyone there?"

"Y-Yes?"

She gave him a squeeze. "Just stay here. I'll look for guards."

"Thank you."

Gertrude didn't expect to see him when she returned but she had to have faith. Whispering a prayer to the Divine Couple, she crept forward a few steps. Then she remembered something and came back to handle him a wrapped nut and candy bar. "In case you get hungry."

He chuckled. "Thank you, Mother."

Smiling grimly, she headed to the mouth of the alley and peered at the street. Seeing it empty, she hurried across into the opposing alley and followed it down. When she reached the center of the block, she hesitated at the intersection of two alleys.

With a frown, Gertrude wasn't sure which way to go. Eustas was to her left. All she had to do was cross two streets and go into the back of the abandoned bakery. They were lucky to hide there when the Baroness's men were looking for them. The ready supply of cornstarch and flour was enough to staunch their wounds and the sacks made makeshift bandages.

She started down that way, but a bad feeling twisted in her stomach. Backing up, she considered going the wrong direction. It didn't feel as wrong.

Trusting her gut, she headed straight to walk parallel to the bakery. If she didn't get a sense of danger, she would break into the front.

Walking in the early winter night was nerve-wracking. The sounds of melting snow splattered around her from the warm snap. There were muted smells and sounds, but it was difficult to identify directions and distances.

It took her over twenty minutes before she was standing in the alley opposite of the bakery. The boarded-up windows had been cracked and shattered. Eviction and other notices were plastered to the wood, ice and humidity had sealed them in place.

Gertrude stared at the store front for a long time and then back and forth along the streets. There was no one, but she couldn't count how long. At least she didn't see an ambush like before.

Muttering quietly to herself, she turned and headed back. She gave each intersection a glance before hurrying across. She could help but imagine Eustas dying while she was being careful, but if she got caught, there would be no one. Though, the waking nightmares of her being tortured for the location of her husband kept intruding on her thoughts.

She was in the midst of one of those nightmares when she didn't catch someone standing around the corner. She started across when his voice stopped her.

"Good evening, Mother."

At the sound of hatred and vileness, she stumbled to a halt. Turning toward him, she backed away. "Ralnak," she said, recognizing the voice.

Ralnak stepped forward, his short-billed hat masking his face in shadows. "Didn't want to go into the bakery from behind."

She hissed.

He smiled, the lower half of his face coming out of the shadows. She could see dark lines across his chin and shoulders. A month ago, they weren't there. "I already know your husband is in there. I was just hoping you'd go in the back. I could have ended you before you came out."

His hand lowered and she saw a curved knife in the light.

Fear rolled through her body. She froze as she stared at it. "The Baroness's reward was for me alive."

"Well, she'll get you dead. You and I still have something to talk about." His voice was low and filled with malice. He stepped forward.

Gertrude stepped back. "Is this for what happened at the farm?"

Ralnak snarled. "If it wasn't for you, my father would still be alive! Now that Hork cow is going to get everything."

Gertrude's mouth open in surprise for a moment. "You were evicting your own father!"

"But he was alive!"

"You killed him!"

Ralnak's eyes flashed. "It was your fault. You and that damned husband of yours! If you didn't convince him to fight, he would still be here."

Gertrude held up her hands in surprise. "You were kicking him out of his house! Where was he going to go? The poor house?"

He shook his head and swiped the blade in front of her.

Gertrude shuffled back with her eyes locked on the knife. Her back bumped on the corner. "Ralnak, you were so proud of stealing from your papa. You were going to ruin his life and take away everything he had, just like you did to us. Why wouldn't---"

"You deserved it, you diseased goat!" He lunged forward.

Even waiting for it, she couldn't move in time. Her foot slipped.

Her winter clothes were no defense. The sharp knife punched into her stomach and she felt the blade scrape against bone.

There was the briefest moment as she stared at him in shock. "W-Why---"

The agony ripped through her body. She let out a gurgling scream as she clutched at her belly. The blade nicked her finger.

Ralnak leaned into her, driving her back against the sharp brick wall. "This is all your fault."

Then he twisted the blade slowly.

Gertrude let out a sobbing cry as the sharp pains blurred her vision and her ears began to ring. Deep inside, she could feel the blade slicing into her organs.

With a shaking hand, she grabbed his shoulder. "You... don't have to do this, Ralnak."

He pushed his face close to hers. "Yes, "Mother," I do. You fought us and you have to lose. Both of you."

She gulped at her dry throat. Her fingers clutched the blade, holding it as still as she could as he tried to twist it inside her gut. She managed to stall him but it would be only seconds before he disemboweled her.

Ralnak chuckled. He shoved her back against the bricks. "You should thank me. No matter how much this is going to hurt, it's nothing to what the Baroness is going to do to your husband."

Gertrude sobbed in pain. Her breath came in wheeze as she stared up at him. Underneath her hand, blood was pouring out of her belly and soaking her hands. The heat and stench of it was choking.

In the face of death, she couldn't think of anyone besides her Husband. "W-Who told you about the bakery?"

Ralnak pulled back with surprise. "You are dying and that is what you ask?"

She tightened her grip on his shoulder, pulling herself closer. A sickening idea rose in her mind: she could use her powers to stop him. But first she had to know who betrayed her. "When I'm... oh, Couple... when I'm at the table of the Divine Couple, I want to know who to throw water at."

Ralnak shrugged his shoulder but she dug her fingers in tightly. Her body tingled as she drew on her power. She had never tried to summon molten stone in this way, but she was at war.

"It doesn't matter now, does it? The laundry girl told me for three crowns and a promise..."

He kept talking but Gertrude wasn't listening. Her knees were starting to buckle. She let her weight slump against his, though only part of it was faking. She would save her husband, even if she died bleeding on the ground in front of him.

She could picture the woman. A young lady of twenty years who was watching over Eustas right now. Light brown hair and a young laugh. She had said they were safe, but now Gertrude knew that she could be lying; she couldn't trust Ralnak to tell the truth even now.

"... ready to die, Mother?" he finished with a chuckle. The pressure on the knife increased, twisting in her stomach and ripping open the wound further.

Fear for her husband drove Gertrude to push back the pain. She gripped his shoulder tightly and clamped her hand on his to keep him from twisting the blade. Heat bubbled through her veins, the whispering call of power that she normally used to craft bricks for Baby, their mechanical brick machine that stood and fought like a human.

"Too bad your husband isn't here to crush me with rocks."

Gertrude swayed. "N-No... he isn't."

"I'll bring your body to him, at least he'll have a chance to say goodbye."

"You are a monster, Ralnak," she groaned through the pain.

"And you are nothing but a wrinkled old cow---"

She summoned her power, summoning molten rock inside his body. It was difficult, more than anything else. He was moving, his body resisted, but she had been making bricks for many years. With all her might, she squeezed her muscles and pushed.

"---who made the mistake... of... thinking...." He frowned as his voice trailed off.

She bore down, grinding her teeth together. The heat was surrounding her. Underneath her feet, the ground crawled as snow and ice melted away.

Underneath her palm, his skin began to glow as molten stone formed underneath his body.

"What---?" His eyes went from confusion to pain to anger in a flash.

Gertrude was already throwing all her weight into keeping his knife still as he twisted hard. She sobbed from the pain but didn't let go of her fingers as his flesh began to smoke.

His body began to glow a deep, flickering red from the inside. Smoke poured out of his mouth, ears, and through fissures that formed in his skin. With the light forming inside, the shadows from his had faded away to reveal the deep scars from his battles with Eustas and herself.

Ralnak gasped, his mouth opening to reveal the glow coming up from his throat.

Gertrude sobbed as she pushed harder, straining with all her might to force the magic to flow into him. At the same time, she forced the blade from her belly.

Hot blood splashed to the ground. It poured out of Ralnak's body, splattering on the ground beneath them. Droplets splashed onto her coat, igniting the fabric almost instantly.

It took a moment to realize that she had forced the blade completely out of her body. Her body screaming in agony and her vision blurred, she blindly shoved with all of her flagging might.

Ralnak fell back, crumbling and melting more than plummeting to the ground. Choking steam flooded through the alley as he finally got a single hissing scream out of his throat before the molten stone destroyed his neck.

Gertrude staggered for a moment and then collapsed to her knees. It was agony as she landed on steaming rocks and mud. With a cry, she slumped forward.

Then Tobias was there, holding her shoulder. "Mother!"

She looked up to see him staring at Ralnak's body thrashing violently. The sight of it was sickening. She reached out with one shaking hand and then drew the heat out of him.

Stone hardened almost instantly, turning into solid brick inside his corpse.

The movements stopped.

Tobias turned to her. "W-We have to get out of here!"

"I-I can't," she gasped. "He gutted me."

Fear burned in his face for a moment, then hardened into something else. "Let me see," he said with only a little crack in his voice.

"C-Can't...."

He shoved his hand to her belly. Then his fingers were up inside the wound.

"Fuck!" she gasped in agony.

His body glowed a brilliant yellow-green. Sparks of magenta lighting streaked between him and her, followed by a pulsating headache and an itch deep in her bones. It scraped against her senses and only grew worse as he continued to use his powers.

The pain only increased.

"Tob---"

"One second," he said firmly.

The agony reached a sharp point, then stopped.

Gertrude sobbed as she wondered if she had finally died.

"I can't heal the wound, but I can glue everything together. That should let us get you to a safe place and let me do properly---"

"No!" she gasped. Dizziness slammed into her and she slumped against him.

"Mother!"

"Eustas is in danger. The laundry girl watching him may have betrayed us."

"May?"

She gave a grim smile. Her fingers dug into his shoulder before she remembered what it felt like to kill Ralnak with her powers. She snatched her palm away and reached for the wall instead. "Never trust anyone in war, second lesson we've learned in this fight."

"What was the first?" he asked.

"Doing the right thing hurts like getting kicked in the throat by a cow."

He smirked. "Can you really afford to go to him injured like this?"

Gertrude looked at him sharply. "My husband is in danger."

"Yes, Mother." He slipped an arm underneath her and helped her up.

She thought she had passed out, but by the time she was aware again, they were limping down the alley toward the bakery. This time, they were coming through the back.

Staring at the dark entrance they had broken into earlier, she prayed to the Couple that her husband was still alive. Now, more than ever, they had to stop the Baroness. With Ralnak dead, there was no doubt that the wanted alive was about to a death sentence.
