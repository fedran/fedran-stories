---
title: Lit for the First Time
format: Flash
genre: [Drama, Slice of Life]
summary: >
  As the years passed, Kilan found it harder to use her magic. Soon it came down to any use could kill her, but if she stopped, she would starve.
---

Kilan gathered up her energy. Magic gnawed at her gut and twisted her stomach into a knot. It sickened her, as it has for the last forty years, but she kept her attention focused on pulling it from her belly.

The flames burned her from the inside, flaring along aching joints and reddening her skin. It clawed down into her sore elbow and wrist.

She couldn't breathe.

She kept pushing.

When the flame of purest white burst out of her hand and lit the forge, it was a victory. She lived.
