---
title: Foxhole Duties
date: 2019-10-17
characters:
  primary:
    - Majoril
  secondary:
    - Ronamar
    - Craid
    - Lafduras (Unnamed) # Lafudras Kalus-Robins
  referenced:
    - Pilanor da Tusar
topics:
  referenced:
    - Border Conflicts Throughout History
    - Kormar-Gepaul Marches War
summary: >
  Pinned down by a sniper, Majoril tried to regroup with her squad members, Ronamar and Craid. However, that proves to be difficult with no supplies, standing water, and everyone injured.
---

> The Kormar-Gepaul Marches War was a dark decade in history where neighbors killed to only shift a border a few miles in one direction or the other. --- Pilanor da Tusar, *Border Conflicts Throughout History*

The canvas rippled and snapped in the cool winds of fall. The sharp smell of an approaching rain blended with the acidic scent of the alchemical flame that pushed back on the seasons. A few brown leaves managed to slip inside, slapping against the stinging burns that covered Majoril's face.

She groaned and wiped the leaves off her face. She shook off her fingers before she leaned against the muddy wall of their hole in the ground. It was wet and sticky, just like the ground underneath her.

"Damn it, Ronamar," muttered Craid. "Couldn't you dig a better hole next time?"

Ronamar looked up from his corner of the hole. His bulky body was in the deepest point and almost an inch of ground water soaked his legs and the bloody bandage around his knee. His eyes were dark and hollow.

Even from Majoril's position, she could see the earth knight was struggling to focus. She waved her hand toward Craid. "Leave him alone, we're safe."

"We'll never see that assassin coming," muttered Craid before he returned to stirring his dagger in the depressingly small pot of soup rations. It barely steamed from the heat. The powdery meal was universally despised by anyone with taste buds, but when one is cowering underneath a soaked canvas to avoid a sniper, one doesn't have much choice.

Ronamar shifted. "You could go out and lead him away." His voice was a rasping growl.

Craid snapped his mouth close.

They sat in an uncomfortable silence.

Rain pattered across the canvas.

Craid looked up and swore before going back to the meal. "I was hoping to get this done before the rain. Just a few more minutes and it will be ready. I don't want to eat in the wet."

At the mention of rain, Majoril glanced at Ronamar. It took her a moment to realize that the knight was still in water. She levered herself away from the wall and crawled through the mud; every part of her body ached from being thrown from the first explosion. "Ron, you need to move."

Ronamar panted and his eyes slowly turned toward her. "What?"

"You need to get yourself out of the water. That isn't going to help." Majoril gasped as she knelt in the cold water, hissing at the pain. She reached out and checked his bloody bandage. It was soaked and smelled badly.

He flailed for a moment before he managed to slap his hand on the water.

The cold water splashed against her and she cringed.

Underneath her knees, the ground buckled and shifted as if something was burrowing underneath her.

It stopped after only a few seconds.

She patted him. "Come on. Just shape the earth to the side. Make a whole we can shit in."

He smiled faintly through his ragged beard.

Encouraged, she patted him. "Come on, Baron. Don't make me hold it all night."

That got him as she knew it would. "I'm not the baron."

"You will be if you don't die here. Come on, Ron, just a little bit."

The ground began to bucket again. A fissure formed near his knee as the earth split apart. Water hissed as it poured into the fissure. The tiny waterfalls grew more rapid as the fissure crawled over to an unoccupied corner. In the corner, the ground suddenly fell away to reveal a foot wide hole.

"Some of us can't aim that well," Majoril said with an exhausted grin.

The hole widened to two feet and two flagstone bobbed up from the mud to settle into place. A ridge of earth and mud formed in front of the hole and rose up a few feet, creating a small privacy wall around the hole.

Majoril turned to watch Ronamar. He was sweating and grunting with the effort to shape stone. She glanced down to the damp mud. "That's good."

Ronamar's jaw clamped tightly tighter and he shook his head.

The fissure filled in, earth and dirt shoving the water into the hole until there no exposed water.

"Okay, that was smart, Baron."

He grunted. "Thanks."

Craid crawled over to the two of them, holding the steaming pot with one hand. His fingers smoked slightly as he held the pot toward Ronamar. "Dinner."

Majoril rubbed her hand on her thigh as she looked at the bandages around Craid's chest and stomach. While Craid's talent was a magical resistance, he had taken the brunt of the last explosion. There wasn't enough fabric to cover all the exposed muscle and bone along his sides and chest. Only his will kept his body together, though none of them knew how long it would last; Majoril wasn't sure Craid could keep his talent working while he slept.

Ronamar took the offered food, grunting as he used a makeshift spoon to work it to his mouth.

Craid glanced at the hole. "You want first?"

Majoril patted him on the shoulder before crawling over to relieve herself. Ronamar's shaped earth wasn't as solid as she expected, it sank underneath her weight and the privacy wall swayed. She tested it, it rocked back and forth. The heavy weight seemed to be the only thing keeping it up.

However, beggars can't choose their coins. She braced herself against the wall and enjoyed a bit of relief.

When she was done and cleaned up, she crawled over.

Craid handed her the pot. "It's cool enough to hold now."

She nodded thanks and looked around, trying to decide if she would return to her corner or remain next to Ronamar. The baron looked in pain and was probably soaked. She gestured. "Share warmth?"

"Yeah, but I wet my pants."

Majoril grinned at the painful attempt at a joke. "Well, do you want to take them off?"

"I will when I get up. Until then, I'm cold."

She turned to sit against him when she saw Craid looking uncomfortable. It wasn't the expression of someone shitting but something else. Hesitating, she caught Craid's eyes.

Craid glanced at Ronamar and then back to Majoril.

At the pleading sight, Majoril set down the pot and crawled over. "What's wrong?" she whispered.

"Do you have a moon pad?"

She frowned. "You're early? Hold on."

Majoril went to her corner and got the only supplies she managed to save. Coming back, she handed Craid all of her menses kits, the wax-wrapped pouch had a couple pads, extra wipes, and some pain killers in glass vials.

"Sorry. Keeping my body together is wrecking---"

"Could you use these as bandages?" She gestured to the exposed bone.

Craid nodded. "I still want one for below though. It's...."

"It's okay. You don't have to explain." She patted him on the shoulder. She gave Craid privacy by returning to Ronamar. It took a few minutes to find a position that was not improper for a farm girl sitting against a baron's son who was a hero in his own right.

Comforted by body warmth, Majoril dipped into the foul-tasting soup. It was her first meal since sundown and she devoured it in seconds.

Ronamar rested his large hand on her shoulder. "Don't move, please? You're warm."

"Of course, my lord."

His hand tightened. "Don't... just don't call me that."

Majoril grinned.

Craid hesitated in the center of the fox hole. He had managed to use most of the menses kits to jury-rig additional bandages; the pads were already soaked with blood. He dragged the small pot of alchemical flame closer. It was a tiny warmth but it was something. "Ron?"

"Come on," Ronamar said. "Just don't sit on my face."

There was a look of relief on Craid's face as he crawled over. He nestled up against Ronamar's head, hissing when something caught on the bloody wounds. Then he settled down with one arm resting on Ronamar's head and the other hooked on the muddy wall. His leg rested against Majoril's.

After a few minutes of shifting, they found positions that were as comfortable as they could. Despite the pain and discomfort, Majoril dozed off to sleep.

It felt like only seconds when Craid suddenly surged to his feet. He yelled something incomprehensible.

Daze, Majoril sat up with her heart racing. It was dim underneath the canvas.

Craid's feet kicked over the alchemical pot, spilling the strange mixture and blue flames everywhere. It sloshed against the side of the hole and lit up the ragged walls in a hellish flame.

Majoril scrambled to her feet. Her hand glowed as she gathered the energies to summon her spear.

Craid launched himself to the far side, slamming against the wall and curling up. He let out a sob as he tightened into a fetal position.

"Craid---?"

"It's a bomb! Get down!" came the strangled yell.

Majoril froze as history repeated itself. Craid had stopped the first explosion from killing their squad by doing the exact thing. The explosion had almost torn him in half; if it wasn't for his talent, he would have been torn to pieces. Even then, the explosion had killed two other squad members.

She gulped as she stared in shock, her body shaking as she waited the second for the explosion. She screamed at herself to move, there was a sniper out there who would pick them off if they fled the hole.

"Sniper," gasped Ronamar. "Watch out for the sniper!"

Majoril's heart pounded in her ears. She wasn't sure she would survive the blast. She needed some place to hide. Her eyes lit on the privacy wall, the stone was unsteady but it might be enough to deflect the blow.

Or stop it.

She gasped and surged forward to grab Craid's shoulder.

Craid twisted. "Get off! It's going to blow."

He was wrapped around an earthenware container, about a foot across. Magical symbols glowed around the outer rim and there was a bright line shining through cracks forming along the surface.

She clutched at it. "The shit hole!"

Craid's eyes came into sharp focus. With a gasp, he rolled over and threw the comb into the corner.

Majoril snapped her fingers and summoned a spear. It slammed into the wall just as the clay container ricocheted off one wall. It struck the faintly glowing haft of her weapon before rolling into the hole.

She stumbled forward. "Ron, close it!"

Without waiting for the earth knight, she rammed her shoulder into the privacy wall. It titled but it was too heavy.

Craid's body rammed into the wall, spraying blood from ruptured veins.

The impact toppled the wall and the heavy stone landed on top of the opening.

Majoril rolled over. "Ron, you need....!" When she stared at the empty corner, her voice trailed off.

There was a deafening boom as the stone covering the hole shattered into shards and a gout of flame burst out of the hole. The concussion wave picked up both Craid and Majoril and threw them across the tiny space.

The impact crushed her organs and she felt bones crack. Her head smacked against the muddy wall. It felt like rock from her speed and stars burned their way across her vision.

Dazed, she slumped to the ground.

Craid landed face-first next to her, his legs twisted obscenely with bones sticking out from where they ruptured flesh. He let out a long exhalation and slumped into the mud.

Majoril sobbed as she tried to get her limbs to move. Nothing was working. Her ears were ringing and there was a high-pitched whine. Blood dribbled from her ears and her chest throbbed. Her head lulled down and she saw two of her ribs sticking out of her right side.

A whisper of sound introduced the sniper. Landing lightly on the ground, the slender man wore tight, dark clothing. His face was bare but painted with black paint. Floating over his shoulder was a sphere of crackling energy.

He didn't say a word as he looked down at Craid and kicked his head.

The fallen warrior didn't move.

The sphere hummed and a narrow beam of energy shot out, piercing Craid's chest. It stopped after a second, leaving only a burn hole in the back of Majoril's friend's body.

Almost casually, the sniper focused on Majoril.

It was too late to play dead. She gasped as she tried to get her body working. Her fingers trembled with her effort to snap her fingers. If she could summon a spear, then at least she had a chance of stopping her killer.

The sphere hummed louder and energy crackled along its surface.

Majoril refused to close her eyes. She gritted her teeth and focused on her fingers.

Energy burst out from the sphere, piercing her shoulder.

Flesh burned away in an instant, peeling back from the beam of energy.

It faded.

The sniper smiled.

A massive sword burst out of the ground between his legs. The foot-wide blade shot straight up, burying into his crotch in a spray of blood and gore. It ripped out of his shoulder.

Ronamar's hand reached out from underneath the mud to plant on the ground. Muscles tensed and his head rose out from the mud and rock.

The sniper's eyes widened in fear.

Ronamar twisted the blade hard and the massive weapon ripped the sniper apart from the inside.

The sphere grew brighter.

Majoril snapped. Her spear appeared in mid-throw, shooting forward to impale her attacker right underneath the chin. The sharpened point thrust deep into his brain and came out the other side.

Ronamar twisted his weapon the other direction and a shower of gore poured down across his face.

With another snap, Majoril dismissed one spear and then summoned another. It pierced the man's skull again. She kept snapping until the sphere of crackling energy dissipated.

Then she impaled him again.

And then one last time to make sure he was dead.

Ronamar groaned as he crawled out of the earth as if it was nothing but water. "Not my smartest move." Once on top, he slumped forward on the now solid earth. "Oh, getting dizzy."

He pitched forward.

Majoril reached for him but couldn't catch him from falling. She groaned and slumped forward. She looked at Craid's body and then Ronamar's.

"Shit," she finally said. "Today sucks."

Craid groaned. "Tell me about it."

"C-Craid!?" Majoril felt a glimmer of hope.

"D-Don't touch."

She froze, fingers from his body.

Craid shuddered. "I'm holding... together, but it's getting... hard...."

With a pained chuckle, Ronamar rolled to his side. "You can do it, I know you can." He panted for a moment. "Just until morning, second squad is coming."

"Don't... me sleep," Craid said in a broken voice. "Sleep and I die."

Majoril was exhausted and in agony. Her ribs were sticking out and there was blood everywhere. She looked up at the night sky where droplets of rain came down to splatter across her face. It seemed so overwhelming in that moment and death was almost holding her hand.

She closed her eyes slowly.

Then opened them.

Majoril had her duty. Keep Craid await, don't let any of them die, survive until morning. She gulped and gestured to Ronamar. "Give me your shirt? I need to stop bleeding."

They had to survive the night. All three of them.

That was her only duty now.
