---
title: Difficulties of Traditions
date: 2019-12-28
summary: >
  At the end of the year, Xaber always had a difficulty in feeding the ship. The captain honored a tradition that required a four day fast and a ban on fishing, but the crew didn't share the same views. Of the years of being the ship's cook, he had finally found an alternative: a regenerating monster in the hold.
---

> The twisted creatures of the ocean only seek blood among the fishermen that patrols the waters. --- Tabor dea Disrobin, *The Horrors of Surf and Sand*

Xaber shoved his hand into the bucket of writhing purple tentacles and grabbed one. The slick muscle slid along his fingers until he dug his nails in to stop it. With a grunt, he pulled it out and threw it across the cutting board to stun it.

Before the suckered tentacle could recover, he grabbed a hammer and a silver spike. With practiced ease, he positioned the nail and slammed it into place.

The dripping tentacle shuddered once and then stopped moving.

He hammered a second silver spike into the other end just to make sure it was properly dead.

With practiced skill, Xaber switched to his filet knife and cut into the soft bottom of the tentacle. It took a bit of strength to force the tip of the blade in, but once he did, it sliced open easily to reveal a layer of silvery fat.

He dug his blade in a bit deeper and cut to the rich red meat underneath. Then, he worked his way around the tentacle, removing the outer layers until he had a loin of edible meat. A few more flicks of his blade and he cut away the poisonous glands that ran along a ridge.

The deck underneath him jerked and he braced his foot across the narrow aisle against the counter. The waves hammered the ship, swaying it from side to side but he kept himself still as he chopped the loin into inch-thick steaks.

"Damn it!"

He looked over his shoulder at Jefor, one of the journeyman fishers on the boat.

Jefor ran his palm over his hair and snapped it away, splattering slime across the counter. "I'm fucking tired of getting groped by dinner!"

He slammed a second, five-gallon bucket of writhing tentacles on the counter next to Xaber. "Every damn time! I have slime in my ass crack! My ass! Dinner should not be grabbing my ass!"

Xaber shrugged as Jefor screamed at him.

The deck heaved to the side and the fresh bucket slid to the edge of the counter.

Xaber slammed his knife into the wooden counter to catch it and then returned back to Jefor who continued to rant. His face didn't crack from his perpetually bored-looking face. He had heard the rant before, not only from Jefor from almost everyone who got volunteered for kitchen duties.

"Why do you have a regenerating tentacle monster in the hold when we have four tons of fish!"

Xaber sighed. "You know what today is?"

"Year's end, so what?"

"New Year's Eve, actually. That's an important day for the captain who is from Benson's Point in Tarsan."

Jefor held out his hands and jerked in a silent but angry "so?"

"One of their traditions is the celebration of the four day battle of the The Respectful Hope of San Goran, a warship that defended the peninsula against the Disrobin Navy during one of the worst winter storms in history."

Jefor started to say something but Xaber held up his finger and kept speaking. "The San Goran was unable to fish because of rough waters and the fact every man was preparing for war. In the memory of the men who starved, the captain's ship will not set a single net down to gather fish for the same four days."

"We have plenty of fish already! I helped catch the four tons of Couple-damned food in the hold!"

Xaber pointed toward the door leading out of the kitchen. "Go tell the captain you'd like to eat some. He's honoring the tradition the proper way, which means he's fasting for four days while working in this storm. I'm sure he'd love the company."

Jefor opened his mouth to snap and then closed it. "That doesn't sound like a good idea."

Xaber finally smiled, it lasted for a few seconds before it dropped from the effort. "So you can see my problem. I have to feed sixty-two men and women for four days without fresh food. Tradition says we don't fish, but not everyone is willing to starve because of that same tradition. The captain is. So, I'm presented with a difficulty."

"But a tentacle monster!?" Jefor said with a whine.

Xaber gestured to the relatively small kitchen. "You see a lot of room for fresh fruit, steak, and vegetables in here? Maybe a cow I can milk every day? I barely have enough for the basics: flour, sugar, and lard."

Looking around, Jefor looked guilty for a moment.

"I've been serving this ship for twenty-nine years. All but three had me down in this place. I improvised because I was tired of the bitching, spoiled food, and trying to jam enough crap into these cabinets to feed all of you."

Xaber turned back to the bucket of tentacles. He pushed it back on the counter and hooked it on a metal hanger he had stolen years ago. Turning back, he gave another ghost of a smile. "Having a ready source of meat makes life a lot easier for me. Besides, I haven't forgotten to remove the poison glans for at least three years."

His face growing pale, Jefor looked around. His gaze focused on the door to the hold and grew paler.

"Fine. I'll ask the captain to assign you to another post."

"Thank you."

"Go on, I can finish this on my own." Xaber gestured to the stairs leading up.

Jefor almost ran for the door.

Xaber watched him and then went back to his cutting board to finish creating the loins out of the tentacles. He used both buckets before he had eighty steaks laid out on trays. After sprinkling them with spices, he threw them into the box that had the fire rune engrave into the bottom.

The magical heat rolled over his face. He noticed it was sparking and the steady red glow flickered instead of remaining steady. It only had a few weeks of energy left before it darkened.

When he closed the door, he updated his task list to order the rune charged as soon as they got into port.

Looking around, he couldn't see anything that needed attention for at least twenty minutes. He wiped his hand and then headed down into the hold with the tentacle monster.

He opened the door and took a deep breath of the briny air that flooded past him. Glancing one more time at the stairs leading up to the deck, he went inside and closed the door behind him.

Before he reached the bottom, he felt the cool intrusion into his thoughts as the creature reached out, <<Cold evenings, Xaber Martin.>>

Xaber came around the bottom of the stairs and looked at the writhing wall of purple tentacles that filled the hold. The smell was overpowering, icy and salty at the same time. The floor had an inch-deep slurry of slime, salt, and fresh black streaks of the creature's blood.

"Was Jefor gentle?"

<<Not really. The human was disgusted and didn't make clean cuts.>> A ragged end of a tentacle pushed out of the wall to show Xaber. Even in the dim light, it was obvious that Jefor had chopped a few times before he reverted to a sawing motion.

Xaber let out a disappointed sigh and patted the ragged end. "Sorry, it's hard to tell how they are going to do down here."

The tentacle wrapped around his wrist, soaking it with slime. There was already a small tendril sticking out from the end. It would be fully healed by morning. <<It did not hurt much. The human was rough, but it still gave relief.>>

It took a while to find the right mental thoughts to communicate that the creature's tentacles never stopped growing. The longer they were, the more they hurt. Having them trimmed down and shortened helped it as much as having a ready source of unspoiled food for the crew helped him.

<<The human was not the one I am looking for.>>

Xaber patted the tentacle. "I'm sorry. We'll keep looking. We've only tried twenty-five of the crew. There are still eighteen that were on this ship three years ago."

<<I miss that night. Why didn't the human come back?>>

Xaber shrugged, he had hear those thoughts before. "Not everyone wants to admit they enjoyed having sex with a tentacle monster. Or they want to but couple come up with an excuse."

What he didn't want to think about were the other situations: that whoever the creature sought didn't enjoy the night at all or that the crew had left the ship.

The tentacle reached around to wrap around his waist. The pressure was steady but he knew what would happen next.

Without his expression changing, Xaber stepped back and twisted away. "I am not the human you were looking for. I have two holes you want to play with and neither is a port of entry."

<<Sorry, I do not remember what the human looked like.>>

Stepping further back, Xaber held out his hand to encourage the tentacle to wrap around it again. "Any chance you remember anything about this lover? Name? Age? Even after three years, it would make it easier to find them again."

<<I am blind and feel by touch. All I know is that it was a human. I think. As for the name, I had not learned how to project my thoughts into humans at that point. You were the reason I can communicate now.>>

"Number of holes?"

Dozens of tentacles rippled along the wall. They slithered over each other, dripping slime onto the floor. <<I cannot count that low. All humans have so few, but I still enjoyed what had happened.>>

Xaber held up his hand. "I still don't want details. I'll ask the captain for another crew."

<<Thank you, Xaber Martin.>>

"This time, don't get so grabby. You need consent, so give a hint of your interest, not stick anything of yours into anything of theirs without permission. Tease, not violate."

<<I apologize, I went too far with Jefor Danisor.>>

Xaber frowned. "Danisor? He said his family name was Grayfal."

<<No, Xaber Martain. Jefor Danisor is the name in the human's head. There are many lies in the human's thoughts.>>

Xaber grunted. "I might need to tell the captain about that."

<<The human has many magical weapons hidden on the ship. Be careful, you are the closest I have for a friend. I would be very sad if you got hurt.>>

Xaber smiled. "I'm honored to be your friend." He had no name for the tentacle monster, but one was never needed.

<<Cold and safe evenings, Xaber Martain.>>

"Good night, old friend. We'll try again tomorrow."
