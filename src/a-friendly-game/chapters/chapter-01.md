---
title: A Friendly Game
date: 2019-10-06
summary: >
  On a bright and cheerful day in the desert, the Shimusògo decide to have some fun playing a game.
---

> Member of the Shimusògo clan are capable running with incredible speed, with the slowest measured at thirty miles an hour and the fastest able to cover thousands in a single day. --- *Registry of the Lost Clans of Lore*

The smoking ball hit the gravel next to Rutejìmo with a thud before it bounced off a rock and shot into the air. He followed it with his eyes, his bare feet dancing on the sun-baked rock for a second and then he chased after it.

The ball sailed about a hundred feet and he managed to get underneath it by sprinting and then bracing himself as he slid backwards through the shallow rocks. His momentum created a furrow in the ground, only one more to the many that crossed the field.

About a quarter mile away, Desòchu's flaming body was racing toward him. He wasn't pushing himself but his brother's speed still kicked up a plume of dust behind him.

Unfortunately for Rutejìmo, Des was also between him and the goal a league away. There was no way he could throw the ball.

Sweat prickled Rutejìmo's brow as he looked for someone on his team, someone without a shirt on. Even though he didn't spot anyone, he started to spin around on his feet in anticipation. His bare feet stamped loudly on the rocks as he spun on his heels. A spectral bird, a road runner, appeared at his feet and raced around him in a circle. He spun after it, turning harder and faster until wind kicked up. He would never catch the bird, but it didn't matter as he felt the air heating around him from the bird's magic.

"Jìmo!" bellowed Hyonèku. "Throw left, five hundred feet ahead!"

As Rutejìmo spun around, he saw a second plume as Chimípu came racing up from behind him. She was running as fast as his brother and would pass him in only a few seconds.

Coming around, he spotted Hyonèku also spinning with a large rock in his hand. His wife's father winked and accelerated until a tornado of fire and wind formed around him. The energies tore at the ground around him.

Then the rock shot out of Hyonèku's tornado. Its passing cracked the air as the stone ignited into flames.

Rutejìmo stamped hard twice more, bringing himself into a wide circle and then threw the ball with all his might. The spectral bird shot forward and the ball followed, also turning into a flaming shot as it streaked through the air.

Chimípu rocketed past. She snatched both burning shots from air as she raced forward and then threw herself into a spin of her own. Unlike Hyonèku and Rutejìmo, she created the column of air and fire without stopping her forward momentum.

The wave of wind created by her wake slammed into Rutejìmo. Still unbalanced by his spinning, then stumbled forward. The rocks blasted at his bare back and shoulder.

The first flaming burst shot out from Chimípu's column, moving so fast that only flames marked its passing. A heartbeat later, the second one came racing after it.

Desòchu didn't stop moving as he grabbed the first shot out of the air. He caught it high and brought it around, spinning around before firing it in the opposite direction.

The two flaming shots, one rock and one ball, crossed each other by inches.

"Shit!" bellowed his brother. He lunged for the ball as it rocketed past him but missed.

Rutejìmo scrambled to his feet, holding his breath as he watched the burning ball fly in a low arc and cover the miles in seconds. Then he raced after it. The spectral bird raced past him and he focused on that, accelerating into a blur to cover the distance rapidly.

In front of the goal was Mapábyo, Rutejìmo's wife. She braced herself and caught the flaming ball. The blast of heat and flame streaked around her as she was shoved back closer to the circle of stones that represented the goal.

Chimípu stopped in front of her, her bare skin slick with sweat. She gave a bright smile and waved to Mapábyo.

Mapábyo looked in confusion, her hands gripping the ball tightly.

Then the blast of wind from Chimípu's approach struck Mapábyo full in the face. With the blast came rocks and sand to pepper Mapábyo's face in a howling stream.

Mapábyo let out a swear as she stumbled back, her bare feet tripping over the goal.

Chimípu stepped forward to catch Mapábyo before she felt. With a swing, she swung Mapábyo around and then carefully set her on the ground.

The wind settled down, raining sand and gravel to obscure some of the furrows their game had left along the ground. The ball spun as it rolled into one of the furrows and stopped. It hissed loudly as it cooled.

Everyone raced up as Chimípu and Mapábyo shared a hug. The wind in all directions before settling rapidly.

Hyonèku patted Rutejìmo's shoulder as he passed. "We should probably call it a night. We still have a thirty league until we get home. They're expecting us for dinner."

Desòchu stopped next to Rutejìmo. Heat rolled off his body as he wiped the sweat from his face. "Don't want to spent the night under the stars, old man?"

Hyonèku glared at him. "No, some of us didn't bring our wives along and I'm lonely. It's been two weeks." He smiled as he turned back to join the celebrations.

Desòchu bumped Rutejìmo with shoulder. "You're right, he's in a much better mood. Good idea, little brother."
