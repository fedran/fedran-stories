---
title: Good News
date: 2020-05-28
summary: &summary >
  A few days later, Astol watches as his crush comes board the ship to deal with the wood bores. Brui only makes it worse by teasing him in Lain's presence.
teaser: *summary
characters:
  primary:
    - Astol
  secondary:
    - Brui
    - Jabil
    - Opil
    - Gria
    - Lain
    - Kimai
    - Nira
  referenced:
    - Rimi # Quote
location:
  primary:
    - Queen of Salt and Sapphire
    - Diamond Cutter of Ten Thousand Days
    - Northern Sea
---

> With the time and distance that separates the Doila ships, relationships are fluid until the point lovers join together on the same boat. --- Rimi of the Black Knife Across the Sun

Two days later, Astol found himself sitting on the topmost beam of the aft sail and watching the bright blue boat that approached theirs. The *Diamond* was a wide-bottomed boat with only a single sail with two spurs spread out in the back dragging nets. It was a small crawler shared with three family members: Lain and his two sisters, Kimai and Nira.

Kimai was the captain of the ship. She had water powers like Opil but with an attitude closer to Brui. Her hair was also woven into a long tight braid that reached almost to the small of her back. She twirled it as she flipped two colored flags in her hands.

Astol's two mothers were standing near the edge. Gria, his younger mother, had flags of her own that they were using to coordinate the two ships as they circled around each other.

He watched the man standing near the stern of the *Diamond*: Lain. Lain was broad-shouldered with large hands and short-cropped hair. Unlike Astol's brothers, he also had a dense beard of black and light blue. While the two boats nuzzled together, he stood easily on the railing swinging the mooring rope with one hand.

Astol's heart beat faster as he listened to Brui and Lain trading verbal jabs. From his height, he couldn't hear most of the words but the playful tone was unmistakable even from on top of the sails. Both Brui and Lain had been friends from long before Astol joined the family.

He wondered if Brui had told Lain about Astol's affections. Just thinking about it brought a burn to his cheeks. He clutched the beam underneath him and double-checked the safety rope.

The leather fluttered with his magic, tightening and coiling over once before growing taut.

He smiled at it and felt a little better. It didn't matter that it was moving under his own magic, it was a nervous tick.

"... Astol?" The tail end of Lain's question rose up in a brief silence of the wind.

Astol inhaled and gripped the beam tighter. He peered down to see that the two boats had moored together and the brothers and sisters were lashing them until it was time to part ways. His chest and stomach ached as he watched Lain looking back and forth across their ship.

Brui said something and gestured up.

Astol let out a whimper.

Lain looked up, the wind blowing through his hair as he scanned up the mast.

Pulse pounding his ears, Astol looked around but there is nowhere to go. He turned back just as Lain's gaze caught his own.

Astol's whimper turned into a whine.

Lain smiled, his teeth bright in the sunlight.

Gasping, Astol held up one hand and gave a shaky wave. He smiled though it would be as visible from on top of the sail.

Lain waved back and the gestured for Astol to come down.

Astol almost fell. He had to grab the beam to avoid the dizziness that slammed into him. He wanted desperately to get closer, to feel the warmth of Lain's chest against his own.

Somehow, he managed to snake out his lead to brace himself then stumble down the mast. His feet slipped near the bottom and he thankfully landed on his rear behind a pile of boxes. Scrambling to his feet, he came around and right into Lain's hug.

"Astol!" said the powerful man as he swept Astol off the ground and squeezed firmly.

Astol lost himself against the instant feeling of warmth and protection. It was also hard and soft at the same time. He felt himself getting harder and had to push himself away to avoid embarrassing himself further.

"It's been too long, Astol. I missed you."

Astol pressed his cheek against one arm. He looked up at the weathered face. He didn't want the moment to leave.

"How long as it been?"

Brui leaned over. "About three months since he dumped dinner in your lap."

Astol blushed even hotter.

Lain smiled broadly. "I remember that. It wasn't what I was hoping was going to land there." His eyes flickered to Astol and then to Brui.

Brui stood there, smirking.

Lain shook his head and turned back to Astol. He was still hugging the smaller man and their mouths were dangerously close to each other. "I heard you had a bug problem?"

Astol opened his mouth but no noise came out.

Brui tapped Lain's arm. "Maybe you're squeezing him too tightly? I think his face is turning red."

"Oh! Sorry." Lain released Astol who stumbled back.

Catching his breath, Astol glared at his older brother.

Brui smirked.

Jabil came up and smacked him in the back of his head.

On the other side of Brui, Nira punched him in the arm. She was much shorter than everyone but Astol. She was also at least three months pregnant but that didn't stop her from reaching out to hold hands with Jabil.

Brui looked at them with a hurt expression. "What?" he said with feigned innocence.

Then Lain reached over and smacked the back of his head.

Spinning around, Brui glared at his friend. "What!?" he said in a raised tone.

Lain didn't say anything.

However, Gria did. Her voice rose out. "Get over here, Brui! Right now!"

Jabil shooed Brui. "Go on, be a pile of rotted fish over there."

With a glare, Brui pushed his way out of the knot of friends and headed straight for Kimai and their mothers.

Lain cleared his throat.

Nira turned to Jabil. "Standing up is wearing me out. Got somewhere cool to rest?"

Jabil's smile reached his eyes. "Come on, the kitchen area awaits, my goddess."

She rolled her eyes but let him draw her away.

Astol looked helplessly as he was quickly left alone with his crush. He looked at Lain, unsure of what to say.

Lain gestured. "Chores before play. Where are these bugs?"

"Up the aft mast, about fifty feet up."

Lain looked up and then shrugged. "As long as you are holding me, there is nothing I'm worried about."

Astol's cheeks burned.

After a few seconds of stammering, he went and gathered up enough ropes for the two of them. It was a more complicated harness to carry the larger man's weight, but his magic could handle it. With a few snaps of his fingers, the coils of rope crawled up the mast and secured themselves against the rings.

Roping a harness around Lain was almost as humiliating as falling. Astol couldn't concentrate enough to use his magic to loop the rope along the hard thighs or around Lain's chest, so he was forced to do it with his hands.

The heat from Lain's body and the soft chuckles escaped his lips only added to his struggles. After he failed to tie a knot three times, he had to step back and take a deep breath.

"Just take your time," said Lain in a low voice.

Astol looked up helplessly. The rope in his hand quivered.

"A deep breath?"

Obeying, Astol inhaled and exhaled.

"There you go. Now, just finish up this last knot." He reached out and took Astol's hand and drew it closer.

"That isn't helping," whispered Astol.

"Why?" Lain smiled.

Astol had no idea why he decided to answer. "I... get clumsy."

"You are very graceful. I've seen you."

"Not around you."

There was a brief pause and then Astol realized what he had just confessed. With a gasp, he looked up with wide eyes.

Lain only smiled.

"I-I didn't mean to say that. I'm sorry."

"Would it be better if I kissed you?"

The world froze in an instant.

Lain leaned forward. "Actually, I've been waiting to kiss you for three months now. So, do you mind if I steal one?" His breath was hot against Astol's face.

Astol didn't know how to respond. He opened his mouth and closed it. He was sure he was about to burst into flames just from his feelings.

Lain drew closer, wrapping his arms around Astol and pulling him close.

The feeling of being enveloped by strong muscles was overwhelming. It was everything Astol had thought it would be like.

"Astol?"

Astol whimpered. Then, he surged forward to kiss Lain. The touch of his lips was an electrical surge that coursed through his body. He trembled as he pressed his palms against Lain's chest to avoid falling over.

He never wanted to move again.

Opil cleared her throat.

Astol and Lain shoved themselves apart. With a blush searing his cheeks, he looked at his older mother.

Opil pointed to the mast. "Chores before play," she said. A spray of cool mist rose out of the ocean behind her and blew across the deck.

When it painted his face, it felt good. He could have sworn it steamed.

Opil shook her head and headed back to the aft were Gria and Kimai were still speaking.

Astol cleared his throat. "Sorry."

"Sorry for kissing me?" Lain said playfully.

"No! No," he gulped. "Yes. No... wait. Every time I'm near you, I can't seem to speak right."

Lain stepped forward with a smile on his lips.

Despite Astol's desire to step forward, he backed away. However, he misjudged his position and his back smacked against the mast. He let out a whimper as he tilted his head to look at into Lain's eyes.

"We should get those beetles," Lain said with a smile.

"Oh, yeah!"

Thankful with the chance to focus on something else, Astol peered up and then snapped his fingers. Ropes came to life, pulling their bodies tight together before hauling them from the ground.

"Besides, I want to kiss you again without your mothers stopping me."
