---
title: Bad News
date: 2020-05-27
summary: &summary >
  Astol and his family are doing their inspections and chores to make sure their boat remains safe to travel across the ocean. However, he finds signs of wood-boring beetles and they realize they need to find someone to get rid of them.
teaser: *summary
characters:
  primary:
    - Astol
  secondary:
    - Brui
    - Jabil
    - Opil
    - Gria
  reference:
    - Lain
    - Bastor da Kasin (Quoted)
location:
  primary:
    - Queen of Salt and Sapphire
    - Northern Sea
  referenced:
    - Diamond Cutters of Ten Thousand Days
topics:
  referenced:
    - The Forgotten Tribes of Lesser Men (Quoted)
---

> The Doila are a wide-spread tribe of travelers who spend most of their lives on boats and never touching dry earth. --- Bastor da Kasin, *The Forgotten Tribes of Lesser Men*

Astol chanted as he walked along the narrow beam between the two masts of the *Queen of Salt and Sapphire*. A leather lead secured around his waist dragged an iron ring mounted around the beam with the rattle of some mechanical dog following after him.

The chant was echoed by the rest of his family on the ship. He could hear his two brothers singing out in the same mixture of spoke word and musical tones that exercised the lungs and the back of the throat.

Jabil was coiling ropes, the thick cords of his arms almost as thick as the weathered loops of hemp that glistened in the salt water. His long black hair was pulled back into a bun, a stern style that matched his personality.

His other brother, Brui, was on the opposite of side with one foot balanced on the railing and the other against one of the stays that kept the aft sail in place. Brui's hair was in a fish tail, with little sparkles of blue and silver glittering in the sun. Like their elder mother, he was a bit flashier with his style even out on the ocean.

Astol looked back at the lead attached to the ring. He reached out and snapped his fingers.

The leather untied itself, coiled up, and then shot itself from the beam to wrap around another ring that had been mounted near the top of the mast. The end slipped around itself into a strong knot and secured him into his new position.

Astol tested it twice before he leaned over the edge of the beam and began to walk down. The ropes over his chest and waist dug into his body with the comfort that they wouldn't let him fall.

His steps were slow and steady. He wasn't afraid of the fall but he need to inspect every inch for signs of rot or weakness. A broken mast could mean death on the ocean in a storm.

He almost missed a shiny spot. With his weight digging into the ropes around his waist and chest, he stopped and bent over, inverting his body so his short hair hung like a brush underneath him.

A wood-boring beetle was trying to find a home. It moved slowly, as if it was afraid to be fifty feet above the deck.

Astol crushed it with his finger and then pocketed the corpse. Then he carefully looked for holes or signs that it had made its home. He didn't find any, but he couldn't just assume. Reaching into his pouch, he pulled out a stick of red wax and circled the area in wide circles.

"Found something?" called his younger mother. He looked down as she walked up the side of the mast. Like himself, she was slender and delicate but her skin had the texture of leather from a lifetime on the seas. The deep brown had a touch of red to it, a burn that would never fade. She crouched down next to him as if he was lying on the ground instead of hanging from a rope.

"Beetle."

"Up here? That isn't good. See its home?"

He shook his head.

She reached up and kissed his forehead. "Come, you look up. I look down."

Then she started to inspect the mast with the same intensity that he had looked. Both of them had been trapped at sea twice with a broken mast during a storm, that was how they were rescued by the men who became his brothers.

Below, Brui started a new song. A winding, somber tale of a ghost trying to escape the gods of the underworld. It was also one of his favorites to start because it meant he got to sing the part of the foolish king.

Astol's younger mother joined in instantly as the role of the ghost. Her higher-pitched voice easily carried over the winds and through the sun that beat down on him. She winked at him and then resumed her search for the beetles.

After a few seconds, he joined with the others. His favorite part was the solider who would sacrifice himself.

They sang and toiled for the song and well into the next. It wasn't until the last bit when he finally spotted three perfect holes in the mast. They weren't there the last time he inspected the mast. He sighed and got out his yellow wax and circled the openings. More searching the area and he found more beetles crawling in the cracks of the weathered wood.

Astol swore as he fished out his wax marker. He circle the bore holes with wide circles in silence, the stark silence as disturbed as his thoughts.

His mother came up. Seeing the marks, she stopped singing to swear herself. "That looks bad," she said in a low voice.

Astol tapped the mast with his thumb to agree.

She got out her own wax marker. It was yellow compared to his red. She circled it also, going down a few feet lower and a foot higher. "Just in case. I'll go tell Opil."

Kissing him on the forehead, she turned and walked down the side of the mast to the desk toward his elder mother.

Opil was at the aft, standing on the furthest part as she peered out across the ocean. The wind whipped at her blue and white dress. It also teased her long, loose hair in a fan of black with blue sparkles.

Unlike the others, Opil rarely sang. She was their spotter, the one looking for dangers on the waves. She also hated the sound of her voice.

Opil and Astol's younger mother spoke.

Astol gave them a minute before rappelling down the mast. Midway down, he had to stop and hook one arm around a thick ring. Snapping his fingers, he untied the leather at the top and brought it down to secure it in place before finishing his trip down to the deck.

Brui came up next him and patted his back. "Bad news?"

"Wood beetles."

"Damn." Then he smiled as he stepped back to face Astol. "Think our moms will head straight for the *Diamond Cutters*? I heard they had a pretty good cleaner."

Astol blushed almost instantly. The *Diamond Cutters of Ten Thousand Days* was Lain's ship.

Brui grinned and peered. "What was his name?"

Astol glared at him. "You know his name is Lain." Lain, another broad-chested man, had an easy smile and Astol found himself stammering whenever they were close. It had been a month since he spilled most of dinner with a misplaced hand while trying not to humiliate himself in front of Lain.

"Stop teasing him," warned Jabil as he swung down and landed lightly next to them.

Brui shrugged. "Why not? Little brother found love. Are we not to celebrate?"

Astol blushed even hotter. Lain was gorgeous and difficult to resist. He hoped that he was also interested in Astol, but it was hard to tell past his own flustered thoughts.

Jabil poked his brother. When Brui just grinned, he poked him harder before punching his arm.

Brui laughed, his muscular form easily taking the blow that would have bruised Astol's slended form. He patted Astol on the back and then headed toward their mothers. Jabil and Astol followed.

By the time they stopped near their mothers, Opil was back to looking across the ocean.

"What's the news, mothers?"

Opil didn't look back. "Let Gria explain, I'm looking for the *Diamond*."

Brui chuckled and Astol blushed hotly. He tried not to think about how Lain felt next to him, or the way their hands always seemed to touch.

Astol's younger mother pointed a finger. "Be nice to your brother."

"Yes, Mother Gria," Brui said in a tone that indicated he wasn't sorry in the slightest. He smirked and bumped his shoulder against Astol.

Opil glanced over her shoulder and then back. She didn't look amused.

The boat bucked.

Astol grabbed for his brothers, the nearest steady objects. Next to the railing, Jabil smacked his hand firmly on it while Brui braced against a rope. The three of them barely moved.

Another wave slammed into the boat.

Opil's lip curled into a smile.

Then one struck hard enough to spray high into air. The water looked like diamonds as it arched up.

Jabil reached out and grabbed Astol by his hips. With a chuckle, he yanked his smaller brother free from Brui just as the water came crashing down to drench Brui completely.

Only a few droplets caught Astol.

Brui laughed and threw back his hair, spraying everyone with water. "I'm sorry, mothers."

Gria gestured to Astol with one hand.

Still laughing, Brui turned to his younger brother. "I'm sorry, little brother, that we are about to visit the man you really---"

The wave caught him on the side, throwing him back toward the stern in a wave of water and laughter.
