**In the Wide Seas, Love Still Meets**

Astol happily traveled the Northern Seas with his two mothers and two brothers. However his heart is taken by Lain, a man with talents to get rid of bugs and infestations.

When an unexpected wood-boring beetle shows up, he finds himself heading toward his crush and doesn't know how he is going to handle being so close.
