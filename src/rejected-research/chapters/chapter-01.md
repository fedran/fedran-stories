---
title: Rejected Research
format: Flash
contentWarning: "Some themes that appear in this story: sexism."
genres: [Slice of Life]
summary: >
  Byodanóma already knew her research would be dismissed. That is why she did it twice and put her boss's name on the second copy.
---

As Byodanóma handed the sheaf of papers to the customer, she knew he wasn't going to accept it. It didn't matter if her penmanship was impeccable or that her research was meticulously, all they saw was the dark skin.

She turned away.

Her boss already had a second version of the same thing. That way he could pretend to redo it and the company would still get paid. It only required her to write everything twice.

Rubbing her sore hand, she sat down for the next project.
