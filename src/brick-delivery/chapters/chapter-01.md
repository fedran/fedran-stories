---
title: Brick Delivery
format: Flash
genres: [Slice of Life]
summary: >
  It is hard to explain why Gertrude needed so many bricks, so she always feined that she and her husband weren't shipping their Baby on the train.
---

"That's a lot of bricks."

Gertrude looked up the train carriage. It was loaded with a couple tons of red bricks. The dusty smell in the air brought a smile to her lips. "Yep."

"Who is going to build a house in the middle of the forest with those?"

She thought about telling him about the mechanical frame underneath. He wouldn't understand. With a shrug, she said. "No idea."

"You are all idiots."

She shrugged again. There was always a need for their baby, the brick mecha.
