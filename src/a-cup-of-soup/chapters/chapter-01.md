---
status: alpha
title: A Cup of Soup
format: Story
genres: [Slice of Life]
summary: >
  While making dinner, Mabel is interrupted by her daughter coming up in tears. The day had finally happened, her little girl had manifested magic.
---

> There is no question about it. With the abandonment of the traditional ways of manifesting magic, now treated as "child abuse" or "hurtful," society is seeing a downfall of magic. --- Gasor Vanor-Medail, *In The Years Before the War*

The front door slammed shut. The impact shook the windows of the brownstone with a sharp report of an upset teenager. Before the vibrations ceased, the rapid beat of feet drummed up the stairs and down the hallway.

Mabel winced as she waited for the second door, rolling the first of her purple-veined *pecycpina* in her hand. The prickly vegetable poked her skin, but it was light enough not to pierce the faded calluses or deep lines that crossed her palm. One of the spines caught on one of the few scars that hadn't healed in the last few decades, but then easily tugged free.

The door upstairs slammed shut, shaking the windows again. The ones in front of Mabel rattled loudly and she felt a brief tickle of cool air. She made a mental note to ask her husband to repair it but kept most of her attention on the noises upstairs.

The ceiling creaked when Karen, Mabel's teenage daughter, thumped in her bed. A few flecks of dust fell from the textured ceiling, but nothing more.

Mabel frowned when she heard nothing else. Typically, Karen's tantrums involved a lot more dramatic screaming and forceful stomping. This time, she raced directly to her room with no effort to involve her mother.

Unsure of how to respond, Mabel went with what she knew, she returned to her cooking. Setting down the *pecycpina*, she picked up her Zotrop E13 Wedge Blade cooking knife and sliced through it. Despite the large pile of vegetables next to her cutting board, the magically-enhanced knife easily cut through the thick rind. She couldn't even remember when she last had it sharpened.

She had just started on the third *pecycpina* when she heard one of Karen's sob drifted through the ceiling. She froze at the sound of it. It was a pitiful, emotional cry, one of a broken heart or personal devastation. Her curiosity turned to concern and then flashed into panic.

Mabel swept the sliced vegetables into her favorite sop pot which already had Naj's Mechanically Trimmed Chicken, still frozen, and Lambert Chicken Broth. With a quick slide along the base of her Robek's Ultra-Low-Resonance Heating Element, Holly Red Series, she brought the heat up for a medium boil and set down her knife.

It was less than fifteen seconds before she was heading up the stairs. Her house slippers scuffed lightly on the carpeted steps as she came up on the second floor. Her eyes caught something on the carpet, which she just had cleaned less than a few days before. She slowed and knelt down next to it, peering down.

There was a petal on the floor. It had a faint, silvery sheen to it and reminded her of her favorite rose, a Lady Pamran's Silver but her perennials were all safely stored in the basement and there was no way Karen would have gone down there before heading to her room.

She glanced along the hallway to Karen's room and spotted a few more petals marking her daughter's path. Pocketing the petal, she headed straight for Karen's room. At the door, she reached for the handle but then froze. Miss Pond's Guide to Raising Girls warned against barging into rooms. She sighed and knocked instead. "Karen? Kar?"

A muffled response drifted through the door.

Frowning, Mabel headed down the hall. "Kar, are you okay?"

"No!"

She stopped at the sound of her daughter's voice. There was pain and fear in the sound, not unlike the noise she made when she broke her leg a few years back. Resting her palm against the door, she leaned into it. "Mar? Let me in? Please?"

"Go away, I'm ugly!"

"No, little one, you aren't ugly. Just this morning---"

"This morning I wasn't a damned plant!"

Stunned, Mabel could only stare at the door. "A-A plant?"

"A plant! A Couples-damned flower!"

Mabel's lips tightened at her daughter blasphemy. She took a deep breath before she respond. "Let me see? Please, Bloss..." Her affectionate term for her daughter was "Blossom," but the petals and Karen's words suggested that it would be a poor choice.

She cleared her throat. "Let me see, Kar? Please?"

Karen didn't respond.

Mabel held her hand up, inches from the door. After a moment, she lowered it with a soft sigh. "All right, Kar. I'll be downstairs if you need me."

It took all of her effort to turn around and walk back down the hall. She looked over her shoulder at her daughter's door before she headed down the stairs. Her hand trailed along the banister, her heart cracking from the effort of leaving her daughter.

She had to wipe a tear from her eye before she returned to her Zotrop E13 Wedge Blade and then realized she had already finished slicing up the vegetables. With a sigh, she set it down and then reached for her chair. Sinking into it, she took a deep breath and let it out.

When silence filled the brownstone, she let the fear rise up and her mind drift.

She knew it was her daughter's talent that had finally manifested. Everyone had some magic, though most in small measure. For her husband, the magic manifested as the ability to perform a single trick of creating flames.

Her daughter appeared to have followed in her father's powers or at least Mabel hoped it was only a talent.

Those who didn't have a talent used magic in a far different way, by crafting the energy inside them into spells. They were called mages and they could do almost anything with magic, but the flexibility of power came with many drawbacks. Mabel wouldn't wish them on anyone, especially her own daughter.

The worst was her civic duty. As mandated by law, she served the country of Kormar as a Mage-Servant. It was a point of pride for her family, a good mark on her credit, and the greatest achievement in her past; it was also the worst two decades of her life.

She had spent her days living in the country's barracks and moving from city to city for whatever duties were required of her. She knew a thousand spells in the back of her head, from stripping paint from buildings to shaping stone for bridges.

Mabel got up and headed over to the pot. The bright red pot glowed from the heater rune underneath. The resonance from the heater rippled underneath her skin, a flutter of muscles that she couldn't stop. Leaning over the pot, she breathed in the smell of cooking chicken, spices, and vegetables. She grabbed the envelope of spices and shook it. Seeing a few dried seeds and herbs, she emptied them into the soup.

Tapping her finger on the second heating rune, she added a pot of water to boil and set out a jar of her favorite loose leaf. It only took a few minutes for the water to boil on the magical heat. She poured herself a cup of tea and sat back down, letting her mind drift through her twenty years of hell.

"Mama?"

Mabel jumped at her daughter's voice from upstairs. "I'm down here, Bl... Kar!"

She stood up as her daughter came down the stairs, one step at a time as if it was the first time she was walking.

The smell of flowers drifted through the kitchen first. It was the same scent as her favorite flower, the Lady Pamran's Silver. The smell grew stronger as Karen inched into the room. Her black hair fluttered with silver petals, the curls of green stems curling around her hair and pulling her long hair into two tails.

Mabel's mouth opened with surprise.

Karen whimpered softly and looked up, her brown eyes shimmering with tears. One of the leaves uncurled around her right temple and wrapped around her hair, pulling it back from her face. It was the same maneuver Karen used to do as a little girl, but she used her finger then.

Struggling not to show the surprise on her face, Mabel closed her mouth and stood up. She spread out her hands. "Oh, Karen, come here."

"I-I'm ugly."

Mabel took a step closer and then another, her heart thumping and sweat prickling her brow, but she knew Karen needed her. She stepped up to her daughter and wrapped her in a tight hug. "Oh, love."

"W-What's happening to me?"

Mabel rested her head against her daughter's head and breathed in the scent of flowers.

One of the petals reached out for her, but she just lifted her head away until it curled back into Karen's hair.

She stroked her hand through her daughter's hair, forcing her palm against the petals as she did. They clung to her fingertips, but released when she pulled back. "It's going to be okay, Kar, it's just your talent."

"M-My talent?" Karen's grip tightened. "This is just a talent? But, it just started happening. I-I was in the rose garden w-with my friends," tears poured down her cheeks, "and then I felt it growing. And then they started to screaming and I-I ran home."

"Oh, honey, that's how talents manifest," murmured Mabel. "You just happen to be the first one of your friends."

"You said it would happen when I was scared or angry."

"Was Robert there?"

The subtle stiffing of Karen's body told her what she needed to know. She smiled and leaned against her daughter. "He was going to kiss you, wasn't he?"

The tension vibrated along Karen's body. The petals spread out as more flowers blossomed in her hair. The scent grew stronger, wafting around both of them. "Y... Yes."

"Strong emotions, not just fear. He's a good boy, though I'd rather he wait. He's not old enough and neither are you." She pulled back from Karen and gave a smile. "But, it made you really blossom, didn't it?"

Karen blushed, the petals in her hair fluttering. "Mom...."

Mabel drew her daughter to the kitchen table and sat her down. Without saying a word, she pulled out two bowls and ladled each one full of soup before serving it. "It's your grandmother's recipe," she said as she sat down.

Karen sniffed it. "Doesn't it usually have basil in it?"

"Yes, but it's out of... season." Mabel grinned. "How did you know?"

Karen looked down and then up. "I... I could smell it. I mean, I knew it was missing."

"A good talent."

Karen gave a hesitant smile, then sipped at her soup.

After a moment, Mabel joined her. It was a good soup, but now that Karen mentioned it, she missed the basil.

"Mom?"

Mabel looked up, the spoon in her hand.

"Why don't you talk about being a mage?"

Tension rippled through Mabel's body, her breath quickening and her chest tightening. "It wasn't a good time in my life."

"You met dad because of it, didn't you?"

Mabel chuckled. "Yeah, he was a bumbling fire-starter who light lanterns at dusk and then drank himself into a stupor all night long."

With a frown, Karen stared at her mother.

Mabel shrugged. "What? I was almost thirty and he was cute. I was shaping stone for the new sewer over by the markets and we just stumbled into each other. One thing led to another and I started sleeping with him."

"Mom!" Karen blushed.

"What? If you're kissing Robert, then you're going to find out about that anyways." Mabel grinned, maybe just a bit evilly. She knew that she was deviated from the Miss Pond's Guide to Raising Girls, one of the best guides for raising young girls in the modern world, but it felt right to say it. Even if she was sure was the wrong thing according to the guide.

"But, I don't need to hear about you and dad."

Mabel smirked and blew lightly on the spoon of soup on her hand. She looked up through the rising steam. "Then maybe you shouldn't be kissing boys?"

Karen, a blush on her cheeks, looked down. "Change the topic?"

Mabel set down her spoon. "Being a Mage-Servant for the country wasn't fun. A lot of work and not a lot of pay. After twenty years, they just left me outside the barracks and told me to live my life. They didn't care that I lost so much time, or that I didn't have any friends left. I was... and then I met your dad again. Both of us were older and a little less into drinking, but the emotion had remained."

"What's it like being a mage?"

For a moment, Mabel hesitated. She didn't want to remember those years. Ever since, she had not used any spell besides ones to keep the insects away and trim her hair.

Karen looked up at her over her half-filled bowl. "Mom?"

"It was... exciting and chaotic, like trying to hold too many flowers in your hand and they keep spilling out." She looked down at her bowl. "I always felt right on the edge of control and only the spells could stop it. Every time I cast, I-I... there was a little order in my life."

She realized she was about to cry and sniffed.

Karen reached out and rested a hand on hers.

Mabel looked into her daughter's eyes and smiled. "I don't really know why I stopped. It was just... it hurt to know I lost so much."

"But you have dad, for at least seventeen years."

"We were together for two years before you were born, Honey."

"B-Blossom, mom. It's okay to call me that." Karen flipped one of the petals in her hair. "I think it's appropriate now."

Mabel smiled through shimmering tears. "Blossom, I always loved that name."

Flipping her hand over, Mabel held her daughter's hand for a moment. Then, she looked down at her empty bowl. "That was good soup, would you like more?"

"Yes, please."

Taking the bowls, Mabel headed over. She ran the finger along the sensitive strip of the Robek's Ultra-Low-Resonance Heating Element, Holly Red Series and lowered the heat underneath the pot. With a soft sigh, she filled both bowls.

"You know, it would be better with some basil," said Karen.

For a moment, Mabel stared at the soup. In the back of her mind, she knew what she wanted to do, but the fear rose up. Glancing over her shoulder, she saw Karen watching her, a curious and hopeful look on her face. Mabel nodded.

Turning back, she pulled out the *pecycpina* rinds and plucked out a few of the white seeds that clung to the inner edge. Coming back, she set the bowls down and rolled the seeds in her palm. One caught on a scar she got from a weather vane on top of a church.

"Mom?"

Mabel closed her fingers over her palm and began to whisper. Sparks of light formed over her fingertips, spiraling around until they formed the runes of the transformation spell. It was a simple one, but one she never forgot. The energy sparkled along her skin and the fluttering increased. A hiss rose from the heating element and a few other other artifacts in the kitchen, but the spell ended before anything shorted.

When she opened her hands, the white seeds had turned into tiny black ones.

"What did you do?" asked Karen.

Mabel held out her hand for her daughter.

Karen took the seeds with a frown.

"Just close your hand, Blossom, and count to thirty. That should be enough."

Hesitating, Karen obeyed and started to count. By ten, she gasped and looked up with surprise.

"Keep going," said Mabel.

A few numbers later, the first green shoots peeked out from Karen's fingers. They grew into deep green, heart-shaped leaves. Karen's voice filled the kitchen as she finished. Trembling, she opened her fingers to reveal freshly-grown basil.

Mabel took the leaves and chopped them up. She sprinkled a little on both bowls of soup before sitting down again.

It was the first time she had used significant magic in nineteen years. To her surprise, it feel good. As did the look on Karen's face.

Her daughter sipped at her soup and smiled. "That's what was missing."

Mabel took a taste herself and agreed. It was exactly what both of them needed.
