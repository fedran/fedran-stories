---
title: Illegal Traditions
character: Furlian
genres: [Drama]
date: 2020-04-23
summary: >
  Furlian gained her magic the traditional way, when her parents set her up to worry for her life. It was natural she would do the same thing for her daughter, though the tides of society had changed and it was no longer acceptable.
---

> Stress breeds spells. The sharper the blade, the more powerful the magic. --- *A Son's Betrayal* (Act 2, Scene 4)

Furlian sat at her kitchen table and worried that she had just killed her daughter. Her long abandoned tea sat next to her bottle of hand lotion. She picked up the cool mug but set it down after a few seconds without drinking from it. After a few minutes, she picked up the bottle of hand lotion next to her mug and unscrewed it. Her hands were smooth enough as it was, the pale skin almost glistening from the contents of the jar. She stared down at it and tried not to think about her daughter dead.

There was a thump outside. She jumped and then glanced at the back door. The lantern over the entrance shone brightly as a beacon. It cost her a pretty crown to buy a new one but she wanted Jenil to be able to see it from a distance in case the robbery went sour. There was a matching lantern over the front door for the same reason, and one at the end of the driveway.

A raccoon raced across the back porch, the brown-ringed tail flashing in the light before it disappeared on the far side. She saw that it had stolen the remains of dinner. It was a waste of a good roast, but she wasn't in the mood to eat.

Furlian turned her head and looked at the front door. Like the back, the inner door was propped open so the screen let in the late spring air stream through the house.

Seeing no one staggering up, she returned her attention to the table. She stared at the open jar for a moment before screwing it close. It made a muffled thunk noise against her third best lace tablecloth. She put away the good one just in case there was blood. No reason to ruin good linen because of traditions.

She leaned against the edge of the table and stared at her tea. Maybe she should put a fresh pot on? Or would it just get cold like the last two? Why couldn't her daughter be robbed faster?

After what felt like years, she decided to get a new pot of tea. Pushing her chair back, she looked out the back door before heading over to the stove. The air above the iron grill wavered and shifted from the fire rune that burned endlessly underneath it. The wafts of heated breeze caressed her face while she filled her pot and then set it down on the grill. A yank of the handle opened up the metal between the pot and the rune. The rune lit up the room in a steady bright orange color as it began to heat the pot.

Furlian rubbed her hands together and then decided to check the front door. She walked to it, half-expecting and half-hoping to hear her daughter come home. If things went well, she would be crying or even lightly injured.

Seeing no one, the worried mother decided to stand on the porch for a while. Her parents never taught her how to wait for someone to hurt their baby. If it wasn't for an overheard fight, she would have never known that her own father had arranged for those men to rob her that night on the promenade.

"Mom!"

At the sound of Jenil's cheerful voice, Furlian's heart fell. She was suppose to be crying or at least injured. Not as cheerful as she had left the house after Furlian's sly suggestion that her daughter go on a walk to enjoy the weather. Taking a deep breath, Furlian lifted her head as her daughter rushed up.

Jenil was a beautiful girl. A bit fat for Furlian's preferences but she was tired of fighting with her daughter who felt she was healthy enough. Jenil's brown hair wasn't in the tight braid that she had left and there was blood or mud splattered across her face and chest. Her dress had been torn and Furlian could see bloody scratches on her knees and ones on her palms.

Furlian's jaw opened in shock. She didn't even have to use her rehearsed reactions. "W-What happened?"

"Someone robbed me!" Jenil quivered with excitement. She wiped her cheek and left a blood smear across it. She beamed and then hugged Furlian tightly. Her bloody clothes ruined her mother's outfit but Furlian had picked her fifth best outfit for that very purpose.

The mother stared in shock as her daughter squeezed tight.

"At first I though I was going to die but then... then... then I hit him back! I realized I needed to make him go away, so I tried to punch him but I missed! But then he was flying back anyways!" Jenil mimicked an uppercut toward the side of the house.

The ground erupted as a dark purple claw burst out of the ground and tore into the side of the house. The wooden planks shattered from the impact and splinters flew out in all directions.

"Bang! Right in the jewels!" Jenil bounced happily. Then she stared at the side of the house. "Oh, damn the Couple, sorry."

"Don't blaspheme," Furlian said automatically. She still couldn't comprehend what had happened. Jenil was suppose to manifest magic but not violent or brutal powers. She was supposed to get fire or earth or something more lady-like. Everything Furlian had done was to better her family; there was no use for such destructive powers with the future she had in mind.

Jenil looked down at her dress. "Oh, mind if I...." Her voice trailed off as she turned around. "Where is he?"

Furlian looked toward the driveway where two guards were coming around the family's newest automobile. One of them was a barrel-chested man with a gut and a sense of power. He had a large hammer slung over his back. The other was a younger man, maybe in his twenties, with a brace of knifes on his thighs and a short sword at his hip.

Jenil giggled and then lifted both hands up in front of her.

A dozen more of the purple limbs rose out of the ground. Each one had three large claws that spread open. As one, they clamped down on whatever plant was nearby. Even Furlian's prized Ghostly Winter rose was crushed by the powerful grips. When Jenil pushed her hands down, the claws dove back into the ground. They left behind divots in the previously manicured lawn, snapped tree branches, and decapitated flowers everywhere.

Furlian opened her mouth to chide her daughter but then closed it.

Jenil spoke to the older guard. "May I get cleaned up, Constable Rail?"

Rail nodded curtly.

"Thank you!" Furlian's daughter said before rushing into the house. Blood and mud trailed behind her as she rushed up the stairs.

Furlian let out a soft whimper.

Rail stopped in front of her. "Your daughter was mugged this evening on her walk."

Stunned, Furlian nodded. She turned to look back at her ruined carpet.

"The robber survived and escaped. I have two good women chasing him down right now, it shouldn't be hard since your daughter managed to rip his right leg off his body. The blood trail will lead to his capture soon enough."

Furlian pale at the mental image. She shook her head and shivered. This wasn't supposed to be what happened.

"Dame? I do have a delicate question though, one that your daughter shouldn't hear."

Furian's eyes widened as she stared at the guard.

"Jenil said you suggested the walk this evening. I also notice that your back door is also open and every light is on. Rather unusual for this neighborhood."

The uncomfortable feeling redoubled in a flash. She pressed one hand against her belly. "Y-Yes?"

Rail pointed to the ground where there was still a small mound of sawdust from where Furian had the light installed. "I also couldn't help but notice that you appeared to have recently put in that light. Your neighbors had filed a complaint with city hall yesterday because it was too bright. I have it on my list to bring it up to you when I heard your daughter's mugging. Right, Soril?"

The younger guard held up a notebook. There was a list of items on it, only two were checked off. She saw her address near the bottom.

Sweat prickled her brow.

"Dame?" started Rail. "If I were to ask this mugger why he robbed your daughter, would your name come up?"

"No, of---"

"Do remember, he did lose his leg and is probably seriously injured. Do you think he's going to keep your arrangement a secret? How much did you pay him? Fifty crowns?"

Furlian whimpered. It was a hundred fifty. She looked at Rail as tears formed in her eyes.

Rail's expression twisted in a scowl. "It is illegal to arrange a crime, dame. Paying a man to rob your daughter is illegal, regardless of your reasons. The sentence is typically ninety days in jail."

She swayed and had to clutch the railing.

"Dame, I will ask again: is he going to mention you?"

Furlian glanced back inside the house. She felt sick and dizzy. Tears ran down her cheeks. "It was for her," she said in a quiet voice. She hoped to have some sympathy, it was tradition after all. "You know what they say, stress breeds spells. It's... tradition."

"It was still a robbery. However, I'm not going to ruin your daughter's night by arresting you."

Furlian let out a sigh of relief. "Oh, thank the Couple."

Rail wrote something in his own notebook. "You have thirty days to present yourself to the courts for sentencing. Expect not to come home for at least a month. You can tell your daughter whatever you want." His voice was hard as he handed over the piece of paper. It was a formal summons.

Furlian stared at it for a long moment and then burst into tears. "I didn't mean to. It wasn't supposed to go this far."

The guard stood there, hand outstretched with the citation as she cried and clutched at his wrist.

"Dame, if you make a scene, your daughter is going to ask questions. If I have to answer those questions honestly, it will not be pleasant for either of you."

Furlia sobbed as she shook her head. She tried to get her cries under control but couldn't, the tears kept rolling down her cheeks. She couldn't go to prison, she didn't do anything wrong. It was the same thing her parents did. Everyone did it. Why were they focusing on her?

"Don't ruin her night. You only manifest your powers once." Rail spoke steadily with just a hint of compassion. Just not what Furlian needed at the moment. She needed him to make her promise to never do it again and then just walk away. Pleading wordlessly, she stared into his hard gaze.

"Mom?" Jenil came down the stairs.

Rail suddenly stepped forward and guided Furlian against his shoulder.

She pressed her cheek to the ridge of his shoulder and sobbed loudly, trying to control herself but still unable. When he pressed the citation into her hand, she shoved it into her dress without looking.

"What's wrong?" asked Jenil. Her hand tightened into a fist as energy started to wrap the air around it.

Rail sighed. "Your mother just realized what happened. Shock, I believe." His voice had somehow gained empathy as he gave Furlian a shoulder to cry again. "I think she needs some tea and then you can celebrate."

Jenil whispered, "Oh, mom," and pulled Furlian away from the guard.

Rail bowed.

Soril did the same.

The lead guard said, "I'll check on you later to see how you are doing. Once we catch the robber, I'll deal with it personally." He turned away as he pointed up to the bright light. "And please, take care of that within a week, otherwise I'd have to come back. Have a good night."

Jenil waved. "Thank you, Constable Rail. Have a good night."

Then she lead her mother into the house. "It's okay, mom. I'm safe. No one hurt me."

That only made Furian cry louder.
