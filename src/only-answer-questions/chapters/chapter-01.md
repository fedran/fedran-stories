---
title: Only Answer Questions
date: 2019-11-28
summary: >
  Tisken was going to meet a new customer. He had very precise rules for contact for his illegal trade and the first rule was simple: only answer questions.
---

> The dalpre are dumb beasts with the shapes of human, incapable of anything other than obeying the loyalty bred into their simple minds. --- Pidor Calsanimum, *The Promise of Genetics*

Tisken hummed cheerfully to himself as he made his way through the kitchen. He had to swayed his hips to avoid a cook bending over to pull something from an oven and lift a tray above the knife of another. His tail trailed behind him, the tuft of dark brown hair wagging while narrowly avoiding touching anything.

"Careful with that tail!" snapped the under chef from where she was inspecting a pot of soup.

Tisken rocked his hip and his tail swung around near a roast that was resting. Tiny muscles tensed as he carefully brought the wag only an inch away before drawing it back with a snap.

"I said careful, Dog! Otherwise I'll have you shaved!" The under chef pointed at him aggressively. Like the other chefs, she was human unlike most of the waiters who were dalpre, animal people.

Tisken started to snap his tail to the side when a firm hand grabbed it. He only had a heartbeat to tug at it before the grip tightened. He cringed and looked back.

Ramil glared at him. She was a canine dalpre like himself. Her short, umber colored hair stood up along the top her head, obscuring parts of her triangular ears.

He relaxed the muscles in his tail.

"Don't. Pick. Fights," whispered Ramil with just a hint of a growl. She had a northern accent compared to the rest of the kitchen's more southern tones of Gepaul.

"Bitch!" snapped the under chef.

Ramil's grip on Tisken's tail tightened for a moment. Then she plastered a face smile on her face. Her ears perked up but he could tell it was just as forced as the rest of her expression. Turning around, Ramil addressed the chef, "Yes, madam?"

"Stop making out with your boyfriend and check waters."

Ramil released his tail.

Tisken glanced down. Her tail had been cropped when she was a little girl, but the little stub wasn't waving back and forth with her thoughts. It was rock steady, frozen in concentration. His attention turned to the curve of her buttocks that were highlighted by the lines of her tight black skirt.

Ramil slammed her foot onto his. "Yes, madam," she said carefully.

With a grin, Tisken snapped his tail. It skimmed the air inches away from Ramil and the roast before he stepped toward the door. The tip flicked the air for a moment before he drew it up against his back.

With the tray of soups braced with his right hand, he glanced through the window on the swinging door to make sure no one was coming before he pushed his way through and into the hallway that connected the kitchen to the dining hall.

Two of waiters---both dalpre wearing white, button-down shirts and black slacks---came in through the door leading to the dining hall with trays of empty plates and used glasses. They nodded curtly to Tisken before entering the kitchen.

He glanced at them as they passed and then turned slightly to look at them from behind. When he saw their cropped tails, he shook his head sadly. Every dalpre in the kitchen had been mutilated as children just so they could serve the humans. Their parents probably saw it as tradition, a way of letting them survive in the human world just so they could toil in kitchens, warehouses, and on the docks.

At least his parents didn't believe in that crap.

The dining hall was about half-filled for the late lunch hour. As he headed to table sixteen, he scanned the room carefully. Most of the tables had small clumps of men sitting at them. He could pick out the ones who had jobs in offices and politics by their traditional black suits, expensive wines, and casual laughter. The rest of the tables had couples enjoying meals together.

He sniffed. He could smell smoke, whiskey, and the subtle undercurrents of people thinking about sex. The strongest smell came from one of the tables he was serving, a couple who had come in with her hands in his pockets and his arm tight around her waist.

Stopping in front of the couple, he bowed deeply.

They ignored him. They always did.

Tisken glanced at the woman. She wore a light green dress with a bit more cleavage that was appropriate for midday. There was a golden dusting across her pale skin, something that proper society ladies wouldn't wear during the day. It didn't matter to him, she wasn't interested in him and he would never have a second thought for a tailless human.

His attention moved to her hands. They were soft and delicate, not even a hint of a rough life.

The woman's date was also unremarkable as herself. Older, maybe in his fifties, with gray on the fringes of his hair and a sense of respectability. There were no scars on the back of his hands or along his wrists. He had a faint ridge along the side of his fingers where someone would have been holding a pen. Judging from his body language and smell, he was obviously hoping to get her into bed at some point.

Tisken set down the soup bowls and gathered up the empty appetizer plates. His gloved hands didn't even make a whisper of noise against the pristine porcelain.

When he left, he kept his tail low. The urge to smack her with it rose up but he knew that he couldn't afford to make trouble. He finished delivering his meals before heading back to the kitchen.

Twenty minutes later, he finished delivering the roast to the tables when he noticed there was a new customer. A single man sat in one of the corners, sweat prickling his brow and the stench of fear wafting along the air currents. He didn't have the customary glass in front of him which Ramil hadn't been summoned.

Curious, Tisken look toward the front door. Goras, the head waiter, leaned against the podium to greet anyone coming in. He was an older man who had just celebrated his forty-third birthday. Human, of course. No restaurant could have a greeter with a tail.

Goras gestured with his thumb toward the newcomer and then pointed at Tisken.

A glimmer of excitement rose up. Tisken glanced at the nervous newcomer and then back again. His tail twitched.

Goras repeated the gesture.

Tisken's tail wagged happily as he headed straight for the door leading to the kitchen and swapped his tray for a pitcher of ice water and a menu. Wisps of steam rose around as he returned to the newcomer's table. "Good afternoon, sir. May I serve you some water?"

The man wiped his forehead without looking up. "Y-Yes."

He looked up and then did a double take.

Tisken glanced down. There was a small card underneath the man's hand. He recognized Ramil's handwriting on it; she had spent months practicing until it looked at good as any scribe's. He also knew the reason for the card, it meant that he had an opportunity for his second job: murder.

The man cleared his throat. "I'm waiting for someone."

"Really?" Tisken said, his heart beating faster. "You mean, you'd like to order something?"

The man inhaled sharply and then picked up the card. He gulped loudly as he read the second instruction on the card.

> 2. Order the seared lamb with mint and lemon on the side in a silver bowl.

"I-I want the seared lamb."

Tisken cocked his head. When the man looked blankly at him, the dalpre rolled his fingers.

"Oh! With mint and lemon on the side. In a silver cup."

It took all of Tisken's effort not to roll his eyes. He hated working with amateurs and nervous, but they paid very well. He picked up the man's glass and slowly filled it with water. "Have you followed the first instruction?"

> 1. Arrive at the Filinco Resturant at the same time as the target.

"Yes, she's---"

Tisken held up his finger while rolling an ice cube off the lip of the pitcher into the glass. "Rule three."

> 3. Only answer questions given. Do not try to explain, justify, or expand on anything not asked. Use as few words as possible.

The man gulped. "Sorry."

Tisken's heart sang as he thought through the various people in the restaurant. One of them was being marked for death. He could almost imagine slicing his knife into a body and the feel of blood on his hands. Was it one of the business men? No, it had to be the women in green or her date.

"Will there be someone else?"

"No, just---"

"Rule three and four."

> 4. You are waiting for the mark to join you. Describe them only when asked.

"Sorry. Yes. She's my wife and her name---"

"Three," Tisken said firmly. His tail and ears lowered with his annoyance. "What does she look like? I can have the head waiter deliver her."

The man glanced toward the front. "She's... wearing a green dress that I bought for...." He stopped talking on his own. "Green dress, auburn hair."

Tisken thought for a moment. She didn't have the appearance of someone capable of fighting off an attack. Nor did the gentlemen with her. He wondered if they had guards but he suspected they didn't.

"H-How much?"

With his tail still snapping back and forth, Tisken had to force himself to keep his voice calm. "We are low on lamb today. A proper cut is going to cost you thirty-one marks."

"That's it?"

"Five." Tisken gestured to the card.

> 5. Pay a hundred times the cost of the dish up front. Do not haggle. Do not ask for specifics. Pay in bills by placing them in the menu.
>
> 6. Pay the same amount when you return after the meal has been delivered. Order the same thing. Follow rule three!

The man read it more than once, his hands shaking lightly. Finally, he cleared his throat. "Oh."

"Will that be a problem, sir?" Tisken said as he handed over a menu.

Sweat ran down the man's cheek. He shook his head before he dug into his pocket.

Tisken tensed for a moment. He glanced around at the table. There was a knife and a fork, he could use those in case it turned nasty.

Slapping his wallet on the table, the man pulled out a stack of hundred mark bills and counted them one by one until he had a good-sized amount neatly piled in front of him.

Tisken stepped to the side to block the view of the money from the rest of the room.

In the corner of his vision, he spotted Goras casually coming over with another menu, the real one for a proper meal.

The man finished counting out the money and closed the menu.

Tisken picked it up. "Very well, sir. I'll tell the kitchen of your request." He tucked the menu underneath his arm and walked back toward the kitchen.

"W-Wait!"

Tisken didn't stop. He tensed as his triangular ears flattened against his head. He dreaded what was coming next. He turned and held up a finger. "Three."

The man was shoving his chair out and standing up. He gulped and held up one hand toward Goras. "Please, I need---"

He paled and looked toward the front window where the woman in green was still lost in the eyes of her date. He gulped and turned back. "Do you know---"

"Three."

"I---"

Goras stepped forward and leaned over to discretely whisper, "Keep your voice down, sir, and remember the third rule." It was rare that Goras got involved with customers, usually when things were about to get disruptive. The air shimmered around all three of them, a faint ripple of the air in the corner of Tisken's vision.

The customer finished standing up. He looked back at the window and then to Tisken before focusing on Goras. "You don't understand, she---"

Without a word, Goras pointedly turned to Tisken.

Tisken sighed. "You don't want to listen to the rules, do you?"

The man's face darkened with anger. "That---!"

Goras sighed. "Please go into the hallway, sir. You wouldn't want to upset your guest." He gestured to the door leading into the kitchen.

Tisken led the way into the hallway. He sighed, things were not turning well but he couldn't afford the man making a scene and alerting the mark. Nor having the city guard come if something went wrong. Of course, the last private conversation in the hall didn't end up well either.

As the door swung behind him and the nervous man, he saw Ramil though the window. As their gazes met, her ears flattened instantly and she glared.

He shrugged.

She turned and put her back to the door. It shuddered as the wooden frame grew more solid. Tisken could almost feel the opening swelling into place, locking it shut.

Behind both Tisken and the man, Goras closed the door and turned his back on it. The older man's magic was more subtle, a sensation that Tisken was being whisked away from the prying eyes and ears of everyone. The sounds of the kitchen and dining hall quieted until there was a thick silence surrounded them.

Tisken turned on the nervous human. "Can you not understand how to follow rules?"

The man looked back at the door and Goras. "I-I thought that man was in charge."

"No, I'm your killer."

"But, you're a dog. You can't possible be good enough to---"

Tisken growled and he stormed closer. "Yes, and you can't follow directions, so we're both disappointed in life. Now, why is so important that you absolutely have to break the rules?"

The man straightened and tried to pull himself up. He was slender but not muscular. However even at his full height, he was a few inches shy of towering over Tisken. "Look here, Dog."

Tisken reached out and grabbed him by the front of his shirt. With power surging through his body, his muscles hardened and his bones grew more solid. He yanked to the side and the idiot was yanked off his feet before Tisken slammed him against the wall. "Now, listen to me, you obnoxious asshole! We have rules! We follow rules! If we don't, then someone dies!"

The man flinched.

"Those rules are for both of us. They provide protection. They how we engage each other. Rule three is the biggest one. Only answer questions asked!"

The man sputtered for a moment. Then his gaze hardened. "She's cheating on me."

"I don't care."

"I care! She comes home every night smelling of Laminal! I know she's fucking him, it's on her clothes, her dress. They go to lunch---!"

"I don't care!" snarled Tisken.

"She deserves this! She is the one who couldn't honor her vows. She's hurting me. Why can't you understand that?"

Tisken's anger rose up. He leaned into the man, a low growl rumbling in his chest. "Are you asking me for permission?"

The man paled.

"Do you need justification to order her killed? To make it look like an accident or a mugging?"

The nameless man began to shake. More sweat dripped down his brow as he glanced toward Goras's side of the hallway.

"What do you think I'm going to say?" Tisken gestured sharply toward the dining hall. "You are paying me six large to kill your wife and you want me to give you permission?"

"I-I-I...."

"Of course I'm going to give you permission, you stupid human! We-I want your money. If you didn't want it, I'm not going to get it, am I? So why would I turn down that much money just to say no?" His voice beat against the walls with his frustration and growing anger.

"I-I need to go back... to my table."

Tisken's grip tightened on his shirt. He leaned into the man, forcing his weight as he crushed him.

The man gulped and flailed at him. "Please! Let me go! I promise, no more questions! Three! Three!"

Ramil smacked the kitchen door.

Tisken looked up to to see her glaring. She mimed him stepping back.

With a sigh, Tisken forced himself to release the man's shirt. He stepped back. "The price is now thirty thousand."

The other man blanched. "How much?"

Forcing himself to calm down, Tisken took a deep breath. "You broke the rules, so the price has risen. Thirty thousand marks due on delivery."

"That's a lot."

Tisken looked at him sharply.

The other man stammered for a moment. "This is the point where I'm in trouble if I don't?"

"Yes."

"Very well, she's---" He stopped and looked up in shock. He closed his mouth. "Three?"

"Three."

The man wiped his hands. He looked at both ends of the hallway and then groaned. "Sorry."

Tisken gestured toward the kitchen. "Go out the back way. I'll get your order done. When it is, then come back and we'll settle up. If you don't, then your next meal will be a delivery."

Shoulders slumped, the other man headed for the kitchen door. It loosened from its frame and Ramil held it open. The bottom scraped for a few inches before the floor finally released it to swing smoothly.

Behind him, Goras walked back to his podium and the sounds of the restaurant returned to the hallway.

"Why in the Wife's Glorious Tit is food piled up? Get it out of here!" screamed the under chef.

Tisken shook his head. They needed a human for the head and under chef but that didn't mean they had to know the full story. It chaffed to obey the bitch, but they needed her as much as she needed to think they were all just stupid animals.

A line of dalpre waiters streamed out of the kitchen as Tisken headed toward Ramil. Each of the waiters nodded to Tisken as they passed. He could almost see the relief in their eyes that there wasn't a body to clean up.

Ramil's stubby tail wagged and her ears her perked as he approached. "How much?"

"Thirty thousand mark."

She smiled at him. "See? Wasn't that better than killing the customer, right? At least we don't have to hide the---"

"Bitch! Get away from your boyfriend and get the damn water out to the table!"

They didn't need to say another word as they went to their separate duties. He let his tail snap to the side, catching her backside before he headed for the next plate to bring up front.

Ramil smiled at him and rocked her hip to the side, almost brushing up against the table holding deserts.

Everyone knew their place and their duties. There was a reason the pack had established the rules for their little business of murder. All of them did, it was the only way to survive among the humans.
