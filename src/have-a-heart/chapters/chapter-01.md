---
title: Have a Heart
date: 2020-02-13
summary: >
  After the Rat Hunters felled another pack of monsters, it was up to Karin to gather proof of their victory.
---

> Typically, a head is required for proof of death but circumstances may require other body parts. --- *Monster Hunters of Kormar*

Karin groaned as she dropped to her knees. Her shoulder injury throbbed from the impact and she grabbed the bloody bandage to keep the stitches from ripping open. "Why do I have to do this?" she asked gesturing to the creature's corpse next to her.

The creature was one of seven creatures a local priest had prayed into existence, fueling their rage with his own anger against a local brothel. The beast looks almost like a ram but the white curls weren't fluffy but somehow harder than steel and sharper than most blades.

The hell sheep had been hunting anyone coming out of the brothel, by smell according to Justan. So far, they had killed five prostitutes, two dancers, and five customers.

"Because you are the only one with something resembling a knife." Roal didn't even try to get up from the rock he was leaning again. His left hand was covered in bloody bandages from where the ram had slammed its razor wool into him; before the bandage had been put on, Karin had seen too much of his bones in the wound.

A severed leg rested against his own, the unfortunate owner had died during the melee. Karin felt a pang of sadness, Gramp was a good kid in his early twenties with naive fantasies of growing old and making millions of crowns from the money he got from Rat Hunters. He knew the risks, most of the Hunters didn't survive a year, but the pay was good.

Karin fumbled with her belt for a moment before unbuckling it. She had to tighten her thighs as she drew it out from the loops. Her bloody trousers threatened to fall down but she needed both hands. Spreading her legs to stop them from falling, she held up her buckle. "This is not a knife."

Roal chuckled. "Come on, we need proof if we want to get paid."

Karin rolled her eyes and then concentrated on the buckle. The metal was smooth but as her magic flowed into the metal, it flattened and sharpened until it was sharper than the razor wool.

As she worked, someone came up. "Looking good, Karin," Justan said. She could hear the leer in his voice.

She tightened her jaw. She knew her crack was visible and her trousers drooping, but she needed to get their proof before the creature crumbled away. She used the now sharp edge against her buckle.

"Watch your tone," snapped Roal.

Karin sliced away the razor wool and used the leather to push it aside. The sharp edges in the wool sliced into the thick material, shredding the sides, but she managed to expose the graying skin underneath before it fell apart. She gingerly used more of her leather belt to protect herself as she cut into the creature's body.

"Come on, Roal. Have a heart. She may be old---"

Karin's jaw tightened.

"---but right now, she's looking very good to me."

Annoyed, Karin slashed into the creature's gut, jamming her hand into the hot organs with her blade. She needed the creature's heart to prove they killed it. The effort caused her to bend over even more, exposing herself.

Justan let out a low whistle.

Roal growled and struggled to get to his feet.

Elbow deep into the mutated creature, Karin found its heart and cut it out blindly. Hot blood poured out around her hand as she tore it free and ripped it out with a wet slurp.

The heart had black runes on it. She could spot some phrases of scriptures but they were twisted and distorted. Not to mention, they were tattooed into the slick surface of the blood-drenched organ.

She stood up and turned around.

Justan was another young kid, in his mid-twenties and obnoxiously proud that he could ignite his weapons on fire. His pale skin had tanned a lot since he started a month ago. This was his first fight. He leaned back and nodded. "Just as---"

"Here's your damn heart." Karin tossed the bloody heart at him.

Justan gasped and grabbed at it. It bounced off his fingers and flew off.

She stepped forward and kicked him in the balls.

With a groan, Justan collapsed.

The heart landed with a smack. When she looked up for it, she saw that Roal had caught it with his injured hand.

Roal chuckled. "Asshole deserved it, but be careful with our payday. This is worth five thousand crowns."

He turned to Justan. "Now, have we learned how to treat our fellow hunters?"

"Y-Yes!"

"Good. Take an extra shift on latrine duty to drive the point home. Act like a shit, you dig out shit." Roal was the leader of the Rat Hunters and usually everyone obeyed him.

Justan limped away.

Roal shook his head. "Kids. He won't make that mistake again."

Karin sighed.

Then her pants fell down.
