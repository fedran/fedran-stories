---
title: Don't Get Involved
format: Flash
genre: [Action, Drama]
summary: >
  In his fading days, Raslor had lost more battles than he won. But there was always a fight, even when he was avoiding his former life.
---

She screamed.

It isn't my problem, Raslor said to himself. He leaned against the building corner and fiddled with the knife hidden under his cloak. The rain thudded against his hood with steady beats.

She was fighting off muggers, using her shopping bags as ineffectual weapons. It was only a matter of seconds before one ripped and she lost.

He watched as the wound in his thigh throbbed.

Don't get involved. No, get in there.

He stepped forward and threw his knife.
