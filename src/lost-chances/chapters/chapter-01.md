---
title: Lost Chances
date: 2020-10-11
character: Tisail
summary: >
  Tisail has a crush on Nanafir but things had happened since he left town to join the military.
teaser: >
  Tisail has a crush on Nanafir but things had happened since he left town to join the military.
---

> A man's heart hardens in war. The fertile joy of youth is quickly replaced with the dry bitterness of experience. --- *The End of Humanity* (Act 2, Scene 1)

Tisail hummed as he worked his hoe along the narrow line of soil. He imagined a song that matched the rhythm of the blade's thunk into the ground and the crunch of earth being pulled apart. Dried roots and stalks snapped with his effort.

Relatively speaking, the garden was only a hundred or so square feet strip of land between one of the major footpaths leading through the village and Old Mary's place. It would only take an hour for him to finish tilling and planting seeds for the next crops.

He finished one row and turned around. He wiped the sweat from his brow and then grabbed the haft of his hoe with both gloved hands. Despite being a small garden, he had responsibility for over fifty gardens scattered throughout Garnet's End. It was a quiet but steady job. He enjoyed it almost as much as being along Mary's Lane in the morning when the sun came up.

Humming, he glanced at the trees to the east. He had about twenty minutes before the sun rose and enough time to finish another row. He focused on his task, upturning a line of dirt until the morning dew glistening on the soil.

He stopped and looked down the lane. Any minute Nanafir would be coming along. He smiled and wiped the sweat from his brow. His leather gloves scraped along his skin, irritating a few scratches from cleaning out a thistle bush a few days ago.

A high-pitched bell rang out. A second bell followed. He started to nod in time with the third but it came a heartbeat too slow. "Might need to fix that," he said with a smile.

It would also give him more time near Nanafir.

Leaning against the hoe, he watched the curve in the road until she came into view.

Her cart had three wheels and a wide bar for her to balance herself. When she walked, her right leg dragged along the dirt. At the end of her step, she leaned into the bar to pick herself up. The movement caused her body to sway and she turned her head in time with the steady scuff of her movements.

Tisail smiled over her cart.

Nanafir caught sight of him and smiled broadly. She pushed her bright blue hair over her ear and continued to push her cart toward him. A breeze ran along the lane, bringing the scents of teas and sweets to him.

Even though it would be easy enough to walk over to her, Tisail would never do that to her. She had her own pace, her own speed. He turned back to his task and started another row of the garden. Every few cuts of his hoe, he peeked up to see her limping closer.

He reached the end of the row. Giving her a glance, he started on the next one but then he thought he saw movement. Turning back, he scanned along the treeline behind her before he spotted the source of the movement.

It was a large pig. The creature had a few arrows sticking out of its back. One of them had a red ribbon on it. Jalen's arrow. There was no doubt the pig was the head female in the sounder, a foul and temperamental creature everyone called Mad Midge.

Tisail's blood ran cold. Midge's sounder has been terrorizing the village for the last few weeks, but usually the wild pigs remained along the southern farms. A few boars had joined into the mess, trying to get into the sounder by picking fights all through the fields.

Nanafir didn't seem to notice the wild pig. She wouldn't have been able to hear it even if Midge was screaming, not until it was too close to miss.

He dropped his hoe and stumbled onto the lane. As soon as he got his balance on the dirt path, he ran toward her while waving his hand. "Nan!"

She focused on him and smiled but didn't stop.

Behind her, the sow's head stopped. The pig started to trot toward Nanafir. In a matter of seconds, it began to pick up speed.

He gestured for Nanafir to turn around. "Turn around!" he yelled after a second. She was hard of hearing but hopefully could discern his tone.

As he ran, Tisail looked around for somewhere to avoid the beast. It had to be sturdy or high. He remembered how easily Midge had burst through a stone fence a few days before.

He spotted a familiar old tree. The gnarled branches had been twisted into a spiral. It had survived the battle between two mages during the last war. Magical poison and resonance and nearly killed the tree, but it managed to stay. It had also survived storms, a runaway boulder, and a thousands kids carving their names.

Nanafir frowned and slowly turned around. He couldn't see her face, but when she jerked violently, his heart almost broke.

Midge growled and charged forward.

Turning back, Nanafir looked back with fear in her eye. Her whimper broke his heart.

He gestured wildly for the twisted oak. "To the tree! Tree!"

She spun on one foot to look in the direction.

He silently begged her not to hesitate.

Nanafir stumbled away from her cart. Her foot caught on the grass but she held out a hand for her balance.

He caught up to her. The smell of tea surrounded her cart and he could feel the heat radiating from the pots that were nestled in their crooks. He looked over it for anything could be used as a weapon.

Through the steam wafting from the cart, he could see the pig tearing through a garden toward them. Leaves and roots tore up in a cloud.

Nanafir whimpered. "My cart!"

For a second, he thought she had turned back but she didn't stop moving. She was calling blindly behind him as she struggled to make the distance to the tree.

"Run! Don't stop!" he yelled, mostly to speak loud enough for her to hear. He tugged her lightly, not wanting to trip her.

Tisail swept his other hand over her cart, snatching up the largest pot of steaming water. It burned his hand but he didn't care. Holding away from his side, he caught up to her and caught her hand.

Nanafir clutched him tightly.

"Lean on me, use me," he said into her ear. He looked over his shoulder. He had a few more seconds. "To the tree. To the tree."

She whimpered but kept stumbling forward.

There was a crash. Midge had reached Nanafir's tea cart.

Tisail released Nanafir and stepped back to turn around. He focused on the furious pig that stood in the remains of the cart. Steaming water and shattered porcelain surround the creature, with shards sticking out of her short bristles.

His heart pounded in his chest. If he had a chance to think about it, he should be running for all his might. Instead, he gripped the tea pot in his hand and stared into the enraged eyes of a wild pig.

Midge shook her head with a snort. Then the dark eyes focused on him.

"Git!" he yelled. "Go away."

She snorted but didn't look away. He could almost feel the hatred in her eyes.

"Damn it, somewhere else!"

Midge charged. The broken wooden and pots crunched violently.

Tisail almost lost control of his bladder. "Shit," he said in a quiet voice.

The world seemed to slow down. Shards of wood and stone flew in all directions. Chunks of dirt cascaded in a shower behind the charging pig.

It took all of his willpower to remain still. He thought about Nanafir. She couldn't move as fast as him so he had to slow down Midge just a bit more.

Gulping, started at the pig. As soon as she came in range, he jerked himself to the side and brought the heavy pot down on sow's muzzle.

Searing hot water and shards of porcelain exploded from the impact. It splashed across his hands, scaling his skin.

The pig let out a high-pitched scream of agony and stumbled to the side. A few steps later, she collapsed but immediately tried to get back to her feet.

"Shit, shit," muttered Tisail. He turned and sprinted to the tree. To his relief, Nanafir was at the twisted wood but was unable to crawl up. Her bad leg kept lifting but wouldn't reach high enough.

That was one thing he could do. Focusing on the tree, he raced as fast as he could. His feet slipped against the early morning ground but he reached the tree before slamming into it with his shoulder.

Nanafir let out a scream.

"No! No! Let me." Slapping his hand against the tree, he summoned his powers. His energies were weak and mostly useless against most living plants, but the old tree had already been twisted with magic. It answered easily to his command. When he wrapped his arm around her waist, one thick branch came around to cup her rear.

She gasped and turned her head to look at him through her good eye.

He grabbed the end and shook it. "Sit! Quickly! Grab tight!"

Nanafir sat down heavily and held onto him. Her grip was tight on his wrist, the fingertips digging into the joints.

Grabbing the end of the branch, he braced one foot against the truck and pulled the branch up. At the same time, he channeled his magic into mimicking the movement.

The tree shuddered as energy flowed through the limb. It pulled them away from the ground in a rush, yanking them into the sky.

Nanafir let out another cry and clutched his arm.

He grunted with the effort to keep his balance, walk vertically up the tree, and hold her hand. With a few seconds of frantic movement, he brought them up to one of the crooks in the thicker branches, a flattened area that made an ideal stop.

Nanafir crawled onto it and sat down heavily. She gripped the wood tightly, her eyes wide and her mouth open with her gasps of fear. After a moment, she released his hand to brace against another branch.

He joined her by giving the branch another pull to let him spill out into the spot. Panting with excitement, he carefully stepped around her legs until he could find a place to sink down and sit firmly.

Below, Midge slammed into the tree. Everything shook violently but the tree held. The sow slammed into it again.

Seeing Nanafir still afraid, Tisail reached out and took her hand with his uninjured one.

She gripped it tightly. "Thank you," she said quietly. She always spoke in a soft voice but she had an unusual accent. Those who didn't now her well would assume she came from another country. Tisail knew that her speech came from unbending instructors from the bigger cities that came in and insisted she learn how to speak even when she could barely hear herself.

The tree shuddered again. Midge growled and slammed into it again.

Nanafir glanced down and then to her cart. "Damn the couple. It's ruined."

"I'm sorry. I'll make you a new one."

She shifted her leg against his thigh. "Do you think you could help me make a new one?"

He smiled broadly and nodded. He had obviously hadn't spoken loudly enough. Over the last year, he had help improve the cart including adding the wider bar and larger tires when she asked. They had made many changes together.

"I will!" he said in a loud voice.

"Thank you, Til." She squeezed his hand again.

A flush of heat and excitement rushed through his veins. He always loved to see her smile. Up close, it was only more brilliant. He wanted to look away so she wouldn't notice that he was blushing.

Below, Midge shook her head and began to circle around the base of the tree. The massive pig looked like it was hunting but he wasn't entirely sure Midge sought the humans cowering in the tree.

Nanafir reached out and patted his knee. "You just wanted to get me up here again, didn't you?"

Tisail blushed. It had been a long time since the two had crawled the tree. He started to speak softly but then realized she couldn't hear him. He looked at her and said loudly, "No! That was an accident!"

Her smile continued to cause him to blush. Her right eye had a slight haze to it, a fogginess that hinted at her inability to see colors.

"How long has it been?"

Tisail glanced away. "Eight years."

She cocked her head.

He repeated himself louder. "Eight years! Back when you were with Balar!"

Nanafir peered at him and then laughed. "Oh, Balar! I had forgotten about him. It's been so long."

He nodded. Embarrassed, he tried to pull his hand away from hers but she tightened her grip. He stared down at her hand as she pulled it closer until his knuckles rested against her thigh.

"That was a few nights before he left for Rougan, wasn't it?"

Tisail remembered. Balar had been drunk that night and tried to break into Nanafir's house. Tisail had heard the noise and investigated. He found Balar chopping through the wood with his shovel, a sibling to the hoe that Tisail had left on the ground to stop Midge.

That night, there had been a brief fight between Balar and Tisail, not unlike him smashing the pot on Midge's head. The only difference was Tisail used a flower pot instead of a tea pot. The memory brought a smile. Balar and Midge weren't much different, bitter and angry and lashing out at everyone.

"What are you smiling about?"

"Midge! He's like Balar. Not only because they look the same!"

For a long moment, she stared at him. Then she burst out laughing. The bright sound widened his smile and he couldn't help but join in.

Together they laughed loudly which only seemed to infuriate Midge who attacked the tree again.

Nanafir gripped him tightly for balance.

Years ago, Balar had chased after them to the same place. In his drunken slurs, he claimed he had rights to her since they were engaged. He yelled them as he chopped at the tree with his shovel, the magically sharp blade cutting into the bark.

Tisail had used his magic to fight back, slapping at Balar with branches and trees. It was a tense moment of violence but then Balar staggered back and dropped to his rear. He screamed obscenities before leaning against his shovel.

She sighed. "Remember how he threatened to hurt us if we came down? Every time we thought he had fallen asleep and tried to crawl down, he would wake up and swing again."

"That was a long night!"

Another smile. "I had good company."

Tisail flushed with joy. "Me too!"

Neither said anything. He stared down at her hand stroking his. It was comforting, as was the way their legs pressed against each other. Being as careful as possible, he reached down and brought her bad leg to rest on his thigh. Then he started to rub the place she always said hurt.

She sighed and leaned her head back. "I should have never accepted his proposal."

"Why did you!?" He hated raising his voice, but it was the only way. For years, he wished he had the courage to ask for her hand but didn't.

Her eyes flickered toward him and then away. "I thought it was time. When he wasn't drunk, he was charming. I needed that." She sighed. "Well, I thought I needed it."

She squeezed his hand. "It also wasn't the right time for us."

Tisail sighed. Then he frowned. "Us?" he asked in a quiet voice.

"You left the next week, you know," she reminded him with a waggling finger.

Stirring, he cleared his throat. "My military service! I had to leave!"

Everyone had to serve the Country of Gepaul for at least four years of their life. Most picked civil service and worked for the government for four years. Others, like Tisail, choose to join the military and then remain in for another four years. The second tour ended in a terrible border conflict.

He looked down but Midge was resting at the foot of the tree. He shook his head and turned his attention back to her.

She said, "You came back changed. We all expected you to move into your mother's place since you took care of her gardens. But you went to the border house on the far side of town." What she didn't say was that they would be living next to each other again.

His blush grew hotter. He had a lot of reasons for not moving into his mother's home. Living where his mother had died of a heart attack was not exactly the way he wanted to return home. The other reason sat across from him in a tree.

Years on the field had him with many wounds, not all of them physical. Most of the time, he slept with a knife in his hand and something pressed against the door. He didn't want Nanafir to see him that way.

She patted his hand. "You shouldn't be sad."

Tisail didn't know what to say.

Nanafir chuckled and gestured over to path where her ruined cart had been scattered. "Do you know why I take this route every morning? I do it because you are going to be along here, waiting for me."

He gave a sheepish smile. "I didn't think I was that obvious!"

She gave him a hard look and then a smile. "Rain, sun, or wind. You are always are standing along one of these gardens when I come up that hill. Every day I push myself up that hill because you're on the top."

A blush burned on his cheeks. "I'm sorry!"

She shook her head and gripped his hand with both of hers. She squeezed tightly and pressed against the softness of her thigh. "Why? I love it. I wake up in the morning knowing I'm going to see you. It gets me out of bed."

Tisail squirmed for a moment. He didn't know what to say. He thought about how he woke up in sweat. Or the way he looked for a weapon whenever he heard someone sharpening a knife. She didn't need to seem him that way. It would break her heart.

She leaned forward. "I want more."

He shook his head. "You can't want me."

Nanafir tapped her ear.

"You don't want me!"

Below, Midge huffed loudly.

Nanafir released his hands and braced herself against the tree trunks next to her. With a grunt, she pushed herself up to her knees.

He stared, his heart pounding faster.

Sweat prickled her brow when she got on her hands and knees and then half-crawled over him until their faces were close. The smell of tea clung to her skin, the sweet smell wafting around them.

He gulped.

"Tisail? Do you like me?"

"O-Of course." Sweat prickled on his brow.

"Then why can't I like you? You've been pining for me and I've been falling for you since we were kids. Why can't we have a chance together?"

He didn't have an answer. He just couldn't tell what was holding him back: fear of rejection, fear of what he had become, a terror that it would end up in his brother's place. He licked his lips and tried to adjust his position.

She smiled. "Then I would very much like to see you a lot more in my life."

"W-Where?" It sounded like his voice wasn't his own.

She leaned forward and kissed him. "When I wake up would be nice."

He tensed.

"When I go to sleep would be better." A smile and another kiss. It was soft and electric, quickening his pulse and bringing a heat to his skin. She leaned more of her weight on him, their legs somehow intertwining until he carried most of her weight.

It was an uncomfortable position but somehow her closeness pushed the discomfort away. He trembled as he wrapped his arms around her.

Nanafir smiled and kissed him again. "Think we can try? Maybe just a night?"

They were closer than they had ever been. He couldn't help but think about her smell, her warmth, and her closeness. He finally had one of his fantasies.

Trembling, he reached up and kissed her back.
