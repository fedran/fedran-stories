---
title: The Call
date: 2020-11-24
summary: &summary >
  Yubanis is woken up when the other family comes back. They lost their only pet and the Penilils are needed to help find them.
teaser: *summary
when:
  start: 44.7.1849 TSC 7.31
characters:
  primary:
    - Yubanis
  secondary:
    - Penilil
    - Tubocak
    - Rainel
    - Unil
    - Osain
    - Gibi
  referenced:
    - Idail (Epigraph)
    - Abasa
organizations:
  referenced:
    - Rainel Family
locations:
  primary:
    - Palifil Nibaras
topics:
  referenced:
    - Tiag
    - "Honor Among the Wanderers (Epigraph)"
---

> The tribes of the Hidanork are isolated during their long family journeys. They can go weeks without seeing a stranger but when they do, they do with open arms and helping hands. --- Idail of Orsina, *Honor Among the Wanderers*

A palm pressed against Yubanis's mouth, a familiar warning to remain still and silent. He tensed as his heart quickened and a rush of wariness flooded through his veins. He slid his hand to his side where he had a short knife strapped to his thigh.

"Yuba," came his brother's low voice inches from his ears. Tubocak's breath smelled of sweets and wood smoke. He pulled his hand away. "We aren't in danger, but we need you up."

Yubanis relaxed minutely. He groaned and sat up slowly. His eyes ached and his thoughts were fuzzy. It felt like he had just put his head down on a pillow. "Is it my turn already? Doesn't feel like I've been sleeping long."

"Sorry, youngest brother. It's only been twenty at the most. I wish I could give you longer, but the other family came back and they need our help."

Yubanis shook his head. "Yeah, of course. Just give me a second."

Tubocak patted Yubanis on the shoulder. "Hurry up."

It took a few minutes for Yubanis before he stumbled out of the back of the family wagon. Despite the warmth, he shivered in the sunlight. Every movement tugged at his limbs as he leaned against the wagon.

He hated the fuzziness of having to wake up quickly. Fighting back a yawn, he came around the back of the wagon.

He saw his brother first. Tubocak was the pride of the family, a muscular man who could go thirty rounds in the sparring resting and had an immense power when it came to tapping the powers of tiags. His was bare-chested, again, and his chest was heavily tattooed.

Tubocak glanced at him and gestured with one hand to where his mother was speaking with another older woman; the other woman would have been the other family's matron. It was always the mothers that made decisions and led the way.

Penilil held the other matron's hands as they spoke quietly. While they both had reddish hair with white streak, Yubanis's mother was broad-shouldered and sturdy compared to the lithe appearance of the other woman.

Both women were products of constant traveling, with brown skin with reddish tones, auburn hairs, and deep wrinkles. Nature had weathered their skins, etching them like stones. His mother had wrinkles that reminded him of her constant smiling.

Behind the mothers stood three younger woman. Two were appeared to be in their twenties while the youngest didn't even look to be six or seven at most. All four women wore patterned white tops though they were patched and repaired heavily. They all shared their mother's slender builds and red hair.

He had not encountered families that didn't travel with a father or brother but it wasn't uncommon beyond a curiosity.

Yubanis stood next to his brother. "Who are they?"

His brother leaned over until their shoulders bumped. He whispered quietly "Their matron is Rainel. The older sisters are Unil and Osain. Youngest is Gibi."

Then Tubocak turned slightly to the side. "You probably want to avoid anything improper with Osain, she's a ram like you. There are five others, but they are further out and won't be back for hours."

Tubocak, Yubanis, and their sisters all had tattoos across their collarbones. On the right, they shared a chain motif which matched their mother's father's linage. The four had different left sides, to represent their biological sires: Tubocak had storms, Yubanis had ram horns, Sophi had thorns, and Opila had waves. The motifs were part of the moots, symbols to indicate parentage and to prevent children from being born of someone too close of blood.

Yubanis curiously looked at the two sisters who were talking to each other. One of them had the buttons of her shirt opened up to reveal a hint of her own tattoos: roses and storm.

Unil was slightly taller than her sister, but not by much. She glanced at him and then back to her sister. One of her brown eyes was cloudy.

Yubanis smiled and looked at his brother. "Hoping to have a chance with Osain?"

"No, Rainel is a storm," he said tapping his chest. "Besides, I think we're not going to have time to enjoy ourselves. Whatever the mothers are talking about, it's pretty serious. Look at the little girl."

Yubanis focused his attention on the girl huddled next to her sisters. Gibi's eyes were red-rimmed and she clutched a stuffed animal tightly in her hand. The freckles on her reddish skin glistened in the fading sunlight.

There was something about the girl's eyes, as if she was devastated about losing something or she had been punished recently. He remembered how Opila had the same look when she found that she hadn't gotten pregnant at the grand moot.

"Shit," muttered Yubanis. With a look like that, they must have just lost one of their family. Despite the realization, he fought back a yawn but failed. The urge to crawl back into the bed rose up. He closed his eyes and rubbed them.

Tubocak elbowed him.

Yubanis jerked awake. He didn't remember falling asleep. "Shit."

Penilil looked sad as she came up to him. "Boys. We need to help."

Tubocak hugged her. "What happened?"

"Their little girl has lost her cat. They've been looking around for three days and haven't seen a whisker or tail."

Yubanis stared at her in confusion. He thought it was something serious, but days of hunting for a cat? He never saw the need for an animal on the wagon but he knew of some families that swore by them. "All this for a pet?"

Penilil reached over and smacked his shoulder. "Just because we don't keep them, doesn't mean they should be treated like that," she snapped. "We do not question the reason of why, only help when there is need."

"Sorry, mother."

"Now, Rainel says they've been looking for three days and it's important they find. The cat goes by the name of Abasa."

Yubanis resisted the urge to scoff. Three days for a pet? It must have been eaten or was long gone. The last thing he wanted to do was go hunting through the woods for some stupid animal.

Tubocak elbowed him again. Then he addressed his mother. "I can ask Nibaras but small creatures are hard to find. Maybe Aba is a stranger to the tiag will be able to detect... her?"

She shrugged. "No idea if Abasa is a queen or a tom but the name sounds like a queen's. I'm going to ask your sisters to help as soon as they come back with getting water."

Penilil patted Yubanis's shoulder, "Help as much as you can. I know you are tired but keep your tongue mild. They are rather upset."

He rested his hand on hers for a moment. He ducked his head in apology.

"Go help them." His mother started to walk away but then stopped. "Don't try anything else until we find Abasa, do you understand? I know it's been weeks and you are feeling pretty cocky about...." Her voice trailed off as she glanced at his brother.

Tubocak shrugged. "I'll keep my brother's dick in his pants until we find the cat."

With a blush, Yubanis shook his head. He peeled away from his parents and headed over to the sisters. As he did, he approached with open hands and palms facing them as a sign of being unarmed. "May the roads see you well," he said.

All three of them tensed with Gibi stepped behind him. "I'm Yubanis, the second son of Penilil."

Osain held out her hand, fingers pointing up and palms to him. "Osain first of Rainel. These are my sisters, Unil and Gibi."

As much as Yubanis didn't want to look for the cat, he knew it was the proper thing to do. Out in the wilderness, between tiags, helping strangers was critical to survival. Even if they insisted on having something as helpless as a cat. He took a deep breath. "We are looking for a cat?"

Gibi sobbed and buried her face into Unil's leg.

"What does Abasa look like? Nibaras can't give me a picture of her."

"Boy," muttered Gibi. "Abasa is a boy. He has balls and scratches a lot."

Yubanis squatted down to bring himself to Gibi's height. He gave his brightest smile. "Sadly, it's really hard to catch little cats to see if they have dangling bits. Usually they scratch when you look."

Gibi's lip almost smiled.

"Do you think you could give more hints? What color is Abasa?"

"W-White. About the size of a melon, but really thin."

"So, a melon slice?"

Gibi smiled and giggled. "Yeah."

Inwardly, Yubanis groaned but he kept it from his face. If they couldn't find the cat, then they would have to spread out in a spiral and look for it outside of the area. After three days, the feline could be hours away.

His train of thought jogged his memory. He looked up to the older girls. "Did you come from the south? Near a fallen tree? It would be about two hours away on horse."

Unil nodded. "Yes, that's our route why?"

He straightened as much of the exhaustion fled. "Is there a chance Abasa jumped off there? I saw a brief flash of a white creature when I was driving past. It was too far away and moving pretty fast, but it was small and about the right size."

All three girls gasped.

Gibi whimpered. "That's where the bags fell out of the wagon!"

Tubocak patted Yubanis's shoulder. "Let's go then. Should only take a few hours to go there, get the cat, and come back. Nothing easier."

Osain shook her head. "It's almost dark, we'd be traveling blind."

Tubocak shrugged. "We don't care, do we, Yubanis?"

Despite wanting to crawl back into bed, his brother had put him on the spot. Yubanis glanced at Unil, then to Gibi who looked hopeful. The little girl's expression broke his reluctance.

Tubocak grinned back.

"No, they will all sleep better if we can get Abasa back."

"Great, I'll tell Mom and the others." Without giving a chance for anyone to respond, he strolled back to the camp.
