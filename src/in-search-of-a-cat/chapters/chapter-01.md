---
title: A Fallen Tree
date: 2020-11-10
summary: &summary >
  Yubanis is driving the family's wagon when his mother wants to have a short conversation about their destination and the events at Yubanis's last moot.
teaser: *summary
when:
  start: 44.7.1849 TSC 5.62
characters:
  primary:
    - Yubanis
  secondary:
    - Penilil
    - Goisay
    - Abasa
  referenced:
    - Jilal (Epigraph)
    - Warin
    - Ami
    - Opila
    - Sophi
    - Tubocak
locations:
  primary:
    - Poisoned Well of Ijarka
  referenced:
    - Mount Ilosigan
topics:
  referenced:
    - Tiag
    - "The United Hidanork Tribes: Before the War (Epigraph)"
---

> The grand moots are the rare occasions the Hidanork gather together to make decisions that affect the entire country. It is also a time of private and public celebration among strangers. --- Jilal of Odir, *The United Hidanork Tribes: Before the War*

Yubanis sighed as he leaned back against the seat. The reins to the wagon horse rested in his palm and he used his thumb to keep the warn leather straps from slipping from his grip. The late spring sun tickled his face and there was a gentle breeze that brought the smell of new flowers.

He took a long, deep breath and smiled. It was a lovely end to a long day of travel. More importantly, it meant he would be sleeping in a few hours and that thought pushed back the exhaustion a little. Somehow sitting on a wagon bench for a few hours at a time dragged down on his eyelids and brought a craving for a soft blanket and dozing off to sleep.

From inside the wagon, he heard a shuffling noise. Judging from the grunts, it was his mother working her way over his sleeping siblings to the front of the wagon.

Yubanis shifted to the side to give her space.

Penilil, his mother, stuck her head out to look around. Her bangs had gotten long in the last few weeks and they stuck to her face. "We're about two hours from sunset."

Before he could say anything, she crawled out of the wagon and sat down heavily on the bench. The wood underneath both of them sagged and creaked. His mother sighed as she scanned the horizon. "It's been a seven years since I've been in this area."

Yubanis shrugged. He had a vague memory of the area but he couldn't quite place exactly when and where he had seen it. His family's traditional route took thirty-one years to complete, but they frequently came back to the same regions to reconnect to the more fragile lands.

His family, like everyone else he knew, gained their powers by connecting to the tiags, the semi-sentient presence tied into areas of land. There were countless tiags of all sizes and shapes, ranging from the trivial spot surrounding a spring to the snowcap of Mount Ilosigan.

He was only twenty-two and had never seen the full route. He hadn't been able to touch all of his family's tiags and establish his own connection to each one. To him, the route they took was memorized but only as abstract lines on a map instead of the drawn by the memories of experience. By the time he started to go over the same trail again, he hoped to have the same ease of remembering as his parents.

His father stuck his head out of the wagon. "Are we there yet?"

"Just looking for a stopping place, old man," said Penilil. "I'm a little fuzzy."

"Getting forgetful already, Peni?"

Yubanis tensed, struggling to keep a smile from his face.

His mother stiffened. "Gois?"

"Yes, my love?"

"Just tell us where we are before I smack you."

Goisay laughed and pointed ahead of them to a gap between two clusters of trees. "See that rotted tree we knocked over? Follow the tree line around it and then head up along the north side. There is a good clearing for the night. They have a spring, but it's down at the bottom of the ravine and a long walk."

Yubanis couldn't help be impressed by his father's memory. "Who's land is this?"

"The tiag claimed Warin's family nine generations back. They return every three years but won't be back until next year. She should accept us for a day or two before we're overstayed our welcome."

Yubanis nodded and focused on the rotted tree. It was a thick one that leaned drunkenly against another pair of trees that were bowed from the weight. Judging from the number of mushrooms and vines hanging off it, it looked like it had fallen over years ago. He wondered how his parents were responsible for it falling over.

"You remember that tree, don't you?" his father said while nudging his mother.

Penilil frowned for a moment.

"Big storm? You were wearing that flower dress before it ripped. The one with the pink flower on the shoulder. We were sitting underneath it when you decided to whisper something...?" His voice trailed off suggestively.

His mother said nothing.

Yubanis glanced over to see his mother blushing. Her beige skin took on a reddish hue. Then she turned and shook her fist at her husband. "I will end you," she said in a tone that balanced between the edges of violence and amusement.

Goisay chuckled. "Oh, you remember now?"

She pointed to the other side of the tree. "I also remember you trying to take me over there, after your father specifically said that the land was sick."

Yubanis followed his mother's gesture curiously. The trees on the other side of the trunks were lighter and more branches were bare. Despite being near the end of the day, he spotted a few wisps of mist clinging to the back of the thick underbrush.

A feeling of dread prickled the back of his spine. Something felt off, like a sour taste in the back of his throat whenever he considered driving the wagon closer.

A small white creature ran along the base, its body half-hidden by the branches that reached out from the trees like a thousand claws preparing to dig down. It stopped for a moment, looking around sharply, before it raced out of sight.

"Yeah," Goisay said. The amusement had fled his voice. "Yub? Why don't you avoid that side? That tiag has never welcomed me and I don't want to risk your sister's pregnancy."

His mother grunted with agreement.

Yubanis steered the wagon closer to the broken tree. Sometimes, it was hard to tell if a land was rejecting the family or it was poisoned. In both cases, the tiag skewed perceptions to make the area look unwelcoming and less appealing. He shivered and looked away. "Should we ask Tubo to look?"

"Your brother is still sleeping as are Opila and Sophi," said his mother. "We are heading further north to Blueberry Point and won't be back here for another few years. I'll wake them up once we get to the patch of flowers."

Goisay chuckled and then yawned. "Finally remembered where we are? Good. You going to stay up?"

Penilil indicated she would remain on the bench.

Yubanis didn't say anything but he would appreciate the company.

Goisay reached up to squeeze her arm and then headed back to return to sleep. "Call me if you need me."

Yubanis yawned.

"Just another hour or so, then you can rest until dinner. Don't worry, I won't let you fall off the bench."

It was his turn to drive the wagon and clean up dinner. Thankfully, having a short nap would help him stay awake for the first shift of the night. Yubanis nodded.

He turned to look at the unwelcoming area and wondered what was inside.

"Don't be curious about places like that. Nothing ever go well venturing into those boundaries. Trust me, specially that place." She groaned and rubbed her shoulder.

When he noticed her glancing at the tree, he looked at her in surprise. "The tree?"

She blushed again and nodded.

"I thought he was...." Yubanis realized he didn't want to continue.

"We were." She smiled and shook her head. "He was a good kisser and talker. I liked spending time with him away from the others."

"Uh---" He was confident didn't want to know about his parent's sex life.

"But then he somehow convinced me to head over there. It was a dare or something; I don't remember exactly what he said at the time. We only made it a few yards into the trees before the trees came to life and threw us out. Your father was thrown across this space---" She gestured up and over the wagon from one side to the broken tree. "---and into that. He broke his arm and leg when the tree snapped in half."

Yubanis snorted. "He broke a tree because he was doing something stupid?"

His mother leaned over and grinned. "He's a man, that's what all you do. You see a pretty bird and have to puff up your chest. I mean, that's what you did with Ami, isn't it?" She reached over and poked his shoulder.

It was his turn to blush. At his first grand moot, he had tried to impress available women with his fighting abilities but he wasn't very good at sprawling. His own strength was his ability to recover from almost any injury in a matter of seconds.

That impressed Ami enough that she helped him discover a new talent of his, a near immunity to being drunk. After guiding him through a night of drinking contents, she then guided him into another first, a night with a woman.

"She saw something in you. Don't ever forget that. You are a good man, Yubanis." She smiled and hugged him with one arm. "I'm just so proud of you."

With a smile of his own, he flicked the reins and let the complement carry him forward.
