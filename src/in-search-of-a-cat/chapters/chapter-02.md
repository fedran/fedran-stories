---
title: Abandoned Wagons
date: 2020-11-17
summary: &summary >
  Yubanis finally reached their stopping spot for the night, but there was another family already there. Though he had rarely encountered any trouble in the wilderness, there was something off; rarely did everyone abandon their wagons.
teaser: *summary
when:
  start: 44.7.1849 TSC 7.9
characters:
  primary:
    - Yubanis
  secondary:
    - Penilil
    - Goisay
    - Tubocak
    - Sophi
    - Opila
  referenced:
    - Ijosil (Epigraph)
    - Warin
locations:
  primary:
    - Palifil Nibaras
organizations:
  referenced:
    - Rainel Family
topics:
  referenced:
    - Tiag
    - "Surviving the Wilderness, One Family's Horror (Epigraph)"
---

> When a family has to spread out, almost always the family matriarch remains in a central location to function as the anchor and a place to return. --- Ijosil Ogamial, *Surviving the Wilderness, One Family's Horror*

They drove in relative silence for almost an hour and a half before they came up to the broad ravine. Following his father's directions, he directed the horse and wagon up along a dirt road that had been marked out with brightly painted rocks and wooden signs that named both the area, Palifil Nibaras, and Warin's family.

Before he reached the top, he could feel the vague presence of the tiag pressing against his thoughts. It was a bright, windy sensation, a memory of a brisk breeze that brought the scent of older leaves. It was not an uncomfortable feeling, just the sensation of someone staring intently with curiosity, the wariness of watching someone and wondering if they were armed with a weapon.

Reflexively, he reached down to the edge of the seat toward a small box underneath the center of the bench.

When he touched his mother hand, he jumped but didn't pull back. Together, they pressed their fingers against the carved wooden door. Inside the locked box were the various token of appreciation their family had gathered over the years. One of them was a coil of braided rope from Warin. His mother had gotten it years ago after repeated encounters as their two families grew closer together.

Yubanis knew that his parents would be allowed into Nibaras without it, but having the rope sped the tiag's acceptance. It also gave one more proof that he belong in case the tiag decided he wasn't his parent's child.

The curiosity faded into a welcoming breeze.

His mother took a deep breath and let it out. There was always a chance that the connection had been broken and they would be rejected. She chuckled and then gestured ahead. "There's smoke. Someone else is camping here."

Even though there was no sense of danger or wariness, Yubanis couldn't help but tense. They had a few bad encounters over the years, though he had to admit he had never seen violence on Warin's lands. The man was welcoming to everyone and his family's lands had a tendency to match the mood.

"I'll get your father and the others ready." She crawled into the back of the wagon.

Unspoken was that she was also getting Tubocak. Yubanis's older brother was adept at connecting to tiag's and using the magic from the land. If things came into a fight.

The winding path led up to an open area. Two other wagons, both with an identical blue pattern, were arranged along the north side of the space. Even though one of the wagons looked a decade older than the other, they both had wide tires with metal rims and a black covering that looked softer than wood.

Whoever the family was, they were one of the experimental folks who either had a lot of money or dealt with merchants from other countries. Most of the families couldn't afford metal rims.

He pulled his wagon up along the east side of the clearing. It wasn't as ideal as the north, which had an rocky outcropping to shield it, but there was shelter and a flattened area.

By the time he positioned the wagon, the rest of his family had already jumped out and were spreading out toward their various chores. His mother headed straight for the other wagons; as the mother in the family, it was her job to introduce herself first.

Tubocak yawned as he grabbed the horse and unhitched it. "There you go, girl. Let's get you something to drink and eat." Then he looked toward his brother. "Good job, Yuba. Why don't you get some rest?"

Yubanis yawned and nodded. Driving was exhausting and he had been steering the wagon all day except for a few hours near midday. He nodded before hopping off to follow the signs to where he could relieve himself.

The back of the wagon was relatively spartan but very colorful. Like the outside, there were a lot of flowers and chains. It had two entrances, a larger one in the back and a small hatch on the starboard side that led up to the bench.

He noticed that Sophi was awake but resting on the bottom bed. She had a small glow lamp while she read a book. The warm light gave her skin a golden appearance as if she was glowing. Her normally bound hair spilled out across her shoulder in copper waves. Her other hand rested on her belly that was just beginning to show her pregnancy.

"Mind if I sleep?"

"Go ahead. I started to get up but then got dizzy. Mom said I had to remain inside until they start the food." She groaned and rubbed her belly again; she had gotten pregnant during the grand moot where he had spent the night with Ami.

"Got to keep the family going," he said. With one foot, he kicked up the fold-away bed that his brother frequently slept in and then hiked himself up to the bed above his sister's. It smelled of his parents but the soft mattress and still-warm blankets called to him.

With a groan, she closed his eyes and sank into the pillows. Just a few hours then they would wake him up.

Even though the curved outer edge of the wagon had been well insulated with batten and canvas, he heard the faint voice of his mother. "Tubo, ask the tiag where the others are."

"Problems?"

"No idea, but usually someone stays behind the wagon."

"I can ask."

A moment later, the air strummed as if someone had plucked a harp string. The taste of a breeze tickled the back of Yubanis's throat.

"She says it was a family of nine that came in. They were worried about something and have spread out in all directions. They've been here two... no three days."

Yubanis sat up, a prickle of concern. He cocked his head.

"Anything else?"

"No," his brother's tone had a worried sound to it. "I can't get a better image from her. Should I get Yuba up?"

"No, not yet. Let him sleep."

Yubanis didn't move.

"Their mom is down at the bottom of the ravine, near the spring."

There was a quiet conversation. "Let your brother and sister sleep." Then she spoke louder, "Goisay! Grab the water barrels, we're going down to the spring!"

Yubanis started to lower himself.

Then someone banged on the side of the wagon. "Go to sleep!" snapped his mother.

With a smile, Yubanis relented and slumped back into blissful unconsciousness.
