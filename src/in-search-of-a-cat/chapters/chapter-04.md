---
title: Driving Along
date: 2020-12-01
summary: &summary >
  Yubanis, Tubocak, and Unil head back to rescue Abasa from where he had seen the cat. Along the way, all three get to know each other better.
teaser: *summary
when:
  start: 44.7.1849 TSC 8.73
characters:
  primary:
    - Yubanis
  secondary:
    - Unil
    - Tubocak
  referenced:
    - Opila
    - Goisay
    - Ochyonáka (Epigraph)
    - Osain
    - Abasa
    - Gibi
locations:
  referenced:
    - Cinifel Abasa
topics:
  secondary:
    - Tiag
  referenced:
    - The Conquest of Sand and Soil (Epigraph)
---

> The tribes do not simply tap a tiag for power, they give something in return. It is a relationship not unlike one of a gardener and her flowers. --- Gaiden Ochyonáka, *The Conquest of Sand and Soil*

Yubanis sat on the front bench of their family wagon as it traveled in a sea of darkness and flashing lights. He had been the one driving when he spotted the cat so they needed his memories of the route to return to the same spot. Like finding ancestral tiags, he knew exactly where to go.

Next to him was Unil, the only one of the sisters to go. Her body was warm and he found it distracting when her hip or shoulder bumped against his own. It didn't hurt or annoy him, but the desire to have her touch him again was making it difficult to concentrate.

His brother insisted on the far side, Yubanis suspected to ensure Unil sat between them, but also because he had to keep his hand out as he channeled his power into the surrounding tiags in keeping them safe.

The tiags responded with endless swarms of fireflies sailing ahead of the horse and lighting up the path. Small patches of moss and mushrooms glowed brighter with his presence, marking dangerous spots in splashes of color and the rush of smaller animals. Along both sides of the pool of light, animals would set up so their eyes reflected the brightness and lead them steadily away from sinkholes, streams, and holes that would damage the wagon.

Traveling at night would have been dangerous without his brother and Yubanis was thankful for the company.

Unil turned to him, her face shadowed by the single glowing stone mounted above them. "I've never seen someone able to touch the tiags like that," she whispered.

Yubanis swallowed and nodded. Up close, he felt more nervous being almost alone with her. "Tubo has a great gift. It has served us well on our travels. It almost matches his ability at sparing and leading."

She glanced at Tubocak and then back. "What about your gift?"

"Less impressive," he wasn't sure why he didn't want to lift himself up. His talent was less impressive as his brother who had maintained a pool of light for almost an hour.

"Don't bury yourself in the dirt, brother," Tubocak. "Your gifts are just as great as mine."

When Unil looked at him expectantly, Yubanis clutched the reins tighter and took a deep breath. "I'm hard to kill. Most injuries heal in a matter of seconds. Including broken bones and cuts."

Her lips parted slightly and he wondered what it would feel like to kiss them. Then he tore his thoughts away, it wasn't the time or place. He had only been with one other woman and the scant weeks since it had happened were still plaguing his thoughts.

Tubocak, on the other hand, wasn't done embarrassing him. "We found his ability one day in summer. Oh, I remember it---"

"Tubo...."

"Opila was running along a field and fell into a snake pit. Venomous snakes. Rust Fangs. She got bit a few times and started screaming. I was too far away but little brother always loved his sisters. He jumped right in, picked her up, and threw her out of the hole."

Yubanis shivered at the memory. He had set off the snakes and they started biting him furiously, their fangs tearing into his legs and arms as he struggled to push his sister out of the hole. More than once, he forced his hand between a snake and her body just so it would bite him instead of her. He thought he was going to die that day.

Unil gasped softly and her body trembled against his.

"Yuba managed to crawl out after her and he dragged her away until the rest of us caught up with them. When I got there, she was already unconscious from the poison but he was knelt over her, bleeding from hundreds of wounds and crying."

It was only a few dozen bites but Yubanis didn't correct his brother's exaggerations.

"I asked the tiag for help with finding something for the venom. It took me so long and I couldn't find enough. I was sure I was going to lost one of them that day."

Around them, the natural lights flared brighter for a moment and a fox yipped loudly.

"But when I got back, he was planting a warning post with our dad near the snake pit without a single scratch. Which was good because I could only find enough herbs for one of them." Tubocak sighed. "I'm glad the tiag told me it was okay, she knew what my brother was capable of."

Unil patted Yubanis's thigh, sending a thrill up his limb. "I bet it was scary."

Tubocak chuckled. "Oh, probably not as scary as that time with the ravine."

With a sudden blush, Yubanis spoke up sharply. "What is your talent?"

Unil didn't move her hand. "I'm unstoppable. As long as I can move forward, I can push or pull almost anything."

"What? Really?" Tubocak sounded impressed.

"Sometimes we give the horses a break because I can drive the wagon on my own. That was actually how we found out I had the talent. It was a rainy day and the wagon got stuck in the mud. We were all pushing and it wasn't going anywhere." Her voice grew softer with the memory. "Osain felt the flood coming through the ground. We only had a few minutes otherwise we would have lost our wagon and Gois, that was our horse, so I just pushed with all my might and everything moved. I ended up dragging the entire family with me just in time to avoid the flash flood."

Tubocak snorted.

"What?" she asked.

"Goisay? The horse was named Goisay?"

"Yes, how did you know?"

Yubanis chuckled. "That's our dad's name."

"Well, he is a horse's ass." Tubocak snorted.

For a while, they laughed at the joke before riding once again in silence. The wagon creaked and shifted with the movements.

"Yuba, Tubo?" Unil spoke in a soft voice. "If we can't find Abasa alive, we really need his collar."

"Why?" asked Tubocak.

"It's our tiag gift. We are heading to the beginning of our trek, where my granddad was born. Mom was born a few months after they left last time and we haven't been back in forty-nine years." She sniffed and a tear sparkled on her cheek. "H-He died a few months ago, in his sleep while we were traveling. We... don't have anyone who has every touched Cinifel Abasa. We only have his bracelet. Its our key to make sure the tiag knows us."

Yubanis closed his eyes tightly. He didn't know what he would do if they lost his mom before they finished the route either. The idea that one of the ancestral tiag's would be abandoned left a hollow space in his gut, a feeling of despair he never wanted to experience.

"The cat has it?"

"Gibi likes to play with it. She knows it's important but you can't also lock those gifts away. They have to be touched, loved, and played with. The innocence of a child helps imprint the tiag's gift on their spirit so we let her." More tears came and she used her sleeve to wipe them off. "I did the same thing when I was younger. Every night we made sure it was safe and that was the first time she had ever lost Abasa. The first time the cat had ever jumped off the wagon while traveling."

Yubanis shook his head. The cat was more than important to the family, it meant the end of a half-century journey could be lost if they didn't find the its collar.

Around them, the lights dimmed. Tubocak took a deep breath. "I promise. We will find it. Even if it takes us a year, we won't leave you without it."

To Yubanis's surprise, it was his hand that she squeezed. "Thank you."
