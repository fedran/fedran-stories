---
title: Thanks
date: 2020-12-29
sex: true
summary: &summary >
  Yubanis wakes up in his bed, alive and mostly uninjured. Happy to be still breathing, he is much happier to find that Unil is under the blankets next to him.
teaser: *summary
when:
  start: 45.7.1849 TSC 5.2
characters:
  primary:
    - Yubanis
  secondary:
    - Unil
  referenced:
    - Garonàga
    - Tubocak
    - Fiáchi
locations:
  primary:
    - Poisoned Well of Ijarka
topics:
  referenced:
    - The Iron King's Betrayal (Epigraph)
---

> The greatest moment in life is this quiet right before the storm, the silence that heralds our great destruction.  --- *The Iron King's Betrayal* (Act 2, Scene 5)

Yubanis woke up in the familiar confines of his bed. The blankets and sheets smelled of his parents and he knew instantly he was back in the wagon, on the top bunk. With a groan, he tried to roll over but his muscles didn't quite work right. Something in his back clicked and sent a little flash of pain coursing up his spine before it shifted enough for him to move.

He tested his wounds. It hurt to take a deep breath, but he was relieved that he was drawing air into his lungs. Most of his joints ached and his back felt like it had been dragged along the ground. For someone who had felt his chest collapse, he felt remarkably healthy. Or at least alive.

When he took a deep breath, he picked up a new scent in the air. There was someone else in the wagon, a stranger. Curious, he inched his fingers through the blankets until they met up against the smooth skin of someone else. He froze, half afraid to wake them up.

"Yuba?" came a sleepy, hoarse voice. It was Unil.

He smiled. "Y-Yes." His voice was in worse condition. He never thought about the limits of his healing, but he suspecting keeping him alive had taken precedence over the little aches and pains.

Yubanis pushed himself to his side. Every movement hurt. Most of the pain focused on his back and his chest. It felt as if he was on fire, a sensation he hadn't felt since his regeneration abilities had manifested. By the time he balanced on his shoulder and hip, he was sweating from the effort.

Unil had been bandaged heavily. Many of the cuts had darker spots in the middle where the blood had soaked through. The slight angle to the cuts on the gauze told him that his brother had done the bandaging; Tubocak had a distinct way of caring for someone.

"Did we make it?"

"All of us," she said quietly. She tried to roll on her side to face him but then stopped. Slumping back, she looked at him with a smile. "Some of us don't recover like you did. The little girl is going to have scars for the rest of her life on her face and side."

"Nàga and my brother?"

"Nàga had some deep wounds that looked infected and he was exhausted. They are sleeping in the other wagon while your brother watches over us. He says he can't sleep with a broken arm. Also he wants to be up if your mother comes."

Yubanis cringed. "He's a brave man. I'd rather go back into the tiag than be the first person she meets."

Unil stroked his cheek. "I thought you were going to die."

He tried to bring up the memory of his final thoughts. "I think I did, actually. It was quite painful. I don't plan on doing that again."

"It was very brave. Foolish but brave."

Yubanis blushed. "I didn't---"

She stopped him by reaching up and grabbing his neck. He started to make a protesting noise but then she had pulled him down into a kiss.

He stopped fighting and returned the kiss. When her other hand slid up along his side, he couldn't help but press his palm against her breast. Underneath the thin material of a borrowed shirt, her nipple was hard. When he circled it, she arched her back and moaned.

When they broke for air, he smiled. "Are you sure you are up to this? How hurt were you?"

"I'm not going to feel it in a moment," she said with a grin.

"Why?"

She grabbed his hand and pushed it down between their bodies. There was no question where she was guiding him and he knew the path. "Because you are going to kiss me again."

That was all either of them needed.
