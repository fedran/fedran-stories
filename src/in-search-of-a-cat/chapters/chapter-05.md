---
title: Amber Lights
date: 2020-12-08
summary: &summary >
  Yubanis finally reaches where he saw the cat the day before. Exhausted and tired, the three decide to look for the feline along the dangerous tiag that even Tubocak couldn't pacify.
teaser: *summary
when:
  start: 44.7.1849 TSC 11.33
characters:
  primary:
    - Yubanis
  secondary:
    - Unil
    - Tubocak
    - Abasa
  referenced:
    - Ijosil (Epigraph)
locations:
  primary:
    - Poisoned Well of Ijarka
topics:
  referenced:
    - "Surviving the Wilderness, One Family's Horror (Epigraph)"
---

> As a living entity, tiags gather energy until they must be either tapped or they twist upon themselves. But even abandoned, there is great power still stored within the land. --- Ijosil Ogamial, *Surviving the Wilderness, One Family's Horror*

Traveling back took longer because of the darkness. When Yubanis stopped the wagon near the broken tree, it was four hours past sunset and close to midnight. His twenty minute nap did little to push back the exhaustion, but duty had kept him awake.

Unil, on the other hand, had fallen asleep with her head on his shoulder. She kept one arm wrapped around his, clutching it in her sleep.

"Poor girl is exhausted," Tubocak said. He hopped off the wagon, deftly avoiding a patch of glowing mushrooms. Heading straight for the horse, he started to unhitch the mare who promptly tried to step on his foot. There was a flash of heat and the mare gently put her foot down. "Be good, girl," chided his brother.

"Sorry," Yubanis said. It was his fault she did that, since he could heal bones instantly. "Dad told me to stop her."

"No big deal. The old man can figure it out himself." Tubocak looked out into the darkness. "It's pretty late. What should we do? Start looking now? Wake her up?"

The only thing Yubanis wanted to do was crawl into a bed and sleep until morning. He yawned. "Bed?"

Tubocak shrugged. "Okay, let's get her moved to the top mattress. You on the bottom."

"I'm not going to do anything."

"Yes, but I'm not going to be there to make sure you behave."

Yubanis tensed. "You're going to look now, right?"

Tubocak lead the mare away to a spot which had started to swarm with fireflies. He clicked his tongue and gestured down to a small spring that bubbled up. As the mare bowed down to drink, he patted her on the neck. "This place feel foul. Twisted. I don't know if I can sleep."

Yubanis oriented himself with the broken tree that his parents had snapped and then pointed across the way. "Mom said that the tiag over there was sick."

Tubocak followed the gaze and then groaned. "Yeah, it is. Where did you see the cat?"

Apologetically, Yubanis gestured toward the poisoned tiag. "Over there. Right along the treeline."

"Damn the God Tree. Of course that cat would be over there. Even from over here, I can feel the tiag's thoughts twisted in pain and agony. It has been abandoned and overgrown."

Yubanis shivered. Abandoned tiags were dangerous, more than anything else he knew. The power that the tiag provided soured when no one tapped it. It turned on itself, becoming a twisted version of its own essence. More than a few horror stories were told about an unsuspecting family that stumbled into one.

"Let's get you two into bed."

Yubanis sighed and shook his head. "I'm not going to let you go alone, brother."

"You're exhausted."

He tightened his muscles. "This is not a place to be alone and you know it. You may have impressive powers, but I also know that the tiag will be calling to you if you aren't careful."

Tubocak shook his head. "Fine."

Unil lifted her head. "All three of us."

Yubanis hadn't felt her wake up. He rested his hand on the back of her hand. "Are you sure? It's late."

"Just let me pee and then look for a little while." She didn't have to say how much missing Abasa worried her.

"Oh, a break sounds good." Tubocak lifted up his hand and two paths started to glow as they burrowed into the bush surrounding the broken tree. "Pick one, we'll take the other. It's safe. The water next to the horse is safe as are the berries. Yubanis, why don't you grab some of the dried fruit and jerky from the wagon before we start off."

They went their separate ways. Yubanis got the food first and set it out on the wagon before heading to the area to relieve himself. He passed his brother with a few short words, did what he needed and returned. Less than a half hour later, they were crossing through the dim light toward the ruined tiag.

Up close, the sick feel grew stronger. It wasn't in his stomach, but his mind insisted it should. The phantom discomfort added a sense of going in the wrong direction as the tiag pushed him away from its borders. He groaned and forced himself forward.

Unil caught his hand. She smiled at him and then gestured forward. "I can always move forward," she said with a whisper and then moved ahead. She didn't hesitate, didn't stumble, and didn't stop. Following after her made it easier to push back as they approached the darkness of the tiag.

"Abasa!" she called out.

Tubocak and Yubanis echoed her cries.

At the edge, Tubocak stopped them. "No further."

He sounded worried as he looked up at the trees. Unlike the their surroundings for the last few hours, the forest was dark and unresponsive.

"You can't produce light?" asked Yubanis. It would help a lot if they could see the cat.

Tubocak grunted. "I might but it's hard. She's resisting me, fighting. There is a lot of power here though. It's been a long time since someone has tried to touch her."

"Be safe, brother."

"I will."

"Just... give me a second." Tubocak sounded distracted.

Yubanis stroked Unil's hand. They had traveled for hours together and he wasn't sure what to talk about. He needed to say something, anything to distract himself from the discomfort growing in his guts and thoughts.

"I was born with a blind eye," Unil said suddenly. "I can't see colors out of my left and everything is hazy."

It wasn't anything he had asked, but one he had been curious. "It's still pretty," he answered and instantly berated himself.

She squeezed his hand and he could almost picture her smiling.

"Okay!" Tubocak said sharply. "This will either give us a bit of light to see the edges of the tiag or will kill me."

"What---?"

The entire woods began to shine. Not specks of fireflies swarming and moss, but something deep in the core of the tiag began to glow with brilliance and spears of yellow light speared out through the mist and fog.

"Shit!" Yubanis said as he twisted Unil around so his body was between her and the light. Tensing, he waited for something to strike.

When no blow came, he relaxed to see her smiling.

"You really don't think when you do that?" she asked in a quiet voice with a hint of amusement.

Blushing, he shook his head. He wanted to run away and lock himself in the wagon. "N-No, I guess not."

He pulled his hands away.

She stared at him for a moment and then lean forward. "Thank you," she said before kissing him on the lips.

Yubanis gasped and kissed her back. She had a sweet taste to her lips, completely unlike anyone else... the only person... he had been with before.

They broke the kiss at the same time.

Cheeks burning, he looked at his brother who smirked. "What?"

"Oh, nothing," Tubocak said with a grin. Yubanis could see exhaustion in his brother's eyes, lit up by the amber glow that radiated from the tiag.

Turning to one side and then the other, Yubanis could see that the entire forest had been lit up by the light. It clearly marked the edges of the tiag as a wavering wall painted by light and fog. On one side, it was pitch dark compared to the brilliance. "I... did not know you could do that."

Tubocak walked up and the other two stepped away from each other. "I didn't either, but there is so much power there, I had to stop her with just this light. This is not a good place and I have no doubts she will try to kill any of us if we try to intrude." He cocked his head. "She is strong with plants and thorns, I suspect defensive originally from the memories I picked up. She had been used for defenses a long time ago."

Yubanis listened curiously. His brother had always picked up more details than anyone else he had known. Where Yubanis could only get a vague feeling of safety or discomfort, his brother received images and memories.

"The tiag's family died in there. No one has come back since." Tubocak sniffed as his eyes shimmered. "So much pain. We should find Abasa as fast as possible. As far as I can tell, he is not inside the tiag but I'm not absolutely sure."

Unil slipped her hand into Yubanis. "Which way?"

Tubocak started to point to the north but then gestured to the south. "Something is that way about a half mile. Not the cat but I can feel it like a thorn."

"Then let's go that way," Unil said as she started forward, pulling Yubanis along.

Tubocak chuckled and joined them. "Well, at least she has a direction."

"Yeah."

"I can always move forward," she said firmly with a smile.

As they walked, they called out to Abasa. Their voices were called out into the silence, the surrounding tiag's responding to Tubocak's desire to let their voices carry.

About twenty minutes later, they heard the first pitiful cry of a cat.

Unil gasped and hurried faster. "Abasa! Abasa!"

To Yubanis's surprise, he didn't stumble after her as he was dragged along. She carried him in her wake, pulled by her power and desire. Even his feet didn't ache as much with her leading the way.

Tubocak grabbed her other hand and was pulled along. He grinned and hopped along. "We should keep her and get rid of the horse."

"Tubo!"

"You know you want to ride---"

"Do. Not. Finish!" snapped Yubanis to his brother.

Tubocak snorted but said nothing.

The cat's cries grew louder with every second until they came upon the white feline sitting on top of a wagon parked right up against the poisoned tiag.

Unil released both brothers as she rushed forward. "Abasa! Oh, Abasa!"

The cat cried out and leaped off the wagon, racing over to her.

She scooped it up and hugged it tightly. The tears on her face glistened in the amber light. "Oh, you stupid cat. I missed you so much!"

Tubocak and Yubanis walked on either side of her and went up to the wagon. It was rare to see one in the wood. Yubanis wanted to see if there was someone inside, maybe it was the tiag's calling them to a family in danger.

"Look at these wheels."

Yubanis stopped and peered down. When he didn't see a steel or wooden rim, he did a double take. The wheels on the wagon were wide, easily two feet across with horizontal ridges. The material was black but not wood. He gingerly touched it and the rubbery material gave slightly.

"This one is flat?" Tubocak sounded confused.

A prickle of fear rose as Yubanis looked at the wagon more closely. It didn't look like anything he had seen before. Instead of the rounded top that was ideal for keeping rain and snow off, the wagon had a peaked top with closely fitted planks shielding it. The surface was scoured smooth, polished by something abrasive.

He circled around the back to where he expected a door. There was a hatch, much smaller than he expected. On either side, he could see swirls of paint or writing. "Tubo? Can you bring some light?"

"Yuba."

The tone in his brother's voice drew him away. Yubanis hurried around the wagon to the front where his brother peered into the poisoned tiag. A few yards inside were two dead horses. Their bodies were riddled with wooden thorns and spikes. Sandwiched between them was a woman. In the light, her skin looked black as tar. Her clothes were pale blue and had strange writing embroidered on them.

Yubanis stepped forward. "Who are they?"

"Yuba! Back---!"

Too late, Yubanis realized he had crossed the threshold of the tiag. Something whistled past him and then another. Before he could turn away, three large thorns slammed into his side, puncturing flesh and scraping against the bone.

He grunted and stepped back. "Damn," he muttered. Reaching up, he pulled out the thorn. It was easily six inches long and made of brittle wood that crumbled between his fingers.

Abasa howled as Unil rushed up.

"Yuba, are you okay?" she asked.

"Yeah, wasn't paying attention," he muttered as he fished out the other thorns. By the time he pulled out the last one, most of his injuries had healed leaving only bloody cloth behind. "Well, now we know what the tiag does to intruders."

She stared at him in shock. She had a bemused smile on her face.

The cat hissed at him.

"Doesn't that hurt?" she asked.

Yubanis shrugged. "Not really. It takes a lot to hurt me."

"I can tell. I'm impressed."

The cat meowed again. He shook his head and the bracelet around his neck rattled loudly.

Yubanis gestured to Abasa. "We found the cat."

He looked at the wagon. It didn't look right but it was too dark to really inspect it. Curiosity wanted him to say, to explore and see what had happened to the woman who tried to bring it into a twisted tiag. "What now?"

Unil turned away from the corpse. "We should go back."

Tubocak peered at the wagon before he also turned away. "I'm curious, but I don't think we should travel tonight. I say we spend the night in the wagon and head out in the morning. I'd..." He glanced at the wagon. "I'd like to look at this when it is brighter. Something seems off to me and this needs investigation."

It was obvious that Unil wanted to hurry home.

When they both turned to Yubanis, he felt the mental pressure from both. He was just as curious as his brother about the wagon but he also wanted to keep Unil happy. He took a deep, shuddering breath. "I've been up since sunrise yesterday. I've been driving a wagon most of that time and only had twenty minutes of sleep. I need more." He focused on her. "I'm sorry."

For a moment, he thought she was going to leave them and head home on her own. Then Unil's shoulders slumped. "I understand."

She worked the bracelet off the cat's neck. "We put this somewhere safe on your wagon though. Even if the little brat runs off again, our family will be able to finish." She held up her hand. "I want to get home, but I also want to see what has happened."
