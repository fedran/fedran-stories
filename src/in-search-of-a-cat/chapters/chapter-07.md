---
title: A Fatal Idea
date: 2020-12-22
summary: &summary >
  Yubanis and the others finish making a shield to protect them from the insane tiag. Without knowing it would be enough to save them, they press past the threshold in hopes of saving the survivors.
teaser: *summary
when:
  start: 44.7.1849 TSC 18.2
characters:
  primary:
    - Yubanis
  secondary:
    - Unil
    - Tubocak
    - Abasa
    - Garonàga
    - Fiáchi
  referenced:
    - Opila
    - Gibi
locations:
  primary:
    - Poisoned Well of Ijarka
topics:
  referenced:
    - Shore Upon Stars (Epigraph)
---

> One will take a thousand arrows for love. --- *Shore Upon Stars* (Act 3, Scene 2)

Some time later, Yubanis hefted their makeshift shield with two hands. It was heavy but thick. They had also used all of their spare boards to make it three layers thick and bound together with nails.

Unil came up next to him, shoulder to shoulder. She grabbed the ropes they crafted into handles. "You know this is probably the stupidest thing we could have done. We probably should get the rest of the family." It was the thing one would say before doing something potentially fatal anyways.

"It's for the right reason," Tubocak muttered as he picked up his end. "Besides, how long have those people been in there? They are pinned down at the best, fighting for their lives at the worst. We've talked while making this thing, they may not survive getting the others."

She sighed and nodded.

"Ready?"

Yubanis grunted.

Together, they used the shield and stepped over the threshold.

Almost instantly, whistling filled the air and something struck the shield. Another thorn slammed into it and then dozens more.

The edge of the shield tilted dangerously away from Yubanis as one of the thorns scored his arm. He grunted through the pain. "At least this is only shooting from one direction."

"Yeah, but it hurts!" Tubocak bellowed. "Forward."

"That I can do!" Olin's eyes flashed and then suddenly they were steadily moving forward. It felt like he was being pulled into the wake as she forced the shield forward, walking it with unstoppable force.

The tiag continued to assault them, firing thorns and barbs with rapid speed. Each one thudded into the shield, splintering wood and causing it to shake violently. The attacks were relentless and brutal.

"We have a problem," Tubocak said through gritted teeth. "The path is going to the right."

"I can't do sideways, only forward." Sweat prickled across Unil's brow.

"We'll turn the shield. Yuba, you are going to be the more at risk."

A wave of thorns crashed into the shield.

"Yeah," Yubanis grunted. "I can handle it."

"Turn on four, three, two, one!"

The shifted the shield so Unil could turn and force her way forward. With the pressure from the thorns, the shield dipped dangerously close and the safe space grew too small. Yubanis had to grab the edge as part of his body was exposed.

Sharp thorns slammed into his side, punching into flesh and digging deep.

Yubanis bit down on his lip and kept his head behind the shield. The agony burned white across his vision but he didn't dare to speak up.

They continued forward until the path straightened again and they were able to tilt the shield back to protect all three of them.

Unil gasped as she looked over at Yubanis. Then her eyes grew wide and her lips parted.

He glanced down. There were a few long pieces of wood that had pierced his arm and stuck out the other side. The entire side of his body was coated in blood. Cringing, he looked at his brother who didn't seem to notice and then to her. He shook his head.

She frowned but didn't say anything. Turning back, she tensed and her body glowed before she forced the shield forward, driving it along the path toward the louder sounds of fear and terror.

"We're getting close to the heart." Tubocak's voice was strained from his effort. "I can feel it raging against me."

"What about the others?"

"Ahead but I can't see around this damn thing."

Yubanis cringed. He knew how to look. Shifting the weight to one hand, he took a deep breath to steel himself from the pain.

Unil took more of her weight. "Go on," she whispered.

The thorns slammed hard into the shield.

Yubanis used his hand to shield his eyes and then peeked around the corner.

Two thorns pierced his palm, splattering his face with hot blood. Before the pain could register, he looked quickly around. Just ahead of him, he could see a streamer of thorns were slamming into a wall of shifting sands. More of the strange letters floated among the grains, flashing brightly with each impact.

He ducked behind the shield. "Three yards forward and then one to the right. There is a ditch of some sort, so watch your step."

"You better have not just looked, damn it!" snapped his brother.

"No, I just used my powers to feel the fucking air around us. Of course I looked."

"Damn it, Yuba! Mom is going to kill me if you die!"

Unil interrupted as she forced the shield forward. The impacts of the thorns grew faster and harder, beating on the shield. Splinters of wood began to buckle on their side from the force of the impact.

"One more step, then to the right!" yelled Yubanis. Then, he yelled at the top of his lungs. "We're coming to help!"

A man yelled back but Yubanis couldn't understand the words, only the intent. It sounded like "save me now" and he could understand that.

Grunting, they forced the shield past the wall of sand and then stepped into the ditch. With all of his might, Yubanis jammed the shield into the ground as the wall of sand died around them.

The strangers had dark skin, not quite black but much darker than anyone he had met before. The older man had graying hair and many fresh scars covering his face. He wore the same blues and greens as the wagon but his clothes were drenched with sweat and he visibly shook.

Next to him was a little girl, about same age as Gibi. She had the older man's coloration but the exhaustion had drawn her frame and she looked as if she was starving. Tears glistened on her cheeks as she fell back.

Her father or grandfather, Yubanis wasn't sure, tried to pull her close as he gasped for breath. He looked up at them and said something in their foreign language.

"Sorry," Tubocak said. "I don't understand you."

The man groaned and muttered something.

The little girl whimpered and clutched to him.

He stroked her sweat-soaked hair, saying something. "... Áchi, Áchi." The way the "A" was said sounded strange though, as if it was spoken in a higher tone.

Unil frowned for a moment. Then she pointed to the girl. "Achi?"

The older man pointed to the girl. "Fiáchi." Then to himself. "Batomatsu Garonàga."

Tubocak chuckled. "Okay, that's a long name."

The desert man pointed again. "Áchi. Nàga."

Unil pointed to herself. "Unil. Yuba. Tubo."

"Ú-nil. Yùba. Yùbo."

After a few tries, all of them seemed to get the foreign names right.

Tubocak sighed. "Okay, how do we get out of here. We need to---damn you, Yuba."

"What?" He looked down to where his brother was staring at his blood drenched side. "Oh yeah, forgot about that." He reached up and pulled out the thorns, wincing as they pulled out but his body quickly healing the wounds.

"We're going to have words," grumbled his brother. "But we have to get out of this first."

He gestured to the shield where the thorns were still hammering on the far side. More splinters were forming where the attacks were witling away at their defenses.

"Quickly too. I don't think we have much time. Something is changing in the tiag and I don't have a good feeling about it."

Unil sighed. "The thorns are coming from one direction, the center. If we keep the shield at our back, it should give us a straight line out."

"Good as any plan," Tubocak said. He grabbed the shield and prepared to lift it. With one hand, he gestured up. "We need to run. Can you get up, Nàga?"

The desert man sighed and got up on his knees. He pulled his daughter close.

"On the count of four." Tubocak held up four fingers. "Four, three, two, shit!"

Yubanis started forward when he heard his brother swear. Looking up, he saw a thorn covered vine swing up from the back, slicing through trees as it came directly for their vulnerable side.

"Yuba!"

Without thinking, Yubanis threw himself up over the ditch and in the path of the vine. With a grunt, he braced himself as it crashed into his chest and arms.

Sharp thorns pierced his chest and into his organs. More of them slapped across his face, tearing at the flesh. The force of the blow pushed him back but he strained to keep it still long enough for the others to find a way of defending themselves.

A blast of sand slammed into the massive vine, relieving the pressure for just a second. Hands grabbed him and yanked him back just as Tubocak dropped the shield over all of them in the ditch.

"Damn you to the roots!" screamed Tubocak.

Yubanis sobbed at the agony. He was blind in one eye from where a thorn had pierced it. With bloody fingers, he reached up to grab it and pull it out. It came with a wet, sucking noise.

The little girl let out a cry as she buried her face into her father's leg.

The warmth of healing flooded inside Yubanis as he fumbled with the other pieces of wood that had impaled him. The burning in his lungs was more than he could imagine but he knew if he could pull them out.

"Damn it," Unil gasp. "Get them out of his chest! He can't breathe!"

They pulled and yanked thorns until his organs were able to knit themselves together.

Pain caused Yubanis to stagger. He clutched at his stomach and thrust hard, trying to get his lungs to work again. It didn't work but he managed to draw in thin thread of air. With tears burning in his eyes, he tried again.

With a rush, his body began to work again. "Shit that hurts!"

Unil hugged him tightly. "You're okay," she sobbed.

"Could you not throw yourself into every chance? Mom would be furious if you sacrifice yourself?" Tubocak snapped but he had a relieved smile on his face and tears in his eyes.

"I didn't think," Yubanis's voice was hoarse from where it can be pierced. "I didn't have time." He blinked as his eye healed itself, the vision slurping into focus with a wet ripping sensation.

Garonàga rested a hand on his shoulder and gave a nod. Words would be meaningless but the intent was clear.

Tubocak groaned. "The tiag has adapted to us. We're pinned down. She's not going to let us leave the way we came in."

"Shit."

Garonàga cleared his throat. He said something but at the same time, he gestured around at the broken shards of wood around them. Then he pointed to his heart while miming stabbing it.

Both Yubanis and Unil looked at Tubocak.

His brother groaned. "Of course, it is up to me."

"Well, you have this amazing power with tiags. If anyone can get us out of here, you would be it."

"I came up with the stupid idea of getting us trapped inside."

"I think," Yubanis started as he sat up. "We all came up with this stupid idea. It was the right thing, maybe not the best of methods."

"Mom is going to kill us."

Yubanis groaned. "Yes, she is. But that also means she's going to come in here after us. If we don't do something about this tiag, she's going to be even more furious at our corpses."

All three of them muttered "shit" under their breaths.

After a few moments of silence, Tubocak spoke up again with grin. "I don't have any good ideas, but I have one remarkably stupid one."

Yubanis looked at him and saw the smile. "Does it have to do with us crawling to the center of this thing?"

"How did you guess?"

"Trapped from all sides. We probably have enough strength to make it there and probably brace ourselves while you do something as foolish as everything else we've done so far. Assuming Garonàga can help with the shield." Yubanis mimed a wall.

The desert man nodded but there was no question that he was already at the end of his limits. He clutched his daughter tightly. His shoulders shook from silent tears.

Tubocak closed his eyes. "I think I can project into the tiag if I can get to her heart. Not to kill her but to put her asleep long enough to escape. Or stun her. But I have to get close, very close."

"But there isn't a center to tiags," Unil said. "Everyone knows that."

"The attacks are coming from a single point. Whatever it is, that's where we need to be. That's where I need to be."

Yubanis asked, "Have you ever done this?" He already knew the answer.

"No, but my gut says it's possible."

"Good enough for me, brother. I'll walk to the end of the world for you if I have to."

"You're probably going to get hurt though. You know we have to keep Unil safe which means your ass is going to get nailed."

Yubanis looked at her and smiled. "Of course, I wouldn't dare put her at risk. Sorry, at any more risk."

Unil smiled back and most of the pain faded away.

He blushed. "Come on, let's do this before mom finds out or the tiag finds a new way to kill us with roots."

All five of them sat down and planned, using the dirt on the ground to trace out their tactics. It was a weak plan, but it also was their best option they had. It also seemed possible with the abilities they had.

"Ready?" Tubocak asked.

Everyone gave various agreements. Garonàga held Fiáchi tight as he nodded.

"On four. Three. Two. One!"

Yubanis grunted as he held Unil and his brother push up the shield. The rapid-fire thudding of shards resumed immediately, shuddering it forward.

At the same time, Garonàga and Fiáchi rose up with their backs to the trio. They both held up their hands and a wall of sand burst to life. The howling winds added to the whistling thorns.

Grunting, Yubanis held the shield steady until Unil could activate her power. Then, they steadily marched forward. He turned his back to the side, knowing it was only a matter of moments before that tiag attacked from that angle.

It was only a short distance to the center point. Yubanis couldn't see or hear it, but he felt the pressure increasing with every step. The rapid fire beating began to rattle the wood as long cracks formed from the impact.

"A few more yards. How are you going, Garonàga?"

The desert man grunted and said something they didn't understand.

Yubanis chuckled. "I really hope he just didn't say he was about to collapse."

Unil groaned. "Oh, don't make this harder!"

"Might was well laugh in danger," Tubocak said as he braced himself. "We're about to cross over. Get ready, Garonàga! Yuba?"

Yubanis steeled himself against the pain that was coming.

The heart of the tiag was unremarkable. Yubanis almost stumbled over the tree truck that had been struck by lightning. A human skeleton had been scorched  and fused into one side of it. Inside the cracked wood, crystals had formed in a pool of liquid.

"That's it!"

The first branch struck Yubanis's back. He could feel the thorns digging deep into his skin, shredding his shirt and baring bone. He groaned and gripped the shield tightly. Around them, the thorns stopped firing but the branches slashing and battering them increased with a fury of a storm.

Unil looked at him with concern.

"J-Just brace yourself. I got this," he gasped through the tears. "Tubo, hurry."

His older brother straddled the stump and then shoved his hands into the water. The air pressure around them increased, like a storm about to burst. It crawled across their skin as the tiag threw herself into a flurry of attacks.

Yubanis sobbed as he felt his bones breaking and knitting together. The hammering blows shook his entire frame and it felt like he was being ripped apart. Bloods ran down his legs, pooling at the ground as he shielded himself.

Across from him, little Fiáchi held up both of her hands as she summoned a shield of sand like her father. Despite her delicate form, the power was almost identical as they blocked the attacks that were coming. She cried out with every impact and many of her letters were malformed and garbled.

Yubanis clenched his eyes tightly together as he bore down on the pain. He didn't have many defenses but the agony was difficult to think past. Blood and tears ran down his face as the tiag tried to break him.

Unil's hand pressed against his own.

He cracked open one eye to look at her through the red-tinged tears. She was crying herself. He tried to say something just as a thorn pierced the back of his neck and tore out his throat.

Unil sobbed as she shook her head. "Tubo!"

"I'm... Trying!" Tubocak stood firmly over the trunk as his entire body glowed with a bright light. Little arcs of pink lightning arched between him and the two desert people; every bolt caused all three to shudder in pain and the sand shields to falter. When his brother glanced up, his eyes were nothing but pools of light.

Fiáchi let out a cry as her shield flared and then collapsed. The little girl fell back with a bloody face from a branch that had gotten through.

Garonàga stepped over her and spread out his hands, widening his own shield but dropped to his knees from the effort. He let out a cry of pain and agony of his own as a few branches slipped past.

Forcing his hand up, Yubanis grabbed his brother and pushed him down before one of the branches could strike him from behind. Blood ran down his arms, splashing onto the ground.

Without looking, Tubocak sank to his knees as he bore down into the pool. The air around him grew suffocating and difficult to move, as if it was hardening around all of them.

"Losing it!" screamed Unil.

A loud crack of wood followed. The shield had snapped in half under the battering of the vines. The thorny branches hammered faster against her shield, striking her from both sides with brutal speed.

Her hot blood splashed across Yubanis's face. He couldn't feel it through his own, but seeing the agony in her face tore his heart in half. He dredged up as much strength as he could and then grabbed her and the remains of their shields.

"Yuba!"

He hugged her tightly and pressed her back against his brother, making her a smaller target and exposing himself more to the tiag's fury.

She started to scream at him but the words weren't many sense anymore. The assault against his body was making it difficult to concentrate or even feel anything.

He closed his eyes tight and pushed back against the vines that were breaking bones and puncturing his back and head. Flashes of light turned into physical sensations of pain.

Then, one powerful blow caught him against the spine. He felt the bones cracking and the entire world went white with agony. The last feeling he had was his chest collapsing from the impact before he ceased to feel anything at all.
