---
title: Voices in the Dark
date: 2020-12-15
summary: &summary >
  In the morning, the trio investigates the wagon to figure out who had died in the poisoned tiag. They just had to survive what they found.
teaser: *summary
when:
  start: 44.7.1849 TSC 17.33
characters:
  primary:
    - Yubanis
  secondary:
    - Unil
    - Tubocak
    - Abasa
    - Garonàga (Unnamed)
    - Fiáchi (Unnamed)
  referenced:
    - Deangel (Epigraph)
    - Opila
locations:
  primary:
    - Poisoned Well of Ijarka
topics:
  referenced:
    - Flaws of the Crystal Spheres (Epigraph)
---

> The question of why each culture's magic is distinctly different is a matter of society's themes. The desert is focused on sand and that found within, therefore their magic also has those same themes. --- Deangel Grousel, *Flaws of the Crystal Spheres*

Morning came too early for Yubanis. Even after a day of exhaustion, waking up to a cat's ass in his face was more than he expected. Sputtering, he shoved Abasa away and crawled out of bed.

Tubocak snorted from the seat near the back of the wagon. "I was wondering how long it would take. You seem remarkably tolerant to a cat in that position."

"You're an asshole."

"No, I'm pretty sure you know what an asshole looks like. It looked close enough you could taste it."

Yubanis glared at his brother and then looked back to the other bed, half hoping to see Unil sleeping.

"She's getting cleaned up outside. I gave her some of Opila's clothes, they were pretty loose but better than wearing the same outfit all day. Get dressed. We're still going to check out the wagon and then head back, right?"

Yubanis grunted. "Clothes, food, then look. I don't want to spend the day on an empty stomach or your favorite, jerky."

Tubocak stood up. "Well, you're in luck. Abasa got us breakfast."

"The cat?"

Without answering, his brother headed out of the wagon.

Yubanis had a few choice words for him as he dressed. Coming out, he saw Unil returning with wet hair and his sister's clothes draped over her slender body. She moved steadily but he noticed she turned her head frequently, no doubt to see with her one good eye.

"Morning," he said when she got closer.

"Good morning." She almost had a song in her voice.

"Sleep better?"

"Oh," she smiled broadly. "I can't tell you how well I slept. It's been days since I wasn't waking up worrying about the future. I put the bracelet in the foot drawer by the bed, next to the first aid kit. Is that okay?"

Tubocak leaned around the wagon. "Why are you asking him? You already asked me. Its almost as if you are just looking for an excuse to talk to him."

Both Yubanis and Unil blushed.

Tubocak smirked and returned to his duties. The smell of smoke and roasting meat filled the air and Yubanis's stomach rumbled. He peered around and saw his brother roasting a hare. "Abasa provided breakfast?"

"Yes," Unil said. "He does that a lot."

She was very close to him. He blushed hotter. "I-I should get cleaned up so we can head back."

Leaning into him, she kissed him again. "Thank you," she said in a quiet voice.

Cheeks burning, Yubanis glanced at his brother to see if Tubocak was smirking but his brother appeared to be focused on breakfast. With a nervous chuckle, he headed back the way she came in hopes there was a spring or small waterfall to clean himself.

After a quick breakfast, they moved the family wagon near the other one. It seemed like a better approach than walking a half mile back and forth.

In the light, the other wagon looked even more foreign. It had greens and blues for coloration but no real designs. The only decoration were swirling symbols written along the edges of almost every surface. None of them could read them but they appeared to say the same things over and over again.

"What language is this?" asked Tubocak.

"I don't know. Nothing I've seen."

Yubanis felt uncomfortable, as if something was about to drop. He headed to the back. "You think it's safe to look inside?"

"I don't know," Unil said. She stood just behind him, looking over his shoulder.

He took a deep breath and reached for the door. It resisted but he wrenched it open to reveal a brightly lit inside. It looked cozy, not unlike his own wagon.

Just as he let out his breath, a symbol appeared in the space of the door frame. It was a series of letters that glowed brightly. The air around them grew tense and his ears popped.

Without thinking, he shifted his body to stand between the opening and Unil. "Look out!"

An explosion of wind and dust slammed into his face, searing him with an intense head. He turned his face to the side as it scoured his face and cut into his flesh. A thousand tiny abrasions ripped across his arms but he refused to let go of the door in fear that it would pull him away and expose Unil.

The wind continued to blow. The heat dried his nostrils and stung his lungs. It beat against his skin, adding the smell of burning hair and flesh before the wind ripped it away from him.

"Yuba!" Tubocak's voice could barely be heard over the howling stream.

Then, just as it appeared, the letters faded and the wind stopped.

Yubanis gasped for breath as his flesh healed. He shook and streamers of sand poured out of his clothes. "Oh, that hurt a little."

"Are you okay!?" gasped Unil. She pulled him away as she looked for injuries.

"Yeah. I said, I'm hard to hurt."

"What was that?" she asked Tubocak.

"A trap of some sort. Who traps their wagons?"

Yubanis groaned. "Plenty of people. But I've never heard of anyone using sand and heat. Do you think these are people from the desert?" They were only a few hundred leagues from the Great Desert but the sand people almost never left their territories. He vaguely recalled they had a writing system that looked like the words on the back of the wagon.

"They have black skin like the woman in the tiag." Tubocak cringed as he peered into the wagon. "Do you think there are more traps?"

Yubanis groaned. "Let me look."

His brother turned to him. "Don't push your luck."

"I'm sure they didn't trap every drawer. Can you imagine opening up your underwear to a blast of air? It's probably just the hatch."

Tubocak snorted. "Fine, but if one more traps goes off, you get out and stay out."

Yubanis nodded and then crawled into the wagon. His entire body was tense as he stood in the opening, half expecting a second trap to go off. When one didn't, he gingerly began to inspect the contents.

It looked like a family wagon but it appeared as if they used sleeping rolls instead of a built-in beds. The storage was also chests strapped to the instead of built-in. Overall, it appeared to be a freight wagon that had been hastily converted into a family one.

He frowned and felt like he was intruding on the dead. But he opened one chest and then the other, finding more clothes than expected. Then he came up to a smaller basket filled with children's toys. There were balls and stuffed animals.

Yubanis crouched down and ran his hands over the smooth wood. The toys had been played with hard, with the polish of constant attention. One of them, a stuffed bird, had some stitching on it that felt rougher than the rest.

He looked around and saw a ball of thread of matching thread and some needles on top of another chest. He picked up the thread and sniffed it. It had the same smell as the toy.

It appeared that there was more than a single traveler in the wagon. Yubanis wanted to be sure. He crawled over to the rolls and sniffed each one, picking up the distinct smell of at least three individuals on them.

"Anything, brother?"

"I think this was a family. A little and two adults judging from the packs." Yubanis shook his hand and more sand came tumbling from the rolls. "They were from the desert. They left in a hurry though, this wagon looks like it was thrown together in a hurry."

Just as he started back, he noticed something jammed behind one of the chests. It was a pair of swords wrapped in cloth with the same words along the ridges. The fabric had been stained with blood.

Next to the swords was a basket with dirty clothes. He pulled out a few unfamiliar outfits, but there were distinctly three different sets that matched his thoughts.

The sense of being in an intruder redoubled. He crawled out of the wagon. "I think they were all together though."

Tubocak groaned. He turned slightly to look at the sick tiag. "What do you think happened to the other two?"

With a sinking sensation, Yubanis followed his gaze. "What if she was bringing the wagons to her family? Would the tiag let them in long enough to think they were safe?"

Tubocak's sigh was all the answer he needed.

"Are they still alive?" asked Unil in a quiet voice.

Tubocak shrugged and headed toward the corpse and the horses. In the daylight, all three of them could see what appeared to be a trail leading straight toward the center of the forest. It was dark but flashes of amber light still rolled in the distance.

Taking a deep breath, he yelled at the top of his lungs, "Anyone in there!?"

Yubanis held his breath and strained to listen.

Around them, the sounds of the grasses and woods faded into quiet. Tubocak's will affected the world around him except for the deathly silence of the poisoned tiag.

"Hello!?"

Yubanis and Unil also added their yelling, making as much noise as they could.

"Anyone?"

"Yell if you can hear us!"

All three of them stopped, leaning toward the threshold as they listened for a sound, any sound.

Unil got too close. There was a whistling noise. Yubanis spun to pull her free but Tubocak snatched her as a thorn speared out of the darkness. It reached the threshold and then lost all of its speed, tumbling to the ground only inches away.

"Be careful," said his brother.

"Sorry, I---"

Tubocak held up his hand sharply and then cocked his head. "Listen!"

Yubanis held his breath and strained to hear.

It was a voice. He didn't understand the language but he could hear fear and exhaustion. A girl then cried out, soft and sobbing.

"Shit," Tubocak said. "They're in there."

He took a deep breath. "Can you hear us!"

A response drifted out, Yubanis assumed it meant yes.

Tubocak looked worried as he turned back. "How do we get in? Those thorns are going to tear us apart."

Yubanis shook his head. "Tear you apart."

"No," snapped his brother. "I'm not going to let you do that."

"Why not? I'm hard to kill."

"Because you are going against a poisoned tiag! This isn't the horse stepping on your toe or you falling in the ravine and getting a spear up your ass! This is more dangerous than anything!"

Yubanis blushed hotly at the reference. He was crawling up the side of the ravine one day when he fell. A friend's spear caught his rear and he was impaled from ass to throat on it. Pulling it out of his throat was one of the most painful experiences in his life. Not to mention one of the most humiliating.

"Can we make a shield?" asked Unil.

Tubocak pointed at her. "See, that's a smart idea! Not a stupid, suicidal charge!"

"Fine, I'm just come up with ideas, brother!"

"We don't know your limits, Yuba! I'm not going to lose you because you were doing something stupid." He took a deep breath. "Damn it. You don't have to show off for her."

"I'm not!" Yubanis forced himself not to glance at Unil. He couldn't stop blushing.

The two brothers stared at each for a long moment.

"Fine. There are some spare boards under our wagon. I don't know anything about this." He gestured to the wagon. "But I know our wood is solid and should help us."

Yubanis spun and hurried away before Unil could see him. He knew where they kept the repair supplies. By the time the others joined him, he had them out. It would take a while to make the shield and he hoped the strangers would be able to hold out long enough.
