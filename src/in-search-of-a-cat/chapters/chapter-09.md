---
title: A New Home
date: 2021-01-05
summary: &summary >
  After enjoying his time with Unil, Yubanis prepared to get ready to head back to the rest of this family. The problem is, his family had already arrived and he was definitely in trouble.
teaser: *summary
when:
  start: 45.7.1849 TSC 7.51
characters:
  primary:
    - Yubanis
  secondary:
    - Unil
    - Penilil
    - Tubocak
    - Rainel
    - Osain
    - Gibi
    - Garonàga
    - Fiáchi
    - Abasa
    - Opila
    - Sophi
  referenced:
    - Jilal (Epigraph)
    - Warin
locations:
  primary:
    - Poisoned Well of Ijarka
    - Melbith Penilil
  referenced:
    - Palifil Nibaras
topics:
  referenced:
    - "The United Hidanork Tribes: Before the War (Epigraph)"
---

> The Hidanork Tribes don't have a concept of marriage like most civilized culture. It is not uncommon for parents to travel separately for a number of years before deciding to travel together. --- Jilal of Odir, *The United Hidanork Tribes: Before the War*

By the time Yubanis opened the door and stepped out of the wagon, most of his aches and pains had faded into the comfortable afterglow of sex. He smiled broadly as he walked down the steps and then held the door open for Unil.

His lover took two steps out and then froze. The smile dropped from her face instantly. "Shit."

A cold shiver of fear ran down his spine. Slowly, he turned to see both of their mothers standing there, arms crossed and furious looks on their faces.

"Shit." It was the only thing he could say either.

Penilil cleared her throat. "Yuba, to the front." Her voice was brimming with the fury of an ice storm.

"I have to pee."

"It can wait," she announced before storming around the side.

He gave one last look to Unil before following after his mother. He knew he was going to get punished, it was only a matter of how severe and how long he would be suffering.

Yubanis barely reached the front when his mother spun on her heels and slapped him hard across the face. "You stupid ass!"

Her eyes glistened with tears. With a sob, she yanked him close and hugged him tightly. "I could have lost you!"

Yubanis hugged her back. "I'm sorry, but I had to do it."

His mother's body shook with her tears. Her arms squeezed him painfully. "I almost lost my baby. I can't do that. I can't. You aren't suppose to die on me, I don't care of you can heal yourself or not."

Yubanis's eyes watered with his own relief. He rested his head against hers and held her tight. "It was the right thing."

"I know, your brother told me. I can be upset about how you went about it, but you were right to go in and save Naga and Achi. Your impatience probably saved them from dying like her mother. But... damn it." She sniffed and wiped the tears from her face. "I love that you did right, but I feel like you had gutted me when your brother told me that you had died while doing it."

He struggled to hide the smile from his lips. As much as it hurt, he was happy that his mother was proud of him.

"Now, where is that rash brother of yours."

Yubanis looked around. He saw his sisters, Osain, and Gibi all petting Abasa near a fire pit. They had the comfortable chairs out. He frowned for a moment, trying to puzzle out how long they were waiting until he left the wagon. Or how they removed anything from the storage cabinets underneath without him noticing.

"There is he. Over by... that place. Come on, I have words."

Together, they walked across the grass toward the tiag.

About halfway across, his mother cleared her throat. "In the future, I would appreciate if you didn't make me wait two hours to yell at you. I wasted my fury trying to get a chance to interrupt you while still giving you your privacy."

"What?" He shook his head. "I don't understand."

"I can see why Ami gave you her bracelet, my son. There is no doubt you earned it. Just realize, most men need more than thirty seconds between sparring rounds with their lovers."

A blush blossomed in his cheeks and ears.

"You never gave me a proper chance to scream at you," she said with a grin. The smile faded slightly. "But no talking about that in front of her family. They heard enough to know what is going on between you but it would be rude. No doubt, your father or brother will want to make a big deal out of it. Keep that private."

"Yes, Mom." He struggled to keep the smile from his lips.

As they approached the treeline, the feeling of dread and discomfort that Yubanis had felt before was missing, only a different sensation that tickled the back of his mind. His brother stood with his hands behind his back and wearing a wide smile.

His mother wagged a finger at Tubocak. "You are supposed---"

"Mom," interrupted Tubocak with a grin. He held out a small necklace with a crystal pendant. It was the same crystal as inside the stump that his brother had found at the heart of the tiag. "I'd like you to meet Melbith Penilil, our new lands."

She froze as her eyes opened wide.

He put the pendant into her palm and closed her fingers around it. Leaning down, he kissed her knuckles. "She's a bit cranky like you, but she would love to meet you."

"Y-You...?" His mother was at a loss for words.

His brother stepped pass the threshold of the tiag.

Yubanis cringed, waiting for the thorns to shoot out.

Nothing happened except for a few trees rustled.

Tubocak's eyes almost shone. He gestured down a small footpath that Yubanis hadn't seen before. "Come on. She's going to need attention every couple years for a while before she settles down, but I think she'll be a good place for us."

"Y-You," his mother sputtered. "You attuned yourself to the land?"

Tubocak sighed. He focused on Yubanis and shook his head. "When I saw my brother fall, something inside me broke. I had to stop the tiag, so I threw myself into her." His eyes grew haunted for a moment. "I tried to kill her, I really did, but she wasn't going to die. Not without taking me."

"Tubo...." His mother shook her head.

"I couldn't do it. She was in pain and I had the only thing that could help her. I don't know how I did it, but Melbith tore something out of me." He thumped his chest. "Not anything physical, but I can feel this... wound in my heart where she clawed it out."

"You sacrificed part of your soul to the tiag. Damn it, why did both of my sons end up martyrs?"

Tubocak suddenly smiled. "Well, Yuba got a few hours of epic fucking out of it. So I don't think he was entirely altruistic---"

Their mother smacked him.

"What? It's true!"

Yubanis blushed.

"We'll deal with that later. Right now, what can we do for Melbith, right? You should have called her Melbith Tubocak."

"Yeah, but she's somewhat of a bitch."

Penilil lifted up her fist. "I will end you."

Around them, the trees rustled in a wind that didn't exist.

Tubocak reached up and gently pushed his mother's fist down. "Yes, let's not do that here. Melbith has been hurt for many years. It's going to take her decades before those wounds are healed and she's entirely safe for anyone. That means no gifting entrance to her for at least fifty or sixty years. I suspect that we'll need to come back here every two to five years until then."

His mother froze, her eyes flashing. "W-We can do that, we come through her frequently if we change our routes on year nine and twenty-two."

"I know. Until then, we'll still need to use Warin's passage. After that though, we'll have a new home."

Penilil smiled and hugged him. "You did the right thing."

Yubanis cleared his throat. "What about Unil? Well, Unil and her family?"

Tubocak smirked. "Thinking of her already? Going to travel with her now?"

Their mother smacked him.

The branches rustled again.

He chuckled. "She was here when I touched her. She's got a connection to Melbith, as you, Nàga, and Áchi. They will never need a key to call this place home, even today. But, brother dear, I made pendant for her family also so they will be able to call Mel their home too. She could use the attention."

Tubocak grinned. "Plus, it means that my brother will have a chance to meet  Unil and their child a few times before they decide to travel together. I mean, after all that, there is no question he gave her a baby."

Penilil tightened her lips together. "Tubo?"

"Yes, mother," he said with a smile.

"Come with me so I can beat you."

He shrugged and then lead the way out of what would become one of their new homes.
