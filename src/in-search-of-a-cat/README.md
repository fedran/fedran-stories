# In Search of a Cat

## Characters

### The Penilils

- Yubanis
  - Chains and Rams
- Tubocak
  - Chains and Storm
- Penilil
  - Chains
- Opila
  - Chains and Waves
- Sophi
  - Chains and Thorns
- Goisay
  - Father, Opila's natural father

### The Rainels

- Rainel
- Unil
  - Storms and Roses
  - Unstoppable
- Osain
  - Storms and Rams
  - Can feel through the ground
- Gibi
- Abasa
  - The cat, white with dark tips at the ears, feet, and tail.

### Batomatsu

- Batomatsu Garonàga (Nàga)
- Fiáchi (Áchi)

### Tiags

- Palifil Nibaras
  - Warin's land
- Cinifel Abasa
  - Rainel's father's tiag
  - End of a 49 year trek
- Melbith Penilil
