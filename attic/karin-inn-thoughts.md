---
title: Night Terrors
availability: private
access: private
---

> Just because the battle is over doesn't mean the fighting has ended. Some warriors never find relief from their wars. --- *The Horror of the Ghost Sword* (Act 2, Scene 4)

Karin woke up with a gasp. Blindly, she smacked around with her hand until she caught the side table and then used that to lever herself up into a sitting position. The sweat-soaked sheets slid along her naked body. She cringed at the slickness, but it wasn't any worse than waking up in knee-deep water.

She grabbed a drier section of her blanket and used it to wipe the sweat from her face and breasts. She tossed it off the edge of the bed before slumping back against the cool headboard.

Nightmares continued to play in the back of her head. Images of monsters tearing into her body sent tremors through her scarred legs. Memories of claws, slashes, and having her intestines spill out across her hands was nothing compared to the startling clear memory of her ex-husband screaming at her the night she had packed up and left.

"Damn that old man," she muttered. She reached out and grabbed the bottle of rotgut. Her efforts to sit up had knocked it over but the clear liquid managed to remain inside. She gripped the neck tightly and took a long deep drink.

The burn down her throat pushed away the memories. Thumping the back of her head against the board, she shook her head and let out a long sigh. "I need to quit doing this crap."

After a moment, she dropped her hand to her stomach. Deep scars crisscrossed the taut lines of her abdomen. Even without looking, she could remember each wound by touch.

She had seen death enough times that she wasn't afraid anymore. Her second life had been brutal, exhausting, and violent. However she wouldn't pass it up for all the money in the world.

With a sigh, she cupped her sex and closed her eyes to drift away in an alcohol-fueled sleep.
