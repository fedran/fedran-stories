Stories of Fedran
=================

* [A Simple Cog](src/a-simple-cog.md): A steampunk story of a baron set on destroying his enemies, and his servant who tries to stop him.
* [Brickpunk](src/brickpunk.md): Defending ones's home against an intruder is more important than anything else. An elderly couple fights against the local baron in a mecha they built out of bricks from their quarry.
* [Figurines](src/figurines.md): A Mudd Fourier tale when he and his assistant are investigating a crime scene that was helpfully destroyed by their fellow guards.
* [Friend of the Guards](src/friend-of-the-guard.md): Life is hard for a lone guard in an old museum working the night shift. There is always a danger that someone will find an artifact worth stealing and there is nothing Shilah could do about it.
* [The New Girl](src/the-new-girl.md): Staple was more interested in playing than learning the family business. At least until the old man and his new apprentice came to recharge the fire cores that drove the family's machines.

Proposed
--------

* Plunge Into the Fires: A group of prisoner dalpre are forced to jump into a forest fire to put it out.
* Sentenced to Fur City: A slender dog dalpre's apartment gets flooded out. He has to find a place to live which means he heads to Fur City, a place where dalpre, but he is unprepared until another dalpre offers to help. M/M romance. He later gets a business helping others figure out the legal system.
